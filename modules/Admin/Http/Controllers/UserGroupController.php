<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/9/2017
 * Time: 4:00 PM
 */

namespace Modules\Admin\Http\Controllers;


use App\Models\Permission;
use App\Models\Role;
use Caffeinated\Themes\Facades\Theme;
use Helper,DB;
use Modules\Admin\Requests\EditRole;
use Modules\Admin\Requests\StoreNewRole;
use Modules\User\Entities\RoleUserEntity;

class UserGroupController extends BaseController
{
    function index()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_group')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $groups = new Role();
        $groups = $groups->select('id', 'name', 'display_name')->get();
        $data = [
            'groups' => $groups,
            'admin' => $admin
        ];
        return Theme::view('user.groups')->with($data);
    }
    
    function editUserGroup($id = false)
    {
        $admin = $this->loggedInStatus();
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
        if ($id && !is_numeric($id)) {
            return ['success' => false];
        }
        $role = false;
        if ($id) {
            if (!$admin->can('edit_group')) return Helper::resultData(false, trans('admin.no_access_privilege'));
            $role = new Role();
            $id = (int) $id;
            $role = $role->find($id);
            if (!$role) return ['success' => false, 'message' => trans('admin.role_not_exists')];;
        } else {
            if (!$admin->can('add_new_group')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        }
        $permissions = new Permission();
        $permissions = $permissions->get();
        $data = [
            'title' => trans('admin.role_name'),
            'display_name' => trans('admin.role_display_name'),
            'admin' => $admin,
            'permissions' => $permissions,
            'group' => $role
        ];
        $view = Theme::view('user.create_group', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    function store(StoreNewRole $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('add_new_group')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $display_name = $r->input('display_name');
        $role = new Role();
        $name = Helper::gen_slug($display_name);
        if ($role->whereName($name)->exists()) {
            return Helper::json(['display_name' => [trans('admin.display_name_cant_be_used')]], 422);
//                return Helper::resultData(false, trans('admin.display_name_cant_be_used'));
        }
        $role->name = $name;
        $role->display_name = $display_name;
        $role->save();
        $permissions = $r->input('permission') ?: [];
        $role->perms()->sync($permissions);
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return Helper::resultData(true);
    }
    
    function patch(EditRole $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('edit_group')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $data = $r->all();
        $roles = new Role();
        $role = $roles->find($data['id']);
        if (!$role) {
            return Helper::resultData(false, trans('admin.role_not_exist'));
        }
        $name = Helper::gen_slug($data['display_name']);
        if ($role->name !== $name) {
            if ($roles->whereName($name)->exists()) {
                return Helper::json(['display_name' => [trans('admin.display_name_cant_be_used')]], 422);
            }
        }
        if ($role->id > 4) {
            $role->name = $name;
            $role->display_name = $data['display_name'];
        }
        $role->save();
        $permissions = $r->input('permission') ?: [];
        $role->perms()->sync($permissions);
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return Helper::resultData(true);
        
    }
    
    function remove()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('delete_group')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $ids = request()->input('ids');
        if (!$ids) return Helper::resultData(false, trans('site.action_failed'));
        $ids = explode(',', $ids);
        $ids = Helper::stringArrayToInt($ids);
        $error = false;
        $roles = new Role();
        $i = 0;
        foreach ($ids as $id) {
            if ($id > 4) {
                $role = $roles->find($id);
                if ($role) {
                    $role->perms()->sync([]);
                    $roleUsers=RoleUserEntity::where('role_id',$role->id)->get();
                    foreach($roleUsers as $item){
                        DB::statement("UPDATE role_user SET role_id=4 WHERE user_id=".$item->user_id);

//                        $item->role_id=4;
//                        $item->save();
                    }
                }
                $roles->whereId($id)->delete();
                $i++;
            } else {
                $error = true;
            }
        }
        if ($error) {
            session()->flash('action_succeeded', trans('admin.some_roles_not_deleted'));
        } else {
            session()->flash('action_succeeded', trans('site.action_succeeded'));
        }
        $result = Helper::resultData(true);
        return Helper::json($result);
    }
    
    function identifier($str, $separator = 'dash', $lowercase = false)
    {
        $foreign_characters = [
            '/ä|æ|ǽ/' => 'ae',
            '/ö|œ/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/À|Á|Â|Ã|Ä|Å|Ǻ|Ā|Ă|Ą|Ǎ|А/' => 'A',
            '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª|а/' => 'a',
            '/Б/' => 'B',
            '/б/' => 'b',
            '/Ç|Ć|Ĉ|Ċ|Č|Ц/' => 'C',
            '/ç|ć|ĉ|ċ|č|ц/' => 'c',
            '/Ð|Ď|Đ|Д/' => 'D',
            '/ð|ď|đ|д/' => 'd',
            '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě|Е|Ё|Э/' => 'E',
            '/è|é|ê|ë|ē|ĕ|ė|ę|ě|е|ё|э/' => 'e',
            '/Ф/' => 'F',
            '/ф/' => 'f',
            '/Ĝ|Ğ|Ġ|Ģ|Г/' => 'G',
            '/ĝ|ğ|ġ|ģ|г/' => 'g',
            '/Ĥ|Ħ|Х/' => 'H',
            '/ĥ|ħ|х/' => 'h',
            '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ|И/' => 'I',
            '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı|и/' => 'i',
            '/Ĵ|Й/' => 'J',
            '/ĵ|й/' => 'j',
            '/Ķ|К/' => 'K',
            '/ķ|к/' => 'k',
            '/Ĺ|Ļ|Ľ|Ŀ|Ł|Л/' => 'L',
            '/ĺ|ļ|ľ|ŀ|ł|л/' => 'l',
            '/М/' => 'M',
            '/м/' => 'm',
            '/Ñ|Ń|Ņ|Ň|Н/' => 'N',
            '/ñ|ń|ņ|ň|ŉ|н/' => 'n',
            '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ|О/' => 'O',
            '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º|о/' => 'o',
            '/П/' => 'P',
            '/п/' => 'p',
            '/Ŕ|Ŗ|Ř|Р/' => 'R',
            '/ŕ|ŗ|ř|р/' => 'r',
            '/Ś|Ŝ|Ş|Š|С/' => 'S',
            '/ś|ŝ|ş|š|ſ|с/' => 's',
            '/Ţ|Ť|Ŧ|Т/' => 'T',
            '/ţ|ť|ŧ|т/' => 't',
            '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ|У/' => 'U',
            '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ|у/' => 'u',
            '/В/' => 'V',
            '/в/' => 'v',
            '/Ý|Ÿ|Ŷ|Ы/' => 'Y',
            '/ý|ÿ|ŷ|ы/' => 'y',
            '/Ŵ/' => 'W',
            '/ŵ/' => 'w',
            '/Ź|Ż|Ž|З/' => 'Z',
            '/ź|ż|ž|з/' => 'z',
            '/Æ|Ǽ/' => 'AE',
            '/ß/' => 'ss',
            '/Ĳ/' => 'IJ',
            '/ĳ/' => 'ij',
            '/Œ/' => 'OE',
            '/ƒ/' => 'f',
            '/Ч/' => 'Ch',
            '/ч/' => 'ch',
            '/Ю/' => 'Ju',
            '/ю/' => 'ju',
            '/Я/' => 'Ja',
            '/я/' => 'ja',
            '/Ш/' => 'Sh',
            '/ш/' => 'sh',
            '/Щ/' => 'Shch',
            '/щ/' => 'shch',
            '/Ж/' => 'Zh',
            '/ж/' => 'zh'
        ];
        
        $str = preg_replace(array_keys($foreign_characters), array_values($foreign_characters), $str);
        
        $replace = ($separator == 'dash') ? '-' : '_';
        
        $trans = [
            '&\#\d+?;' => '',
            '&\S+?;' => '',
            '\s+' => $replace,
            '[^a-z0-9\-\._]' => '',
            $replace . '+' => $replace,
            $replace . '$' => $replace,
            '^' . $replace => $replace,
            '\.+$' => ''
        ];
        
        $str = strip_tags($str);
        
        foreach ($trans as $key => $val) {
            $str = preg_replace("#" . $key . "#i", $val, $str);
        }
        
        if ($lowercase === true) {
            if (function_exists('mb_convert_case')) {
                $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
            } else {
                $str = strtolower($str);
            }
        }
        
        $str = preg_replace('#[^a-z 0-9~%.:_\-]#i', '', $str);
        
        return trim(stripslashes($str));
    }
}