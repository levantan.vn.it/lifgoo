<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Assets, Helper, LaravelLocalization;
use Caffeinated\Themes\Facades\Theme;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\NewsEntity;

class BaseController extends Controller
{
    public function __construct()
    {
        Theme::setActive('admin');
        \Assets::addCss([
            Helper::getThemePlugins("bootstrap/dist/css/bootstrap.min.css"),
            Helper::getThemePlugins("font-awesome/css/font-awesome.min.css"),
            Helper::getThemePlugins("nprogress/nprogress.min.css"),
            Helper::getThemePlugins("messi/messi.min.css"),
            Helper::getThemePlugins('iCheck/skins/flat/green.css'),
            /**
             * bootstrap-wysiwyg
             */
            /*Helper::getThemePlugins('google-code-prettify/bin/prettify.min.css'),*/
            Helper::getThemePlugins('datatables.net-bs/css/dataTables.bootstrap.min.css'),
            Helper::getThemePlugins('datatables.net-buttons-bs/css/buttons.bootstrap.min.css'),
            Helper::getThemePlugins('datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'),
            Helper::getThemePlugins('datatables.net-responsive-bs/css/responsive.bootstrap.min.css'),
            Helper::getThemePlugins('datatables.net-scroller-bs/css/scroller.bootstrap.min.css'),
            Helper::getThemePlugins('simple-cropper/css/jquery.Jcrop.css'),
            Helper::getThemePlugins('simple-cropper/css/style-example.css'),
            /**
             * bootstrap-daterangepicker
             */
            Helper::getThemePlugins('bootstrap-daterangepicker/daterangepicker.css'),
            /**
             * Custom Theme Style
             */
            Helper::getThemeCss("custom.min.css"),
            Helper::getThemeCss("loading.css"),
            Helper::getThemeCss("admin.css?v=".Helper::version()),
        ]);
        \Assets::addJs([
            Helper::getThemePlugins("jquery/dist/jquery.min.js"),
            Helper::getThemePlugins("jquery/jquery.cropit.min.js"),
            Helper::getThemePlugins("jquery/jquery.validate.min.js"),
            Helper::getThemePlugins("bootstrap/dist/js/bootstrap.min.js"),
            Helper::getThemePlugins("nprogress/nprogress.min.js"),
            Helper::getThemePlugins("messi/messi.min.js"),
            Helper::getThemePlugins("iCheck/icheck.min.js"),
            url('themes/default/assets/js/user.js'),
            /**
             * bootstrap-wysiwyg
             */
            /*Helper::getThemePlugins("bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"),
            Helper::getThemePlugins("google-code-prettify/src/prettify.js"),*/
            Helper::getThemePlugins("jquery.hotkeys/jquery.hotkeys.js"),
            Helper::getThemePlugins("tinymce/js/tinymce/tinymce.min.js"),
            Helper::getThemePlugins('datatables.net/js/jquery.dataTables.min.js'),
            Helper::getThemePlugins('datatables.net-bs/js/dataTables.bootstrap.min.js'),
            Helper::getThemePlugins('datatables.net-buttons/js/dataTables.buttons.min.js'),
            Helper::getThemePlugins('datatables.net-buttons-bs/js/buttons.bootstrap.min.js'),
            Helper::getThemePlugins('datatables.net-buttons/js/buttons.flash.min.js'),
            Helper::getThemePlugins('datatables.net-buttons/js/buttons.html5.min.js'),
            Helper::getThemePlugins('datatables.net-buttons/js/buttons.print.min.js'),
            Helper::getThemePlugins('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'),
            Helper::getThemePlugins('datatables.net-keytable/js/dataTables.keyTable.min.js'),
            Helper::getThemePlugins('datatables.net-responsive/js/dataTables.responsive.min.js'),
            Helper::getThemePlugins('datatables.net-responsive-bs/js/responsive.bootstrap.js'),
            Helper::getThemePlugins('datatables.net-scroller/js/dataTables.scroller.min.js'),
            Helper::getThemePlugins('simple-cropper/scripts/jquery.Jcrop.js'),
            Helper::getThemePlugins('simple-cropper/scripts/jquery.SimpleCropper.js'),
            /**
             * bootstrap-daterangepicker
             */
            Helper::getThemePlugins('moment/min/moment.min.js'),
            Helper::getThemePlugins('bootstrap-daterangepicker/daterangepicker.js'),
            /**
             * Custom Theme Scripts
             */
            Helper::getThemeJs("custom.min.js"),
            Helper::getThemeJs("admin.js?v=".Helper::version()),
        ]);
    }
    
    public function index()
    {
        $admin = $this->loggedInStatus();
        $news = NewsEntity::count();
        $data = [
            'news'=>$news,
            'admin'=>$admin,
            'guides'=>User::where('type','=','guide')->count(),
            'users'=>User::where('type','=','user')->count(),
            'bookings'=>BookingEntity::count()
        ];
        return Theme::view('index', $data);
    }
    
    public function isLoggedIn()
    {
        $admin = $this->loggedInStatus();
        if (empty($admin)) {
            $currentLocale = LaravelLocalization::getCurrentLocale();
            $locale = $currentLocale === 'en' ? '' : $currentLocale;
            return redirect()->to($locale . '/admin/login')->withErrors(['login_required' => 'You have to log in before accessing the page'])->send();
        }
        return $admin;
    }
    
    public function loggedInStatus()
    {
        $admin = session('admin');
        if (empty($admin)) return false;
        return $admin;
    }
}
