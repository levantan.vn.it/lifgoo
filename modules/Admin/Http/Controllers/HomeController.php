<?php

namespace Modules\Admin\Http\Controllers;

use Modules\User\Entities\NewsEntity;
use Modules\User\Entities\NotificationEntity;
use Illuminate\Http\Request;
use Modules\Frontend\Models\UsersModel;
use Modules\Frontend\Models\GuideInfoModel;
use Helper;
use Caffeinated\Themes\Facades\Theme;

class HomeController extends BaseController
{
    private $userModel;
    private $count_Star;
    public function __construct()
    {
        parent::__construct();
        Theme::setActive('admin');
        Theme::setLayout('admin::layouts.master');
        $this->userModel = new UsersModel();
        $this->count_Star = new GuideInfoModel();

    }
    
    /**
     * show homepage
     * @return mixed
     */
    function about(){
        return Theme::view('frontend.about');
    }

    function updateIsClick(Request $r){
        $user = $this->loggedInStatus();
        if (!$user || !$r->ajax()) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        NotificationEntity::where('type','=','approve')->where('is_click','=',0)->update(['is_click' => '1']);
        return Helper::jsondata(true);
    }

    function markRead(Request $r){
        $user = $this->loggedInStatus();
        if (!$user || !$r->ajax()) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $id = $r->input('id');
        NotificationEntity::where('id','=',$id)->where('is_read','=',0)->update(['is_click' => '1', 'is_read' => 1]);
        return Helper::jsondata(true);
    }

    /**
     * count all notification new message, new cart, new notification
     * @param Request $r
     * @return mixed
     */
    function intervalNotify(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::jsondata(false);
        }
        $countBell = $r->input('bell');
        $count = NotificationEntity::where('type','=','approve')->where('is_click', '=', 0)->count();
        $htmlBell = '';
        if ($count != $countBell) {
            $box_notify = NotificationEntity::notify_system();
            if (count($box_notify))
                foreach ($box_notify as $no) {
                    $htmlBell .= Theme::view('system.notification', ['notify' => $no, 'user_notify' => []])->render();
                }
        }
        $data = [
            'countBell' => $count,
            'htmlBell'=>$htmlBell
        ];
        return Helper::jsondata(true,'',$data);
    }
}
