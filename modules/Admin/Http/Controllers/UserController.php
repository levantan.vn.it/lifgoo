<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\User;
use App\UserProfile;
use Caffeinated\Themes\Facades\Theme;
use Helper;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use LaravelLocalization;
use Modules\Admin\Http\Controllers\BaseController as Controller;
use Modules\Admin\Requests\EditUser;
use Modules\Admin\Requests\StoreNewUser;
use Modules\Frontend\Models\UsersModel;
use Modules\User\Entities\TravellerEntity;

/**
 * Class UserController
 * @package App\Modules\Admin\Http\Controllers
 *  All functions related to user is written here
 */
class UserController extends BaseController
{
    /**
     * UserController constructor.
     * Extends BaseController constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Show login page
     * @return mixed
     */
    public function index()
    {
        if (session('admin')) {
            return Helper::redirect('admin/home');
        }
        \Assets::reset();
        \Assets::addCss([
            Helper::getThemePlugins("bootstrap/dist/css/bootstrap.min.css"),
            Helper::getThemePlugins("font-awesome/css/font-awesome.min.css"),
            Helper::getThemeCss("custom.min.css"),
            Helper::getThemeCss("admin.css"),
        ]);
        \Assets::addJs([
            Helper::getThemePlugins("jquery/dist/jquery.min.js")
        ]);
        return Theme::view('login.login');
    }
    
    /** Log user into site
     * @param {string} username
     * @param {string} password
     * redirect to admin dashboard on success
     * self-redirect with error messages on failure
     */
    public function login()
    {
        if (session('admin')) {
            return Helper::redirect('admin');
        }
        $username = request('username');
        $password = request('password');
        $users = new User();
        $user = $users->whereUsername($username)->first();
        if ($user && ($user->hasRole('admin') ||$user->hasRole('manager') ||$user->can('admin_login'))) {
            if (Hash::check($password, $user->password)) {
                session()->put('admin', $user);
                return Helper::redirect('admin');
            }
        }
        return Helper::redirect('admin/login')->withErrors(['login_required' => 'Incorrect user information']);
    }
    
    /**
     *
     */
    public function logout()
    {
        session()->flush();
        return Helper::redirect('admin/login');
    }
    
    /** Show list of all users
     * @return mixed
     */
    public function userList()
    {
        $admin = $this->isLoggedIn();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $user = new User();
        if($admin->hasRole('admin'))
            $user = $user->whereIn('type',['user','manager'])->orderBy('created_at','DESC')->get();
        else
            $user = $user->where('type','=','user')->orderBy('created_at','DESC')->get();
        $data = [
            'users' => $user,
            'admin' => $admin,
        ];
        return Theme::view('user.list', $data);
    }
    
    /**
     * Get user details
     * @param (int) $id : show self details if id is not provided
     * @return array
     */
    public function details($id = false)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can(['add_new_user', 'edit_user'])) return Helper::resultData(false, trans('admin.no_access_privilege'));
        if (!$id) {
            $user = $admin;
        } elseif (!is_numeric($id)) {
            return Helper::resultData(false, trans('admin.user_not_exists'));
        } else {
            $users = new User();
            $id = (int) $id;
            $user = $users->find($id);
        }
        if (!$user) return ['success' => false, 'message' => trans('admin.user_not_exists')];;
        $role = $user->roles()->first();
        $data = [
            'admin' => $admin,
            'user' => $user,
            'role' => $role
        ];
        $view = Theme::view('user.view', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    /**
     * @return mixed
     */
    public function newUser()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('add_new_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $roles = new Role();
        $roles = $roles->get();
        $is_guide = request('type') === 'guide';
        $data = [
            'admin' => $admin,
            'roles' => $roles,
            'user' => false,
            'is_guide' => $is_guide,
            'guide' => false
        ];
        $view = Theme::view('user.add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    /**
     * @param StoreNewUser $r
     * @return array
     */
    public function store(StoreNewUser $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('add_new_user')) exit;
        $users = new User();
        $request = $r->all();
        $role = (int) $request['role_id'];
        $admin_role = $admin->roles()->select('id')->first();
        if ($role <= $admin_role->id) return Helper::resultData(false, trans('admin.no_access_privilege'));
        //All fields to be inserted into user table
        $fields = ["username", "email", "full_name", "phone_number", "address", "password","birth_date","hobby","slogan"];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $data['password'] = Hash::make($data['password']);
        $data['status'] = 'active';
        $data['type'] = $this->roleIdToType($role);
        $user = Helper::mapData($users, $data);
        $user->save();
        if($r->file('img')){
            UsersModel::uploadAvatar($r->file('img'), $user);
        }
        $traveller = new TravellerEntity();
        $traveller->language_code = 'en';
        $traveller->user_id = $user->id;
        if($traveller->save()){
            //All fields to be inserted into user profile table
            $user->roles()->sync([$role]);
            return ['success' => true];
        }
        return Helper::jsondata(false,trans('site.error_occured'));
    }
    
    /**
     * @param bool $id
     * @return array
     */
    public function editUser($id = false)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('edit_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $id = (int) $id;
        if ($id === $admin->id) {
            $user = $admin;
        } else {
            $users = new User();
            $user = $users->select(['id', "username", "email", "full_name", "phone_number", "address", 'birth_date',"hobby","avatar","slogan"])->find($id);
        }
//        Show error if user does not exist
        if (!$user) return Helper::resultData(false, trans('admin.user_not_exists'));;
        if ($id !== $admin->id) {
            $role = $user->roles()->select('id')->first();
            if ($role) {
                $admin_role = $admin->roles()->select('id')->first();
//                Admin is not allowed to edit users with higher authority
                if ($role->id <= $admin_role->id) {
                    return Helper::resultData(true, trans('admin.no_access_privilege'));
                }
            }
        }
        $roles = new Role();
        $roles = $roles->get(['id', 'name', 'display_name']);
//        Slice birth date into day, month, and year to show
        $is_guide = request('type') === 'guide';
        $data = [
            'admin' => $admin,
            'user' => $user,
            'roles' => $roles,
            'is_guide'=>$is_guide,
            'is_admin'=>$admin->hasRole('admin')
        ];
        $view = Theme::view('user.add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    /**
     * @param EditUser $r
     * @return array
     */
    public function patch(EditUser $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('edit_user')) exit;
//        Get all the post variables
        $request = $r->all();
        $id = (int) $request['id'];
        $users = new User();
        $user = $users->select(['id', "email", "full_name", "phone_number", "address", "birth_date", "hobby"])->find($id);
        $result = $this->editValidation($user, $admin, $request);
        if (!$result['success']) return $result;
        //All fields to be inserted into user table
        $fields = ["full_name", "phone_number", "address", "birth_date", "hobby"];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
//        If password field is filled, make a new password hash out of it, remove the password field otherwise
        $role = (int) $request['role_id'];
        $data['type'] = $this->roleIdToType($role);

        $user = Helper::mapData($user, $data);
        $user->save();
        //All fields to be inserted into user profile table
        $profile = $user->traveller()->first();
        if (!$profile) {
            $profile = new TravellerEntity();
            $profile->slogan = $r->input('slogan');
            $profile->user_id = $user->id;
            $profile->save();
        }
//        Update avatar and cover if present
        if ($r->file('img')) {
            UsersModel::uploadAvatar($r->file('img'), $user);
        }
        
        if ($id !== $admin->id) {
            $role_id = $user->roles()->first()->id;
            if ($role_id !== $role) {
                $user->roles()->sync([$role]);
            }
        }
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return $result;
    }
    
    /** Check if new information is valid before accepting user changes
     * @param {Object} $user - user to be edited
     * @param {Object} $admin - current logged in admin
     * @param {Array} $request - $_POST from the client
     * @return array
     */
    function editValidation($user, $admin, $request)
    {
        $result = Helper::resultData(false);
        if (!$user) {
            $result['message'] = 'User does not exist';
            return $result;
        }
        $id = $user->id;
        if ($id !== $admin->id) {
            $role = (int) $request['role_id'];
            $admin_role = $admin->roles()->select('id')->first();
//            Check if the new role is higher than what admin can assign
            if ($role <= $admin_role->id) {
                $result['message'] = 'You don\'t have the authority to assign the role to this user';
                return $result;
            }
//            If user is not current admin, check if admin can edit this user
            $user_role = $user->roles()->first()->id;
            if ($user_role <= $admin_role->id) {
                $result['message'] = 'You don\'t have the authority to edit this user';
                return $result;
            }
        }
        $users = new User();
//        If current admin is the user to be edited, check if the old password is correct before allowing him to change his details
        /*if (!Hash::check($request['old_password'], $user->password)) {
            $result['message'] = 'Your old password is not correct';
            return $result;
        }*/
//        If user change email, check if the email exists
        if ($user->email !== $request['email']) {
            $user_exists = $users->whereEmail($request['email'])->exists();
            if ($user_exists) {
                $result['message'] = 'The email you entered has already been used';
                return $result;
            }
        }
//        If user change his personal identification number, check if it exists
        return ['success' => true];
    }
    
    /**
     * change status for traveller
     */
    function statustoggle()
    {
        $id = request()->input('id');
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('edit_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        
        $user = User::find($id);
        if (!$user)
            return Helper::resultData(false, trans('admin.user_not_exists'));
        $user->status = $user->status == 'active' ? 'inactive' : 'active';
        $user->save();
        return ['success' => true];
    }
    /** Remove user from the database
     * @return mixed
     */
    public function remove_temp()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('delete_user')) exit;
//        Get string of id(s) to be deleted
        $ids = request()->input('ids');
        if (!$ids) return Helper::resultData(false, trans('site.action_failed'));
//         Slice id(s) into array
        $ids = explode(',', $ids);
        $ids = \Helper::stringArrayToInt($ids);
        $users = new User();
        $admin_role = $admin->roles()->first();
        $error = false;
//        Check if current admin id is in the array, remove it if it is
        if (($key = array_search($admin->id, $ids)) !== false) {
            unset($ids[$key]);
        }
        $i = 0;
        foreach ($ids as $id) {
//            Show error if any of the user is not deleted
            if ($id === $admin->id) {
                $error = true;
                continue;
            } else {
                $user = $users->find($id);
                $role = $user->roles()->first();
                if ($role->id > $admin_role->id) {
                    if ($user) {
                        $user->roles()->sync([]);
                    }
                    $profile = $user->profile()->first();
                    if ($profile) {
                        $avatar = $profile->avatar;
                        $cover = $profile->cover;
                        $this->removeUserImages('cover', $cover);
                        $this->removeUserImages('avatar', $avatar);
                    }
                    $users::where('id', $id)->delete();
                    $i++;
                } else {
                    $error = true;
                }
            }
        }
        
        if ($error) {
            session()->flash('action_succeeded', trans('admin.some_users_not_deleted'));
        } else {
            session()->flash('action_succeeded', trans('site.action_succeeded'));
        }
        $result = Helper::resultData(true);
        return Helper::json($result);
    }
    
    /** Remove user from the database
     * @return mixed
     */
    public function remove()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('delete_user')) exit;
//        Get string of id(s) to be deleted
        $id = request()->input('ids');
        if (!$id) return Helper::resultData(false, trans('site.action_failed'));
//         Slice id(s) into array
        $user = User::find($id);
        if (!$user)
            return Helper::resultData(false, trans('admin.user_not_exists'));
        $type = $user->status == 'blocked' ? 'active' : 'blocked';
        $user->status = $type;
        $user->save();
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        $result = Helper::resultData(true);
        return Helper::json($result);
    }
    /** Delete user images if exist
     * @param $type
     * @param $name
     */
    public function removeUserImages($type, $name)
    {
        if (!$name) return;
        if (file_exists(public_path("images/" . $type) . '/' . $name))
            unlink(public_path("images/" . $type . '/' . $name));
    }
    
    /**
     * @param {Object} $file - image sent from client
     * @param {string} $name - image name
     * @param {string} $type - image type, should be either "cover" or "avatar"
     * @param {int} $width - max image width in pixel
     * @param {int} $height - max image height in pixel
     *              Image will be cropped if the ratio is not correct
     * @return {string} file name if succeeds
     */
    public function uploadUserImage($file, $name, $type = 'avatar', $width = 0, $height = 0)
    {
        if ($file) {
            $image = Image::make($file);
            $mime = $image->mime();
            $mime_arr = explode('/', $mime);
            $file_type = $mime_arr[1];
            $file_type = $file_type === 'jpeg' ? 'jpg' : $file_type;
            $img_width = $image->width();
            $img_height = $image->height();
            $ratio = $img_width / $img_height;
            $mWidth = $width ?: config('image.base_width', 250);
            $mHeight = $height ?: config('image.base_height', 250);
            $baseRatio = $mWidth / $mHeight;
            if ($ratio !== $baseRatio) {
                if ($ratio > $baseRatio) {
                    $image->crop($img_height * $baseRatio, $img_height);
                } else {
                    $image->crop($img_width, $img_width / $baseRatio);
                }
            }
            if ($image->width() > $mWidth || $image->height() > $mHeight) {
                if ($ratio > $baseRatio) {
                    $image->resize($mWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $image->resize(null, $mHeight, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }
            $image->encode($file_type, 90);
            $file_name = $name . '.' . $file_type;
            if ($image->save(public_path("images/" . $type . '/' . $file_name))) {
                return $file_name;
            }
        }
        return '';
    }
    
    function cities(){
        $country_id = request('country_id');
        if(!$country_id)
            return Helper::resultData(false);
        $cities = UsersModel::getCity($country_id);
        return Helper::resultData(true,'',$cities);
    }
    
    /*
     * Convert role id to role type
     * */
    private function roleIdToType($id)
    {
        switch ($id) {
            case 1:
                return 'admin';
            case 2:
                return 'manager';
            case 3:
                return 'guide';
            default:
                return 'user';
        }
    }
    
    /**
     *run only once
     */
    public function newAdmin()
    {
        /*$users = new User();
        $users->username = 'admin';
        $users->email = 'admin@lifgoo.com';
        $users->password = Hash::make('lifgoo@2017');
        $users->full_name = 'Administration';
        $users->address = 'Hue';
        $users->phone_number = '123456789';
        $users->birth_date = date('Y-m-d', strtotime('1994-02-24'));
        $users->birth_place = 'Hue';
        $users->status = 'active';
        $users->type = 'admin';
        $users->save();
        $users->roles()->attach(1);*/ //id only
    }
    
    /**
     *run only once
     */
    public function newRoles()
    {
        /*$admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'User Administrator';
        $admin->description = 'User is allowed to manage and edit other users';
        $admin->save();
        $manager = new Role();
        $manager->name = 'manager';
        $manager->display_name = 'Site Manager';
        $manager->save();

        $guide = new Role();
        $guide->name = 'guide';
        $guide->display_name = 'Tour Guide';
        $guide->save();

        $user = new Role();
        $user->name = 'user';
        $user->display_name = 'User';
        $user->save();*/
    }
    
    /**
     * run only once
     */
    public function newPermissions()
    {
        /*$permissions = [];

        $user = new Permission();
        $user->display_name = 'View user list';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'Add new user';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'Delete user';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'Edit user';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'View user group';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'Add new group';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'Delete group';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'Edit group';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'config language';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'config service';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $user = new Permission();
        $user->display_name = 'access news';
        $user->name = Helper::gen_slug($user->display_name);
        $user->save();
        $permissions[] = $user->id;
        $admin = new Role();
        $admin = $admin->first();
        $admin->perms()->sync($permissions);*/
//        $admin->perms()->sync([1,2,3,4,5,6,7,8,9,10,11]);
        //$admin->attachPermissions($user);
        
    }
}
