<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/20/2017
 * Time: 8:35 AM
 */

namespace Modules\Admin\Http\Controllers;

use Caffeinated\Themes\Facades\Theme;
use Helper;
use Illuminate\Support\Facades\Route;
use Modules\Admin\Requests\Image;
use Modules\Admin\Requests\Language;
use Modules\Admin\Requests\System;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\UsersModel;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\GuideServicesEntity;
use Modules\User\Entities\LanguageEntity;
use Modules\User\Entities\NewsEntity;
use Modules\User\Entities\ServiceEntity;
use Modules\User\Entities\ServiceLanguageEntity;
use Modules\User\Entities\SucriberEntity;

class SystemController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        Theme::setLayout('frontend.layouts.master');
        //$serviceModel = new ServiceEntity();
    }
    
    /**
     * Show login page
     * @return mixed
     */
    public function index()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('config_service')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $service = new ServiceEntity();
        $service = $service->get();
        $data = [
            'services' => $service,
            'admin' => $admin,
            
        ];
        return Theme::view('system.service', $data);
    }
    
    public function editService($id = false)
    {
        $admin = $this->loggedInStatus();
        if ($id && !is_numeric($id))
            return Helper::resultData(false, trans('admin.service_not_exists'));
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_service')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $languages = new LanguageEntity();
        $languages = $languages->get();
        $data = [
            'admin' => $admin,
            'languages' => $languages,
        ];
        if($id){
            $service = new ServiceEntity();
            $id = (int) $id;
            $service = $service->find($id);
            if (!$service) return Helper::resultData(false, trans('admin.service_not_exists'));
            $data['service'] = $service;
        }
        
        $view = Theme::view('admin::system.service_add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    function patch(System $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_service')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $data = $r->all();
        $service = new ServiceEntity();
        $service = $service->find($data['id']);
        if (!$service) {
            return Helper::resultData(false, trans('admin.service_not_exist'));
        }
        $service->type = $data['type'];
        $service->icon = $r->input('icon');
        $service->save();
        if ($service) {
            //$serviceLang = new ServiceLanguageEntity();
            $serviceLang = ServiceLanguageEntity::whereService_id($data['id'])->whereLanguage_code('en')->first();
            if(!$serviceLang){
                $serviceLang = new ServiceLanguageEntity();
                $serviceLang->service_id = $data['id'];
                $serviceLang->language_code = 'en';
            }
            $serviceLang->service_name = $r->input('service_name_en');
            $serviceLang->save();
            $serviceLang = ServiceLanguageEntity::whereService_id($data['id'])->whereLanguage_code('vi')->first();
            if(!$serviceLang){
                $serviceLang = new ServiceLanguageEntity();
                $serviceLang->service_id = $data['id'];
                $serviceLang->language_code = 'vi';
            }
            $serviceLang->service_name = $r->input('service_name_vi');
            $serviceLang->save();
            session()->flash('action_succeeded', trans('site.action_succeeded'));
            return Helper::resultData(true);
        }
        return Helper::resultData(false, trans('site.error_occured'));
    }
    
    /**
     * create new service
     * @param System $r
     * @return mixed
     */
    function store(System $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_service')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $service = new ServiceEntity();
        $service->type = $r->input('type');
        $service->icon = $r->input('icon');
        $service->status = 'active';
        $service->save();
        if($service){
            $serviceLang = new ServiceLanguageEntity();
            $serviceLang->service_id = $service->id;
            $serviceLang->service_name = $r->input('service_name_en');
            $serviceLang->language_code = 'en';
            $serviceLang->save();
            $serviceLang = new ServiceLanguageEntity();
            $serviceLang->service_id = $service->id;
            $serviceLang->service_name = $r->input('service_name_vi');
            $serviceLang->language_code = 'vi';
            $serviceLang->save();
            session()->flash('action_succeeded', trans('site.action_succeeded'));
            return Helper::resultData(true);
        }
        return Helper::resultData(false, trans('site.error_occured'));
    }
    
    function remove()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_service')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $ids = request()->input('ids');
        if (!$ids) return Helper::resultData(false, trans('site.action_failed'));
        $ids = explode(',', $ids);
        $ids = Helper::stringArrayToInt($ids);
        foreach ($ids as $id) {
            $guide_services = GuideServicesEntity::where('service_id',$id)->select('id')->get();
            if($guide_services)
                foreach ($guide_services as $gs){
                    $gs->traveller_booking_service()->delete();
                    $gs->delete();
                }
            UsersModel::deleteRelateService($id);
        }
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        $result = Helper::resultData(true);
        return Helper::json($result);
    }
    
    /**
     * Show login page
     * @return mixed
     */
    public function language()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('config_language')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $languages = new LanguageEntity();
        $languages = $languages->get();
        $data = [
            'languages' => $languages,
            'admin' => $admin,
        
        ];
        return Theme::view('admin::system.language', $data);
    }
    
    public function editLanguage($id = false)
    {
        $admin = $this->loggedInStatus();
        if ($id && !is_numeric($id))
            return Helper::resultData(false, trans('admin.language_not_exists'));
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_language')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        
        $data = [
            'admin' => $admin,
        ];
        if ($id) {
            $language = new LanguageEntity();
            $id = (int) $id;
            $language = $language->find($id);
            if (!$language) return Helper::resultData(false, trans('admin.language_not_exists'));
            $data['language'] = $language;
        }
        
        $view = Theme::view('admin::system.language_add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    function patchLanguage(System $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_language')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $data = $r->all();
        $language = new LanguageEntity();
        $language = $language->find($data['id']);
        if (!$language) {
            return Helper::resultData(false, trans('admin.language_not_exist'));
        }
        $language->name = $data['name'];
        $language->code = $data['code'];
        $language->save();
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return Helper::resultData(true);
    }
    
    /**
     * create new language
     * @param System $r
     * @return mixed
     */
    function storeLanguage(System $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_language')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $data = $r->all();
        $language = new LanguageEntity();
        $language->name = $data['name'];
        $language->code = $data['code'];
        $language->save();
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return Helper::resultData(true);
        
    }
    
    function removeLanguage()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('config_language')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $ids = request()->input('ids');
        if (!$ids) return Helper::resultData(false, trans('site.action_failed'));
        $ids = explode(',', $ids);
        $ids = Helper::stringArrayToInt($ids);
        foreach ($ids as $id) {
            $language = LanguageEntity::find($id)->delete();
        }
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        $result = Helper::resultData(true);
        return Helper::json($result);
    }
    
    /**
     * Show login page
     * @return mixed
     */
    public function news()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $news = new NewsEntity();
        $news = $news->orderBy('created_at','desc')->get();
        $data = [
            'news' => $news,
            'admin' => $admin
        ];
        return Theme::view('news.news', $data);
    }
    
    /**
     * create new news
     * @param System $r
     * @return mixed
     */
    function editNews($id = false)
    {
        $admin = $this->loggedInStatus();
        //Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax() || !$admin->can('access_news') )
            return Helper::resultData(false, trans('admin.no_access_privilege'));
        if ($id && !is_numeric($id))
            return Helper::resultData(false, trans('admin.news_not_exists'));
        $country = UsersModel::getCountry(null, ['country_id', 'country_name']);
        $data = [
            'admin' => $admin,
            'countries' => $country,
            'news'=>''
        ];
        if($id){
            $news = new NewsEntity();
            $id = (int) $id;
            $news = $news->find($id);
            if (!$news) return Helper::resultData(false, trans('admin.news_not_exists'));
            $data['news'] = $news;
        }
        $view = Theme::view('news.news_add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    /**
     * create new news
     * @param System $r
     * @return mixed
     */
    function storeNews(System $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('access_news')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $file = $r->file('img');
        if(!$file)
            return Helper::resultData(false, 'Thumbnail of news is required');
        $data = $r->all();
        $news = [
            'city_id'=>$data['city_id'],
            'country_id'=>$data['country_id'],
            'user_id'=>$admin->id
        ];
        $news = SystemModel::storeNews($news);
        if(!$news)
            return Helper::resultData(false, trans('site.error_occured'));
        $newsLanguage = [
            'title'=>$data['title_en'],
            'language_code'=> 'vi',
            'content'=>$data['content_en'],
            'content_summary'=>$data['content_summary_en'],
            'news_id'=>$news->id
        ];
        SystemModel::storeNewsLanguage($newsLanguage);
        $newsLanguage = [
            'title' => $data['title_en'],
            'language_code' => 'en',
            'content' => $data['content_en'],
            'content_summary' => $data['content_summary_en'],
            'news_id' => $news->id
        ];
        SystemModel::storeNewsLanguage($newsLanguage);
        $upload = UsersModel::uploadImg($file,$news);
        if($upload['success']){
            session()->flash('action_succeeded', trans('site.action_succeeded'));
        }
        return Helper::json($upload);
    }
    
    /**
     * Get news details
     * @param (int) $id : show self details if id is not provided
     * @return array
     */
    public function detailsNews($id = false)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('access_news')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        if ($id && !is_numeric($id)) {
            return Helper::resultData(false, trans('admin.news_not_exists'));
        } else {
            $news = new NewsEntity();
            $id = (int) $id;
            $news = $news->find($id);
        }
        if (!$news) return ['success' => false, 'message' => trans('admin.news_not_exists')];;
        $data = [
            'admin' => $admin,
            'news' => $news,
        ];
        $view = Theme::view('news.news_add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    /**
     * edit news
     * @param System $r
     * @return mixed
     */
    function patchNews(System $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('access_news')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $data = $r->all();
        $news = [
            'id'=>$data['id'],
            'city_id' => $data['city_id'],
            'country_id' => $data['country_id'],
            'user_id' => $admin->id
        ];
        $news = SystemModel::patchNews($news);
        if (!$news)
            return Helper::resultData(false, trans('site.error_occured'));
        $newsLanguage = [
            'title' => $data['title_en'],
            'language_code' => 'vi',
            'content' => $data['content_en'],
            'content_summary' => $data['content_summary_en'],
            'news_id' => $news->id
        ];
        SystemModel::patchNewsLanguage($newsLanguage);

        $newsLanguage = [
            'title' => $data['title_en'],
            'language_code' => 'en',
            'content' => $data['content_en'],
            'content_summary' => $data['content_summary_en'],
            'news_id' => $news->id
        ];
        SystemModel::patchNewsLanguage($newsLanguage);
        $file = $r->file('img');
        if($file){
            $upload = UsersModel::uploadImg($file, $news);
        }
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return Helper::json(true);
    }
    
    /**
     * delete news
     * @return mixed
     */
    function removeNews()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('access_news')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $ids = request()->input('ids');
        if (!$ids) return Helper::resultData(false, trans('site.action_failed'));
        $ids = explode(',', $ids);
        $ids = Helper::stringArrayToInt($ids);
        foreach ($ids as $id) {
            SystemModel::deleteRelateNews($id);
        }
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        $result = Helper::resultData(true);
        return Helper::json($result);
    }
    /**
     * Show subcriber
     * @return mixed
     */
    public function subcriber()
    {
        $admin = $this->loggedInStatus();
        if(!$admin)
            return Helper::resultData(false, trans('admin.no_access_privilege'));
        $subcriber = new SucriberEntity();
        $subcribers = $subcriber->get();
        $data = [
            'subcribers' => $subcribers,
        ];
        return Theme::view('admin::system.subcriber', $data);
    }
}