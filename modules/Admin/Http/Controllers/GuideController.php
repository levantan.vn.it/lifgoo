<?php

namespace Modules\Admin\Http\Controllers;

use App\User;
use Caffeinated\Themes\Facades\Theme;
use Helper;
use Illuminate\Support\Facades\Hash;
use LaravelLocalization;
use Modules\Admin\Requests\EditGuide;
use Modules\Admin\Requests\StoreNewGuide;
use Modules\Frontend\Entities\GuideInfoEntities;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\UsersModel;
use Modules\User\Entities\GuideCertificationEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\GuideLocationTourEntity;
use Modules\User\Entities\LanguageEntity;
use Modules\User\Entities\ServiceEntity;

/**
 * Class UserController
 * @package App\Modules\Admin\Http\Controllers
 *  All functions related to user is written here
 */
class GuideController extends BaseController
{
    /**
     * UserController constructor.
     * Extends BaseController constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Show login page
     * @return mixed
     */
    public function index()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $guides = new GuideEntity();
        $guides = $guides->orderBy('created_at','desc')->get();
        $data = [
            'guides' => $guides,
            'admin' => $admin
        ];
        return Theme::view('guides.list', $data);
    }
    
    /**
     * Get user details
     * @param (int) $id : show self details if id is not provided
     * @return array
     */
    public function details($id = false)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can(['add_new_user', 'edit_user'])) return Helper::resultData(false, trans('admin.no_access_privilege'));
        if (!$id || !is_numeric($id)) {
            return Helper::resultData(false, trans('admin.user_not_exists'));
        } else {
            $guides = new GuideEntity();
            $id = (int) $id;
            $guide = $guides->find($id);
        }
        if (!$guide) return ['success' => false, 'message' => trans('admin.user_not_exists')];;
        $user = $guide->user()->first();
        if (!$user) return ['success' => false, 'message' => trans('admin.user_not_exists')];;
        $role = $user->roles()->first();
        $guideModel = new GuidesModel();
        $userModel = new UsersModel();
        $discount = $userModel->getAllDiscount($id);
        $data = [
            'admin' => $admin,
            'guide' => $guide,
            'user' => $user,
            'role' => $role,
            'knownLanguage'=>$guide->known_languages()->select(['language_id'])->get(),
            'discounts'=>json_encode($guideModel->getDiscount($discount)),
            'guideServices' => $guideModel->guideServices($guide->id,'service'),
            'guidetransportations' => $guideModel->guideServices($guide->id,'transportation'),
            'certification' => $user->certification()->select('id', 'idcard', 'expiry_date', 'place_of_issue', 'type', 'cert_lang','location_allowed')->first(),
            'guide_locations'=>$guide->locationTour()->get()
        ];
        $view = Theme::view('guides.view', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    public function newGuide()
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('add_new_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $country = UsersModel::getCountry(null, ['country_id', 'country_name']);
        $services = ServiceEntity::whereType('service')->get();
        $transportation = ServiceEntity::whereType('transportation')->get();
        $language = new LanguageEntity();
        $data = [
            'admin' => $admin,
            'user' => false,
            'guide' => false,
            'countries' => $country,
            'languages'=> $language->get(),
            'services'=>$services,
            'transportations'=>$transportation
        ];
        $view = Theme::view('guides.add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    public function storeProfile(StoreNewGuide $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('add_new_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $users = new User();
        $request = $r->all();
        $role = 3;
        //All fields to be inserted into user table
        $fields = ["username", "email", "full_name", "phone_number", "address", "password", "birth_date", "hobby","gender"];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $data['password'] = Hash::make($data['password']);
        $data['status'] = 'active';
        $data['type'] = $this->roleIdToType($role);
        $user = Helper::mapData($users, $data);
        $user->save();
        if ($r->file('img')) {
            UsersModel::uploadAvatar($r->file('img'), $user);
        }
        $user->roles()->sync([$role]);
    
        $fields = ["hourly_rate", "slogan", "introduce", "diploma_info", "experience"];
        $fields = array_flip($fields);
        $info = array_intersect_key($request, $fields);
        $info['idnumber'] = $request['passport_id_number'];
        $info['status'] = 'available';
        $info['user_id'] = $user->id;
        $info['type'] = 'guide';
        $guideInfo = new GuideInfoEntities();
        $guideInfo = Helper::mapData($guideInfo, $info);
        $guideInfo->save();
        
        $fields = ["idcard", "expiry_date", "place_of_issue", "cert_lang", "type", "status"];
        $fields = array_flip($fields);
        $cerarr = array_intersect_key($request, $fields);
        $cerarr['user_id'] = $user->id;
        $cerarr['user_type'] = 'guide';
        $cer = new GuideCertificationEntity();
        $cer = Helper::mapData($cer, $cerarr);
        $cer->save();
        
        $location = new GuideLocationTourEntity();
        $location->guide_id = $guideInfo->id;
        $location->country_id = $request['country_id'];
        isset($request['city_id']) && $location->city_id = $request['city_id'];
        $location->save();
    
        $service = [
            'guide_id' => $guideInfo->id,
            'type' => 'service',
        ];
    
        if (isset($request['service_id']) && $ids = $request['service_id']) {
            foreach ($ids as $id) {
                $service['service_id'] = $id;
                SystemModel::storeService($service);
            }
        }
    
        if (isset($request['transportation_id']) && $ids = $request['transportation_id']) {
            foreach ($ids as $id) {
                $service['service_id'] = $id;
                $service['type'] = 'transportation';
                SystemModel::storeService($service);
            }
            }
        return ['success' => true];
    }
    
    /*
     * Convert role id to role type
     * */
    private function roleIdToType($id)
    {
        switch ($id) {
            case 1:
                return 'admin';
            case 2:
                return 'manager';
            case 3:
                return 'guide';
            default:
                return 'user';
        }
    }
    /**
     * change status for guide
     */
    function statustoggle()
    {
        $id = request()->input('id');
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('edit_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        
        $user = User::find($id);
        if (!$user)
            return Helper::resultData(false, trans('admin.user_not_exists'));
        $user->status = $user->status == 'active' ? 'inactive' : 'active';
        $user->save();
        session()->flash('action_succeeded', trans('site.action_succeeded'));
        return ['success' => true];
    }
    
    /**
     * edit guide
     * @param bool $id
     * @return mixed
     */
    public function editGuide($id = false)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('edit_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $id = (int) $id;

        $users = new User();
        $user = $users->select(['id', "username", "email", "full_name", "phone_number", "address", 'birth_date', "hobby", "avatar"])->find($id);
//        Show error if user does not exist
        if (!$user) return Helper::resultData(false, trans('admin.user_not_exists'));;
        $country = UsersModel::getCountry(null, ['country_id', 'country_name']);
//        Slice birth date into day, month, and year to show
        $language = new LanguageEntity();
        $services = ServiceEntity::whereType('service')->get();
        $transportation = ServiceEntity::whereType('transportation')->get();
        $data = [
            'admin' => $admin,
            'user' => $user,
            'countries' => $country,
            'languages' => $language->get(),
            'services' => $services,
            'transportations' => $transportation
        ];
        $view = Theme::view('guides.add', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }
    
    public function patch(EditGuide $r)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$admin->can('add_new_user')) return Helper::resultData(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $id = $request['id'];
        $user = User::find($id);
        if (!$user && $user->type == 'guide') return Helper::resultData(false, trans('admin.user_not_exists'));
        $fields = ["full_name", "phone_number", "address", "birth_date", "hobby", "gender"];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $user = Helper::mapData($user, $data);
        $user->save();
        if ($r->file('img')) {
            UsersModel::uploadAvatar($r->file('img'), $user);
        }
        $guideInfo = $user->guide()->first();
        if ($guideInfo) {
            $fields = ["hourly_rate", "slogan", "introduce", "diploma_info", "experience"];
            $fields = array_flip($fields);
            $info = array_intersect_key($request, $fields);
            $info['idnumber'] = $request['passport_id_number'];
            $guideInfo = Helper::mapData($guideInfo, $info);
            $guideInfo->save();
        
            $guideInfo->services()->delete();
            $service = [
                'guide_id' => $guideInfo->id,
                'type' => 'service',
            ];
            if (isset($request['service_id']) && $ids = $request['service_id']) {
                foreach ($ids as $id) {
                    $service['service_id'] = $id;
                    SystemModel::storeService($service);
                }
            }
        
            if (isset($request['transportation_id']) && $ids = $request['transportation_id']) {
                foreach ($ids as $id) {
                    $service['service_id'] = $id;
                    $service['type'] = 'transportation';
                    SystemModel::storeService($service);
                }
            }
        }
    
        $cer = $user->certification()->first();
        if ($cer) {
            $fields = ["idcard", "expiry_date", "place_of_issue", "cert_lang", "type", "status"];
            $fields = array_flip($fields);
            $cerarr = array_intersect_key($request, $fields);
            $cer = Helper::mapData($cer, $cerarr);
            $cer->save();
        }
        return ['success' => true];
    }

    /**
     * show approval page
     * @return $this
     */
    public function approval()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $guides = new GuideEntity();
        $guides = $guides->where('is_approved','=',0)->orderBy('created_at','desc')->get();
        $data = [
            'guides' => $guides,
            'admin' => $admin
        ];
        return Theme::view('guides.approval', $data);
    }

    function approved($id=null){
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $user = UsersModel::getUserById($id);
        $lackInfo = $user->lackOfInformation($user);
        $isMissing = $lackInfo['missing_profile'] || $lackInfo['missing_jobInfo'];
        if(!$isMissing){
            $guide = $user->guide()->select('id','is_approved')->first();
            $guide->is_approved = 1;
            $guide->save();
            session()->flash('action_succeeded', trans('site.action_succeeded'));
            return ['success' => true];
        }
        return Helper::resultData(false, 'Information of guide is missing');
    }
}
