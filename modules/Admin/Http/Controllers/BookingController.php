<?php

namespace Modules\Admin\Http\Controllers;

use App\User;
use Caffeinated\Themes\Facades\Theme;
use Helper;
use Illuminate\Support\Facades\Hash;
use LaravelLocalization;
use Modules\Admin\Requests\EditGuide;
use Modules\Admin\Requests\StoreNewGuide;
use Modules\Frontend\Entities\GuideInfoEntities;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\UsersModel;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\GuideCertificationEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\GuideLocationTourEntity;
use Modules\User\Entities\LanguageEntity;
use Modules\User\Entities\ServiceEntity;

/**
 * Class UserController
 * @package App\Modules\Admin\Http\Controllers
 *  All functions related to user is written here
 */
class BookingController extends BaseController
{
    /**
     * UserController constructor.
     * Extends BaseController constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Show login page
     * @return mixed
     */
    public function index()
    {
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $booking = new BookingEntity();
        $bookings = $booking->orderBy('created_at','desc')->get();
        $data = [
            'bookings' => $bookings,
        ];
        return Theme::view('booking.list', $data);
    }
    function editUser($filter)
    {
        
        $admin = $this->loggedInStatus();
        if (!$admin->can('view_user_list')) {
            return redirect()->back()->withErrors(['no_access' => trans('admin.no_access_privilege')]);
        }
        $booking = new BookingEntity();        
        $bookings = $booking->where('status','=',$filter)->orderBy('created_at','desc')->get();
        $data = [
            'bookings' => $bookings,
        ];
        return Theme::view('booking.list', $data);
    }
    
    /**
     * Get user details
     * @param (int) $id : show self details if id is not provided
     * @return array
     */
    public function details($id = false)
    {
        $admin = $this->loggedInStatus();
//        Show errors if admin is not logged in or request is not ajax
        if (!$admin || !request()->ajax()) return Helper::resultData(false, trans('admin.no_access_privilege'));
//        Show errors if admin does not have the authority to do the action
        if (!$id || !is_numeric($id)) {
            return Helper::resultData(false, 'Booking does not exists');
        }
        $booking = BookingEntity::find($id);
        $data = [
            'admin' => $admin,
            'messages' => $booking->messageBooking()->orderBy('created_at', 'desc')->limit(100)->get(),
            'dates' => $booking->datesBooking()->get(),
            'booking'=>$booking
        ];
        $view = Theme::view('booking.view', $data)->render();
        $result = Helper::resultData(true, $view);
        return Helper::json($result);
    }

}
