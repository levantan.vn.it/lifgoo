<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale() . '/admin/',
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function () {
    Route::get('login', 'UserController@index');
    Route::post('login', 'UserController@login');
    
    /*
     *  Run only once
     * */
    Route::get('newRole', 'UserController@newRoles');
    Route::get('createAdmin', 'UserController@newAdmin');
    Route::get('newPermissions', 'UserController@newPermissions');
    Route::get('cities', 'UserController@cities');
});

Route::group(['prefix' => LaravelLocalization::setLocale() . '/admin/',
    'middleware' => ['admin', 'localeSessionRedirect', 'localizationRedirect']
], function () {
    Route::get('/', 'BaseController@index');
    Route::get('logout', 'UserController@logout');
    /*
     * User-related routes
     * */
    Route::get('user/list', 'UserController@userList');
    Route::get('user/details', 'UserController@details');
    Route::get('user/details/{id}', 'UserController@details');
    Route::post('user/delete', 'UserController@remove');
    Route::get('user/new', 'UserController@newUser');
    Route::post('user/new', 'UserController@store');
    Route::get('user/edit/{id}', 'UserController@editUser');
    Route::post('user/edit', 'UserController@patch');
    Route::post('user/statustoggle', 'UserController@statustoggle');
    /*
     *  User group routes, role permission
     * */
    Route::get('roles/list', 'UserGroupController@index');
    Route::post('roles/delete', 'UserGroupController@remove');
    Route::get('roles/create', 'UserGroupController@editUserGroup');
    Route::post('roles/create', 'UserGroupController@store');
    Route::get('roles/edit/{id}', 'UserGroupController@editUserGroup');
    Route::post('roles/edit', 'UserGroupController@patch');
    /*
     *  Guide-related routes
     * */
    Route::get('guide/list', 'GuideController@index');
    Route::get('guide/approval', 'GuideController@approval');
    Route::get('guide/details/{id}', 'GuideController@details');
    Route::get('guide/approve/{id}', 'GuideController@approved');
    Route::post('guide/delete', 'GuideController@remove');
    Route::get('guide/new', 'GuideController@newGuide');
    Route::post('guide/newProfile', 'GuideController@storeProfile');
    Route::get('guide/edit/{id}', 'GuideController@editGuide');
    Route::post('guide/editProfile', 'GuideController@patch');
    Route::post('guide/statustoggle', 'GuideController@statustoggle');
    /**
     * system routes
     */
    Route::get('service/list', 'SystemController@index');
    Route::get('service/edit', 'SystemController@editService');
    Route::post('service/new', 'SystemController@store');
    Route::get('service/edit/{id}', 'SystemController@editService');
    Route::post('service/edit', 'SystemController@patch');
    Route::post('service/delete', 'SystemController@remove');
    /**
     * language routes
     */
    Route::get('language/list', 'SystemController@language');
    Route::get('language/edit', 'SystemController@editlanguage');
    Route::post('language/new', 'SystemController@storeLanguage')->name('language.new');;
    Route::get('language/edit/{id}', 'SystemController@editlanguage');
    Route::post('language/edit', 'SystemController@patchLanguage')->name('language.edit');
    Route::post('language/delete', 'SystemController@removeLanguage');
    /**
     * news routes
     */
    Route::get('news/list', 'SystemController@news');
    Route::get('news/edit', 'SystemController@editNews');
    Route::post('news/new', 'SystemController@storeNews')->name('news.new');
    Route::get('news/edit/{id}', 'SystemController@editNews');
    Route::post('news/edit', 'SystemController@patchNews')->name('news.edit');
    Route::post('news/delete', 'SystemController@removeNews');
    Route::post('news/upload', 'SystemController@upload');
    Route::get('news/details/{id}', 'SystemController@detailsNews');
    /**
     * booking routes
     */
    Route::get('booking/list', 'BookingController@index');
    Route::get('booking/list/{type}', 'BookingController@editUser');
    Route::get('booking/details/{id}', 'BookingController@details');
    /**
     * subcriber
     */
    Route::get('subcriber/list', 'SystemController@subcriber');
    /**
     * notify
     */
    Route::get('updateIsClick', 'HomeController@updateIsClick');
    Route::get('markRead', 'HomeController@markRead');
    Route::get('intervalNotify', 'HomeController@intervalNotify');
});
