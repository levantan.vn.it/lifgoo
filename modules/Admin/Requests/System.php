<?php

namespace Modules\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class System extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Route::currentRouteName();
        $rules = [
            'type' => 'required|max:20',
            'service_name_en' => 'required|max:100',
            'service_name_vi' => 'required|max:100',
        ];
        if ($route == 'language.new') {
            $rules = [
                'name' => 'required|max:100',
                'code' => 'required|max:20|unique:system__languages',
            ];
        }
        if ($route == 'language.edit') {
            $rules = [
                'name' => 'required|max:100',
                'code' => 'required|max:20',
            ];
        }
        if($route == 'news.new'){
            $rules = [
                'title_en' => 'required|max:100',
                'content_en' => 'required',
                'content_summary_en' => 'required|max:300',
                'city_id' => 'required',
                'country_id' => 'required',
                'img'=> 'file|image|max:4194304',
            ];
        }
        if ($route == 'news.edit') {
            $rules = [
                'title_en' => 'required|max:100',
                'content_en' => 'required',
                'content_summary_en' => 'required|max:300',
                'city_id' => 'required',
                'country_id' => 'required'
            ];
        }
        return $rules;
    }
}

