<?php

namespace Modules\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:100|unique:user__users,username|unique:user__users,email',
            'full_name' => 'required|max:100',
            'email' => 'unique:user__users,email|required|email|max:100|unique:user__users,username',
            'birth_date' => 'max:31|max:10',
            'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
            'phone_number' => 'regex:/^[0-9]+$/',
            'role_id' => 'required',
            'password' => 'required',
            'password-confirmation' => 'required|same:password',
            'address'=>'required'
        ];
    }
}
