<?php

namespace Modules\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Image extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'img' => 'file|image|max:4194304|dimensions:min_width=' . env('thumb_width') . ',min_height=' . env('thumb_height'),
        ];
    }
}
