<?php

namespace Modules\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class StoreNewGuide extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Route::currentRouteName();
        $rule = [
            'username' => 'required|max:100|unique:user__users,username|unique:user__users,email',
            'full_name' => 'required|max:100',
            'email' => 'unique:user__users,email|required|email|max:100|unique:user__users,username',
            'birth_date' => 'max:31|max:10',
            'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
            'phone_number' => 'regex:/^[0-9]+$/',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'hobby' => 'required',
            'slogan' => 'required',
            'address' => 'required',
            'country_id' => 'required|regex:/^[0-9]+$/',
            'city_id' => 'regex:/^[0-9]+$/',
            'idcard' => 'required',
            'place_of_issue' => 'required',
            'type' => 'required',
            'status' => 'required',
            'cert_lang' => 'required',
            'experience' => 'required',
            'introduce' => 'required',
            'passport_id_number' => 'required',
            'hourly_rate' => 'required|integer',
        ];
        if($route == 'newInfo')
            $rule = [
                'username' => 'required|max:100|unique:user__users,username',
                'full_name' => 'required|max:100',
                'email' => 'unique:user__users,email|required|email|max:100',
                'birth_date' => 'max:31|max:10',
                'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
                'phone_number' => 'regex:/^[0-9]+$/',
                'role_id' => 'required',
                'password' => 'required',
                'password-confirmation' => 'required|same:password',
                'hobby' => 'required',
                'slogan' => 'required',
                'address' => 'required'
            ];
        return $rule;
    }
}
