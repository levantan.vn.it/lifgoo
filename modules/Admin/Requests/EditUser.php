<?php

namespace Modules\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'password' => 'filled',
            'full_name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'birth_date' => 'max:31|max:10',
            'phone_number' => 'required|regex:/^[0-9]+$/',
            'role_id' => 'required',
            'password-confirmation' => 'same:password',
            'address' => 'required'
        ];
    }
}
