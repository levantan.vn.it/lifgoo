<?php

namespace Modules\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditGuide extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required',
            'username' => 'required|max:100',
            'full_name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'birth_date' => 'max:31|max:10',
            'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
            'phone_number' => 'regex:/^[0-9]+$/',
            'hobby' => 'required',
            'slogan' => 'required',
            'address' => 'required',
            'idcard' => 'required',
            'place_of_issue' => 'required',
            'type' => 'required',
            'status' => 'required',
            'cert_lang' => 'required',
            'experience' => 'required',
            'introduce' => 'required',
            'passport_id_number' => 'required',
            'hourly_rate' => 'required|integer',
        ];
    }
}
