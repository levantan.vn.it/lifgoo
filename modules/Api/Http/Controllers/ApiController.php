<?php
namespace Modules\Api\Http\Controllers;
use App\User;
use Helper;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Modules\Frontend\Models\GuideInfoModel;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\TravelerModel;
use Modules\Frontend\Models\UsersModel;
use Modules\Frontend\Requests\Contact;
use Modules\User\Entities\BookingDatesEntity;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\BookingMessagesEntity;
use Modules\Frontend\Http\Controllers\BaseController;
use Modules\Frontend\Http\Controllers\HomeController;
use Modules\User\Entities\CountryEntity;
use Modules\User\Entities\LanguageWorldEntity;
use Modules\User\Entities\NotificationEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\NewsEntity;
use Modules\User\Entities\ServiceEntity;
use Modules\User\Entities\GalleryEntity;
use phpDocumentor\Reflection\Types\Object_;
use Modules\Frontend\Requests\Register;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\User\Entities\CityEntity;
use Modules\User\Entities\GuideScheduleEntity;
use DB;
class ApiController extends BaseController{
    public function __construct()
    {
        parent::__construct();
    }
    public function getLastestNews(){
        $news = NewsEntity::orderBy('created_at','desc')->limit(5)->get();
        $data=array(
            'status'=>FALSE,
            'msg'=>'Data retrieval failed!'
        );
        if($news){
            $data['data']=array();
            foreach($news as $detail){
                $newsLang=$detail->news_language()->where('language_code','en')->first();
                $resultNews=new Object_();
                $resultNews->id=$detail->id;
                $resultNews->news_name=$newsLang->title;
                $resultNews->news_address=$detail->cities()->first()->city_name.', '.$detail->countries()->first()->country_name;
                $resultNews->news_image=url(Helper::getThemeNewsUpload($detail->url).'?id='.Helper::versionImg());
                $resultNews->news_content=$newsLang->content;
                $resultNews->create_by=$detail->users()->first()->full_name;
                $resultNews->created_at=date('Y/m/d',strtotime($detail->created_at));
                $resultNews->updated_at=date('Y/m/d',strtotime($detail->updated_at));
                array_push($data['data'],$resultNews);
            }
            $data['status']=TRUE;
            $data['msg']='Get data of news successfully';
        }
        return Helper::json($data);

    }

    public function getNewsDetail(Request $r){
        $id=$r->input('id');
        $system = new SystemModel();
        $detail = $system->detailNews($id);
        if($detail){
            $newsLang=$detail->news_language()->where('language_code','en')->first();
            $resultNews=new Object_();
            $resultNews->id=$detail->id;
            $resultNews->news_name=$newsLang->title;
            $resultNews->news_address=$detail->cities()->first()->city_name.', '.$detail->countries()->first()->country_name;
            $resultNews->news_image=url(Helper::getThemeNewsUpload($detail->url).'?id='.Helper::versionImg());
            $resultNews->news_content=$newsLang->content;
            $resultNews->create_by=$detail->users()->first()->full_name;
            $resultNews->created_at=date('Y/m/d',strtotime($detail->created_at));
            $resultNews->updated_at=date('Y/m/d',strtotime($detail->updated_at));

            $data=array(
                'status'=>TRUE,
                'msg'=>"Get data of news successfully!",
                'data'=>$resultNews
            );
            return Helper::json($data);
        }
        $data=array(
            'status'=>FALSE,
            'msg'=>'Data retrieval failed!'
        );
        return Helper::json($data);
    }

    public function getSuggestGuide(){
        try{
            $news_id=$_GET['news_id'];
            $system = new SystemModel();
            $detail = $system->detailNews($news_id);
            $city_id = null;
            $data=[];
            if ($detail && $detail->city_id){
                $city_id = $detail->city_id;
                $guides=$system->suggestfriend($city_id, $detail->country_id);
                foreach($guides as $guide){
                    $user=$guide->user()->first();
                    $result=new Object_();
                    $result->user_id=$user->id;
                    $result->uide_id=$guide->id;
                    $result->email=$user->email;
                    $result->username=$user->username;
                    $result->full_name=$user->full_name;
                    $result->is_guide=1;
                    $result->is_traveler=0;
                    if($user->avatar!='')
                        $result->url_image=url(Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg());
                    else{
                        $result->url_image=url(Helper::getThemeImg('user.png'));
                    }
                    $result->birth_date=$user->birth_date;
                    $result->phone=$user->phone_number;
                    $result->address=$user->address;
                    $result->id_number=$guide->idnumber;
                    $result->slogan=$user->slogan;
                    $result->about_me=$guide->introduce;
                    $result->rating_star=$guide->star;
                    $result->local_active=[];
                    foreach($guide->locationTour()->get() as $location){
                        $local_active= $location->cities()->first()->city_name.'-'.$location->countries()->first()->country_name;
                        array_push($result->local_active,$local_active);
                    }
                    $result->experience=$guide->experience;
                    $result->language=[];
                    foreach($guide->known_languages()->get() as $known){
                        $language=$known->language()->first()->name;
                        array_push($result->language,$language);
                    }
                    $result->created_at=date('Y/m/d',strtotime($guide->created_at));
                    $result->updated_at=date('Y/m/d',strtotime($guide->updated_at));
                    array_push($data,$result);
                }
                return Helper::apiJson(true, "Get data success!",$data);

            }else{

                return Helper::apiJson(false, "Get data failed!");
            }
        }catch(\Exception $e){
            return Helper::apiJson(false, "Get data failed!");
        }
    }

    public function getPopularNews(){
        try{
            $news_id=$_GET['news_id'];
            $system = new SystemModel();
            $detail = $system->detailNews($news_id);
            $city_id = null;
            $popularNews=$system->randPopularNews($news_id, 3);
            $data=[];
            if(count($popularNews)>0){
                foreach($popularNews as $news){
                    $newsLang=$news->news_language()->where('language_code','en')->first();
                    $resultNews=new Object_();
                    $resultNews->id=$news->id;
                    $resultNews->news_name=$newsLang->title;
                    $resultNews->news_address=$news->cities()->first()->city_name.', '.$news->countries()->first()->country_name;
                    $resultNews->news_image=url(Helper::getThemeNewsUpload($news->url).'?id='.Helper::versionImg());
                    $resultNews->created_at=date('Y/m/d',strtotime($news->created_at));
                    $resultNews->updated_at=date('Y/m/d',strtotime($news->updated_at));
                    array_push($data,$resultNews);
                }
                return Helper::apiJson(true, "Get data success!",$data);
            }else{
                return Helper::apiJson(false, "Get data failed!");
            }
        }catch (\Exception $e){
            return Helper::apiJson(false, "Get data failed!");
        }
    }

    public function countDetails(){
        try{
            $system = new SystemModel();
            $data = [
                'status'=>TRUE,
                'msg'=>'Get data successfully',
                'news' => $system->getNews()['count'],
                'guide' => GuideInfoModel::countGuide(),
                'countTraveler' => GuideInfoModel::countTraveler(),
            ];
            return Helper::json($data);
        }catch(Exception $e){
            $data=array(
                'status'=>FALSE,
                'msg'=>'Data retrieval failed!'
            );
            return Helper::json($data);
        }
    }

    public function getBestGuides(){
        $guideModel = new GuidesModel();
        $guides = $guideModel->getGuide(NULL, 3)['guides'];
        $data=[];
        $data['status']=TRUE;
        $data['msg']='Get data of  best guides successfully!';
        $data['data']=[];
        foreach($guides as $guide){
            $user=$guide->user()->first();
            $result=new Object_();
            $result->user_id=$user->id;
            $result->guide_id=$guide->id;
            $result->email=$user->email;
            $result->username=$user->username;
            $result->full_name=$user->full_name;
            $result->gender=$user->gender;
            $result->is_guide=1;
            $result->is_traveler=0;
            if($user->avatar!='')
                $result->url_image=url(Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg());
            else{
                $result->url_image=url(Helper::getThemeImg('user.png'));
            }
            $result->birth_date=$user->birth_date;
            $result->phone=$user->phone_number;
            $result->address=$user->address;
            $result->id_number=$guide->idnumber;
            $result->slogan=$user->slogan;
            $result->about_me=$guide->introduce;
            $result->hourly_rate=$guide->hourly_rate;
            $result->rating_star=$guide->star;
            $result->diploma=$guide->diploma_info;
            $result->local_active=[];
            foreach($guide->locationTour()->get() as $location){
                $local_active= $location->cities()->first()->city_name.'-'.$location->countries()->first()->country_name;
                array_push($result->local_active,$local_active);
            }
            $result->experience=$guide->experience;
            $result->language=[];
            foreach($guide->known_languages()->get() as $known){
                $language=$known->language()->first()->name;
                array_push($result->language,$language);
            }
            $result->created_at=date('Y/m/d',strtotime($guide->created_at));
            $result->updated_at=date('Y/m/d',strtotime($guide->updated_at));

            array_push($data['data'],$result);
        }
        return Helper::json($data);
    }

    public function register(Request $r){
        $username=$r->input('username');
        $find=User::where('username',$username)->first();
        $find2=User::where('email',$r->input('email'))->first();
        if($find||$find2){
            $data=[
                'status'=>FALSE,
                'msg'=>"Email or username existed!",
            ];
            return Helper::json($data);
        }else{
            if($r->input('password')!=$r->input('confirmpassword')|| strlen($username)>30 || strlen($r->input('full_name'))>100){
                $data=[
                    'status'=>FALSE,
                    'msg'=>"Check input again!",
                ];
                return Helper::json($data);
            }
        }
        if ($this->loggedInStatus()){
            $data=[
                'status'=>FALSE,
                'msg'=>"Register failed!",
            ];
            return Helper::json($data);
        }
        $request = $r->all();
        $fields = ['email', 'username', 'full_name', 'type'];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $data['password'] = Hash::make($request['password']);
        $user = new UsersModel();
        $data['status'] = 'inactive';
        if ($result = $user->storeUser($data, $request['type'])) {
            UsersModel::sendMailActive($result);
            session()->flash('flash_confirm_signup', trans('site.login_fail'));
            $data=[
                'status'=>TRUE,
                'msg'=>"Register success!",
            ];
            return Helper::json($data);
        }
        $data=[
            'status'=>FALSE,
            'msg'=>"Register failed!",
        ];
        return Helper::json($data);    }

    public function login(Request $r)
    {
        if ($this->loggedInStatus()){
            $data=[
                'status'=>FALSE,
                'msg'=>"Login failed!",
            ];
            return Helper::json($data);
        }

        $request = $r->all();
        $email = $request['email'];
        $password = $request['password'];
        if(!$email||!$password){
            $data=[
                'status'=>FALSE,
                'msg'=>"Missing arguments!",
            ];
            return Helper::json($data);
        }

        $users = new User();
        $guideModel = new GuidesModel();
        $user = $users->whereEmail($email)->orWhere('username', '=', $email)->first();

        if ($user&&!$user->can('admin_login')) {
            if($user->status == 'inactive'){
                $data=[
                    'status'=>FALSE,
                    'msg'=>"Please verify account first!",
                ];
                return Helper::json($data);
            }
            if($user->status == 'blocked'){
                $data=[
                    'status'=>FALSE,
                    'msg'=>"Account blocked!",
                ];
                return Helper::json($data);
            }
            if (Hash::check($password, $user->password)) {
                unset($user['password']);
                unset($user['twitter_id']);
                unset($user['google_id']);
                unset($user['facebook_id']);
                if ($user->hasRole('guide')) {
                    $missing = $guideModel->isMissingInformation($user);
                    session()->put('missingInfor', $missing);
                }
                if(session('flash_search_city')){
                    $search_city=session('flash_search_city');
                    session()->flash('flash_search_city',$search_city);
                }
                UsersModel::storeSession($user, $user->type);
                if($user->token==""||$user->token==NULL){
                    $user->token=hash('md5',$email.$password).date('YmdHis');
                    $user->save();
                }
                $result=new Object_();
                $result->token=$user->token;
                $result->user_id=$user->id;
                $result->email=$user->email;
                $result->username=$user->username;
                $result->full_name=$user->full_name;
                if($user->type=='guide'){$result->is_guide=1;}else{$result->is_guide=0;}
                if($user->type=='user'){$result->is_traveler=1;}else{$result->is_traveler=0;}
                if($user->avatar!='')
                    $result->url_image=url(Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg());
                else{
                    $result->url_image=url(Helper::getThemeImg('user.png'));
                }
                $result->created_at=$user->created_at;
                $result->updated_at=$user->updated_at;
                $data=[
                    'status'=>TRUE,
                    'msg'=>"Login success!",
                    'data'=>$result
                ];
                return Helper::json($data);
            }
        }
        $data=[
            'status'=>FALSE,
            'msg'=>"Login failed!",
        ];
        return Helper::json($data);    }

    public function forgotPassword(Request $r){
        $email = $r->input('email');
        $user = User::where('email','=',$email)->select('id','email','full_name')->first();
        if($user){
            $id = base64_encode($user->id);
            $new_pass = rand().time();
            $content = HomeController::htmlEmail('We received a request to reset your Lifgoo password.');
            $content .= HomeController::htmlEmail('<a href="'.Helper::url('reset/'.$id.'/'.$new_pass).'">Click here to change your password.</a>');
            $content .= HomeController::htmlEmail('the new password after clicking on the link is '.$new_pass);
            $guideModel = new GuidesModel();
            $guideModel->sendmail($email, $content, $user->full_name,'Reset Password Email!');
            session()->flash('flash_success', trans('site.send_mail_success'));
            $data=[
                'status'=>TRUE,
                'msg'=>"Please check email to reset password!",
            ];
            return Helper::json($data);
        }
        $data=[
            'status'=>FALSE,
            'msg'=>"Reset password failed!",
        ];
        return Helper::json($data);
    }

    public function getUserInfo(Request $r){
        $id=$r->input('user_id');
        $user = User::where('id',$id)->first();
        if($user!=NULL&&($user->type=='guide'||$user->type=='user')){
            if($user->type=='user'&&FALSE){
                $data=[
                    'status'=>FALSE,
                    'msg'=>"Get info failed!",
                ];
                return Helper::json($data);
            }
            $result=new Object_();
            $result->user_id=$user->id;
            $result->email=$user->email;
            $result->username=$user->username;
            $result->full_name=$user->full_name;
            $result->is_guide=0;
            $result->is_traveler=1;
            if($user->avatar!='')
                $result->url_image=url(Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg());
            else{
                $result->url_image=url(Helper::getThemeImg('user.png'));
            }
            $result->gender=$user->gender;
            $result->birth_date=$user->birth_date;
            $result->address=$user->address;
            $result->phone_number=$user->phone_number;
            $result->slogan=$user->slogan;
            if($user->type=='user')
                $result->about_me=$user->traveller()->first()->introduce;
            $result->hobby=$user->hobby;
            if($user->type=='guide'){
                $guide=$user->guide()->first();
                $result->is_guide=1;
                $result->is_traveler=0;
                $result->guide_id=$guide->id;
                $result->about_me=$guide->introduce;
                $result->idnumber=$guide->idnumber;
                $result->rating_star=$guide->star;
                $result->local_active=[];
                foreach($guide->locationTour()->get() as $location){
                    $local_active= $location->cities()->first()->city_name.'-'.$location->countries()->first()->country_name;
                    array_push($result->local_active,$local_active);
                }
                $result->language=[];
                foreach($guide->known_languages()->get() as $known){
                    $language=$known->language()->first()->name;
                    array_push($result->language,$language);
                }
                $result->hourly_rate=$guide->hourly_rate;
                $cert=$user->certification()->first();
                $result->certification=$cert;
                $result->guide_card_language=[];

                if($cert!=NULL&&$cert->cert_lang!=NULL){
                    foreach(explode(',',$cert->cert_lang) as $cert_lang_id){
                        $lang=$cert->nameLanguage($cert_lang_id);
                        array_push($result->guide_card_language,$lang->name);
                    }
                }
                $result->experience=$guide->experience;
                $result->diploma=$guide->diploma_info;
                $result->service=[];
                $result->transportation=[];
                $guideModel = new GuidesModel();
                $guideServices = $guideModel->guideServices($guide->id,'service');
                $guidetransportations = $guideModel->guideServices($guide->id,'transportation');
                foreach($guideServices as $service){
                    array_push($result->service,$service->service()->first()->serviceLang()->first()->service_name);
                }
                foreach($guidetransportations as $service){
                    array_push($result->transportation,$service->service()->first()->serviceLang()->first()->service_name);
                }
                $result->guide_promotion=$guide->discount()->select('id','guide_id','number_people','time','price')->get();
                $result->guide_gallery=array();
                foreach ($guide->galleries()->get() as $gallery){
                    $gallery->image=(url(Helper::getThemeGallery('gallery' . $user->id.'/'.$gallery->url)));
                    array_push($result->guide_gallery,$gallery->image);
                }
            }
            $data=array(
                'status'=>TRUE,
                'msg'=>'Get data success!',
                'data'=>$result
            );
            return Helper::json($data);

        }
        else{
            $data=[
                'status'=>FALSE,
                'msg'=>"Get info failed!",
            ];
            return Helper::json($data);
        }
    }

    public function getNotifications(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Get data failed!');

            $notifications = $user->notifications()->orderBy('created_at','DESC')->get();
            $data=[];
            foreach($notifications as $notification){
                $object = new Object_();
                $object->notify_id=$notification->id;
                $object->sender_user_id=$notification->sender_id;
                $sender=$notification->sender()->first();
                $object->sender_full_name=$sender->full_name;
                $object->booking_id=$notification->booking_id;
                $object->is_read=$notification->is_read;
                $object->created_at=$notification->created_at;
                if($sender->avatar!='')
                    $object->sender_avatar=url(Helper::getThemeAvatar($sender->avatar).'?id='.Helper::versionImg());
                else{
                    $object->sender_avatar=url(Helper::getThemeImg('user.png'));
                }
                $object->notify_content=$notification->apiContent($notification->type,'',$sender,$notification->booking_id);
                array_push($data,$object);
            }
            return Helper::apiJson(true,'Get data success!',$data);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getAllNews(){
        try{
            $all=NewsEntity::all();
            $resultArray=[];
            foreach($all as $detail){
                $newsLang=$detail->news_language()->where('language_code','en')->first();
                $resultNews=new Object_();
                $resultNews->id=$detail->id;
                $resultNews->news_name=$newsLang->title;
                $resultNews->news_address=$detail->cities()->first()->city_name.', '.$detail->countries()->first()->country_name;
                $resultNews->news_image=url(Helper::getThemeNewsUpload($detail->url).'?id='.Helper::versionImg());
                $resultNews->create_by=$detail->users()->first()->full_name;
                $resultNews->created_at=date('Y/m/d',strtotime($detail->created_at));
                $resultNews->updated_at=date('Y/m/d',strtotime($detail->updated_at));
                array_push($resultArray,$resultNews);
            }
            $data=array(
                'status'=>TRUE,
                'msg'=>"Get data of news successfully!",
                'data'=>$resultArray
            );
            return Helper::json($data);
        }catch (\Exception $e){
            $data=array(
                'status'=>FALSE,
                'msg'=>"Get data of news failed!",
            );
            return Helper::json($data);
        }
    }

    public function updateProfile(Request $r){
        $request = $r->all();
        $user = User::where('token',$r['access_token'])->first();
        if(!$user){
            return Helper::apiJson(false, "Account doesn't exist!");
        }
        $fields = ["full_name", "phone_number", "address", "birth_date", "hobby", "gender", "slogan"];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
//        isset($request['email']) && $data['email'] = $request['email'];
//        isset($request['username']) && $data['username'] = $request['username'];
        $profile = new UsersModel();
        $profile->patchProfile($data, $user);
        if ($profile) {
            if ($user->hasRole('guide')) {
                $fields = [
                    'introduce' => isset($request['introduce']) ? $request['introduce'] : '',
                    'idnumber' => isset($request['passport_id_number']) ? $request['passport_id_number'] : '',
                ];
                $profile->patchGuideInfo($fields, $user->id);
            } else {
                $fields = [
                    'introduce' => isset($request['introduce']) ? $request['introduce'] : '',
                    'language_code' => 'en'
                ];
                $profile->patchTravellerInfo($fields, $user->id);
            }
            /**
             * check Information's guide is missing
             */
            if ($user->hasRole('guide')) {
                $guideModel = new GuidesModel();
                $missing = $guideModel->isMissingInformation($user);
                session()->put('missingInfor', $missing);
            }
            return Helper::apiJson(true, trans('site.edit_profile_success'));
        }
        return Helper::apiJson(false, 'Update profile failed!');
    }


    public function updateJobInfo(Request $r){
        if (!$user = $this->loggedInStatus())
            return Helper::apiJson(false, trans('site.edit_profile_unsuccess'));
        if (!$user->hasRole('guide'))
            return Helper::apiJson(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $guideModel = new GuidesModel();
        $guide = $user->guide()->select('id')->first();
        if ($guide) {
            /**
             * edit certification
             */
            $fields = ["idcard", "expiry_date", "place_of_issue", "type", "status"];
            $fields = array_flip($fields);
            $cerarr = array_intersect_key($request, $fields);
            $cerarr['user_id'] = $user->id;
            $cerarr['user_type'] = 'guide';
            if(isset($request['cert_lang']))
                $cerarr['cert_lang'] = implode(",", $request['cert_lang']);
            else
                $cerarr['cert_lang']='';
            $guideModel->storeCertification($cerarr);
            /**
             * store location of guide
             */
            $guideModel->deleteLocation($guide);
            if ($request['country_id'])
                foreach ($request['country_id'] as $key => $item) {
                    $location = [
//                        'country_id' => $item,
                        'city_id' => isset($request['city_id'][$key]) ? $request['city_id'][$key] : '',
                        'guide_id' => $guide->id
                    ];
                    $guideModel->storeLocationTour($location);
                }
            /**
             * edit guide information
             */
            $fields = ["diploma_info", "experience", "hourly_rate"];
            $fields = array_flip($fields);
            $info = array_intersect_key($request, $fields);
            $profile = new UsersModel();
            $profile->patchGuideInfo($info, $user->id);
            $systemModel = new SystemModel();
            if ($languages = $request['languages'])
                $guide->known_languages()->delete();
            foreach ($languages as $langs) {
                $systemModel->storeKnownLanguage(['guide_id' => $guide->id, 'language_id' => $langs]);
            }
            /**
             * store service of guide
             * delete all service of guide before insert new service
             */
            $guide->services()->delete();
            $service = [
                'guide_id' => $guide->id,
//                'type' => 'service',
            ];
            if (isset($request['service_id']) && $ids = $request['service_id']) {
                foreach ($ids as $id) {
                    $service['service_id'] = $id;
                    SystemModel::storeService($service);
                }
            }

            if (isset($request['transportation_id']) && $ids = $request['transportation_id']) {
                foreach ($ids as $id) {
                    $service['service_id'] = $id;
//                    $service['type'] = 'transportation';
                    SystemModel::storeService($service);
                }
            }
            /**
             * check Information's guide is missing
             */

            $guideModel = new GuidesModel();
            $missing = $guideModel->isMissingInformation($user);
            session()->put('missingInfor', $missing);

            session()->flash('flash_success', trans('site.edit_price_success'));
            return Helper::apiJson(true, trans('site.edit_price_success'));
        }
        return Helper::apiJson(false, trans('site.edit_profile_unsuccess'));
    }

    public function updatePromotion(Request $r){
        try{
            if (!$user = $this->loggedInStatus()||$this->loggedInStatus()->type!='guide')
                return Helper::apiJson(false, "Update promotion failed!");
            $request = $r->input('data');
            $user=$this->loggedInStatus();
//            dd($request);
            $guide_id = $user->guide()->select('id')->first()->id;
            $userModel = new UsersModel();
            $userModel->removeDiscount($guide_id);
            if ($request) {
                foreach ($request as $capacity => $item) {
                    if ($item)
                        foreach ($item as $hour => $price) {
                            $data = [
                                'guide_id' => $guide_id,
                                'number_people' => $capacity,
                                'time' => $hour,
                                'price' => $price
                            ];
                            $userModel->storeGuideDiscount($data);
                        }
                }
            }
            return Helper::apiJson(true, trans('site.edit_price_success'));
        }catch (\Exception $e){
            return Helper::apiJson(false, "Update promotion failed!");
        }
    }

    public function updateTime(Request $r){
        try{

            $user = $this->loggedInStatus();
            $guide = $user->guide()->select(['id'])->first();
            if (!$user || !$user->hasRole('guide') || !$guide) {
                return Helper::apiJson(false, "Update schedule failed!");
            }
            $request = $r->all();
            $guideModel = new GuidesModel();
            $guide->schedules()->delete();
            if ($guideModel->processSchedule($guide->id, $request)) {
                return Helper::apiJson(true, "Update schedule success!");
            } else
                return Helper::apiJson(false, "Update schedule failed!");
        }catch(\Exception $e){
            return Helper::apiJson(false, "Update schedule failed!");

        }
    }

    public function updatePassword(Request $r){
        if (!$user = $this->loggedInStatus())
            return Helper::apiJson(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $validator = Validator::make($request, [
            'new_password' => 'required|min:6;',
            'cfr_password' => 'required|same:new_password'
        ]);
        if ($validator->fails()) {
            return Helper::apiJson(false, 'Please confirm password!');
        } else {
            $userCurrent = UsersModel::getUserById($user->id);
            if (Hash::check($request['old_password'], $userCurrent->password) || (($userCurrent->facebook_id || $userCurrent->google_id || $user->twitter_id) && !$userCurrent->password)) {
                $userCurrent->password = Hash::make($request['new_password']);
                $userCurrent->save();
                return Helper::apiJson(true, trans('site.edit_password_success'));
            }
        }
        return Helper::apiJson(false, trans('site.Please_enter_correct_current_password'));
    }

    public function getGalleries(){
        try{
            if (!$user = $this->loggedInStatus()||$this->loggedInStatus()->type!='guide')
                return Helper::apiJson(false, "Update promotion failed!");
            $user=$this->loggedInStatus();
            $guide = $user->guide()->select(['id'])->first();
            $galleries=$guide->galleries()->get();
            $data=[];
            foreach($galleries as $gallery){
                $gallery->image=(url(Helper::getThemeGallery('gallery' . $user->id.'/'.$gallery->url)));
                $object= new Object_();
                $object->image_id=$gallery->id;
                $object->link=$gallery->image;
                array_push($data,$object);
            }
            return Helper::apiJson(true, 'Get data success!',$data);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getSetupTime(){
        try{
            $data=[];
            if (!$user = $this->loggedInStatus()||$this->loggedInStatus()->type!='guide')
                return Helper::apiJson(false, "Get data failed!");
            $user=$this->loggedInStatus();
            if(isset($_POST['guide_id'])){
                $guide = GuideEntity::where('id',$_POST['guide_id'])->first();
            }else{
                $guide = $user->guide()->select(['id'])->first();
            }
            $galleries=$guide->schedules()->get();
            foreach($galleries as $gallery){
                $data[$gallery->day_of_week]=[];
                foreach(explode(",",$gallery->start_end_time) as $array){
                    $object = new Object_();
                    $object->from = explode("-",$array)[0];
                    $object->to = explode("-",$array)[1];
                    array_push($data[$gallery->day_of_week],$object);
                }
            }
            return Helper::apiJson(true, 'Get data success!',$data);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getUserBookings(){
        try{
            $data=[];
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, "Get data failed!");
            $user=$this->loggedInStatus();
            $type = $user->type == 'guide' ? 'guide_id' : 'traveler_id';
            $guideModel=new GuidesModel();
            $bookings=$guideModel->getBooking($user->id, $type, [], 0, 0, NULL);
            foreach($bookings as $booking){
                $guide=$booking->guide()->first();
                $traveler=$booking->guide('traveler_id')->first();
                $object = new Object_();
                $object->booking_id=$booking->id;
                $object->guide_id=$booking->guide_id;
                $object->guide_full_name=$guide->full_name;
                if($guide->avatar!='')
                    $object->guide_avatar=url(Helper::getThemeAvatar($guide->avatar).'?id='.Helper::versionImg());
                else{
                    $object->guide_avatar=url(Helper::getThemeImg('user.png'));
                }

                $object->traveler_id=$booking->traveler_id;
                $object->traveler_full_name=$booking->full_name;
                if($traveler->avatar!='')
                    $object->traveler_avatar=url(Helper::getThemeAvatar($traveler->avatar).'?id='.Helper::versionImg());
                else{
                    $object->traveler_avatar=url(Helper::getThemeImg('user.png'));
                }

                $object->people_number=$booking->capacity;

                $total_dates = $booking->datesBooking()->count();
                $total_hours=0;
                $dates = $booking->datesBooking()->get();
                if ($dates) {
                    foreach ($dates as $date) {
                        $count = count(explode(',', $date->hours));
                        $total_hours += $count;
                    }
                }

                $object->total_days=$total_dates;
                $object->total_hours=$total_hours;
                $object->total_price=$booking->price;
                $object->status=$booking->status;
                $object->created_at=date("Y/m/d H:i:s",strtotime($booking->created_at));
                array_push($data,$object);
            }
            return Helper::apiJson(true, 'Get data success!',$data);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function updateAvatar(){
        try{
            if(!$this->loggedInStatus())
                return Helper::apiJson(false, 'Update data failed!');
            $user=$this->loggedInStatus();
            $input=Input::all();
            $data=$input['avatar'];
            $img = base64_decode($data);
            file_put_contents('uploads/newImage.jpg',$img);
            $uploadedFile = new \Symfony\Component\HttpFoundation\File\File((public_path('uploads/newImage.jpg')));
            UsersModel::uploadAvatar($uploadedFile, $user,'avatar','api');
            unlink(public_path('uploads/newImage.jpg'));
            return Helper::apiJson(true, 'Update data success!');
        }catch (\Exception $e){
            dd($e->getMessage());
            return Helper::apiJson(false, 'Update data failed!');
        }
    }

    public function uploadGallery(){
        try{
            $user = $this->loggedInStatus();
            $guide = $user->guide()->select(['id'])->first();
            if (!$user || !$user->hasRole('guide') || !$guide) {
                return Helper::apiJson(false, trans('admin.the_guide_is_invalid'));
            }
            $input=Input::all();
            $data=$input['image'];
            $img = base64_decode($data);
            file_put_contents('uploads/newImage.jpg',$img);
            $uploadedFile = new \Symfony\Component\HttpFoundation\File\File((public_path('uploads/newImage.jpg')));
            UsersModel::uploadGallery($uploadedFile, $user->id, $guide->id,'api');
            unlink(public_path('uploads/newImage.jpg'));
            return Helper::apiJson(true, trans('site.upload_success'));
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Update data failed!');
        }
    }

    public function deleteGallery(){
        try{

            $user = $this->loggedInStatus();
            $guide = $user->guide()->select(['id'])->first();
            if (!$user || !$user->hasRole('guide') || !$guide) {
                return Helper::apiJson(false, 'Update data failed!');
            }
            $input=Input::all();
            $id=$input['image_id'];
            $gallery = GalleryEntity::find($id);
            if (!$gallery) {
                return Helper::apiJson(false, "Image doesn't exist!");
            }
            $destinationPath = public_path() . Helper::getThemeGallery('gallery' . $user->id, $gallery->url);
            if (is_file($destinationPath)) {
                unlink($destinationPath);
            }
            if ($gallery->delete()) {
                return Helper::apiJson(true, 'Delete success!');
            }
            return Helper::apiJson(false, 'Update data failed!');
        }catch(\Exception $e){
            return Helper::apiJson(false, 'Update data failed!');
        }
    }

    public function findGuides(Request $r){
        try{

            $request = $r->all();

            if (Input::has('city_id_to')) {
                $request['city_id'] = $request['city_id_to'];
                $search_city=CityEntity::where('city_id',$request['city_id'])->first();
            }
            if (Input::has('language'))
                $request['language_id'] = $request['language'];
            if (Input::has('services') && Input::has('transportations'))
                $request['guide_services'] = array_merge($request['services'], $request['transportations']);
            if (Input::has('services') && !Input::has('transportations'))
                $request['guide_services'] = $request['services'];
            if (!Input::has('services') && Input::has('transportations'))
                $request['guide_services'] = $request['transportations'];
            if (Input::has('hourly_rate') && $hourly_rate = explode(';', $request['hourly_rate'])) {
                $request['rate_end'] = $hourly_rate[1];
                $request['rate_begin'] = $hourly_rate[0];
            }
            $guideModel = new GuidesModel();
            $findGuide = $guideModel->findGuide($request, 100);
            $data=[];
            if (isset($findGuide['guides']))
                foreach ($findGuide['guides'] as $guide) {
                    $object = new Object_();
                    $userJoin = $guide->user()->select('full_name', 'avatar', 'id')->first();
                    $object->user_id=$userJoin->id;
                    $object->full_name=$userJoin->full_name;
                    if($userJoin->avatar!='')
                        $object->avatar_url=url(Helper::getThemeAvatar($userJoin->avatar).'?id='.Helper::versionImg());
                    else{
                        $object->avatar_url=url(Helper::getThemeImg('user.png'));
                    }
                    $object->local_active=[];
                    foreach($guide->locationTour()->get() as $location){
                        $local_active= $location->cities()->first()->city_name.'-'.$location->countries()->first()->country_name;
                        array_push($object->local_active,$local_active);
                    }

                    $object->rating_star=$guide->star;
                    $object->price=$guide->hourly_rate;
                    array_push($data,$object);
                }
            return Helper::apiJson(true, 'Search success!', $data);

        }catch (\Exception $e){
            return Helper::apiJson(false, 'Search guide failed!');
        }
    }

    public function getAllServices($langcode="en"){
        try{

            $services=ServiceEntity::where('type','service')->where('status','active')->get();
            $data=[];
            foreach($services as $service){
                $object=new Object_();
                $object->service_id=$service->id;
                $object->service_name=$service->serviceLang()->where('language_code',$langcode)->first()->service_name;
                $object->icon=$service->icon;
                array_push($data,$object);
            }
            return Helper::apiJson(true, 'Get data success!', $data);

        }catch(\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }

    }

    public function getAllTransportations($langcode="en"){
        try{

            $services=ServiceEntity::where('type','transportation')->where('status','active')->get();
            $data=[];
            foreach($services as $service){
                $object=new Object_();
                $object->transportation_id=$service->id;
                $object->transportation_name=$service->serviceLang()->where('language_code',$langcode)->first()->service_name;
                $object->icon=$service->icon;
                array_push($data,$object);
            }
            return Helper::apiJson(true, 'Get data success!', $data);

        }catch(\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }

    }

    public function getAllCountries($langcode='en'){
        try{
            $countries = CountryEntity::select('country_id','country_name')->get();
            return Helper::apiJson(true,'Get data success',$countries);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getAllCities(){
        try{
            $country_id=$_GET['country_id'];
            $cities = CityEntity::select('city_id','city_name','country_id')->where('country_id',$country_id)->get();
            foreach($cities as $city){
                $city->country_name = $city->country()->first()->country_name;
            }
            return Helper::apiJson(true,'Get data success',$cities);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getAllLanguages(){
        try{
            $languages=LanguageWorldEntity::select('id','name')->get();
            return Helper::apiJson(true,'Get data success',$languages);

        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getBookingDetail(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Get data failed!');
            $input=Input::all();
            $id=$input['booking_id'];
            $guideModel = new GuidesModel();
            $detaiBooking = $guideModel->detailBooking($id);
            if($detaiBooking==NULL){
                return Helper::apiJson(false, 'Get data failed!');
            }
            if($user->id!=$detaiBooking->guide_id&&$user->id!=$detaiBooking->traveler_id){
                return Helper::apiJson(false, 'Get data failed!');
            }
//            $message = '';
//            if ($detaiBooking)
//                $message = $detaiBooking->messageBooking()->orderBy('created_at', 'desc')->limit(500)->get();
//            $data = [
//                'detail' => $detaiBooking,
//                'user' => $user,
//                'messages' => $message
//            ];
            $object=new Object_();
            $object->booking_id=$detaiBooking->id;
            $object->traveler_user_id=$detaiBooking->traveler_id;
            $object->traveler_full_name=$detaiBooking->guide('traveler_id')->first()->full_name;
            $object->guide_user_id=$detaiBooking->guide_id;
            $object->guide_full_name=$detaiBooking->guide('guide_id')->first()->full_name;
            $object->booking_dates=[];
            $total_dates = $detaiBooking->datesBooking()->count();
            $total_hours=0;
            $object->booking_dates=$detaiBooking->getConvertHours();
            $object->status=$detaiBooking->status;
            $object->number_of_people=$detaiBooking->capacity;
            $object->total_days=$total_dates;
            $object->total_hours=$total_hours;
            $object->total_price=$detaiBooking->price;
            $city=CityEntity::find($detaiBooking->city_id);
            $object->country=$city->country()->first()->country_name;
            $object->city=$city->city_name;
            $object->pickup_place=$detaiBooking->pickup_place;
            $object->destination=$detaiBooking->destination;
            if($user->id==$detaiBooking->guide_id){
                $detaiBooking->is_read = 1;
                $detaiBooking->save();
            }
            $user->bookingMessages()->where('booking_id', '=', $id)->where('receiver_id', '=', $user->id)->where('is_read', '=', 0)->update(['is_read' => 1]);
            return Helper::apiJson('true',"Get data success!",$object);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getGuideBusyHours(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Get data failed!');
            $input=Input::all();
            $guide_id=$input['guide_id'];
            $city_id=$input['city_id'];
            $guide=GuideEntity::find($guide_id);
            $busyHours=$guide->getConvertBusyHours($city_id);
            return Helper::apiJson(true,'Get data success',$busyHours);
        }catch(\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getBookingMessages(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Get data failed!');
            $input=Input::all();
            $booking=BookingEntity::find($input['booking_id']);
            if($booking->traveler_id==$user->id || $booking->guide_id==$user->id){
                $messages=$booking->messageBooking()->orderBy('created_at')->get();
                $data=array();
                foreach($messages as $message){
                    $object = new Object_();
                    $object->message_id = $message->id;
                    $object->user_id = $message->user_id;
                    $sender = $message->user()->first();
                    $object->user_id=$sender->id;
                    $object->full_name=$sender->full_name;
                    if($sender->avatar!='')
                        $object->avatar_url=url(Helper::getThemeAvatar($sender->avatar).'?id='.Helper::versionImg());
                    else{
                        $object->avatar_url=url(Helper::getThemeImg('user.png'));
                    }
                    $object->message=$message->content;
                    $object->created_at=$message->created_at;
                    array_push($data,$object);
                }
                return Helper::apiJson(true,'Get data success',$data);
            }else{
                return Helper::apiJson(false, 'Get data failed!');
            }
        }catch(\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function getGuideReviews(){
        try{
            $input=Input::all();
            $id=$input['guide_user_id'];
            $user = User::where('id',$id)->first();
            if($user!=NULL&&($user->type=='guide')) {
                $travelModel = new TravelerModel();
                $ratings = $travelModel->getRating($id);
                $data=array();
                foreach ($ratings as $rating){
                    $object=new Object_();
                    $traveler=$rating->guide('traveler_id')->select('id','avatar', 'full_name')->first();
                    $object->traveler_user_id=$traveler->id;
                    $object->full_name=$traveler->full_name;
                    if($traveler->avatar){
                        $object->traveler_avatar=Helper::getThemeAvatar($traveler->avatar).'?id='.Helper::versionImg();
                    }
                    else {
                        $object->traveler_avatar=Helper::getThemeImg('user.png');
                    }
                    $object->rating_star=$rating->ratings;
                    $object->comment=$rating->review;
                    $object->created_at=$rating->updated_at;
                    $object->booking_id=$rating->id;
                    array_push($data,$object);
                }
                return Helper::apiJson(true, 'Get data success!',$data);
            }
            else{
                return Helper::apiJson(false, 'Get data failed!');
            }
        }catch(\Exception $e){
            return Helper::apiJson(false, 'Get data failed!');
        }
    }

    public function reviewGuide(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Review failed!');
            $input=Input::all();
            $booking=BookingEntity::find($input['booking_id']);
            if($booking->traveler_id==$user->id && $booking->status=='finished'&&$booking->review==NULL) {
                $booking->review=$input['review'];
                if(isset($input['rating'])){$booking->ratings=$input['rating'];}
                $booking->save();
                $notifi = [
                    'type' => 'review',
                    'user_id' => $booking->guide_id,
                    'is_read' => 0,
                    'sender_id' => $user->id,
                    'booking_id' => $booking->id
                ];
                $systemModel = new SystemModel();
                $systemModel->insertNotify($notifi);
                return Helper::apiJson(true, 'Review success!');
            }
            else{
                return Helper::apiJson(false, 'Review failed!');
            }
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Review failed!');
        }
    }

    public function bookGuide(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Booking failed!');
            $request=Input::all();
            if (!$user->hasRole('user'))
                return Helper::apiJson(false, trans('admin.no_access_privilege'));
            $request['guide_id']=$request['guide_user_id'];
            unset($request['guide_user_id']);
            $guide = UsersModel::getUserById($request['guide_id']);
            if (!$guide && $guide->hasRole('guide'))
                return Helper::apiJson(false, trans('admin.the_guide_is_invalid'));
            $fields = ["guide_id", "phone_number", "capacity", "price", "email",'pickup_place','destination','city_id'];

            $fields = array_flip($fields);
            $data = array_intersect_key($request, $fields);
            $data['traveler_id'] = $user->id;
            $guideModel = new GuidesModel();
            //save booking

            if(isset($request['city_id'])){
                $city=CityEntity::where('city_id',$request['city_id'])->first();
            }
            $exactguide=$guide->guide()->first();
            $guideschedules = $exactguide->schedules()->get();
            $schedulesconvert=array();
            foreach($guideschedules as $schedule){
                $schedulesconvert[$schedule->day_of_week]=explode(',',$schedule->hours);
            }
            if ($request['hours_booking']&&(isset($city)&&$city!=NULL)) {
                foreach ($request['hours_booking'] as $key => $value) {
                    foreach($value as $hour){
                        $time=strtotime($key.' '.$hour.':00:00 ');
                        $timedayinweek=strtolower(date('l',$time));
                        if(!isset($schedulesconvert[$timedayinweek])||!in_array($hour,$schedulesconvert[$timedayinweek]))
                        {
                            return Helper::apiJson(false, 'Your booked hours is unavailable!');
                        }
                    }
                }
            }
            $booking = $guideModel->storeBooking($data);
            if (!$booking)
                return Helper::apiJson(false, trans('site.error_occured'));
            // save hours of booking
            $userBooking = $booking->guide()->select('full_name', 'email')->first();

            if ($request['hours_booking']&&(isset($city)&&$city!=NULL)) {
                $total_hour = 0;
                $total_dates = 0;
                $dataDateHours=[];
                foreach ($request['hours_booking'] as $key => $value) {
                    $total_hour += count($value);
                    $total_dates++;
                    foreach($value as $hour){
                        $time=strtotime($key.' '.$hour.':00:00 '.$city->timezone);
                        if(!isset($dataDateHours[date('Y-m-d',$time)])||!is_array($dataDateHours[date('Y-m-d',$time)])){
                            $dataDateHours[date('Y-m-d',$time)]=[];
                        }
                        array_push($dataDateHours[date('Y-m-d',$time)],date('H:i:s',$time));
                    }
                }

                foreach ($dataDateHours as $key => $value) {
                    $data_hours = [
                        'booking_id' => $booking->id,
                        'date' => $key,
                        'hours' => implode(',', $value)
                    ];
                    $guideModel->storeHoursBooking($data_hours);
                }

                $content = HomeController::htmlEmail('You received a booking total ' . Helper::addPlural($total_hour, 'hour') . ' in ' . Helper::addPlural($total_dates, 'day') . ' from ' . $user->full_name . '. Please visit <a href="' . Helper::url('detail/'.$booking->id) . '">lifgoo.com</a> for more details');
                $guideModel = new GuidesModel();
                $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
            }
            if (isset($request['message']) && $request['message']) {
                $data_message = [
                    'content' => $request['message'],
                    'user_id' => $user->id,
                    'booking_id' => $booking->id,
                    'receiver_id' => $guide->id
                ];
                $guideModel->storeMessageBooking($data_message);
            }
            $notifi = [
                'type' => 'request_booking',
                'user_id' => $guide->id,
                'is_read' => 0,
                'sender_id' => $user->id,
                'booking_id' => $booking->id
            ];
            $systemModel = new SystemModel();
            $systemModel->insertNotify($notifi);
            $data['id_booking']=$booking->id;
            return Helper::apiJson(true, trans('site.booking_success'),$data);
        }catch (\Exception $e){
            dd($e);
            return Helper::apiJson(false, 'Booking failed!');
        }
    }

    function statusBooking(Request $r){
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::apiJson(false, trans('admin.no_access_privilege'));
        }
        $request = $r->all();
        $booking = BookingEntity::find($request['booking_id']);
        if($user->id!=$booking->traveler_id&&$user->id!=$booking->guide_id){
            return Helper::apiJson(false, "This booking isn't your!");
        }
        if (isset($request['booking_id']) && isset($request['status'])) {
            $booking = BookingEntity::find($request['booking_id']);
            if($booking->status=='finished')
            {
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
            }
            if($booking->status=='declined')
            {
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
            }
            if($booking->status=='expired')
            {
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
            }

            $process=false;
            if ($booking->status != $request['status']) {

                if ($request['status'] == 'canceled') {
                    $booking->is_read = 1;
                    $booking->status = $request['status'];

                    if($booking->save()){
                        $userBooking = $booking->guide('traveler_id')->select('email','full_name','gender')->first();
                        if($userBooking->gender=='female')$gen='her'; else $gen='his';
                        $guideBooking = $booking->guide()->select('id', 'full_name','email')->first();
                        $content = HomeController::htmlEmail($userBooking->full_name . '</a> canceled '.$gen.' booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                        $guideModel = new GuidesModel();
                        $guideModel->sendmail($guideBooking->email, $content, $guideBooking->full_name);
                        $process=true;
                    }
                }
                if ($request['status']== 'accepted'){
                    if($booking->status=='pending'){
                        $booking->status = $request['status'];
                        if($booking->save()){
                            $userBooking = $booking->guide('traveler_id')->select('email','full_name')->first();
                            $guideBooking = $booking->guide()->select('id', 'full_name')->first();
                            $content = HomeController::htmlEmail('<a href="' . Helper::url('profile/' . $guideBooking->id) . '">' . $guideBooking->full_name . '</a> accepted your booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                            $guideModel = new GuidesModel();
                            $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
                            $process=true;
                        }
                    }else{
                        session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                        return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
                    }
                }

                if ($request['status']== 'declined'){
                    if($booking->status=='pending'){
                        $booking->status = $request['status'];
                        if($booking->save()) {
                            $receiver_id = $user->id == $booking->traveler_id ? $booking->guide_id : $booking->traveler_id;
                            $data = [
                                'user_id' => $user->id,
                                'booking_id' => $booking->id,
                                'content' => $request['message'],
                                'receiver_id' => $receiver_id,
                                'is_read' => 1
                            ];
                            $userBooking = $booking->guide('traveler_id')->select('email','full_name')->first();
                            $guideBooking = $booking->guide()->select('id', 'full_name')->first();
                            $content = HomeController::htmlEmail('<a href="' . Helper::url('profile/' . $guideBooking->id) . '">' . $guideBooking->full_name . '</a> declined your booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                            $guideModel = new GuidesModel();
                            $guideModel->storeMessageBooking($data);
                            $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
                            $process=true;
                        }
                    }else{
                        session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                        return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
                    }
                }

                if($process==true){
                    $notifi = [
                        'is_read' => 0,
                        'booking_id' => $booking->id,
                        'type' => Helper::typeNotify($request['status'])
                    ];
                    if ($request['status'] == 'canceled') {
                        $notifi['user_id'] = $booking->guide_id;
                        $notifi['sender_id'] = $booking->traveler_id;
                        $booking->readAllMessage();
                    } else {
                        $notifi['user_id'] = $booking->traveler_id;
                        $notifi['sender_id'] = $booking->guide_id;
                    }
                    $systemModel = new SystemModel();
                    $systemModel->insertNotify($notifi);
                    session()->flash('flash_success', trans('site.action_success'));
                    return Helper::apiJson(true, 'Booking status changed!');
                }else{
                    session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                    return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
                }
            }else{
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
            }
        }
        else{
            session()->flash('flash_error', trans('admin.the_data_is_invalid'));
            return Helper::apiJson(false, trans('admin.the_data_is_invalid'));
        }
    }


    function sendMessageBooking(){
        try{
            $user = $this->loggedInStatus();
            if (!$user) {
                return Helper::apiJson(false, trans('admin.no_access_privilege'));
            }
            $request = Input::all();
            $booking = BookingEntity::find($request['booking_id']);
            if($user->id!=$booking->traveler_id&&$user->id!=$booking->guide_id){
                return Helper::apiJson(false, "This booking isn't your!");
            }
            if ($booking->status != 'accepted' && $booking->status != 'pending')
                return Helper::apiJson(false, "Booking status has been changed! You can't send message now.");
            $receiver_id = $user->id == $booking->traveler_id ? $booking->guide_id : $booking->traveler_id;
            $data = [
                'user_id' => $user->id,
                'booking_id' => $request['booking_id'],
                'content' => $request['message'],
                'receiver_id' => $receiver_id
            ];
            $guideModel = new GuidesModel();

            $result = $guideModel->storeMessageBooking($data);

            if ($result) {
                $result->type = $user->roles->first()->name;
                $result->avatar = $user->avatar;
                $result->full_name=$user->full_name;
                return Helper::apiJson(true, trans('Send message success!'), $result);
            }
            return Helper::apiJson(false, trans('site.error_occured'));
        }catch(\Exception $e){
            return Helper::apiJson(false, "Send message failed!");
        }
    }

    function deleteNotify(Request $r)
    {
        try{
            $user = $this->loggedInStatus();
            if (!$user) {
                return Helper::jsondata(false, trans('admin.no_access_privilege'));
            }
            $ids = $r->input('notify_ids');
            if (count($ids)) {
                foreach ($ids as $value)
                    $user->notifications()->where('id', '=', $value)->delete();
            }
            return Helper::apiJson(true,"Delete success!");
        }catch (\Exception $e){
            return Helper::apiJson(false,"Delete failed!");
        }
    }


    public function countUnreadNotify(){
        try{
            if (!$user = $this->loggedInStatus())
                return Helper::apiJson(false, 'Count failed');
            $data['user_id']=$user->id;
            if($user->type=='guide'){
                $data['count_message_booking']=DB::table('traveler__booking_messages')->select('traveler__booking_messages.*')
                    ->join('traveler__booking', 'traveler__booking_messages.booking_id', '=', 'traveler__booking.id')
                    ->where('traveler__booking.guide_id','=',$user->id)
                    ->where('traveler__booking_messages.user_id','<>',$user->id)
                    ->where('traveler__booking_messages.is_read',0)
                    ->count();
            }else{
                $data['count_message_booking']=DB::table('traveler__booking_messages')->select('traveler__booking_messages.*')
                    ->join('traveler__booking', 'traveler__booking_messages.booking_id', '=', 'traveler__booking.id')
                    ->where('traveler__booking.traveler_id','=',$user->id)
                    ->where('traveler__booking_messages.user_id','<>',$user->id)
                    ->where('traveler__booking_messages.is_read',0)
                    ->count();
            }

            $data['count_notification']=NotificationEntity::where('user_id',$user->id)->where('is_click',0)->get();
            return Helper::apiJson(true, 'Get data success!',$data);
        }catch (\Exception $e){
            return Helper::apiJson(false, 'Count failed!');
        }
    }
}