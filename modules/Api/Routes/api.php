<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '',
], function () {
    Route::get('getNewsDetail','ApiController@getNewsDetail');
    Route::get('getLastestNews','ApiController@getLastestNews');
    Route::get('countDetails','ApiController@countDetails');
    Route::get('getBestGuides','ApiController@getBestGuides');
    Route::post('register','ApiController@register');
    Route::post('login','ApiController@login');
    Route::post('forgotPassword','ApiController@forgotPassword');
    Route::get('getUserInfo','ApiController@getUserInfo');
    Route::get('getAllNews','ApiController@getAllNews');
    Route::post('findGuides','ApiController@findGuides');
    Route::get('getAllServices','ApiController@getAllServices');
    Route::get('getAllTransportations','ApiController@getAllTransportations');
    Route::get('getAllCountries','ApiController@getAllCountries');
    Route::get('getAllCities/','ApiController@getAllCities');
    Route::get('getAllLanguages/','ApiController@getAllLanguages');
    Route::get('getSuggestGuide/','ApiController@getSuggestGuide');
    Route::get('getPopularNews/','ApiController@getPopularNews');
    Route::get('getGuideReviews/','ApiController@getGuideReviews');

});
Route::group(['prefix' => '','middleware'=>'checkToken'
], function () {
    Route::post('updateProfile','ApiController@updateProfile');
    Route::post('updateJobInfo','ApiController@updateJobInfo');
    Route::post('updatePromotion','ApiController@updatePromotion');
    Route::post('updatePassword','ApiController@updatePassword');
    Route::post('updateTime','ApiController@updateTime');
    Route::post('getGalleries','ApiController@getGalleries');
    Route::post('getSetupTime','ApiController@getSetupTime');
    Route::post('getUserBookings','ApiController@getUserBookings');
    Route::post('updateAvatar','ApiController@updateAvatar');
    Route::post('uploadGallery','ApiController@uploadGallery');
    Route::post('deleteGallery','ApiController@deleteGallery');
    Route::post('getBookingDetail','ApiController@getBookingDetail');
    Route::post('getBusyHours','ApiController@getGuideBusyHours');
    Route::post('getNotifications','ApiController@getNotifications');
    Route::post('getBookingMessages','ApiController@getBookingMessages');
    Route::post('bookGuide','ApiController@bookGuide');
    Route::post('reviewGuide/','ApiController@reviewGuide');
    Route::post('countUnreadNotify/','ApiController@countUnreadNotify');
    Route::post('sendMessageBooking/','ApiController@sendMessageBooking');
    Route::post('statusBooking/','ApiController@statusBooking');
    Route::post('deleteNotify/','ApiController@deleteNotify');

});