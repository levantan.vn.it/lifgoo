<?php

namespace Modules\Frontend\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GuideInfoEntities extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'guide__info';
    public $timestamps = false;
    
    public function user(){
        return $this->belongsTo(new User(),'user_id','id');
    }
}