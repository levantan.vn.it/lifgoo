<?php

namespace Modules\Frontend\Requests;
use Illuminate\Foundation\Http\FormRequest;

class Booking extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hours_booking' => 'required',
            'email' => 'email|required|email|max:100',
            'phone_number' => 'required|regex:/^[0-9]+$/',
            'capacity' => 'required',
            'price' => 'required',
            'guide_id'=>'required'
        ];
    }
}