<?php

namespace Modules\Frontend\Requests;
use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|max:100',
            'username' => 'required|max:100|unique:user__users,username',
            'email' => 'unique:user__users,email|required|email|max:100',
            'password' => 'required',
            'confirmpassword' => 'required|same:password',
            'type' => 'required'
        ];
    }
}