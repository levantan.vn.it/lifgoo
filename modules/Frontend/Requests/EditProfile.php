<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 4/13/2017
 * Time: 3:53 PM
 */

namespace Modules\Frontend\Requests;


use Illuminate\Foundation\Http\FormRequest;

class EditProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'username' => 'max:100|unique:user__users,username|unique:user__users,email',
            'full_name' => 'max:100',
            'email' => 'email|max:100|unique:user__users,email|unique:user__users,username',
            'birth_date' => 'max:31|max:10',
            'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
            'phone_number' => 'regex:/^[0-9]+$/',
        ];
        if(session('users') && session('users.guide')){
            $rules = [
                'username' => 'max:100|unique:user__users,username|unique:user__users,email',
                'full_name' => 'required|max:100',
                'email' => 'email|max:100|unique:user__users,email|unique:user__users,username',
                'birth_date' => 'required|max:31|max:10',
                'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
                'phone_number' => 'required|regex:/^[0-9]+$/',
                'introduce' => 'required',
            ];
        }
        return $rules;
    }
}