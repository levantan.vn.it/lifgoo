<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 4/13/2017
 * Time: 3:53 PM
 */

namespace Modules\Frontend\Requests;


use Illuminate\Foundation\Http\FormRequest;

class EditJob extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'city_id' => 'required',
            'expiry_date' => 'required',
            'type' => 'required',
//            'status' => 'required',
            'experience' => 'required',
            'diploma_info' => 'required',
        ];
    }
}