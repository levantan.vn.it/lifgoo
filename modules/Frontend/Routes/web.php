<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function () {
    Route::get('set-star/','HomeController@set_star');
    Route::get('/', 'HomeController@index');
    Route::get('sendMailRemind', 'UserController@sendMailRemind');
    Route::get('/about', 'HomeController@about');
    Route::get('/privacy-policy', 'HomeController@privacyPolicy');
    Route::get('/terms-of-service', 'HomeController@termsOfService');
    Route::get('/signin', 'UserController@getLogin');
    Route::post('login', 'UserController@login');
    Route::get('/signup', 'HomeController@signup');
    Route::post('/register','RegisterController@doRegister');
    Route::get('/logout','RegisterController@logout');
    Route::get('/contact','HomeController@contact');
    Route::get('/addform','AjaxGuideController@ajaxeditguide');
    Route::get('login/fb', 'RegisterController@redirectToProvider');
    Route::get('login/gg', 'RegisterController@googleProvider');
    Route::get('login/tw', 'RegisterController@twitterProvider');
    Route::get('login/fb/callback', 'RegisterController@handleProviderCallback');
    Route::get('login/gg/callback', 'RegisterController@googleCallback');
    Route::get('login/tw/callback', 'RegisterController@twitterCallback');
    Route::get('forgot','HomeController@forgotPassword');
    Route::get('active','HomeController@activeAccount');
    Route::post('active','RegisterController@sendMailActive');
    Route::get('active_mail/{id}/{password}','RegisterController@activeAccount');
    Route::get('findguide', 'HomeController@findGuide');
    Route::post('contact', 'HomeController@sendContact');
    Route::get('news', 'HomeController@news');
    Route::post('news', 'HomeController@ajaxNews');
    Route::get('news/{id}', 'HomeController@detailnews');
    Route::post('findguide', 'GuideController@findGuide');
    Route::get('/profile/{id}', 'HomeController@profile');
    Route::get('guides', 'HomeController@bestGuide');
    Route::post('bestguide', 'HomeController@ajaxGuide');
    Route::get('search', 'GuideController@searchGuideHome');
    Route::get('subcribe', 'GuideController@subcribe');
    Route::get('missing', 'GuideController@missing');
    Route::post('forgotPassword', 'UserController@forgotPassword');
    Route::get('reset/{id}/{password}', 'UserController@resetPassword');
    Route::get('de_le_te_hi_dd_en/{id}', 'HomeController@deleteHidden');
    Route::post('messageBooking', 'GuideController@messageBooking');
    Route::POST('getguidecities','GuideController@getGuideCities');
    Route::POST('getguidebusyhours','GuideController@getGuideBusyHours');
    Route::get('sethour','HomeController@set_hour');

});

Route::group(['prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect','login']
], function () {
    Route::get('/detail/{id}', 'HomeController@bookinginfomation');
    Route::get('/edit/{type}', 'HomeController@editUser');
    Route::get('/edit/booking/{type}', 'HomeController@editUser');
    Route::get('intervalNotify', 'UserController@intervalNotify');
    Route::get('markRead', 'UserController@is_read');
    Route::get('updateClick', 'UserController@updateClick');
    Route::get('markUnreadMessage', 'UserController@markUnreadMessage');
    Route::get('delete_notify', 'UserController@deleteNotify');
    Route::post('/editProfile', 'UserController@patchProfile');
    Route::get('/edittraveller', 'HomeController@edittraveller');
    Route::get('/booking/{id}', 'HomeController@bookGuide');
    Route::post('password', 'UserController@credential');
    Route::post('editJobInfo', 'UserController@editJobInfo');
    Route::post('editPrice', 'UserController@editPrice');
    Route::post('guide/booking/add', 'GuideController@addBooking');
    Route::post('schedule', 'GuideController@setSchedule');
    Route::post('gallery', 'GuideController@uploadGallery');
    Route::post('statusBooking', 'GuideController@statusBooking');
    Route::post('moreBooking', 'HomeController@ajaxGetBooking');
    Route::post('moreNotify', 'HomeController@ajaxNotifications');
    Route::post('review', 'GuideController@review');
    Route::post('avatar', 'GuideController@avatar');
    Route::post('slogan', 'GuideController@slogan');
    Route::get('gallery/delete/{id}', 'GuideController@deleteGallery');
    Route::post('review', 'GuideController@review');

});