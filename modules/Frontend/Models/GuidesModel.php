<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 4/13/2017
 * Time: 10:43 AM
 */

namespace Modules\Frontend\Models;


use App\Mail\MailBooking;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\User\Entities\BookingDatesEntity;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\BookingMessagesEntity;
use Modules\User\Entities\GuideCertificationEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\GuideKnownLanguagesEntity;
use Modules\User\Entities\GuideLocationTourEntity;
use Modules\User\Entities\GuideScheduleEntity;
use Modules\User\Entities\GuideServicesEntity;
use Modules\User\Entities\NewsEntity;
use Modules\User\Entities\ServiceEntity;
use Helper;

class GuidesModel
{

    function patchGuide($data)
    {
        if (!$data && !$data['id'])
            return false;
        //$guide = GuideEntity::fi
    }

    /**
     * get service of system
     * @param $data
     * @return mixed
     */
    function getservice($data)
    {
        if (!$data)
            return ServiceEntity::get();
        if ($data['type'] == 'service')
            return ServiceEntity::whereType('service')->get();
        return ServiceEntity::whereType('transportation')->get();
    }

    /**
     * store certification. if not find certification then create new certification of specify guide
     * @param $data
     * @return bool
     */
    function storeCertification($data)
    {   

        if (!$data)
            return false;
        $cer = new GuideCertificationEntity();
        if (isset($data['user_id']) && $certifi = GuideCertificationEntity::where('user_id', '=', $data['user_id'])->first())
            $cer = $certifi;
        $guide = Helper::mapdata($cer, $data);
        return $guide->save();
    }

    /**
     * store new location of guide
     * @param $data
     * @return bool
     */
    function storeLocationTour($data)
    {
        if (!$data)
            return false;
        $location = new GuideLocationTourEntity();
        $guide = Helper::mapdata($location, $data);
        return $guide->save();
    }

    /**
     * delete all location of guide
     * @param null $guide
     * @param null $user_id
     * @return bool
     */
    function deleteLocation($guide = null, $user_id = null)
    {
        if ($guide)
            return $guide->locationTour()->delete();
        if ($user_id)
            return GuideLocationTourEntity::where('guide_id', '=', $user_id)->delete();
        return false;
    }

    /**
     * convert discount to array
     * @param $discount
     * @return array
     */
    function getDiscount($discount)
    {
        if (!$discount)
            return [];
        $arrDiscount = [];
        if ($discount) {
            if ($discount)
                foreach ($discount as $item) {
                    if (!isset($arrDiscount[$item->number_people]))
                        $arrDiscount[$item->number_people] = [];
                    $arrDiscount[$item->number_people][$item->time] = $item->price;
                }
        }
        return $arrDiscount;
    }

    /**
     * save booking of traveler
     * @param $data
     * @return bool
     */
    function storeBooking($data)
    {
        if (!$data)
            return false;
        $booking = new BookingEntity();
        $booking = Helper::mapdata($booking, $data);
        $booking->save();
        return $booking;
    }

    /**
     * save message's booking
     * @param $data
     * @return bool
     */
    function storeMessageBooking($data)
    {
        if (!$data)
            return false;
        $booking = new BookingMessagesEntity();
        $booking = Helper::mapdata($booking, $data);
        $booking->save();
        return $booking;
    }

    /**
     * @param $data
     * @return bool
     */
    function storeHoursBooking($data)
    {
        if (!$data)
            return false;
        $booking = new BookingDatesEntity();
        $booking = Helper::mapdata($booking, $data);
        return $booking->save();
    }

    function detailBooking($id){
        return BookingEntity::find($id);
    }
    /**
     * @param $user_id
     * @param array $fields
     * @return bool
     */
    function getBooking($user_id, $type = "traveler_id", $fields = [], $limit = 0, $offset = 0,$where=null)
    {
        if (!$user_id)
            return false;
        $booking = BookingEntity::where($type, '=', $user_id)->orderBy('created_at', 'desc');
        if($where)
            $booking->where('status','=',$where);
        if (!empty($fields)) {
            $booking->select($fields);
        }
        if ($limit)
            $booking = $booking->skip($offset * $limit)->take($limit);
        $booking = $booking->get();
        return $booking;
    }

    /**
     * load best guide soort follow star of guide
     * @param array $fields
     * @param int $limit
     * @param int $offset
     * @return array
     */
    function getGuide($fields = [], $limit = 0, $offset = 0)
    {
        $guide = GuideEntity::where('is_approved', '=', 1)->whereRaw('user_id in (select id from user__users where status="active")');
        if ($fields)
            $guide = $guide->select($fields);
        $count = $guide->count();
        if ($limit)
            $guide = $guide->skip($offset * $limit)->take($limit);
        return ['guides' => $guide->orderBy('star', 'desc')->get(), 'count' => $count];
    }

    /**
     * count booking
     * @param $user_id
     * @param string $type
     * @return bool
     */
    function countBooking($user_id, $type = "traveler_id",$where = null)
    {
        if (!$user_id)
            return false;
        $booking = BookingEntity::where($type, '=', $user_id);
        if($where)
            $booking->where('status','=',$where);
        return $booking->count();
    }

    /**
     * store a schedule of guide
     * @param $data
     * @return bool
     */
    function storeSchedule($data)
    {
        if (!$data)
            return false;
        $schedule = new GuideScheduleEntity();
        $schedule = Helper::mapdata($schedule, $data);
        return $schedule->save();
    }

    /**
     * process data of schedule after insert into db
     * @param $guide_id
     * @param $data
     * @return bool
     */
    function processSchedule($guide_id, $data)
    {
        if (!$guide_id || !$data)
            return false;
        $arr = [
            'guide_id' => $guide_id,
        ];
        if (is_array($data) && count($data)) {
            if (isset($data['multi_from']) && count($data['multi_from']))
                foreach ($data['multi_from'] as $key => $from) {
                    $to = $data['multi_to'][$key];
                    $arr['day_of_week'] = $key;
                    $arr['hours'] = Helper::ValueBetween($from, $to);
                    $arr['start_end_time'] = $from . '-' . $to;

                    if (isset($data['multi_from1'][$key]) && $from1 = $data['multi_from1'][$key]) {
                        $to1 = $data['multi_to1'][$key];
                        $arr['hours'] = $arr['hours'] . ',' . Helper::ValueBetween($from1, $to1);
                        $arr['start_end_time'] = $arr['start_end_time'] . ',' . $from1 . '-' . $to1;
                    }
                    $this->storeSchedule($arr);
                }
            return true;
        }
        return false;
    }

    /**
     * get all schedule of guide
     * @param $id
     * @param array $fields
     * @return bool
     */
    function getSchedule($id, $fields = [])
    {
        if (!$id)
            return false;
        $user = GuideScheduleEntity::where('guide_id', $id);
        if (!empty($fields)) {
            $user->select($fields);
        }
        return $user->get();
    }

    /**
     *get schedule hours
     * @param $schedules
     * @return array
     */
    function hoursExplodeSchedule($schedules)
    {
        $data = [];
        if (!$schedules)
            return $data;
        foreach ($schedules as $schedule) {
            $startHours = explode(',', $schedule->start_end_time);
            $h = explode('-', $startHours[0]);

            if (count($h) < 2)
                return $data;
            $data[$schedule->day_of_week] = [
                'start' => $h[0],
                'end' => $h[1],
            ];
            if (count($startHours) > 1) {
                $h = explode('-', $startHours[1]);
                $data[$schedule->day_of_week]['start1'] = $h[0];
                $data[$schedule->day_of_week]['end1'] = $h[1];
            }
        }
        return $data;
    }

    /**
     * send mail when booking, remind schedule
     * @param $email
     * @param $content
     * @throws \Exception
     */
    function sendmail($email, $content, $full_name,$subject="Mail Booking")
    {
        try {
            Mail::to($email)->send(new MailBooking($content, $full_name,$subject));
        } catch (\Exception $e) {
            // Get error here
            throw $e;
        };

    }
    /**
     * find guide follow destination
     * @param $data
     * @return bool
     */
    function findGuideByDestination($data)
    {
        if (!$data)
            return false;
        $guide = GuideLocationTourEntity::select('guide_id')->where('country_id', '=', $data['country_id']);
        if (isset($data['city_id']))
            $guide = $guide->where('city_id', '=', $data['city_id']);
        return $guide->groupBy('guide_id')->get();
    }

    function findGuideByLangguage($data)
    {
        if (!$data)
            return false;
        $guide = GuideKnownLanguagesEntity::select('guide_id')->where('language_id', '=', $data['language_id']);
        return $guide->groupBy('guide_id')->get();
    }

    /**
     * find guide
     * @param $data
     * @param int $limit
     * @param int $offset
     * @return array
     */
    function findGuide($data, $limit = 0, $offset = 0)
    {
//        $statement = 'country_id=:country_id';
        $statement='';
        if (isset($data['city_id']))
            $statement .= ' city_id=' . $data['city_id'];
        $sqlSevices = 1;
        if (isset($data['guide_services']))
            $sqlSevices = ' id in (select guide_id from guide__services where service_id in (' . implode(',', $data['guide_services']) . '))';
        $guide = GuideEntity::select('id', 'user_id', 'hourly_rate', 'star', 'is_approved')
            ->whereRaw('is_approved=:is_approved', ['is_approved' => 1])
            ->whereRaw('hourly_rate <= :rate_end and hourly_rate >= :rate_begin', ['rate_end' => $data['rate_end'], 'rate_begin' => $data['rate_begin']])
            ->whereRaw('id in (select guide_id from guide__location_tour  where ' . $statement . ')')
            ->whereRaw('id in (select guide_id from guide__known_languages where language_id=:language_id)', ['language_id' => $data['language_id']])
            ->whereRaw($sqlSevices)->orderBy('star', 'desc');
        $count = 0;
        if ($limit)
            $guide = $guide->skip($offset * $limit)->take($limit);
        return ['guides' => $guide->get(), 'count' => $count];
    }

    function findGuiByDesLang($data, $limit = 0, $offset = 0)
    {
        $guide = GuideEntity::where('is_approved', '=', 1)
            ->whereRaw('id in (select guide_id from guide__location_tour join system__cities on guide__location_tour.city_id=system__cities.city_id join system__countries on system__cities.country_id=system__countries.country_id  where system__countries.country_id=' . $data['country_id'] .')')
            ->whereRaw('id in (select guide_id from guide__known_languages where language_id=' . $data['language_id'] . ')')
            ->select('id', 'star', 'user_id', 'hourly_rate')
            ->orderBy('star', 'desc')
            ->limit($limit)
            ->get();
        return $guide;
    }

    /**
     * get all service of guide
     * @param $guide_id
     * @return bool
     */
    function guideServices($guide_id, $type = null)
    {
        if (!$guide_id)
            return false;
        if ($type) {
            $temp_service = DB::table('guide__services')->select('guide__services.*')->join('system__services', 'guide__services.service_id', '=', 'system__services.id')->where('system__services.type', $type)->where('guide__services.guide_id', $guide_id)->get();
            $service=[];
            foreach($temp_service as $item)
            {
                $ent = GuideServicesEntity::where('id',$item->id)->first();
                array_push($service,$ent);
            }
        }
        else
            $service = GuideServicesEntity::where('guide_id', '=', $guide_id)->get();

        return $service;
    }

    /**
     * check Information's guide is missing
     * @param $user
     */
    function isMissingInformation($user)
    {
        $guide = $user->guide()->select('id', 'idnumber', 'hourly_rate', 'introduce', 'diploma_info', 'experience')->first();
        $location = $guide->locationTour()->first();
        $language = $guide->known_languages()->first();
        $certification = $user->certification()->first();

        $profile = array_merge($this->setValueToArray(Helper::arrayCheckMissing()['profile_user'], $user), $this->setValueToArray(Helper::arrayCheckMissing()['profile_info'], $guide));
        $jobInfo = array_merge($this->setValueToArray(Helper::arrayCheckMissing()['jobInfo_certification'], $certification), $this->setValueToArray(Helper::arrayCheckMissing()['jobInfo_location'], $location), $this->setValueToArray(Helper::arrayCheckMissing()['jobInfo_language'], $language),$this->setValueToArray(Helper::arrayCheckMissing()['jobInfo_info'], $guide));

        $data = array_merge($this->checkEmpty($profile,'profile'),$this->checkEmpty($jobInfo,'jobInfo'));
        return $data;
    }

    /**
     * set value equal value
     * @param $array
     * @param $entity
     * @return array
     */
    function setValueToArray($array, $entity)
    {
        $result = [];
        foreach ($array as $i) {
            if (!$entity)
                $result[$i] = null;
            else
                $result[$i] = $entity->$i;

        }
        return $result;
    }

    /**
     * check element in array is empty
     * @param $array
     */
    function checkEmpty($array,$type)
    {
        $profile = [];
        foreach ($array as $key => $value) {
            if (!$value)
                array_push($profile, $key);
        }
        return [
            $type => $profile,
            'missing_'.$type => count($profile) > 0
        ];
    }
}