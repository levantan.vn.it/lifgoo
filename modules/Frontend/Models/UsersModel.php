<?php
    namespace Modules\Frontend\Models;

    use App\User;
    use Helper;
    use Image;
    use Modules\Frontend\Entities\GuideInfoEntities;
    use Modules\Frontend\Entities\UserEntities;
    use Modules\Frontend\Http\Controllers\HomeController;
    use Modules\User\Entities\BookingMessagesEntity;
    use Modules\User\Entities\CityEntity;
    use Modules\User\Entities\CountryEntity;
    use Modules\User\Entities\GalleryEntity;
    use Modules\User\Entities\GuideDiscountEntity;
    use Modules\User\Entities\GuideEntity;
    use Modules\User\Entities\ServiceEntity;
    use Modules\User\Entities\TravellerEntity;

    class UsersModel{
    
        public function getGuide(){
            return UserEntities::get();
        }

        public function findGuide($id){
            return GuideInfoEntities::find($id);
        }

        /*public function updateStar($guide, $star){
            $guide->star = $star;
            $result = $guide->save();
            return $result;
        }*/
    
        /**
         * get user data by id
         * @param $id
         * @return mixed
         */
        public static function getUserById($id, $fields = [])
        {
            if (!$id)
                return false;
        
            $user = User::where('id', $id);
            if (!empty($fields)) {
                $user->select($fields);
            }
            $user = $user->first();
            unset($user['permissions']);
            return $user;
        }
    
        /**
         * get all country on the world or the country by country_id
         * @return mixed
         */
        public static function getCountry($country_id = null, $fields = [])
        {
            $country = new CountryEntity();
            if (!empty($fields)) {
                $country->select($fields);
            }
            if ($country_id)
                return $country->where('country_id', '=', $country_id)->first();
            return $country->orderBy('country_name', 'ASC')->get();
        }
    
        /**
         * get all city by $country_id or the city by city_id
         * @param $country_id
         * @param null $city_id
         * @param array $fields
         * @return bool
         */
        public static function getCity($country_id, $city_id = null, $fields = [])
        {
            if (!$country_id)
                return false;
            $city = CityEntity::where('country_id', $country_id);
            if (!empty($fields)) {
                $city->select($fields);
            }
            if($city_id)
                return $city->where('city_id','=',$city_id)->first();
            return $city->orderBy('city_name', 'ASC')->get();
        
        }
    
        /**
         * delete all table relate table service
         * @param $service_id
         */
        static function deleteRelateService($service_id){
            if (!$service_id)
                return false;
            $service = ServiceEntity::find($service_id);
            $service && $service->serviceLang()->delete();
            $guide_services = $service->guide_service()->get();
            if($guide_services)
                foreach ($guide_services as $g_s){
                   $g_s->traveller_booking_service()->delete() && $g_s->delete();
                }
            return $service->delete();
        }

        /**
         * upload image of news
         * @param null $file
         * @param $news
         * @return mixed
         */
        static function uploadImg($file = null,$news)
        {

            $news_id = $news->id;
            $file_name = strtolower('news' . $news_id . '.'. $file->extension());
            $img = Image::make($file->getRealPath());
            if ($img->width() > env('thumb_width'))
                $img->resize(env('thumb_width'), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            if ($img->height() > env('thumb_height'))
                $img->resize(null, env('thumb_height'), function ($constraint) {
                    $constraint->aspectRatio();
                });
            $destinationPath = public_path() . Helper::getThemeNewsUpload();
            if ($img->save($destinationPath . $file_name)){
                $news->url = $file_name;
                $news->save();
                return Helper::resultData(true, trans('site.action_succeeded'), ['url' => $destinationPath . '/' . $file_name]);
            }
            return Helper::resultData(false, trans('site.action_succeeded'));
        }

        /**
         * upload avatar of user
         * @param null $file
         * @param $user
         * @param string $type
         * @return mixed
         */
        static function uploadAvatar($file = null, $user,$type = 'avatar',$method='web'){

            $user_id = $type=='avatar'?$user->id:$user->user_id;
            if($method=='web'){
                $file_name = strtolower($user_id.'_'.$type. '.' . $file->extension());
            }
            if($method=='api'){
                $file_name = strtolower($user_id.'_'.$type. '.' . $file->getExtension());
            }
            $img = Image::make($file->getRealPath());
            if($type =='avatar'){
                unset($user['cover']);
                if ($img->width() > env('avatar'))
                    $img->resize(env('avatar'), null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                if ($img->height() > env('avatar'))
                    $img->resize(null, env('avatar'), function ($constraint) {
                        $constraint->aspectRatio();
                    });
            }
            $destinationPath = public_path() . Helper::getThemeAvatarUpload();
            if ($img->save($destinationPath . $file_name)) {
                $user->$type = $file_name;
                $user->save();
                return Helper::resultData(true, trans('site.action_succeeded'), ['url' => $destinationPath . '/' . $file_name]);
            }
            return Helper::resultData(false, trans('site.action_succeeded'));
        }

        /**
         * upload gallery of guide after insert db
         * @param null $file
         * @param $user_id
         * @param $guide_id
         * @return bool
         */
        static function uploadGallery($file = null, $user_id,$guide_id,$method='web'){
            if($method=='web'){
                $file_name = strtolower(rand(0,time()).'.' . $file->extension());
            }
            if($method=='api'){
                $file_name = strtolower(rand(0,time()).'.' . $file->getExtension());
            }
            $img = Image::make($file->getRealPath());
            $destinationPath = public_path() . Helper::getThemeGallery('gallery'.$user_id);
            if(!is_dir($destinationPath))
                mkdir($destinationPath,0777);
            if ($img->save($destinationPath . $file_name)) {
                $gallery = new GalleryEntity();
                $gallery->guide_id = $guide_id;
                $gallery->url = $file_name;
                $gallery->save();
                return  true;
            }
            return false;
        }

        /**
         * save Basic information of user
         * @param $data
         * @return bool|User
         */
        function storeUser($data,$type = 'user')
        {
            if (!$data)
                return false;
            $user = new User();
            $user = Helper::mapdata($user, $data);
            if (!$user->save())
                return false;
            unset($user['password']);
            if ($type == 'guide') {
                $this->patchGuideInfo(['status'=>'available'], $user->id);
                $user->roles()->sync([3]);
            } else{
                $this->patchTravellerInfo(['language_code'=>'en'], $user->id);
                $user->roles()->sync([4]);
            }
            /*self::storeSession($user, $type);*/
            return $user;
        }
    
        static function storeSession($user, $type)
        {
            if (!$user)
                return false;
            if ($type == 'guide') {
                session()->put('users.guide', $user);
            } elseif ($type == 'user')
                session()->put('users.user', $user);
            return true;
        }
    
        /**
         * Return user if exists; create and return if doesn't
         *
         * @param $fbUser
         * @return User
         */
        public function findOrCreateUser($fbUser,$type = 'facebook_id')
        {
            $user = $this->getUserByEmail($fbUser->email);
            if($user && $user->type == 'guide'){
                session()->flash('flash_error', trans('This email registed by guide account.'));
                return 'guide';
            }
            if ($authUser = $this->getUserByFbId($fbUser->id,$type)) {
                if (!empty($authUser->email))
                    return $authUser;
                if (!empty($fbUser->email)) {
                    if ($authUser->email == $fbUser->email || $authUser->username == $fbUser['email'])
                        return $authUser;
                    else {
                        $authUser->email = $fbUser->email;
                        $authUser->username = $fbUser->email;
                        $authUser->save();
                        return $authUser;
                    }
                }
                return $authUser;
            }
        
            if (!empty($fbUser->email) && $user) {
                if ($user->status == 'blocked') {
                    session()->flash('action_unsuccess', trans('home::home.email_acc_failed'));
                    return redirect('/signin');
                }
                if ($user->type == 'admin' || $user->type == 'manager')
                    return redirect()->back();
                if (!empty($user->facebook_id)) {
                    return $user;
                }
                if($user->type == 'guide'){
                    session()->flash('action_unsuccess', trans('This email registed by guide account.'));
                    return redirect('/signin');
                }

                $user->$type = $fbUser->id;
                //$user->avatar = $fbUser->avatar;
                $user->save();
                return $user;
            }
            $user = new User();
            $new_user = clone $user;
            $user->full_name = $fbUser->name;
            $user->email = $fbUser->email;
            $user->$type = $fbUser->id;
            //$user->avatar = $fbUser->avatar;
            $user->status = 'active';
            //$new_user->unset('id');
            unset($new_user['id']);
            $user->save();
            $user->roles()->attach(4);
            return $user;
        }

        function storeUserWithSocial($fbUser,$type = 'facebook_id'){
            $email = $fbUser->email;
            $user = $this->getUserByEmail($email);
            return $fbUser;
        }
    
        public function getUserByFbId($id,$type = 'facebook_id')
        {
            return User::where($type, $id)->first();
        }
    
        /** Get user by their unique email
         * @param $email
         * @return mixed
         */
        public function getUserByEmail($email)
        {
            //return User::where('email', 'regexp', "/^$email$/i")->first();
            return User::where('email', '=', $email)->first();
        }
    
        /**
         * patch profile of user
         * @param $data
         * @param $user
         * @return bool
         */
        public function patchProfile($data,$user)
        {
            if(!$data && $data['id'])
                return false;
            $user = Helper::mapdata($user,$data);
            if($user->save())
                return self::storeSession($user, $user['type']);
            return false;
        }
    
        /**
         * patch guide information
         * @param $data
         * @param $user_id
         * @return bool
         */
        function patchGuideInfo($data,$user_id)
        {
            if (!$data || !$user_id)
                return false;
            $guide = GuideEntity::where('user_id', '=', $user_id)->first();
            if (!$guide) {
                $guide = new GuideEntity();
                $data['user_id'] = $user_id;
            }
            $guide = Helper::mapdata($guide, $data);
            return $guide->save();
        }
        
        function patchTravellerInfo($data, $user_id)
        {
            if (!$data || !$user_id)
                return false;
            $guide = TravellerEntity::where('user_id', '=', $user_id)->first();
            if (!$guide) {
                $guide = new TravellerEntity();
                $data['user_id'] = $user_id;
            }
            $guide = Helper::mapdata($guide, $data);
            return $guide->save();
        }
        /**
         * save discount of guide include capacity, hour, price
         * @param $data
         * @return bool
         */
        function storeGuideDiscount($data){
            if(!$data)
                return false;
            $discount = new GuideDiscountEntity();
            $guide = Helper::mapdata($discount, $data);
            return $guide->save();
        }
    
        /**
         * delete all discout of specify guide
         * @param $guide_id
         * @return bool
         */
        function removeDiscount($guide_id){
            if(!$guide_id)
                return false;
            return GuideDiscountEntity::where('guide_id', '=', $guide_id)->delete();
        }
        
        function getAllDiscount($guide_id){
            if (!$guide_id)
                return false;
            return GuideDiscountEntity::where('guide_id', '=', $guide_id)->get();
        }

        /**
         * get guide data by id
         * @param $id
         * @return mixed
         */
        public static function getGuideById($id, $fields = [])
        {
            if (!$id)
                return false;
            $user = GuideEntity::where('id', $id);
            if (!empty($fields)) {
                $user->select($fields);
            }
            $user = $user->first();
            return $user;
        }

        function storeMessageBooking($data){
            if(!$data)
                return false;
            $user_id = $data['user_id'];
            $message = BookingMessagesEntity::orderBy('id', 'desc')->select('user_id')->first();
            if(!$message || $user_id != $message->user_id){
                $msg = new BookingMessagesEntity();
                $msg = Helper::mapdata($msg,$data);
                return $msg->save();
            }
            return false;
        }

        /**
         * send mail active when resgister
         * @param $user
         */
        static function sendMailActive($user){
            $id = base64_encode($user->id);
            $new_pass = rand().time();
            $content = HomeController::htmlEmail('You recently registered for lifgoo. To complete your Lifgoo registration, please confirm your account.');
            $content .= HomeController::htmlEmail('<a href="'.Helper::url('active_mail/'.$id.'/'.$new_pass).'">Click here to confirm your account.</a>');
            $guideModel = new GuidesModel();
            return $guideModel->sendmail($user->email, $content, $user->full_name,'Activate Email');

        }
    }
?>