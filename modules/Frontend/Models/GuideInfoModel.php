<?php

namespace Modules\Frontend\Models;

use App\User;
use Illuminate\Support\Facades\DB;
use Modules\Frontend\Entities\GuideInfoEntities;
use Modules\User\Entities\GuideEntity;

class GuideInfoModel
{
    public function findGuide($id)
    {
        return GuideInfoEntities::find($id);
    }

    public function getGuide($limit = false, $orderby = false)
    {
        $guide = new GuideEntity();

        $guide = $guide->where('is_approved', '=', 1)
            ->whereExists(function ($query) {
                $query->select(DB::raw('id'))
                    ->from('user__users')
                    ->whereRaw('user__users.id = guide__info.user_id')
                    ->whereRaw('user__users.status="active"');
            });

        if ($limit) {
            $guide = $guide->limit($limit);
        }
        if ($orderby) {
            $guide = $guide->orderBy($orderby[0], $orderby[1]);
        }
        return $guide->get();
    }

    public function getStar($idd)
    {
        $st = GuideInfoEntities::select('star')->where('id', $idd)->first();
        return $st;
    }

    public function updateStar($guide, $star)
    {
        $guide->star = $star;
        $result = $guide->save();
        return $result;
    }

    static function countGuide()
    {
        return GuideEntity::select('id')->where('is_approved', '=', 1)->count();
    }

    static function countTraveler()
    {
        return User::where('type', '=', 'user')->count();
    }
}

?>