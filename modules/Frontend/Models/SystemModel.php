<?php

namespace Modules\Frontend\Models;

use Helper;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\GuideKnownLanguagesEntity;
use Modules\User\Entities\GuideLocationTourEntity;
use Modules\User\Entities\GuideServicesEntity;
use Modules\User\Entities\LanguageWorldEntity;
use Modules\User\Entities\NewsEntity;
use Modules\User\Entities\NewsLanguageEntity;
use Modules\User\Entities\NotificationEntity;
use Modules\User\Entities\ServiceEntity;

class SystemModel
{
    /**
     * create new news
     * @param $data
     * @return bool|NewsEntity
     */
    static function storeNews($data)
    {
        if (!$data)
            return false;
        $news = new NewsEntity();
        $news = Helper::mapdata($news, $data);
        $news->save();
        return $news;
    }

    /**
     * create new news language
     * @param $data
     * @return bool|NewsLanguageEntity
     */
    static function storeNewsLanguage($data)
    {
        if (!$data)
            return false;
        $newsLanguage = new NewsLanguageEntity();
        $newsLanguage = Helper::mapdata($newsLanguage, $data);
        $newsLanguage->save();
        return $newsLanguage;
    }

    /**
     * save data when edit news
     * @param $data
     * @return bool|NewsEntity
     */
    static function patchNews($data)
    {
        if (!$data)
            return false;
        $news = NewsEntity::find($data['id']);
        if (!$news)
            return Helper::resultData(false, trans('admin.news_not_exists'));
        $news = Helper::mapdata($news, $data);
        $news->save();
        return $news;
    }

    /**
     * create new news language
     * @param $data
     * @return bool|NewsLanguageEntity
     */
    static function patchNewsLanguage($data)
    {
        if (!$data)
            return false;
        $newsLanguage = NewsLanguageEntity::whereNews_id($data['news_id'])->where('language_code', '=', $data['language_code'])->first();
        if (!$newsLanguage)
            return Helper::resultData(false, trans('admin.news_language_not_exists'));
        $newsLanguage = Helper::mapdata($newsLanguage, $data);
        $newsLanguage->save();
        return $newsLanguage;
    }

    /**
     * delete all table relate table news
     * @param $service_id
     */
    static function deleteRelateNews($news_id)
    {
        if (!$news_id)
            return false;
        $news = NewsEntity::find($news_id);

        $news && $news->news_language()->delete();
        if ($news && $news->url) {
            $filepath = public_path() . Helper::getThemeNewsUpload() . $news->url;
            file_exists($filepath) && unlink($filepath);
        }
        return $news->delete();
    }

    /**
     * store service of guide
     * @param $data
     * @return GuideServicesEntity
     */
    static function storeService($data)
    {
        $service = new GuideServicesEntity();
        $service = Helper::mapdata($service, $data);
        $service->save();
        return $service;
    }

    /**
     * get language in the world by id, or get all language in the world
     * @param null $language_id
     * @return mixed
     */
    static function getLanguagesWorld($language_id = null)
    {
        if ($language_id)
            return LanguageWorldEntity::find($language_id);
        return LanguageWorldEntity::get();
    }

    /**
     * store known language of guide
     * @param $data
     * @return bool
     */
    function storeKnownLanguage($data)
    {
        if (!$data)
            return false;
        $language = new GuideKnownLanguagesEntity();
        $language = Helper::mapdata($language, $data);
        return $language->save();
    }

    /**
     *  obtain services' system
     * @param null $type
     * @return mixed
     */
    static function serviceSystem($type = null)
    {
        $service = new ServiceEntity();
        if ($type)
            $service = $service->where('type', '=', $type);
        return $service->get();
    }

    /**
     * obtain news' system
     * @param array $fields
     * @param int $limit
     * @param int $offset
     * @return array
     */
    function news($fields = [], $limit = 0, $offset = 0)
    {
        $news = new NewsEntity();
        if ($fields)
            $news = $news->select($fields);
        $count = $news->count();
        if ($limit)
            $news = $news->skip($offset * $limit)->take($limit);
        return ['guides' => $news->orderBy('created_at','desc')->get(), 'count' => $count];
    }


    function getNews(){
        $news = NewsEntity::orderBy('created_at','desc');
        $count = NewsEntity::select('id')->count();
        return ['news'=>$news->limit(6)->get(),'count'=>$count];
    }

    /**
     * get detai news
     * @param $id
     * @return bool
     */
    function detailNews($id)
    {
        if (!$id)
            return false;
        return NewsEntity::where('id', '=', $id)->first();
    }

    /**
     * get popular news
     * @param $idNotEqual
     * @param int $take
     * @return mixed
     */
    function randPopularNews($idNotEqual,$take = 0)
    {
        /*$min = NewsEntity::select('id')->first();
        $max = NewsEntity::orderBy('created_at', 'desc')->select('id')->first();
        $number = 0;
        $count = NewsEntity::count();*/
        /*if ($min && $max && $max->id > $min->id && $count>8)
            $number = rand($min->id, $max->id-8);*/
        return NewsEntity::where('id','!=',$idNotEqual)->orderBy('created_at','desc')->skip(4)->take($take)->get();
    }

    function suggestfriend($city_id= null,$counttry_id){
        if(!$counttry_id)
            return false;
//        $statement = 'country_id='.$counttry_id;
        $statement='';
        if ($city_id)
            $statement .= ' city_id=' . $city_id;
        $guide = GuideEntity::where('is_approved','=','1')
                ->whereRaw('id in (select guide_id from guide__location_tour  where ' . $statement . ')');
        return $guide->select('id','star','user_id','hourly_rate')->orderBy('star','desc')->limit(8)->get();
    }

    /**
     * insert notify of guide and traveler
     * has 4 status: request_booking, accept_booking, decline_booking, message
     * @param $data
     * @return bool|NotificationEntity
     */
    function insertNotify($data){
        if(!$data)
            return false;
        $notify = new NotificationEntity();
        $notify = Helper::mapdata($notify,$data);
        $notify->save();
        return $notify;
    }

    function notifications($user_id,$fields = [], $limit = 0, $offset = 0)
    {
        $notify = NotificationEntity::where('user_id','=',$user_id);
        if ($fields)
            $notify = $notify->select($fields);
        $count = $notify->count();
        if ($limit)
            $notify = $notify->skip($offset * $limit)->take($limit)->orderBy('created_at','desc');
        return ['notify' => $notify->get(), 'count' => $count];
    }
}