<?php

namespace Modules\Frontend\Models;

use Helper;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\TravelerReviewEntity;

class TravelerModel
{
    /**
     * get all review's guide
     * @param $guide_id
     * @param array $field
     * @return mixed
     */
    function getRating($guide_id, $field = [])
    {
        /*$rating = TravelerReviewEntity::where('guide_id', '=', $guide_id);
        if ($field)
            $rating = $rating->select($field);
        return $rating->orderBy('created_at', 'desc')->limit(100)->get();*/
        $rating = BookingEntity::where('guide_id','=',$guide_id)->where('review','!=','')->select('id','traveler_id','review','ratings','updated_at')->orderBy('updated_at', 'desc')->limit(100)->get();
        return $rating;
    }

    /**
     * Check whether guests have booked or not
     * @param $user_id
     * @param $guide_id
     */
    function checkBooking($user_id, $guide_id)
    {
        if (!$user_id || !$guide_id)
            return false;
        return BookingEntity::where('traveler_id', '=', $user_id)->where('guide_id', '=', $guide_id)->whereIn('status', ['accepted', 'finished'])->count();
    }

    /**
     * store review of traveler
     * @param $data
     * @return bool
     */
    function storeReview($data)
    {
        $booking = BookingEntity::find($data['booking_id']);
        if($booking){
            $booking->review = $data['review'];
            $booking->ratings = $data['ratings'];
            $booking->save();
            $notifi = [
                'type' => 'review',
                'user_id' => $booking->guide_id,
                'is_read' => 0,
                'sender_id' => $booking->traveler_id,
                'booking_id' => $booking->id
            ];
            $systemModel = new SystemModel();
            $systemModel->insertNotify($notifi);
            $this->averageStar($booking->guide_id);
        }
    }

    function averageStar($guide_id)
    {
        $avgStar = round(BookingEntity::where('guide_id', '=', $guide_id)->avg('ratings'));
        $guide = GuideEntity::where('user_id', '=', $guide_id)->first();
        if ($avgStar)
            $guide->star = $avgStar;
        return $guide->save();
    }


}

?>