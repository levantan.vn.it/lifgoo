<?php

namespace Modules\Frontend\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Helper;

class BaseController extends Controller
{
    public function __construct()
    {
        \Assets::addCss([
            /*Link Css ionRangeSlider*/
            // Helper::getThemePlugins("ionRangeSlider/css/normalize.css"),
            Helper::getThemePlugins("ionRangeSlider/css/normalizev.min.css"),
            // Helper::getThemePlugins("ionRangeSlider/css/ion.rangeSlider.css"),
            Helper::getThemePlugins("ionRangeSlider/css/ion.rangeSliderv.min.css"),
            // Helper::getThemePlugins("ionRangeSlider/css/ion.rangeSlider.skinFlat.css"),
            Helper::getThemePlugins("ionRangeSlider/css/ion.rangeSlider.skinFlatv.min.css"),
            /*messi*/
            Helper::getThemePlugins("messi/messiv.min.css"),

            /*Link Css Font Awesome*/
            Helper::getThemePlugins('Font-Awesome-master/css/font-awesomev.min.css'),
            /*Link Css Revolution Slider*/
            // Helper::getThemePlugins('revolution-slider/css/navstylechange.css'),
            Helper::getThemePlugins('revolution-slider/css/navstylechangev.min.css'),
            // Helper::getThemePlugins('revolution-slider/css/settings.css'),
            Helper::getThemePlugins('revolution-slider/css/settingsv.min.css'),
            /*Link Css bootstrap datepicker*/
            // Helper::getThemePlugins('bootstrap-daterangepicker/daterangepicker.css'),
            Helper::getThemePlugins('bootstrap-daterangepicker/daterangepickerv.min.css'),
            Helper::getThemePlugins('bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepickerv.min.css'),
            /*Link Css Slick Slider*/
            // Helper::getThemePlugins('slick-slider/slick/slick.css'),
            Helper::getThemePlugins('slick-slider/slick/slickv.min.css'),
            // Helper::getThemePlugins('slick-slider/slick/slick-theme.css'),
            Helper::getThemePlugins('slick-slider/slick/slick-themev.min.css'),
            /*Link Css Slick Lightbox*/
            Helper::getThemePlugins('slick-lightbox/slick-lightboxv.min.css'),
            /*Link Css bootstrap*/
            Helper::getThemePlugins('bootstrap/css/bootstrapv.min.css'),
            /*Link Css bootstrap menu*/
            // Helper::getThemePlugins('bootsnav_files/css/bootsnav.css'),
            Helper::getThemePlugins('bootsnav_files/css/bootsnavv.min.css'),
            // Helper::getThemePlugins('bootsnav_files/css/animate.css'),
            Helper::getThemePlugins('bootsnav_files/css/animatevv.min.css'),
            /*Link Css Bootstrap Tabs Vertical*/
            Helper::getThemePlugins('bootstrap_vertical_tabs/bootstrap.vertical-tabsv.min.css'),
            
            /*Link Css Jcopper*/
            // Helper::getThemePlugins('cropit/css/style.css'),
            Helper::getThemePlugins('cropit/css/stylev.min.css'),
            // Helper::getThemePlugins('cropit/css/style-example.css'),
            Helper::getThemePlugins('cropit/css/style-examplev.min.css'),
            // Helper::getThemePlugins('cropit/css/jquery.Jcrop.css'),
            Helper::getThemePlugins('cropit/css/jquery.Jcropv.min.css'),
            /*Link Css Upload Gallery*/
            // Helper::getThemePlugins('upload-gallery/css/jquery.fileuploader.css'),
            Helper::getThemePlugins('upload-gallery/css/jquery.fileuploaderv.min.css'),
            // Helper::getThemePlugins('upload-gallery/css/jquery.fileuploader-theme-thumbnails.css'),
            Helper::getThemePlugins('upload-gallery/css/jquery.fileuploader-theme-thumbnailsv.min.css'),
            /*Link Css Select2*/
            Helper::getThemePlugins('select2/select2v.min.css'),
            /*Link Css Bootstrap Select*/
            Helper::getThemePlugins('bootstrap-select/bootstrap-selectv.min.css'),
            /*Links Css Bootstrap Tagsinput*/
            Helper::getThemePlugins('bootstrap-tokenfield/dist/css/bootstrap-tokenfieldv.min.css'),

            Helper::getThemeCss('frontend/style.css?v='.Helper::version()),
            // Helper::getThemeCss('frontend/stylev.min.css?v='.Helper::version()),
            // Helper::getThemeCss('loading.css'),
            Helper::getThemeCss('loadingv.min.css'),

        ]);
        \Assets::addJs([
            // Helper::getThemeJs('frontend/jquery-2.1.1.js'),
            Helper::getThemeJs('frontend/jquery-2.1.1v.min.js'),
            Helper::getThemePlugins('bootstrap/js/bootstrapv.min.js'),
            // Helper::getThemePlugins('bootstrap/js/bootstrap.min.js'),
           /* Helper::getThemePlugins('bootsnav_files/js/bootsnav.js'),*/
            /*messi*/
            Helper::getThemePlugins("messi/messiv.min.js"),
            // Helper::getThemePlugins("messi/messi.min.js"),
            /*Link js Slick Slider*/
            // Helper::getThemePlugins('slick-slider/slick/slick.min.js'),
            Helper::getThemePlugins('slick-slider/slick/slickv.min.js'),
            /*Link js Slick Lightbox*/
            // Helper::getThemePlugins('slick-lightbox/slick-lightbox.min.js'),
            Helper::getThemePlugins('slick-lightbox/slick-lightboxv.min.js'),
            /*Link js bootstrap datepicker*/
            // Helper::getThemePlugins('bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js'),
            Helper::getThemePlugins('bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepickerv.min.js'),
            /**
             * bootstrap-daterangepicker
             */
            // Helper::getThemePlugins('moment/min/moment.min.js'),
            Helper::getThemePlugins('moment/min/momentv.min.js'),
            // Helper::getThemePlugins('bootstrap-daterangepicker/daterangepicker.js'),
            Helper::getThemePlugins('bootstrap-daterangepicker/daterangepickerv.min.js'),
            /*Link js Revolution Slider*/
            // Helper::getThemePlugins('revolution-slider/js/jquery.themepunch.plugins.min.js'),
            Helper::getThemePlugins('revolution-slider/js/jquery.themepunch.pluginsv.min.js'),
            // Helper::getThemePlugins('revolution-slider/js/jquery.themepunch.revolution.min.js'),
            Helper::getThemePlugins('revolution-slider/js/jquery.themepunch.revolutionv.min.js'),
            /*Link js validate*/
            // Helper::getThemePlugins('validate/jquery.validate.min.js'),
            Helper::getThemePlugins('validate/jquery.validatev.min.js'),
            /*Helper::getThemePlugins('validate/jquery-validate.bootstrap-tooltip.min.js'),*/
            /*Link js Counter Up*/
            Helper::getThemePlugins('counter_up/waypointv.min.js'),
            // Helper::getThemePlugins('counter_up/waypoint.js'),
            Helper::getThemePlugins('counter_up/jquery.counterupv.min.js'),
            // Helper::getThemePlugins('counter_up/jquery.counterup.min.js'),
            /*Link Css Bootstrap Tabs Responsive*/
            // Helper::getThemePlugins('bootstrap-responsive-tabs-master/responsive-tabs.js'),
            Helper::getThemePlugins('bootstrap-responsive-tabs-master/responsive-tabsv.min.js'),
            /*Link js Jcopper*/
            // Helper::getThemePlugins('cropit/scripts/jquery.Jcrop.js'),
            Helper::getThemePlugins('cropit/scripts/jquery.Jcropv.min.js'),
            // Helper::getThemePlugins('cropit/scripts/jquery.SimpleCropper.js'),
            Helper::getThemePlugins('cropit/scripts/jquery.SimpleCropperv.min.js'),
            /*Link Js tinymce*/
            Helper::getThemePlugins('tinymce/tinymcev.min.js'),
            // Helper::getThemePlugins('tinymce/tinymce.min.js'),
            /*select2*/
            // Helper::getThemePlugins('select2/select2.min.js'),
            Helper::getThemePlugins('select2/select2v.min.js'),
            /*Link Js MultiDatesPicker*/
            /*Helper::getThemePlugins('MultiDatesPicker/js/jquery-ui-1.11.1.js'),
            Helper::getThemePlugins('MultiDatesPicker/js/jquery-ui.multidatespicker.js'),*/
            /*Link Js Upload Gallery*/
            // Helper::getThemePlugins('upload-gallery/js/jquery.fileuploader.min.js'),
            // Helper::getThemePlugins('upload-gallery/js/jquery.fileuploader.js'),
            Helper::getThemePlugins('upload-gallery/js/jquery.fileuploaderv.min.js'),
            // Helper::getThemePlugins('upload-gallery/js/custom.js?v='.Helper::version()),
            Helper::getThemePlugins('upload-gallery/js/customv.min.js'),
            /*Link Js Bootstrap Select*/
            // Helper::getThemePlugins('bootstrap-select/bootstrap-select.min.js'),
            Helper::getThemePlugins('bootstrap-select/bootstrap-selectv.min.js'),
            /*Links Js Bootstrap Tagsinput*/
            // Helper::getThemePlugins('bootstrap-tokenfield/dist/bootstrap-tokenfield.min.js'),
            Helper::getThemePlugins('bootstrap-tokenfield/dist/bootstrap-tokenfieldv.min.js'),
            /*Link Css ionRangeSlider*/
            // Helper::getThemePlugins('ionRangeSlider/js/ion.rangeSlider.js'),
            Helper::getThemePlugins('ionRangeSlider/js/ion.rangeSliderv.min.js'),


            Helper::getThemeJs('frontend/index.js?v='.Helper::version()),
            Helper::getThemeJs('user.js?v='.Helper::version()),
            Helper::getThemeJs('lifgoo.js?v='.Helper::version()),
            // Helper::getThemeJs('frontend/indexv.min.js?v='.Helper::version()),
            // Helper::getThemeJs('userv.min.js?v='.Helper::version()),
            // Helper::getThemeJs('lifgoov.min.js?v='.Helper::version()),
            /*Link Js easing master*/
            Helper::getThemePlugins('easing_master/jquery.easing.min.js'),
        ]);
    }

    public function loggedInStatus()
    {
        session('users') && $user = session('users.user')? session('users.user'): session('users.guide');
        if (empty($user)) return false;
        return $user;
    }
}
