<?php

namespace Modules\Frontend\Http\Controllers;

use App\User;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\UsersModel;
use Modules\Frontend\Requests\Register;
use Socialite;


class RegisterController extends BaseController
{
    function doRegister(Register $r)
    {
        if ($this->loggedInStatus())
            return redirect('');
        $request = $r->all();
        $fields = ['email', 'username', 'full_name', 'type'];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $data['password'] = Hash::make($request['password']);
        $user = new UsersModel();
        $data['status'] = 'inactive';
        if ($result = $user->storeUser($data, $request['type'])) {
            UsersModel::sendMailActive($result);
            session()->flash('flash_confirm_signup', trans('site.login_fail'));
            return Helper::jsondata(true, trans('site.login_success'));
        }
        return Helper::errors('error_occured', trans('site.error_occured'), 404);
    }

    function logout()
    {
        if (session()->has('users'))
            session()->forget('users');
        return redirect('');
    }

    /** Facebook login redirection
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToProvider()
    {
        if ($this->loggedInStatus()) return redirect()->back();
        return \Socialite::driver('facebook')->scopes(['email'])->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $r)
    {
        if($r->error)
            return redirect(Helper::url('signin'));
        if ($this->loggedInStatus()) return redirect()->back();
        $user = \Socialite::driver('facebook')->user();
        if (!$user) return redirect(url());
        if($user->email===NULL){
            session()->flash('flash_error',"We can't get your email! Please try again!");
            return redirect('signin');
        }
        $auth = new UsersModel();
        $authUser = $auth->findOrCreateUser($user);

        if($authUser == 'guide'){
            session()->flash('flash_error','This email registered by guide account.');
            return redirect('signin');
        }
        if($authUser->status=='blocked'){
            session()->flash('flash_block_signin', trans('site.login_fail'));
            return redirect('signin');
        }
        $user['user'] = $authUser;
        session()->put('users', $user);
        if(session()->has('id_guide_continue')){
            return redirect('profile/'.session('id_guide_continue'));
        }else{
            return redirect('edit/edit-profile');
        }
    }

    /** google login redirection
     * @return \Illuminate\Http\RedirectResponse
     */
    public function googleProvider()
    {
        if ($this->loggedInStatus()) return redirect()->back();
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from google.
     *
     * @return Response
     */
    public function googleCallback(Request $r)
    {
        if($r->error)
            return redirect(Helper::url('signin'));
        if ($this->loggedInStatus()) return redirect()->back();
        $user = Socialite::driver('google')->user();
        if (!$user) return redirect(url());
        if($user->email===NULL){
            session()->flash('flash_error',"We can't get your email! Please try again!");
            return redirect('signin');
        }
        $auth = new UsersModel();
        $authUser = $auth->findOrCreateUser($user, 'google_id');
        if($authUser == 'guide'){
            session()->flash('flash_error','This email registered by guide account.');
            return redirect('signin');
        }
        if($authUser->status=='blocked'){
            session()->flash('flash_block_signin', trans('site.login_fail'));
            return redirect('signin');
        }
        $user['user'] = $authUser;
        session()->put('users', $user);
        if(session()->has('id_guide_continue')){
            return redirect('profile/'.session('id_guide_continue'));
        }else{
            return redirect('edit/edit-profile');
        }
    }

    /** twitter login redirection
     * @return \Illuminate\Http\RedirectResponse
     */
    public function twitterProvider()
    {
        if ($this->loggedInStatus()) return redirect()->back();
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from twitter.
     *
     * @return Response
     */
    public function twitterCallback(Request $r)
    {
        $input=Input::all();
        if(!empty($input['denied'])){
            return redirect(Helper::url('signin'));
        }
        if($r->error)
            return redirect(Helper::url('signin'));
        if ($this->loggedInStatus()) return redirect()->back();
        $user = Socialite::driver('twitter')->user();
        if (!$user) return redirect(url());
        if($user->email===NULL){
            session()->flash('flash_error',"We can't get your email! Please try again!");
            return redirect('signin');
        }
        $auth = new UsersModel();
        $authUser = $auth->findOrCreateUser($user, 'twitter_id');
        if($authUser == 'guide'){
            session()->flash('flash_error','This email registered by guide account.');
            return redirect('signin');
        }
        if($authUser->status=='blocked'){
            session()->flash('flash_block_signin', trans('site.login_fail'));
            return redirect('signin');
        }
        $user['user'] = $authUser;
        session()->put('users', $user);
        if(session()->has('id_guide_continue')){
            return redirect('profile/'.session('id_guide_continue'));
        }else{
            return redirect('edit/edit-profile');
        }
    }

    function sendMailActive(Request $r)
    {
        if (!request()->ajax())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $email = $r->input('email');
        $user = User::where('email', '=', $email)->select('id', 'email', 'full_name')->first();
        if ($user) {
            UsersModel::sendMailActive($user);
            session()->flash('flash_success', trans('site.send_mail_success'));
            return Helper::jsondata(true, trans('site.send_mail_success'));
        }
        return Helper::jsondata(false, 'This email is not registered. Please enter another email');
    }

    /**
     * reset password when forgotten
     * @param $id
     * @param $password
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function activeAccount($id, $password)
    {
        $id = base64_decode($id);
        $user = UsersModel::getUserById($id);
        if (!$user) {
            session()->flash('flash_error', 'This account has not been registered');
            return redirect('signup');
        }
        if($user->status == 'blocked'){
            session()->flash('flash_error', 'Your account has been locked. Please contact us for more information.');
            return redirect('contact');
        }
        if ($user->status != 'inactive') {
            session()->flash('flash_error', 'This account has actually been activated. Please login to continue');
            return redirect('signin');
        }
        $user->status = 'active';
        if ($user->save()) {
            if($user->hasRole('guide')){
                $notifi = [
                    'type' => 'approve',
                    'user_id' => User::select('id')->first()->id,
                    'is_read' => 0,
                    'sender_id' => $user->id,
                ];
                $systemModel = new SystemModel();
                $systemModel->insertNotify($notifi);
                $guideModel=new GuidesModel();
                $missing = $guideModel->isMissingInformation($user);
                session()->put('missingInfor', $missing);
            }
            UsersModel::storeSession($user, $user->type);
            session()->flash('flash_success', 'The email has been activated successfully ');
            return redirect('edit/edit-profile');
        }else{
            session()->flash('flash_error', 'Activate failed! Please try again later');
            return redirect('signin');
        }
    }
}
