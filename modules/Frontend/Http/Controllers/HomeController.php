<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/15/2017
 * Time: 3:21 PM
 */

namespace Modules\Frontend\Http\Controllers;


use Caffeinated\Themes\Facades\Theme;
use Helper;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Frontend\Models\GuideInfoModel;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\TravelerModel;
use Modules\Frontend\Models\TravelerRatingModel;
use Modules\Frontend\Models\UsersModel;
use Modules\Frontend\Requests\Contact;
use Modules\User\Entities\BookingDatesEntity;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\BookingMessagesEntity;
use Modules\User\Entities\CityEntity;
use Modules\User\Entities\GuideServicesEntity;
use Modules\User\Entities\NotificationEntity;
use Modules\User\Entities\TravellerBookingServiceEntity;
use Modules\User\Entities\GuideEntity;

class HomeController extends BaseController
{
    private $userModel;
    private $count_Star;
    private $limitBooking;
    private $limitGuide;
    private $limitNews;
    private $limitNotify;

    public function __construct()
    {
        parent::__construct();
        /*Theme::setActive('bootstrap');*/
        Theme::setLayout('frontend.layouts.master');
        $this->userModel = new UsersModel();
        $this->count_Star = new GuideInfoModel();
        $this->limitBooking = 50;
        $this->limitGuide = 24;
        $this->limitNews = 30;
        $this->limitNotify = 50;
    }

    /**
     * show homepage
     * @return mixed
     */
    function index()
    {
        /*get info best guide slider*/
        $user = $this->loggedInStatus();
        $guide = $this->count_Star->getGuide(16, ['star', 'DESC']);
        $systemModel = new SystemModel();
        $country = UsersModel::getCountry(null, ['country_id', 'country_name']);
        $cities = UsersModel::getCity(140);
        $languageWorld = SystemModel::getLanguagesWorld();
        $data = [
            'countries' => $country,
            'cities' => $cities,
            'guide' => $guide,
            'user' => $user,
            'news' => $systemModel->getNews()['news'],
            'countNews' => $systemModel->getNews()['count'],
            'countGuides' => GuideInfoModel::countGuide(),
            'countTraveler' => GuideInfoModel::countTraveler(),
            'langWorlds' => $languageWorld
        ];
        return Theme::view('frontend.index', $data);
    }

    function set_star(){
        $list=BookingEntity::all();
        foreach($list as $item){
            $avgStar = round(BookingEntity::where('guide_id', '=', $item->guide_id)->avg('ratings'));
            $guide = GuideEntity::where('user_id', '=', $item->guide_id)->first();
            if ($avgStar)
                $guide->star = $avgStar;
            $guide->save();
        }
    }

    function set_hour(){
        $list=BookingDatesEntity::all();
        foreach($list as $date){
            $hours=explode(',',$date->hours);
            $edithours=[];
            foreach($hours as $hour){
                array_push($edithours,$hour.':00:00');
            }
            $date->hours=implode(',',$edithours);
            $date->save();
        }
    }

    function about()
    {
        return Theme::view('frontend.about');
    }

    function privacyPolicy()
    {
        return Theme::view('frontend.privacypolicy');
    }

    function termsOfService()
    {
        return Theme::view('frontend.termofservices');
    }

    function bookinginfomation($id = null)
    {
        if (!$user = $this->loggedInStatus())
            return redirect('');
        $guideModel = new GuidesModel();
        $detaiBooking = $guideModel->detailBooking($id);
        if($detaiBooking==NULL){
            return redirect('edit/booking');
        }
        if($user->id!=$detaiBooking->guide_id&&$user->id!=$detaiBooking->traveler_id){
            return redirect('edit/booking');
        }
        $message = '';
        if ($detaiBooking)
            $message = $detaiBooking->messageBooking()->orderBy('created_at', 'desc')->limit(500)->get();
        $data = [
            'detail' => $detaiBooking,
            'user' => $user,
            'messages' => $message
        ];
        if($user->id==$detaiBooking->guide_id){
            $detaiBooking->is_read = 1;
            $detaiBooking->save();
        }
        $user->bookingMessages()->where('booking_id', '=', $id)->where('receiver_id', '=', $user->id)->where('is_read', '=', 0)->update(['is_read' => 1]);
        return Theme::view('frontend.booking.bookingInfomation', $data);
    }

    function login()
    {
        return Theme::view('frontend.user.login');
    }

    function signup()
    {
        if ($this->loggedInStatus())
            return redirect('');
        \Assets::addJs([
            Helper::getThemeJs('login.js?v=' . Helper::version()),
        ]);
        $islogin = 1;
        $data = [ 'islogin' => $islogin ];
        return Theme::view('frontend.user.signup',$data);
    }

    function forgotPassword()
    {
        \Assets::addJs([
            Helper::getThemeJs('login.js?v=' . Helper::version()),
        ]);
        $islogin = 1;
        $data = [ 'islogin' => $islogin ];
        return Theme::view('frontend.user.forgotPass',$data);
    }

    function activeAccount()
    {
        \Assets::addJs([
            Helper::getThemeJs('login.js?v=' . Helper::version()),
        ]);
        $islogin = 1;
        $data = [ 'islogin' => $islogin ];
        return Theme::view('frontend.user.active_account',$data);
    }

    /**
     * edit profile of user both guide and user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function editUser($filter = null)
    {
        if ($filter != 'pending' && $filter != 'accepted' && $filter != 'canceled' && $filter != 'declined' && $filter != 'finished')
            $filter = null;
        if (!$user = $this->loggedInStatus())
            return redirect('');
        $country = UsersModel::getCountry(null, ['country_id', 'country_name']);
        $type = $user->type == 'guide' ? 'guide_id' : 'traveler_id';
        $guideModel = new GuidesModel();
        $countBooking = $guideModel->countBooking($user->id, $type, $filter);
        $total_page = $countBooking / $this->limitBooking;
        $languageWorld = SystemModel::getLanguagesWorld();
        $systemModel = new SystemModel();
        $notifications = $systemModel->notifications($user->id, [], $this->limitNotify);
        // $discount = $this->userModel->getAllDiscount($guide_id);
        $data = [
            'user' => $user,
            'countries' => $country,
            'bookings' => $guideModel->getBooking($user->id, $type, [], $this->limitBooking, 0, $filter),
            'flag_show' => $total_page - 1 > 0,
            'languagesWorld' => $languageWorld,
            'notifications' => $notifications['notify'],
            'flag_show_notify' => $notifications['count'] / $this->limitNotify > 1,
            'filter' => $filter
        ];
        if ($user->hasRole('guide'))
            $data = array_merge($this->valueGuide($user), $data);
        if ($user->hasRole('user'))
            $data = array_merge($this->valueTraveller($user), $data);
        return Theme::view('frontend.user.editguide', $data);
    }

    function valueGuide($user = null, $type = null)
    {

        $data = [];
        if ($user->hasRole('guide')) {
            $guide = $user->guide()->select('experience', 'id', 'cover', 'diploma_info', 'star', 'introduce', 'idnumber', 'hourly_rate')->first();
            $guideModel = new GuidesModel();
            $services = $guideModel->getservice(['type' => 'service']);
            $transportation = $guideModel->getservice(['type' => 'transportation']);
            $data = [
                'services' => $services,
                'transportations' => $transportation,
                'guide' => $guide,
                'certification' => $user->certification()->select('id', 'idcard', 'expiry_date', 'place_of_issue', 'type', 'cert_lang','location_allowed')->first(),
                'missingProfile' => session()->has('missingInfor') && session('missingInfor.missing_profile'),
                'missingJobInfo' => session()->has('missingInfor') && session('missingInfor.missing_jobInfo'),
            ];

            if ($guide) {
                $guide_id = $guide->id;
                $discount = $this->userModel->getAllDiscount($guide_id);
                $guide_locations = $guide->locationTour()->get();
                $data['guide_locations'] = $guide_locations;
                $data['discounts'] = json_encode($guideModel->getDiscount($discount));
                $data['guideServices'] = $guideModel->guideServices($guide->id,'service');
                $data['guidetransportations'] = $guideModel->guideServices($guide->id,'transportation');

                $schedules = $guideModel->getSchedule($guide_id);

                $data['schedules'] = $guideModel->hoursExplodeSchedule($schedules);
                if ($type != 'profile') {
                    $knownLanguage = $guide->known_languages()->select(['language_id'])->get();
                    $data['knownLanguage'] = [];
                    if ($knownLanguage)
                        foreach ($knownLanguage as $kn) {
                            array_push($data['knownLanguage'], $kn->language_id);
                        }
                }
            }
        }
        return $data;
    }

    function valueTraveller($user = null)
    {
        $data = [];
        if ($user->hasRole('user')) {
            $guide = $user->traveller()->first();
            $data = [
                'guide' => $guide,
                'user' => $user
            ];
        }
        return $data;
    }

    function contact()
    {
        return Theme::view('frontend.contact');
    }

    /**
     * show page news
     * @return mixed
     */
    function news()
    {
        $systemModel = new SystemModel();
        $news = $systemModel->news(['city_id', 'created_at', 'country_id', 'id', 'user_id', 'url'], $this->limitNews);
        $data = [
            'news' => $news['guides'],
            'flag_show' => $news['count'] / $this->limitNews > 1
        ];
        return Theme::view('frontend.news.allNews', $data);
    }

    /**
     * lazy load news' system
     * @param Request $r
     * @return mixed
     */
    function ajaxNews(Request $r)
    {
        $systemModel = new SystemModel();
        $page = $r->input('page');
        $news = $systemModel->news(['city_id', 'created_at', 'country_id', 'id', 'user_id', 'url'], $this->limitNews, $page);
        $data = [
            'news' => $news['guides'],
            'flag_show' => $news['count'] / $this->limitNews - 1 > $page,
            'page' => ++$page
        ];
        $html = '';
        if (count($news['guides']))
            foreach ($news['guides'] as $news) {
                $html .= Theme::view('frontend.news.theNews', ['news' => $news])->render();
            }
        $data['html'] = $html;
        return Helper::jsondata(true, '', $data);
    }

    /**
     * show page detail news
     * @return mixed
     */
    function detailnews($id)
    {
        $system = new SystemModel();
        $detail = $system->detailNews($id);
        $city_id = null;
        if ($detail && $detail->city_id){
            $city_id = $detail->city_id;

            $data = [
                'news' => $detail,
                'populars' => $system->randPopularNews($id, 3),
                'recents' => $system->randPopularNews($id, 5),
                'suggest' => $system->suggestfriend($city_id, $detail->country_id)
            ];

            if ($leng = count(session('news')) > 6)
                foreach (session('news') as $key => $value) {
                    session()->forget('news.' . $key);
                    break;
                }
            session()->put('news.' . $id, $detail);
            return Theme::view('frontend.news.detailNews', $data);
        }
        return Theme::view('frontend.news.detailNews');
    }

    function edittraveller()
    {
        if (!$user = $this->loggedInStatus())
            return redirect('');
        $data = [
            'user' => $user
        ];
        return Theme::view('frontend.user.edittraveller', $data);
    }

    /**
     * show detail profile of guide
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function profile($id)
    {

        $traveler = $this->loggedInStatus();
        $user = UsersModel::getUserById($id, ['full_name', 'avatar', 'id', 'birth_date', 'phone_number', 'slogan', 'hobby']);
        if($user!=NULL){
            if (!$user->hasRole('guide')) {
                session()->flash('flash_error', trans('admin.the_link_is_not_correct'));
                return redirect('');
            }
            $guide = $user->guide()->select('experience', 'id', 'cover', 'diploma_info', 'star', 'introduce', 'idnumber', 'hourly_rate')->first();
            if (!$guide) {
                session()->flash('flash_error', trans('admin.the_guide_does_not_find'));
                return redirect('');
            }
            session()->put('id_guide_continue',$id);
            //auto choose city
            if(session('flash_search_city')){
                $search_city=session('flash_search_city');
                session()->flash('flash_search_city',$search_city);
            }
            $data = $this->valueGuide($user, 'profile');
            $knownLanguage = $guide->known_languages()->select(['language_id'])->get();
            $data['knownLanguage'] = $knownLanguage;
            $data['user'] = $user;
            $data['galeries'] = $guide->galleries()->get();
            $travelModel = new TravelerModel();
            $data['ratings'] = $travelModel->getRating($id);
            $travelerModel = new TravelerModel();
            if ($traveler)
                $checkBooking = $travelerModel->checkBooking($traveler->id, $id);
            else
                $checkBooking = false;
            $data['checkBooking'] = $checkBooking;
            $data['owner'] = $traveler;
            $data['empty_user']=0;

            return Theme::view('frontend.user.profile', $data);
        }
        $data['empty_user']=1;
        return Theme::view('frontend.user.profile',$data);
    }

    /**
     * show booking of guide
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function bookGuide($id)
    {
        if (!$user = $this->loggedInStatus()) {
//            session()->flash('flash_error', trans('admin.please_login_before_booking'));
            session()->put('id_guide_continue',$id);
            return redirect('signin');
        }
        if (!$user->hasRole('user') || (!$userGuide = UsersModel::getUserById($id))) {
            session()->flash('flash_error', trans('admin.the_link_is_not_correct'));
            return redirect('');
        }
        $guide = $userGuide->guide()->first();


        if (!$guide)
            return Helper::jsondata(false, trans('admin.the_guide_is_invalid'));

        $schedules = $guide->schedules()->get();
        $guideModel = new GuidesModel();

        $sch = [];
        if ($schedules)
            foreach ($schedules as $schedule) {
                $sch[$schedule->day_of_week] = explode(',', $schedule->hours);
            }
        $fields = Helper::daysOfWeek();
        $fields = Helper::convertArrayEmpty($fields);
        $fields = array_merge($fields, $sch);

        $discount = $this->userModel->getAllDiscount($guide->id);

        $data = [
            'guide' => $guide,
            'schedule' => json_encode($fields),
            'userGuide' => $userGuide,
            'discounts' => json_encode($guideModel->getDiscount($discount)),
            'user' => $user,
        ];
        if(session('flash_search_city')){
            $data['search_city']=session('flash_search_city');
        }
        return Theme::view('frontend.user.bookGuide', $data);
    }

    /**
     * display view find guide for user
     * @return mixed
     */
    function findGuide()
    {
        $country = UsersModel::getCountry(null, ['country_id', 'country_name']);
        $cities = UsersModel::getCity(140);
        $languageWorld = SystemModel::getLanguagesWorld();
        $guideModel = new GuidesModel();
        $services = $guideModel->getservice(['type' => 'service']);
        $transportation = $guideModel->getservice(['type' => 'transportation']);
        $data = [
            'countries' => $country,
            'cities' => $cities,
            'languagesWorld' => $languageWorld,
            'services' => $services,
            'transportations' => $transportation
        ];
        return Theme::view('frontend.findguide', $data);
    }

    /**
     * send content message of contact for email Administrator
     * @param Contact $r
     */
    function sendContact(Contact $r)
    {
        if (!request()->ajax())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $info = '<div><span>Full name:</span>' . $request['full_name'] . '</div>';
        $info .= '<div><span>Phone number:</span>' . $request['phone_number'] . '</div>';
        $info .= '<div><span>Email:</span>' . $request['email'] . '</div>';
        $content = self::htmlEmail($info);
        $content .= self::htmlEmail('Message: ' . $request['message']);
        $guideModel = new GuidesModel();
        $guideModel->sendmail(env('MAIL_USERNAME'), $content, 'Administrator');
        session()->flash('flash_success', trans('site.send_mail_success'));
        return Helper::jsondata(true, trans('site.send_mail_success'));
    }

    /**
     * generate content html mail
     * @param $st
     * @return string
     */
    static function htmlEmail($st)
    {
        $content = '<tr>
                    <td style="width:40px"></td>
                    <td style="font-family:\'Lucida Grande\',Helvetica,Arial,sans-serif;line-height:25px;padding-top:30px;text-align:left;font-size:14px;color:#333">';
        $content .= $st;
        $content .= '</td>
                    <td style="width:40px"></td>
                   </tr>';
        return $content;
    }

    /**
     * load show more booking
     * @param Request $r
     * @return mixed
     */
    function ajaxGetBooking(Request $r)
    {
        if (request()->ajax() && Input::has('page')) {
            $user = $this->loggedInStatus();
            if (!$user) {
                return Helper::jsondata(false, trans('admin.no_access_privilege'));
            }
            $request = $r->all();
            $page = $request['page'];
            $guideModel = new GuidesModel();
            $type = $user->type == 'guide' ? 'guide_id' : 'traveler_id';
            $countBooking = $guideModel->countBooking($user->id, $type);
            $bookings = $guideModel->getBooking($user->id, $type, [], $this->limitBooking, $page);
            $total_page = $countBooking / $this->limitBooking;
            $html = '';
            $data = [
                'isGuide' => $user->type == 'guide' ? true : false,
                'guideArray' => [],
                'total_count' => 0,
                'li' => '',
                'total_dates' => 0
            ];
            if (count($bookings))
                foreach ($bookings as $booking) {
                    $data['booking'] = $booking;
                    $html .= Theme::view('frontend.booking.detailBooking', $data)->render();
                }
            return Helper::jsondata(true, '', ['html' => $html, 'flag_show' => ($page < $total_page - 1), 'page' => ++$page]);
        }
    }

    /**
     * load show more booking
     * @param Request $r
     * @return mixed
     */
    function ajaxNotifications(Request $r)
    {
        if (request()->ajax() && Input::has('page')) {
            $user = $this->loggedInStatus();
            if (!$user) {
                return Helper::jsondata(false, trans('admin.no_access_privilege'));
            }
            $request = $r->all();
            $page = $request['page'];
            $systemModel = new SystemModel();
            $notifications = $systemModel->notifications($user->id, [], $this->limitNotify, $page);
            $html = '';
            $total_page = $notifications['count'] / $this->limitNotify;
            $data = [
                'user_notify' => [],
                'total_count' => $total_page
            ];
            if (count($notifications['notify'])) {
                foreach ($notifications['notify'] as $notify) {
                    $data['notify'] = $notify;
                    $html .= Theme::view('frontend.notifications.detail_notification', $data)->render();
                }
            }
            return Helper::jsondata(true, '', ['html' => $html, 'flag_show_notify' => ($page < $total_page - 1), 'page' => ++$page]);
        }
    }

    /**
     * show best guide. guide display sort star
     * @return mixed
     */
    function bestGuide()
    {
        $guideModel = new GuidesModel();
        $guide = $guideModel->getGuide(['id', 'star', 'user_id', 'hourly_rate'], $this->limitGuide);
        $total_page = $guide['count'] / $this->limitGuide;
        $data = [
            'guides' => $guide['guides'],
            'total_page' => $guide['count'],
            'flag_show' => ($total_page - 1 > 0),
            'title' => 'Guides'
        ];
        return Theme::view('frontend.guide.bestguide', $data);
    }

    /**
     * load show more best guide
     * @param Request $r
     * @return mixed
     */
    function ajaxGuide(Request $r)
    {
        $guideModel = new GuidesModel();
        $page = $r->input('page');
        $guide = $guideModel->getGuide(['id', 'star', 'user_id'], $this->limitGuide, $page);
        $total_page = $guide['count'] / $this->limitGuide;
        $data = [
            'guides' => $guide['guides'],
            'total_page' => $guide['count'],
            'flag_show' => ($page < $total_page - 1),
            'page' => ++$page
        ];
        $html = '';
        if ($data['guides'] && count($data['guides']))
            foreach ($data['guides'] as $guide) {
                $html .= Theme::view('frontend.guide.detailBestGuide', ['guide' => $guide])->render();
            }
        $data['html'] = $html;
        unset($data['guides']);
        return Helper::jsondata(true, '', $data);
    }

    /**
     * delete traveller
     * @param $id
     */
    function deleteHidden($id)
    {
        $user = UsersModel::getUserById($id);
        /*if ($user->hasRoles(['admin', 'manager']))
            die();*/
        //$user->roles()->delete();
        BookingMessagesEntity::where('user_id', $id)->orWhere('receiver_id')->delete();
        $booking = BookingEntity::where('traveler_id', $id)->orWhere('guide_id')->get();
        if ($booking)
            foreach ($booking as $bk) {
                $bk->datesBooking()->delete();
                $bk->bookingService()->delete();
                $bk->notifications()->delete();
                $bk->messageBooking()->delete();
                $bk->delete();
            }
        NotificationEntity::where('user_id',$id)->orWhere('sender_id',$id)->delete();
        $user->traveller()->delete();
        //$user->roles_users()->delete();
        $user->roles()->sync([]);
        $user->delete();
    }
}