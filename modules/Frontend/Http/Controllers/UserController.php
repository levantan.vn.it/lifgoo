<?php

namespace Modules\Frontend\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: THINHGLORY
 * Date: 3/16/2017
 * Time: 3:24 PM
 */

use App\Http\Requests;
use App\User;
use Carbon\Carbon;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\UsersModel;
use Modules\User\Entities\CityEntity;
use Modules\Frontend\Requests\EditJob;
use Modules\Frontend\Requests\EditProfile;
use Modules\Frontend\Requests\Login;
use Modules\User\Entities\BookingDatesEntity;
use Theme;
use Illuminate\Support\Facades\Input;

class UserController extends BaseController
{
    /**
     * display login page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogin()
    {
        if ($this->loggedInStatus())
            return redirect('');
        \Assets::addJs([
            Helper::getThemeJs('login.js?v='.Helper::version()),
        ]);
        if(session('flash_search_city')){
            $search_city=session('flash_search_city');
            session()->flash('flash_search_city',$search_city);
        }
        $islogin = 1;
        $data = [ 'islogin' => $islogin ];
        return Theme::view('frontend.user.login',$data);
    }

    /**
     * check login after store user into session
     * @param Login $r
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Login $r)
    {
        if ($this->loggedInStatus())
            return Helper::jsondata(false, trans('site.login_fail'));
        $request = $r->all();
        $email = $request['email'];
        $password = $request['password'];
        $users = new User();
        $guideModel = new GuidesModel();
        $user = $users->whereEmail($email)->orWhere('username', '=', $email)->first();
        if ($user&&!$user->can('admin_login')) {
            if($user->status == 'inactive'){
                session()->flash('flash_confirm_signin', trans('site.login_fail'));
                return Helper::jsondata(false, trans('site.login_fail'));
            }
            if($user->status == 'blocked'){
                session()->flash('flash_block_signin', trans('site.login_fail'));
                return Helper::jsondata(false, trans('site.login_fail'));
            }
            if (Hash::check($password, $user->password)) {
                unset($user['password']);
                unset($user['twitter_id']);
                unset($user['google_id']);
                unset($user['facebook_id']);
                if ($user->hasRole('guide')) {
                    $missing = $guideModel->isMissingInformation($user);
                    session()->put('missingInfor', $missing);
                }
                if(session('flash_search_city')){
                    $search_city=session('flash_search_city');
                    session()->flash('flash_search_city',$search_city);
                }
                UsersModel::storeSession($user, $user->type);
                return Helper::jsondata(true, '', ['type' => $user->type]);
            }
        }
        session()->flash('signin_fail', trans('site.login_fail'));
        return Helper::jsondata(false, trans('site.login_fail'));
    }

    /**
     * store profile of user include guide and traveller
     * @param EditProfile $r
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function patchProfile(EditProfile $r)
    {
        if (!$user = $this->loggedInStatus())
            return redirect('');
        if (!request()->ajax())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $user = UsersModel::getUserById($user->id);
        $request = $r->all();
        $fields = ["full_name", "phone_number", "address", "birth_date", "hobby", "gender", "slogan"];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        isset($request['email']) && $data['email'] = $request['email'];
        isset($request['username']) && $data['username'] = $request['username'];
        $profile = new UsersModel();
        $profile->patchProfile($data, $user);
        if ($profile) {
            if ($user->hasRole('guide')) {
                $fields = [
                    'introduce' => $request['introduce'],
                    'idnumber' => isset($request['passport_id_number']) ? $request['passport_id_number'] : '',
                ];
                $profile->patchGuideInfo($fields, $user->id);
            } else {
                $fields = [
                    'introduce' => $request['introduce'],
                    'language_code' => 'en'
                ];
                $profile->patchTravellerInfo($fields, $user->id);
            }
            if ($r->file('img')) {
                UsersModel::uploadAvatar($r->file('img'), $user);
            }
            /**
             * check Information's guide is missing
             */
            if ($user->hasRole('guide')) {
                $guideModel = new GuidesModel();
                $missing = $guideModel->isMissingInformation($user);
                session()->put('missingInfor', $missing);
            }
            session()->flash('flash_success', trans('site.edit_profile_success'));
            return Helper::jsondata(true, trans('site.edit_profile_success'));
        }
        return Helper::jsondata(false, trans('site.edit_profile_unsuccess'));
    }

    /**
     * change password of user
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function credential(Request $r)
    {
        if (!$user = $this->loggedInStatus())
            return redirect('');
        if (!request()->ajax())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $validator = Validator::make($request, [
            'new_password' => 'required|min:6;',
            'cfr_password' => 'required|same:new_password'
        ]);
        if ($validator->fails()) {
            return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
        } else {
            $userCurrent = UsersModel::getUserById($user->id);
            if (Hash::check($request['old_password'], $userCurrent->password) || (($userCurrent->facebook_id || $userCurrent->google_id || $user->twitter_id) && !$userCurrent->password)) {
                $userCurrent->password = Hash::make($request['new_password']);
                $userCurrent->save();
                session()->flash('flash_success', trans('site.edit_password_success'));
                return Helper::jsondata(true, trans('site.edit_password_success'));
            }
        }
        return Helper::jsondata(false, trans('site.Please_enter_correct_current_password'));
    }

    /**
     * gg
     * edit job information of guide include
     * store certification
     * store location
     * store guide information include knowledge language,experience,diploma info
     * store services of guide
     * @param EditJob $r
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function editJobInfo(EditJob $r)
    {
        if (!$user = $this->loggedInStatus())
            return redirect('');
        if (!request()->ajax() || !$user->hasRole('guide'))
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $guideModel = new GuidesModel();
        $guide = $user->guide()->select('id')->first();
        if ($guide) {
            /**
             * edit certification
             */
            $fields = ["idcard", "expiry_date", "place_of_issue", "type", "status","location_allowed"];
            $fields = array_flip($fields);
            $cerarr = array_intersect_key($request, $fields);
            $cerarr['user_id'] = $user->id;
            $cerarr['user_type'] = 'guide';
            if(isset($request['cert_lang']))
                $cerarr['cert_lang'] = implode(",", $request['cert_lang']);
            else
                $cerarr['cert_lang']='';
            $guideModel->storeCertification($cerarr);
            /**
             * store location of guide
             */
            $guideModel->deleteLocation($guide);
            if ($request['country_id'])
                foreach ($request['country_id'] as $key => $item) {
                    $location = [
//                        'country_id' => $item,
                        'city_id' => isset($request['city_id'][$key]) ? $request['city_id'][$key] : '',
                        'guide_id' => $guide->id
                    ];
                    $guideModel->storeLocationTour($location);
                }
            /**
             * edit guide information
             */
            $fields = ["diploma_info", "experience", "hourly_rate"];
            $fields = array_flip($fields);
            $info = array_intersect_key($request, $fields);
            $profile = new UsersModel();
            $profile->patchGuideInfo($info, $user->id);
            $systemModel = new SystemModel();
            if ($languages = $request['languages'])
                $guide->known_languages()->delete();
            foreach ($languages as $langs) {
                $systemModel->storeKnownLanguage(['guide_id' => $guide->id, 'language_id' => $langs]);
            }
            /**
             * store service of guide
             * delete all service of guide before insert new service
             */
            $guide->services()->delete();
            $service = [
                'guide_id' => $guide->id,
//                'type' => 'service',
            ];
            if (isset($request['service_id']) && $ids = $request['service_id']) {
                foreach ($ids as $id) {
                    $service['service_id'] = $id;
                    SystemModel::storeService($service);
                }
            }

            if (isset($request['transportation_id']) && $ids = $request['transportation_id']) {
                foreach ($ids as $id) {
                    $service['service_id'] = $id;
//                    $service['type'] = 'transportation';
                    SystemModel::storeService($service);
                }
            }
            /**
             * check Information's guide is missing
             */

            $guideModel = new GuidesModel();
            $missing = $guideModel->isMissingInformation($user);
            session()->put('missingInfor', $missing);

            session()->flash('flash_success', trans('site.edit_price_success'));
            return Helper::jsondata(true, trans('site.edit_price_success'));
        }
        return Helper::jsondata(false, trans('site.edit_profile_unsuccess'));
    }

    /**
     * change price of guide (promotion)
     * @param Request $r
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function editPrice(Request $r)
    {
        if (!$user = $this->loggedInStatus())
            return redirect('');
        if (!$r->ajax())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $request = $r->all();
        $guide_id = $user->guide()->select('id')->first()->id;
        $userModel = new UsersModel();
        $userModel->removeDiscount($guide_id);
        if ($request) {
            foreach ($request as $capacity => $item) {
                if ($item){
                    foreach ($item as $hour => $price) {
                        $data = [
                            'guide_id' => $guide_id,
                            'number_people' => $capacity,
                            'time' => $hour,
                            'price' => $price
                        ];
                        $userModel->storeGuideDiscount($data);
                    }
                }
            }
        }
        session()->flash('flash_success', trans('site.edit_price_success'));
        return Helper::jsondata(true, trans('site.edit_price_success'));
    }

    /**
     * update is_read of notification and booking
     * @param Request $r
     * @return mixed
     */
    function updateClick(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user || !$r->ajax()) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $user->notifications()->where('is_click','=',0)->update(['is_click' => '1']);
        return Helper::jsondata(true);
    }

    /**
     * update unread notification and booking
     * @param Request $r
     * @return mixed
     */
    function is_read(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user || !$r->ajax()) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $type = $r->input('type');
        if ($type == 'bell')
            $user->notifications()->where('id', '=', $r->input('notify_id'))->update(['is_click' => 1, 'is_read' => 1]);
        if ($user->hasRole('guide') && $type == 'cart')
            $user->bookings()->where('id', '=', $r->input('notify_id'))->update(['is_read' => 1]);
        /*if ($type == 'message')
            $user->bookingMessages()->where('booking_id', '=', $r->input('notify_id'))->where('receiver_id', '=', $user->id)->where('is_read','=',0)->update(['is_read' => 1]);*/
        return Helper::jsondata(true);
    }

    /**
     * delete muitiple item notification
     * @param Request $r
     * @return mixed
     */
    function deleteNotify(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user || !$r->ajax()) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $ids = $r->input('notify_ids');
        if (count($ids)) {
            foreach ($ids as $value)
                $user->notifications()->where('id', '=', $value)->delete();
        }
        return Helper::jsondata(true);
    }

    /**
     * count all notification new message, new cart, new notification
     * @param Request $r
     * @return mixed
     */
    function intervalNotify(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::jsondata(false);
        }
        $request = $r->all();
        $count = $user->notifications()->where('is_click', '=', 0)->count();
        $countMessage = $user->countMessage($user->id);
        $htmlBell = '';
        $data = [
            'countBell'=>$count,
            'countMessage'=>count($countMessage)
        ];
        if ($count != $request['bell']) {
            $box_notify = $user->notifications()->limit(50)->orderBy('created_at', 'desc')->get();
            if (count($box_notify))
                foreach ($box_notify as $no) {
                    $htmlBell .= Theme::view('frontend.notifications.box_notifcation', ['no' => $no, 'userGlobal_notify' => []])->render();
                }
        }
        if ($user->hasRole('guide')) {
            $countBooking = $user->bookings()->where('is_read', '=', 0)->count();
            $data['countCart'] = $countBooking;
        }
        $data['htmlBell'] = $htmlBell;
        return Helper::jsondata(true,'',$data);
    }

    /**
     * send mail reset password
     * @param Request $r
     * @return mixed
     */
    function forgotPassword(Request $r){
        if (!request()->ajax())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        $email = $r->input('email');
        $user = User::where('email','=',$email)->select('id','email','full_name')->first();
        if($user){
            $id = base64_encode($user->id);
            $new_pass = rand().time();
            $content = HomeController::htmlEmail('We received a request to reset your Lifgoo password.');
            $content .= HomeController::htmlEmail('<a href="'.Helper::url('reset/'.$id.'/'.$new_pass).'">Click here to change your password.</a>');
            $content .= HomeController::htmlEmail('the new password after clicking on the link is '.$new_pass);
            $guideModel = new GuidesModel();
            $guideModel->sendmail($email, $content, $user->full_name,'Reset Password Email!');
            session()->flash('flash_success', trans('site.send_mail_success'));
            return Helper::jsondata(true, trans('site.send_mail_success'));
        }
        return Helper::jsondata(false,'This email is not registered. Please enter another email');
    }

    /**
     * reset password when forgotten
     * @param $id
     * @param $password
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function resetPassword($id,$password){
        $id = base64_decode($id);
        $user = UsersModel::getUserById($id);
        if($user){
            $user->password = Hash::make($password);
            $user->save();
            session()->flash('flash_success', 'The new password was reset successfully');
        }else{
            session()->flash('flash_error', trans('site.error_occured'));
        }
        return redirect('signin');
    }

    /**
     * send mail remind and update status
     */
    function sendMailRemind(){
        /*$content = HomeController::htmlEmail('the new password after clicking on the link is ');
        $guideModel = new GuidesModel();
        $guideModel->sendmail('lethihongquy24@gmail.com', $content, 'quy');*/
        $today = Carbon::now();
        $tomorrow = $today->addDay();
        $tomorrow = date_format($tomorrow,'Y-m-d');
        $today = date_format($today,'Y-m-d');
        $dates = BookingDatesEntity::whereBetween('date',[$today,$tomorrow])->get();
        $guideModel = new GuidesModel();
        if($dates)
            foreach ($dates as $date){
                $booking = $date->booking()->first();
                $guide = $booking->guide()->select('email','full_name','id')->first();
                $travel = $booking->guide('traveler_id')->select('full_name')->first();
                $content = HomeController::htmlEmail('This email is a reminder');
                $guideModel->sendmail($guide->email, $content, $guide->full_name);
            }
    }
}