<?php

namespace Modules\Frontend\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: THINHGLORY
 * Date: 3/16/2017
 * Time: 3:24 PM
 */

use App\Http\Requests;
use App\Mail\MailBooking;
use App\User;
use DateTime,DateTimeZone;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\TravelerModel;
use Modules\Frontend\Models\UsersModel;
use Modules\Frontend\Requests\Booking;
use Modules\Frontend\Requests\EditJob;
use Modules\Frontend\Requests\EditProfile;
use Modules\Frontend\Requests\FindGuide;
use Modules\Frontend\Requests\Login;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\CityEntity;
use Modules\User\Entities\GalleryEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\SucriberEntity;
use Theme;

class GuideController extends BaseController
{
    private $limitGuide;

    public function __construct()
    {
        parent::__construct();
        /*Theme::setActive('bootstrap');*/
        Theme::setLayout('frontend.layouts.master');
        $this->limitGuide = 30;
    }

    /**
     * traveler book guide
     * request include time, capacity, email, phone number, message of traveler
     * @param Booking $r
     * @return \Illuminate\Http\`Response|\Illuminate\Routing\Redirector
     */
    function addBooking(Booking $r)
    {
        if (!$user = $this->loggedInStatus())
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        if (!request()->ajax() || !$user->hasRole('user'))
            return Helper::jsondata(false, trans('admin.no_access_privilege'));

        $request = $r->all();
        $guide = UsersModel::getUserById($request['guide_id']);
        if (!$guide && $guide->hasRole('guide'))
            return Helper::jsondata(false, trans('admin.the_guide_is_invalid'));
        $fields = ["guide_id", "phone_number", "capacity", "price", "email",'pickup_place','destination','city_id'];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $data['traveler_id'] = $user->id;
        $guideModel = new GuidesModel();
        //save booking
        $booking = $guideModel->storeBooking($data);
        if (!$booking)
            return Helper::jsondata(false, trans('site.error_occured'));
        // save hours of booking
        $userBooking = $booking->guide()->select('full_name', 'email')->first();
        if(isset($request['city_id'])){
            $city=CityEntity::where('city_id',$request['city_id'])->first();
        }
        if ($request['hours_booking']&&(isset($city)&&$city!=NULL)) {
            $total_hour = 0;
            $total_dates = 0;
            $dataDateHours=[];
            foreach ($request['hours_booking'] as $key => $value) {
                $total_hour += count($value);
                $total_dates++;
                foreach($value as $hour){
                    $time=strtotime($key.' '.$hour.':00:00 '.$city->timezone);
                    if(!isset($dataDateHours[date('Y-m-d',$time)])||!is_array($dataDateHours[date('Y-m-d',$time)])){
                        $dataDateHours[date('Y-m-d',$time)]=[];
                    }
                    array_push($dataDateHours[date('Y-m-d',$time)],date('H:i:s',$time));
                }
            }

            foreach ($dataDateHours as $key => $value) {
                $data_hours = [
                    'booking_id' => $booking->id,
                    'date' => $key,
                    'hours' => implode(',', $value)
                ];
                $guideModel->storeHoursBooking($data_hours);
            }

            $content = HomeController::htmlEmail('You received a booking total ' . Helper::addPlural($total_hour, 'hour') . ' in ' . Helper::addPlural($total_dates, 'day') . ' from ' . $user->full_name . '. Please visit <a href="' . Helper::url('detail/'.$booking->id) . '">lifgoo.com</a> for more details');
            $guideModel = new GuidesModel();
            $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
        }
        if (isset($request['message']) && $request['message']) {
            $data_message = [
                'content' => $request['message'],
                'user_id' => $user->id,
                'booking_id' => $booking->id,
                'receiver_id' => $guide->id
            ];
            $guideModel->storeMessageBooking($data_message);
        }
        $notifi = [
            'type' => 'request_booking',
            'user_id' => $guide->id,
            'is_read' => 0,
            'sender_id' => $user->id,
            'booking_id' => $booking->id
        ];
        $systemModel = new SystemModel();
        $systemModel->insertNotify($notifi);
        return Helper::jsondata(true, trans('site.booking_success'),$booking->id);
    }


    function getGuideCities(){
        $input=Input::all();
        $guide=GuideEntity::where('id',$input['id'])->first();
        return Helper::jsondata(true,'',$guide->getAvailableCities($input['country_id']));
    }

    function getGuideBusyHours(){
        $input=Input::all();
        $guide=GuideEntity::find($input['guide_id']);
        $busyHours=$guide->getConvertBusyHours($input['city_id']);
        return Helper::jsondata(true,'',$busyHours);
    }

    /**
     *
     * @param Request $r
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function setSchedule(Request $r)
    {
        $user = $this->loggedInStatus();
        $guide = $user->guide()->select(['id'])->first();
        if (!$user || !$user->hasRole('guide') || !$guide) {
            session()->flash('flash_error', trans('admin.no_access_privilege'));
            return redirect('');
        }
        $request = $r->all();
        $guideModel = new GuidesModel();
        $guide->schedules()->delete();
        if ($guideModel->processSchedule($guide->id, $request)) {
            session()->flash('flash_success', trans('site.action_success'));
        } else
            session()->flash('flash_error', trans('site.error_occured'));
        return redirect('edit/setup-time');
    }

    /**
     * upload gallery: find and delete all images in folder gallery after insert new img into it
     * @param Request $r
     * @return mixed
     */
    function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}
    function uploadGallery(Request $r)
    {
        $user = $this->loggedInStatus();
        $guide = $user->guide()->select(['id'])->first();
        if (!$user || !$user->hasRole('guide') || !$guide) {
            return Helper::jsondata(false, trans('admin.the_guide_is_invalid'));
        }

        if ($r->hasFile('files')) {
            $input=($r->all());
            
//            get list file from input
            $array=explode(',',$input['fileuploader-list-files']);
            $list=[];
            foreach($array as $item){
                $item=$this->get_string_between($item,'0:/','"');
                array_push($list,$item);
            }
            
            
            /*$guide->galleries()->delete();*/
            $files = $r->file('files');
            /*$destinationPath = public_path() . Helper::getThemeGallery('gallery' . $user->id);*/
            /*if (is_dir($destinationPath))
                $this->removeAllFile($destinationPath);*/
            foreach ($files as $file) {
                //check file is in list upload or not
                if(in_array($file->getClientOriginalName(),$list)){
                    UsersModel::uploadGallery($file, $user->id, $guide->id);
                }
            }
            return Helper::jsondata(true, trans('site.upload_success'));
        }
        return Helper::jsondata(false, trans('site.no_file_selected'));
    }

    function deleteGallery($id)
    {
        $user = $this->loggedInStatus();
        $guide = $user->guide()->select(['id'])->first();
        if (!$user || !$user->hasRole('guide') || !$guide) {
            session()->flash('flash_error', trans('admin.the_guide_is_invalid'));
            return redirect('edit/gallery');
        }
        $gallery = GalleryEntity::find($id);
        if (!$gallery) {
            session()->flash('flash_error', 'The image does not exists');
            return redirect('edit/gallery');
        }
        $destinationPath = public_path() . Helper::getThemeGallery('gallery' . $user->id, $gallery->url);
        if (is_file($destinationPath)) {
            unlink($destinationPath);
        }
        if ($gallery->delete()) {
            session()->flash('flash_success', 'Delete successfully');
            return redirect('edit/gallery');
        }
        session()->flash('flash_error', trans('site.error_occured'));
        return redirect('edit/gallery');
    }

    /**
     * remove all file in folder
     * @param $directory
     */
    function removeAllFile($directory)
    {
        foreach (glob("{$directory}/*") as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    /**
     * update status of booking: pending, canceled, accepted, decline, finished
     * @param Request $r
     * @return mixed
     */
    function statusBooking2(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $request = $r->all();
        if (isset($request['booking_id']) && isset($request['status'])) {
            $booking = BookingEntity::find($request['booking_id']);
            if ($booking->status != $request['status']) {
                $booking->status = $request['status'];
                if ($request['status'] == 'canceled') {
                    $booking->is_read = 1;
                }
                if ($booking->save()) {
                    $notifi = [
                        'is_read' => 0,
                        'booking_id' => $booking->id,
                        'type' => Helper::typeNotify($request['status'])
                    ];
                    if ($request['status'] == 'canceled') {
                        $notifi['user_id'] = $booking->guide_id;
                        $notifi['sender_id'] = $booking->traveler_id;
                        $booking->readAllMessage();
                    } else {
                        $notifi['user_id'] = $booking->traveler_id;
                        $notifi['sender_id'] = $booking->guide_id;
                    }
                    $systemModel = new SystemModel();
                    $systemModel->insertNotify($notifi);
                    if ($request['status'] == 'declined') {

                        $receiver_id = $user->id == $booking->traveler_id ? $booking->guide_id : $booking->traveler_id;
                        $data = [
                            'user_id' => $user->id,
                            'booking_id' => $booking->id,
                            'content' => $request['message'],
                            'receiver_id' => $receiver_id,
                            'is_read' => 1
                        ];
                        $userBooking = $booking->guide('traveler_id')->select('email','full_name')->first();
                        $guideBooking = $booking->guide()->select('id', 'full_name')->first();
                        $content = HomeController::htmlEmail('<a href="' . Helper::url('profile/' . $guideBooking->id) . '">' . $guideBooking->full_name . '</a> declined your booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                        $guideModel = new GuidesModel();
                        $guideModel->storeMessageBooking($data);
                        $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);

                    }
                    if ($request['status'] == 'accepted') {
                        $userBooking = $booking->guide('traveler_id')->select('email','full_name')->first();
                        $guideBooking = $booking->guide()->select('id', 'full_name')->first();
                        $content = HomeController::htmlEmail('<a href="' . Helper::url('profile/' . $guideBooking->id) . '">' . $guideBooking->full_name . '</a> accepted your booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                        $guideModel = new GuidesModel();
                        $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
                    }
                }
            }
            session()->flash('flash_success', trans('site.action_success'));
            return Helper::jsondata(true, trans('site.upload_success'));
        }
        return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
    }

    function statusBooking(Request $r){
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $request = $r->all();
        if (isset($request['booking_id']) && isset($request['status'])) {
            $booking = BookingEntity::find($request['booking_id']);
            if($booking->status=='finished')
            {
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
            }
            if($booking->status=='declined')
            {
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
            }
            if($booking->status=='expired')
            {
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
            }

            $process=false;
            if ($booking->status != $request['status']) {

                if ($request['status'] == 'canceled') {
                    $booking->is_read = 1;
                    $booking->status = $request['status'];

                    if($booking->save()){
                        $userBooking = $booking->guide('traveler_id')->select('email','full_name','gender')->first();
                        if($userBooking->gender=='female')$gen='her'; else $gen='his';
                        $guideBooking = $booking->guide()->select('id', 'full_name','email')->first();
                        $content = HomeController::htmlEmail($userBooking->full_name . '</a> canceled '.$gen.' booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                        $guideModel = new GuidesModel();
                        $guideModel->sendmail($guideBooking->email, $content, $guideBooking->full_name);
                        $process=true;
                    }
                }
                if ($request['status']== 'accepted'){
                    if($booking->status=='pending'){
                        $booking->status = $request['status'];
                        if($booking->save()){
                            $userBooking = $booking->guide('traveler_id')->select('email','full_name')->first();
                            $guideBooking = $booking->guide()->select('id', 'full_name')->first();
                            $content = HomeController::htmlEmail('<a href="' . Helper::url('profile/' . $guideBooking->id) . '">' . $guideBooking->full_name . '</a> accepted your booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                            $guideModel = new GuidesModel();
                            $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
                            $process=true;
                        }
                    }else{
                        session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                        return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
                    }
                }

                if ($request['status']== 'declined'){
                    if($booking->status=='pending'){
                        $booking->status = $request['status'];
                        if($booking->save()) {
                            $receiver_id = $user->id == $booking->traveler_id ? $booking->guide_id : $booking->traveler_id;
                            $data = [
                                'user_id' => $user->id,
                                'booking_id' => $booking->id,
                                'content' => $request['message'],
                                'receiver_id' => $receiver_id,
                                'is_read' => 1
                            ];
                            $userBooking = $booking->guide('traveler_id')->select('email','full_name')->first();
                            $guideBooking = $booking->guide()->select('id', 'full_name')->first();
                            $content = HomeController::htmlEmail('<a href="' . Helper::url('profile/' . $guideBooking->id) . '">' . $guideBooking->full_name . '</a> declined your booking request. Please visit <a href="' . Helper::url('detail') . '/' . $request['booking_id'] . '">here </a> for more details');
                            $guideModel = new GuidesModel();
                            $guideModel->storeMessageBooking($data);
                            $guideModel->sendmail($userBooking->email, $content, $userBooking->full_name);
                            $process=true;
                        }
                    }else{
                        session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                        return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
                    }
                }

                if($process==true){
                    $notifi = [
                        'is_read' => 0,
                        'booking_id' => $booking->id,
                        'type' => Helper::typeNotify($request['status'])
                    ];
                    if ($request['status'] == 'canceled') {
                        $notifi['user_id'] = $booking->guide_id;
                        $notifi['sender_id'] = $booking->traveler_id;
                        $booking->readAllMessage();
                    } else {
                        $notifi['user_id'] = $booking->traveler_id;
                        $notifi['sender_id'] = $booking->guide_id;
                    }
                    $systemModel = new SystemModel();
                    $systemModel->insertNotify($notifi);
                    session()->flash('flash_success', trans('site.action_success'));
                    return Helper::jsondata(true, trans('site.upload_success'));
                }else{
                    session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                    return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
                }
            }else{
                session()->flash('flash_error', trans('admin.the_data_is_invalid'));
                return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
            }
        }
        else{
            session()->flash('flash_error', trans('admin.the_data_is_invalid'));
            return Helper::jsondata(false, trans('admin.the_data_is_invalid'));
        }
    }

    /**
     * insert message of booking
     * @param Request $r
     * @return mixed
     */
    function messageBooking(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        $request = $r->all();
        $booking = BookingEntity::find($request['booking_id']);
        if ($booking->status != 'accepted' && $booking->status != 'pending')
            return Helper::jsondata(false, 'Booking status has been changed. Please refresh the page to see information');
        $receiver_id = $user->id == $booking->traveler_id ? $booking->guide_id : $booking->traveler_id;
        $data = [
            'user_id' => $user->id,
            'booking_id' => $request['booking_id'],
            'content' => $request['message'],
            'receiver_id' => $receiver_id
        ];
        $guideModel = new GuidesModel();

        $result = $guideModel->storeMessageBooking($data);

        if ($result) {
            $result->type = $user->roles->first()->name;
            $result->avatar = $user->avatar;
            $result->full_name=$user->full_name;            
            return Helper::jsondata(true, trans('site.upload_success'), $result);
        }
        return Helper::jsondata(false, trans('site.error_occured'));

    }

    /**
     * search guide search based on the country_id from-to, the city_id from-to, the language_id, hourly_rate is in range, services array service_id, transportations array transportation_id, begin_date, end_date
     * @param Request $r
     */
    function findGuide(FindGuide $r)
    {
        $request = $r->all();
        if (Input::has('city_id_to')) {
            $request['city_id'] = $request['city_id_to'];
            $search_city=CityEntity::where('city_id',$request['city_id'])->first();
            session()->flash('flash_search_city',$search_city);
        }
        if (Input::has('language'))
            $request['language_id'] = $request['language'];
        if (Input::has('services') && Input::has('transportations'))
            $request['guide_services'] = array_merge($request['services'], $request['transportations']);
        if (Input::has('services') && !Input::has('transportations'))
            $request['guide_services'] = $request['services'];
        if (!Input::has('services') && Input::has('transportations'))
            $request['guide_services'] = $request['transportations'];
        if (Input::has('hourly_rate') && $hourly_rate = explode(';', $request['hourly_rate'])) {
            $request['rate_end'] = $hourly_rate[1];
            $request['rate_begin'] = $hourly_rate[0];
        }
        $page = $request['page'];
        $guideModel = new GuidesModel();
        $findGuide = $guideModel->findGuide($request, $this->limitGuide, $page);
        $html = '';
        $total_page = 0;
        if (isset($findGuide['guides']))
            foreach ($findGuide['guides'] as $guide) {
                $userJoin = $guide->user()->select('full_name', 'avatar', 'id')->first();
                $html .= Theme::view('frontend.guide.detailGuide', ['guide' => $guide, 'userJoin' => $userJoin])->render();
            }
        if (isset($findGuide['count']))
            $total_page = $findGuide['count'];
        return Helper::jsondata(true, '', ['html' => $html, 'flag_show' => ($request['page'] < $total_page - 1), 'page' => ++$request['page']]);
    }

    /**
     * store review of traveler
     * condiction to traeler review: traveler used to booked guide successfully
     * @param Request $r
     * @return mixed
     */
    function review(Request $r)
    {
        $user = $this->loggedInStatus();
        if (!$user) {
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        }
        if (!$user->hasRole('user')) {
            return Helper::jsondata(false, trans('admin.sorry.you_do_not_visitor'));
        }
        $request = $r->all();
        $travelerModel = new TravelerModel();
        $fields = ['review', 'ratings', 'booking_id'];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $travelerModel->storeReview($data);
        //$travelerModel->averageStar($request['guide_id']);
        return Helper::jsondata(true);

    }

    /**
     * update avatar and cover
     * @param Request $r
     * @return mixed
     */
    function avatar(Request $r)
    {
        if (!$user = $this->loggedInStatus())
            return Helper::jsondata(false, trans('admin.you_must_login'));
        if (!request()->ajax() || $user->id != $r->input('user_id'))
            return Helper::jsondata(false, trans('admin.no_access_privilege'));
        if ($r->file('img')) {
            $type = $r->input('type');
            if ($type == 'cover' && $user->hasRole('guide'))
                $user = $user->guide()->first();
            UsersModel::uploadAvatar($r->file('img'), $user, $type);
            session()->flash('flash_success', $type == 'avatar' ? trans('site.update_avatar_success') : trans('site.update_cover_success'));
            return Helper::jsondata(true, trans('site.update_avatar_success'));
        }
        return Helper::jsondata(false, trans('site.error_occured'));
    }

    /**
     * update slogan of guide
     */
    function slogan(Request $r)
    {
        if (!$user = $this->loggedInStatus())
            return Helper::jsondata(false, trans('admin.you_must_login'));
        if ($r->has('slogan') && $user->id == $r->input('user')) {
            unset($user['cover']);
            $user->slogan = $r->input('slogan');
            $user->save();
            session()->flash('flash_success', 'Update slogan successfully');
            return Helper::jsondata(true);
        }
        return Helper::jsondata(false, trans('site.error_occured'));
    }

    /**
     * search guide into homepage
     * @param Request $r
     * @return mixed
     */
    function searchGuideHome(Request $r)
    {
        $request = $r->all();
        $fields = ['country_id', 'language_id'];
        $fields = array_flip($fields);
        $data = array_intersect_key($request, $fields);
        $guideModel = new GuidesModel();
        $result = $guideModel->findGuiByDesLang($data, $this->limitGuide);
        $data = [
            'guides' => $result,
            'total_page' => 0,
            'flag_show' => false,
            'title' => 'Result find guide'
        ];
        return Theme::view('frontend.guide.bestguide', $data);
    }

    /**
     * add sucribe into db
     * @param Request $r
     * @return mixed
     */
    function subcribe(Request $r)
    {
        $email = $r->input('email');
        if (!SucriberEntity::where('email', '=', $email)->exists()) {
            $subribe = new SucriberEntity();
            $subribe->email = $email;
            $subribe->save();
        }
        session()->flash('flash_success', 'Add Subscribe success');
        return Redirect::back();
    }

    function missing()
    {
        $user = $this->loggedInStatus();
        $guideModel = new GuidesModel();
        $guideModel->isMissingInformation($user);
    }
}