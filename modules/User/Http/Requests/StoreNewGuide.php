<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'full_name' => 'required|max:100',
            'email' => 'unique:user__users,email|required|email|max:100',
            'personal_id' => 'required|max:20|unique:user__users,personal_id',
            'birth_day' => 'required|digits_between:1,2|max:31|min:1',
            'birth_month' => 'required|digits_between:1,2|max:12|min:1',
            'birth_year' => "required|digits:4",
            'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
            'cover' => 'file|image|max:8388608|dimensions:min_width=' . config('image.cover_width', 800) . ',min_height=' . config('image.cover_height', 200),
            'phone_number' => 'required|digits_between:8,20',
            'role_id' => 'required',
            'password' => 'required',
            'password-confirmation' => 'required|same:password'
        ];
    }
}
