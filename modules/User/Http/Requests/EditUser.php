<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'full_name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'personal_id' => 'required|max:20',
            'birth_day' => 'required|numeric|digits_between:1,2|max:31|min:1',
            'birth_month' => 'required|numeric|digits_between:1,2|max:12|min:1',
            'birth_year' => "required|digits:4|numeric",
            'phone_number' => 'required|digits_between:8,20',
            'avatar' => 'file|image|max:4194304|dimensions:min_width=' . config('image.avatar_width', 250) . ',min_height=' . config('image.avatar_width', 250),
            'cover' => 'file|image|max:8388608|dimensions:min_width=' . config('image.cover_width', 800) . ',min_height=' . config('image.cover_height', 200),
            'role_id' => 'filled',
            'old_password' => 'filled',
//            'password' => 'filled',
            'password_confirmation' => 'same:password',
        ];
    }
}
