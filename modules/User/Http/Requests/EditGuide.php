<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditGuide extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'user_id' => 'required|integer',
            'hourly_rate' => 'required|digits_between:1,11',
            'country_id' => 'required|integer',
            'city_id' => 'required|integer',
            'max_time' => 'digits_between:1,11',
            'promotional_media' => '',
            'experience' => '',
            'diploma_info' => 'max:255'
        ];
    }
}
