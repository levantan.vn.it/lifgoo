<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use App\User;
use Illuminate\Database\Eloquent\Model;

class NewsEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system__news';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'url','city_id','country_id','user_id'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    function countries(){
        return $this->belongsTo(new CountryEntity(),'country_id','country_id');
    }
    
    function cities()
    {
        return $this->belongsTo(new CityEntity(), 'city_id', 'city_id');
    }
    
    function users()
    {
        return $this->belongsTo(new User(), 'user_id', 'id');
    }
    
    function news_language(){
        return $this->hasMany(new NewsLanguageEntity(),'news_id','id');
    }
}