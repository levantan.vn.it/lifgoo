<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use Illuminate\Database\Eloquent\Model;

class ServiceEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system__services';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'service_name', 'icon', 'status','deleted_at'
    ];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    function serviceLang(){
        return $this->hasOne(new ServiceLanguageEntity(), 'service_id', 'id');
    }
    
    public function guide_service()
    {
        return $this->hasMany(new GuideServicesEntity(), 'service_id', 'id');
    }
    
}