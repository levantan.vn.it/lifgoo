<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 9:01 AM
 */

namespace Modules\User\Entities;
use Illuminate\Database\Eloquent\Model;



class RoleUserEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role_user';
    public $timestamps = false;
}