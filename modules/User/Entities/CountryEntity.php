<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use Illuminate\Database\Eloquent\Model;

class CountryEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system__countries';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id', 'country_name', 'short_country', 'continent_id','dial_code'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    protected $primaryKey = 'country_id';
    
    public function cities()
    {
        return $this->hasMany(new CityEntity(), 'country_id', 'country_id');
    }
}