<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use App\User;
use DB,DateTime,DateTimeZone;
use Illuminate\Database\Eloquent\Model;

class GuideEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'guide__info';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'location', 'hourly_rate', 'min_time','max_time','capacity','status','promotional_media','experience','cover','diploma_info','star','introduce'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(new User(), 'user_id');
    }

    public function discount(){
        return $this->hasMany(new GuideDiscountEntity(),'guide_id','id');
    }

    public function locationTour(){
        return $this->hasMany(new GuideLocationTourEntity(),'guide_id','id');
    }
    
    function services(){
        return $this->hasMany(new GuideServicesEntity(),'guide_id','id');
    }
    
    function schedules(){
        return $this->hasMany(new GuideScheduleEntity(),'guide_id','id');
    }

    function galleries(){
        return $this->hasMany(new GalleryEntity(),'guide_id','id');
    }

    function known_languages(){
        return $this->hasMany(new GuideKnownLanguagesEntity(),'guide_id','id');
    }

    function getAvailableCountries(){
        $results=DB::table('system__countries')->select('system__countries.*')
        ->join('system__cities', 'system__countries.country_id', '=', 'system__cities.country_id')
        ->join('guide__location_tour', 'guide__location_tour.city_id', '=', 'system__cities.city_id')
        ->where('guide__location_tour.guide_id', $this->id)->groupBy('system__countries.country_id')->get();
        return $results;
    }

    function getAvailableCities($country_id=NULL){
        if($country_id!=NULL){
            $results=DB::table('system__cities')->select('system__cities.*')
                ->join('guide__location_tour', 'guide__location_tour.city_id', '=', 'system__cities.city_id')
                ->where('guide__location_tour.guide_id', $this->id)
                ->where('system__cities.country_id',$country_id)
                ->groupBy('system__cities.city_id')->get();

        }else{
            $results=DB::table('system__cities')->select('system__cities.*')
                ->join('guide__location_tour', 'guide__location_tour.city_id', '=', 'system__cities.city_id')
                ->where('guide__location_tour.guide_id', $this->id)->groupBy('system__cities.city_id')->get();

        }
        return $results;
    }
    function getBusyHours(){
        $guide_bookings= BookingEntity::where('guide_id',$this->user()->first()->id)->where('status','accepted')->get();
        $bookedHours=[];
//        dd($guide_bookings);
        foreach($guide_bookings as $booking){
            foreach($booking->datesBooking()->get() as $date){
                $editedDate=strtotime($date->date);
                $editedDate=date('Y/m/d',$editedDate);
                if(!isset($bookedHours[$editedDate])){
                    $bookedHours[$editedDate]=explode(',',$date->hours);
                }else{
                    foreach(explode(',',$date->hours) as $hour){
                        array_push($bookedHours[$editedDate],$hour);
                    }
                }
            }
        }
        return $bookedHours;
    }

    function getConvertBusyHours($city_id){
        $city = CityEntity::find($city_id);
        $busyHoursUTC=$this->getBusyHours();
        $bookedDateHours=[];
        $tz=new DateTimeZone($city->timezone);
        foreach($busyHoursUTC as $key=>$value){
            foreach($value as $hour){
                $time=new DateTime(($key.' '.$hour));
                $time->setTimezone($tz);
                if(!isset($bookedDateHours[$time->format('Y/m/d')])||!is_array($bookedDateHours[$time->format('Y/m/d')])){
                    $bookedDateHours[$time->format('Y/m/d')]=[];
                }
                array_push($bookedDateHours[$time->format('Y/m/d')],$time->format('G'));
            }
        }
//        $currentUTC=new DateTime(Date('Y/m/d H:i:s'));
//        $currentUTC->setTimezone($tz);
//        $currentHour=$currentUTC->format('G');
//        if(!isset($bookedDateHours[$currentUTC->format('Y/m/d')])||!is_array($bookedDateHours[$currentUTC->format('Y/m/d')])){
//            $bookedDateHours[$currentUTC->format('Y/m/d')]=[];
//        }
//        for ($i=0;$i<=$currentHour;$i++){
//            array_push($bookedDateHours[$currentUTC->format('Y/m/d')],(string)$i);
//        }
        return $bookedDateHours;
    }
}