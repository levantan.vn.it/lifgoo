<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use App\User;
use Illuminate\Database\Eloquent\Model;

class GuideCertificationEntity extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'guide__certification';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_type', 'idcard', 'expiry_date', 'place_of_issue', 'type', 'status','location_allowed', 'cert_lang'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function user()
    {
        return $this->belongsTo(new User(), 'user_id');
    }

    function nameLanguage($lang_id){
        return LanguageWorldEntity::where('id','=',$lang_id)->first();
    }
}