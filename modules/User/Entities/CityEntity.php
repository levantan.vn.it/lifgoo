<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use Illuminate\Database\Eloquent\Model;

class CityEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system__cities';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id', 'city_name', 'country_id', 'city_type'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    
    protected $primaryKey = 'city_id';

    function country(){
        return $this->belongsTo(new CountryEntity(),'country_id','country_id');
    }
}