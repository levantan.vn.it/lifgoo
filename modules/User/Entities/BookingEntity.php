<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use App\User;
use Illuminate\Database\Eloquent\Model;
use DateTime,DateTimeZone;
use Modules\Admin\Http\Controllers\BookingController;

class   BookingEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'traveler__booking';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'traveler_id', 'guide_id', 'capacity', 'status','destination','price','phone_number'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * join with dates booking table
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function datesBooking(){
        return $this->hasMany(new BookingDatesEntity(),'booking_id','id');
    }

    /**
     * join with message booking table
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function messageBooking(){
        return $this->hasMany(new BookingMessagesEntity(),'booking_id','id');
    }

    function guide($type = 'guide_id'){
        return $this->belongsTo(new User(),$type,'id');
    }

    function countMessage($booking_id,$user_id){
        $message = BookingMessagesEntity::where('is_read','=',0)->where('booking_id','=',$booking_id)->where('user_id','!=',$user_id)->count();
        return $message;
    }

    function bookingService(){
        return $this->hasMany(new TravellerBookingServiceEntity(),'booking_id','id');
    }

    function notifications(){
        return $this->hasMany(new NotificationEntity(),'booking_id','id');
    }

    function readAllMessage(){
        $all_message=$this->messageBooking()->get();
        foreach($all_message as $message){
            $message->is_read=1;
            $message->save();
        }
    }

    function getCity(){
        return $this->belongsTo(new CityEntity(),'city_id','city_id');
    }

    function getConvertHours(){
        $city = CityEntity::find($this->city_id);
        $dates = $this->datesBooking()->get();
        $bookedDateHours=[];
        $tz=new DateTimeZone($city->timezone);
        foreach($dates as $date){
            foreach(explode(',',$date->hours) as $hour){
                $time=new DateTime(($date->date.' '.$hour));
                $time->setTimezone($tz);
                if(!isset($bookedDateHours[$time->format('Y/m/d')])||!is_array($bookedDateHours[$time->format('Y/m/d')])){
                    $bookedDateHours[$time->format('Y/m/d')]=[];
                }
                array_push($bookedDateHours[$time->format('Y/m/d')],$time->format('G'));
            }
        }
        return $bookedDateHours;
    }
}