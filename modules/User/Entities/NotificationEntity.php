<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Frontend\Models\UsersModel;
use Helper;

class NotificationEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system__notifications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'message', 'user_id', 'is_read','sender_id','booking_id'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function users($foreignKey=null)
    {
        return $this->belongsTo(new User(), $foreignKey, 'id');
    }

    function sender($foreignKey=null)
    {
        return $this->belongsTo(new User(), 'sender_id');
    }

    function switchNotify($type){
        switch ($type) {
            case 'request_booking':
                return 'pending';
            case 'accept_booking':
                return 'accepted';
            case 'decline_booking':
                return 'declined';
            case 'cancel_booking':
                return 'canceled';
            case 'review':
                return 'canceled';
        }
    }

    function content($type=null,$user=null,$sender=null,$booking_id=null){
        $st = '';
        $name = '<a class="profile-user" href="'.Helper::url('profile/'.$sender->id).'" style="color: #263038;" title="'.$sender->full_name.'">'.$sender->full_name.'</a>';
        if($type == 'request_booking')
            $st='You received a booking request from '.$name.'.';
        if($type == 'accept_booking')
            $st=$name.' accepted your booking request.';
        if($type == 'review'){
            $st=$name.' reviewed about you.';
            $st .= ' Please click <a href="'.Helper::url('profile').'/'.$this->user_id.'">here</a> for details.';
        }
        if($type == 'decline_booking')
            $st=$name.' refused your booking request.';
        if($type == 'cancel_booking')
            $st=$name.' canceled booking request.';
        if($type == 'approve')
            $st=$sender->full_name.' recently registered the account. Please review it';
        if($st && $type != 'approve' &&$type!='review')
            $st .= ' Please click <a href="'.Helper::url('detail').'/'.$booking_id.'">here</a> for details.';
        return $st;
    }

    function apiContent($type=null,$user=null,$sender=null,$booking_id=null){
        $st = '';
//        $name = '<a href="'.Helper::url('profile/'.$sender->id).'" style="color: #263038;" title="'.$sender->full_name.'">'.$sender->full_name.'</a>';
        $name=$sender->full_name;
        if($type == 'request_booking')
            $st='You received a booking request from '.$name.'.';
        if($type == 'accept_booking')
            $st=$name.' accepted your booking request.';
        if($type == 'decline_booking')
            $st=$name.' refused your booking request.';
        if($type == 'cancel_booking')
            $st=$name.' canceled booking request.';
        if($type == 'approve')
            $st=$sender->full_name.' recently registered the account. Please review it';
//        if($st && $type != 'approve')
//            $st .= ' Please click <a href="'.Helper::url('detail').'/'.$booking_id.'">here</a> for details.';
        return $st;
    }

    static function notify_system(){
        return NotificationEntity::where('type','=','approve')->orderBy('created_at','desc')->limit(100)->get();
    }

    static function countNotifySystem(){
        return NotificationEntity::where('type','=','approve')->where('is_click','=',0)->limit(100)->count();
    }
}