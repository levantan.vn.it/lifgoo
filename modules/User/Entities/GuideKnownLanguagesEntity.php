<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use Illuminate\Database\Eloquent\Model;

class GuideKnownLanguagesEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'guide__known_languages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guide_id', 'type', 'language_id'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $timestamps = false;
    protected $hidden = [];
    
    public function language()
    {
        return $this->belongsTo(new LanguageWorldEntity(), 'language_id', 'id');
    }
}