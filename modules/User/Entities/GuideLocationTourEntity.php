<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 3/8/2017
 * Time: 8:24 AM
 */

namespace Modules\User\Entities;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\CountryEntity;

class GuideLocationTourEntity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'guide__location_tour';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id', 'guide_id', 'country_id'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $timestamps = false;
    protected $hidden = [];
    
    public function user()
    {
        return $this->belongsTo(new User(), 'user_id');
    }
    
    public function certification()
    {
        return $this->hasOne(new GuideCertificationEntity(), 'user_id', 'user_id');
    }
    
//    public function countries(){
//        return $this->belongsTo(new CountryEntity(),'country_id','country_id');
//    }
    
    public function cities()
    {
        return $this->belongsTo(new CityEntity(), 'city_id', 'city_id');
    }

    public function countries(){
        return CountryEntity::where('country_id',$this->cities()->first()->country_id);
    }
}