<style>

</style>

<html>
<head>
    <title>Lifgoo</title>
</head>
<body>
<div class="container-mail">
    <div class="logo" style="text-align: center;"></div>
    <div class="body">
        <table style="table-layout:fixed;width:100%">
            <tbody>
            <tr>
                <td>
                    <table align="center"
                           style="margin:0 auto;font-size:inherit;line-height:inherit;text-align:center;border-spacing:0;border-collapse:collapse;padding:0;border:0"
                           cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td style="font-family:'Lucida Grande',Helvetica,Arial,sans-serif;height:16px"
                                height="16"></td>
                        </tr>
                        <tr>
                            <td style="width:685px">
                                <table style="font-family:'Lucida Grande',Helvetica,Arial,sans-serif;font-size:inherit;line-height:18px;padding:0px;border:0px">
                                    <tbody>
                                    <tr>
                                        <td style="width:40px"></td>
                                        <td style="text-align:center;width:600px"><img
                                                    src="{{$message->embed(public_path().'/themes/default/assets/img/logo.png')}}">
                                        </td>
                                        <td style="width:40px"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:40px"></td>


                                        <td style="font-family:'Lucida Grande',Helvetica,Arial,sans-serif;line-height:18px;padding-top:44px;text-align:left;font-size:14px;color:#333">
                                            Dear @if($full_name){!! $full_name!!}@endif,
                                        </td>
                                        <td style="width:40px"></td>
                                    </tr>
                                    @if($content){!!$content!!}@endif
                                    <tr>
                                        <td style="width:40px"></td>
                                        <td style="text-align:left;font-family:'Lucida Grande',Helvetica,Arial,sans-serif;line-height:18px;color:#333;padding:3px 0 19px 0;font-size:14px">
                                            Sincerely,
                                        </td>
                                        <td style="width:40px"></td>
                                    </tr>


                                    <tr>
                                        <td style="width:40px"></td>
                                        <td
                                                style="text-align:left;font-family:'Lucida Grande',Helvetica,Arial,sans-serif;line-height:18px;font-size:14px;color:#333;padding:17px 0 13px 0">

                                            Lifgoo Support
                                        </td>
                                        <td style="width:40px"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr
                                style="height:17px" height="17">
                            <td style="font-family:'Geneva',Helvetica,Arial,sans-serif"></td>
                        </tr>
                        <tr>
                            <td colspan="3"
                                style="font-family:'Geneva',Helvetica,Arial,sans-serif;width:685px;font-size:11px;line-height:14px;color:#888;text-align:center;background-repeat:no-repeat;background-position:center top;padding:15px 0 12px"
                                align="center">
                                <span>
                              <span style="white-space:nowrap">
                                <a href="{{Helper::url('')}}" style="color:#08c;text-decoration:none" target="_blank"> lifgoo </a>
                            </span> |
                                <span style="white-space:nowrap">
                              <a href="{{Helper::url('contact')}}" target="_blank"> lifgoo support </a>
                                </span>
                                </span>
                                <br>
                                <span colspan="3">
                                    <span style="white-space:nowrap">
                                      Copyright © 2017
                                    </span>
                                    <span>
                                       <a href="http://vietprojectgroup.com/">vietprojectgroup</a></span>
                                    </span>
                                </span>
                            </td>
                        </tr>
                        <tr style="height:50px" height="50">
                            <td style="font-family:'Geneva',Helvetica,Arial,sans-serif">
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
