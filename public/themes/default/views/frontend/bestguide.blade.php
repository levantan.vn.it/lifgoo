@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-aboutus">
        <!--SLIDER REVOLUTION-->
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul class="slide-list">
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-title="Intro Slide">
                        <img src="{{Helper::getThemeImg('slider1.jpg')}}" alt="slidebg1"
                             data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    </li>
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-title="Intro Slide">
                        <img src="{{Helper::getThemeImg('slider2.jpg')}}" alt="slidebg1"
                             data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    </li>
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <div class="cnt_find_guide content--minheight">
            <div class="container">
            
                <div class="advance_search">
                    <h3 class="font-size-24">Bets Guide</h3>
                </div>
                <div class="row guide_selected flex-row">

                    <!-- GUIDE 1 -->
                    <div class="col-md-3 col-sm-4 col-xs-4 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4 font-size-14">From <i class="font-size1 font-size-18">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide font-size-14" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align font-size">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 2 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 3 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 4 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 5 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 6 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 7 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->

                    <!-- GUIDE 8 -->
                    <div class="col-md-3 item_vp_guide">
                        <div class="img-guide">
                            <figure style="background-image: url('{{Helper::getThemeAssets('img/user.png')}}')"></figure>
                            <div class="hover_profile-guide">
                                <div class="item-cnt-infoGuide">
                                    <span class="from-price color4">From <i class="font-size1">$50</i></span>
                                    <div class="item-thumb-cnt-infoGuide">
                                        <a class="view_profile_guide" href="#" target="_blank">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item-profile-guide">
                            <div class="no-hover">
                                <h4 class="item-name-profile color2 txt-align">Guide 1</h4>
                            </div>
                            <div class="hover-y" style="display: none;">
                                <a class="color2" href="javascript:void(0)">View Profile</a>
                            </div>

                            <div class="rating-star txt-align">
                                @for($i=1; $i<= 2; $i++)
                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                @endfor
                                @for($i=4; $i >= 2; $i--)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                            </div>
                        </div>
                    </div> <!-- end GUIDE ITEM -->
                </div>
                
                {{--@if($flag_show)--}}
                <div class="wraper-show-more">
                    <a class="show-more find-guide" href="javascript:void(0)" data-url="">VIEW MORE</a>
                </div>
                {{--@endif--}}
            </div>
        </div>
    </div>
@stop
@section('js_wrapper')
    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('.tp-banner').show().revolution(
                {
                    dottedOverlay: "none",
                    delay: 16000,
                    startwidth: 1170,
                    startheight: 600,
                    touchenabled: "on",
                    onHoverStop: "off",
                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,
                    parallax: "mouse",
                    parallaxBgFreeze: "on",
                    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                    keyboardNavigation: "off",
                    navigationHAlign: "center",
                    navigationVAlign: "bottom",
                    navigationHOffset: 0,
                    navigationVOffset: 20,
                    shadow: 0,
                    fullWidth: "off",
                    fullScreen: "off",
                    spinner: "spinner4",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "on",
                    forceFullWidth: "off",
                    hideThumbsOnMobile: "off",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "off",
                    hideArrowsOnMobile: "off",
                    hideThumbsUnderResolution: 0,
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    fullScreenOffsetContainer: ".header"
                });
        });

    </script>
@stop