<div class="tab-pane edit-guide" id="edit-price">
    <div class="row">
        <div class="col-md-12 mytable--thumb">
            <table id="mytable" class="tablePrice table table-striped table-bordered">
                <thead class="thead-edit-price">
                <tr>
                    <th class="font-size-14 color666">Number of passengers<br>(person)</th>
                    <th class="font-size-14 color666">Time (h)</th>
                    <th class="font-size-14 color666">Money (usd)</th>
                    <th colspan="2" class="font-size-14 color666">Active</th>
                </tr>
                </thead>
                <tbody class="edit-price">
                <tr id="row-add-edit-price">
                    <td><input class="input-add-price" type="number" step="1" min="1" max="9999" maxlength="4" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" name="edit_count_user" placeholder=""></td>
                    <td><input class="input-add-price" type="number" step="1" min="1" max="9999" maxlength="4" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" name="edit_hour" placeholder=""></td>
                    <td><input class="input-add-price price" type="number" step="1" name="edit_price" placeholder="" min="1" max="9999999999" maxlength="10" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" pattern="^\d+(?:\.\d{1,2})?$">
                    </td>
                    <td class="td_add font-size-14 color4 fnt-weight bgBlue1" colspan="2"><a id="add-row-price" href="javascript:void(0)">Add</a></td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="col-md-12 btnUpdate--thumb">
            <button type="button" class="btn btn_update btn-edit-price font-size-14 color4 fnt-weight bgBlue"
                    data-url="{{Helper::url('editPrice')}}">Update
            </button>
        </div>
    </div>
</div>