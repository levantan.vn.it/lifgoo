<div class="tab-pane edit-guide" id="setup-time">
    <div class="callout callout-error"></div>
    <div class="row setup_time--thumb">
        {!!Form::open(['url'=>Helper::url('schedule'),'method'=>'post','class'=>'edit-setup-time']) !!}
        <?php $days = Helper::daysOfWeek();?>
        @if($days)
            @foreach($days as $day)
                <?php
                    $to = 1;
                    $to1 = 1;
                    $from1 = 1;
                $isDay = isset($schedules) && array_key_exists($day, $schedules);

                $isdayAdd = $isDay && $schedules && array_key_exists('start1',$schedules[$day]) && array_key_exists('end1',$schedules[$day]);
                if($isDay){
                    $to = $schedules[$day]['start'];
                    $from1 = $schedules[$day]['end'];
                    if(isset($schedules[$day]['start1']))
                        $to1 = $schedules[$day]['start1'];
                }
                ?>
                <div class="setup-time-item">
                    <div class="row">
                        <div class="col-md-12 on-off-time">
                            <div class="col-md-12 btnOnOff--thumb">
                                <div class="setup-date color333 font-size-14 fnt-weight">{{trans('site.'.$day)}}</div>
                                <div class="choose-on-off">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default btn-on btn-xs font-size-14 @if($isDay) active @endif">
                                            <input type="radio" value="1"
                                                   name="multifeatured_module[{{$day}}]" {{$isDay?'checked':''}}>ON</label>
                                        <label class="btn btn-default btn-off btn-xs font-size-14 @if(!$isDay) active @endif">
                                            <input type="radio" value="0"
                                                   name="multifeatured_module[{{$day}}]" {{$isDay?'':'checked'}}>OFF</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-time-first @if(!$isDay)hidden @endif">
                        <div class="col-md-12 on-off-time">
                            {{--<div class="col-md-12 btnOnOff--thumb">
                                <div class="setup-date color333 font-size-14 fnt-weight">{{trans('site.'.$day)}}</div>
                                <div class="choose-on-off">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default btn-on btn-xs font-size-14 @if($isDay) active @endif">
                                            <input type="radio" value="1"
                                                   name="multifeatured_module[{{$day}}]" {{$isDay?'checked':''}}>ON</label>
                                        <label class="btn btn-default btn-off btn-xs font-size-14 @if(!$isDay) active @endif">
                                            <input type="radio" value="0"
                                                   name="multifeatured_module[{{$day}}]" {{$isDay?'':'checked'}}>OFF</label>
                                    </div>
                                </div>
                            </div>--}}
                            <div class="col-md-12">
                                {{--<div class="font-size-14 setup-select-from select-setTime from @if(!$isDay) hidden @endif">--}}
                                <div class="font-size-14 setup-select-from select-setTime from">
                                    {{--<label for="from[]">from</label>--}}
                                    <select name="multi_from[{{$day}}]" class="selectpicker form-control font-size-14"
                                            data-style="btn-primary" @if(!$isDay) disabled @endif required step='Any'>
                                        <option value="" hidden>From</option>
                                        @for($i=0; $i<=23; $i++)
                                            <option @if($isDay && $schedules[$day]['start']==$i) selected
                                                    @endif value="{{$i}}">{{Helper::addZero($i)}}:00
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                                {{--<div class="font-size-14 setup-select-to select-setTime to @if(!$isDay) hidden @endif">--}}
                                <div class="font-size-14 setup-select-to select-setTime to">
                                    {{--<label for="to[]">To</label>--}}
                                    <select name="multi_to[{{$day}}]" class="selectpicker form-control font-size-14" data-style="btn-primary"
                                            @if(!$isDay) disabled @endif required>
                                        <option value="" hidden>To</option>
                                        @for($i=$to; $i<= 23; $i++)
                                            <option @if($isDay && $schedules[$day]['end']==$i) selected
                                                    @endif value="{{$i}}">{{Helper::addZero($i)}}:59
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="btn-setupTime--thumb @if(!$isDay || $isdayAdd)hidden @endif @if($isdayAdd)hasRemove @endif @if(!empty($schedules[$day])&&$schedules[$day]['end']==23)hidden @endif">
                                    <div class="choose-add-hour"><a href="javascript:void(0);" class="font-size-14"><i class="fa fa-plus click-add font-size-18" aria-hidden="true"></i></a></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row @if(!$isdayAdd)hidden @endif add-setup-time">
                        <div class="col-md-12">
                            <div class="setup-select-from from1">
                                {{--<label for="from[]">from</label>--}}
                                <select name="multi_from1[{{$day}}]" class="selectpicker form-control" data-style="btn-primary" @if(!$isdayAdd) disabled @endif required>
                                    <option value="" hidden>From</option>
                                    @for($i=$from1+1; $i<=23; $i++)
                                        <option @if($isdayAdd && $schedules[$day]['start1']==$i) selected @endif value="{{$i}}">{{Helper::addZero($i)}}:00</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="setup-select-to to1">
                                {{--<label for="to[]">to</label>--}}
                                <select name="multi_to1[{{$day}}]" class="selectpicker form-control" data-style="btn-primary" @if(!$isdayAdd) disabled @endif required>
                                    <option value="" hidden>To</option>
                                    @for($i=$from1+1; $i<=23; $i++)
                                        <option @if($isdayAdd && $schedules[$day]['end1']==$i) selected @endif value="{{$i}}">{{Helper::addZero($i)}}:59</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="btn-setupTime--thumb">
                                <div class="choose-remove-hour"><a href="javascript:void(0);" class="font-size-14"><i class="fa fa-minus click-remove font-size-18" aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        {{--Button Update--}}
        <div class="col-md-12 btnUpdate--thumb">
            <button type="submit" class="btn btn_update btn-edit-submit font-size-14 fnt-weight bgBlue color4">Update</button>
        </div>
        {!!Form::close() !!}
    </div>
</div>