@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site" data-id="{{$user->id}}">
        <div class="bg-profile">
            <div class="cover-images-abc">
                <div class="img-cover transition" style="width: 100%; max-width: 1170px; background-image: url('{{Helper::getThemeImg('cover.jpg')}}')">
                </div>
            </div>
            <div class="container">
                <div class="content-top-info">
                    <div class="cover-images">
                        
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 info-profile">
                                <div class="col-md-3 thumb-avataProfile">
                                    <div class="avatar-profile transition bg-property"
                                         style="width: 100%; max-width: 199px; height: 200px; @if(isset($user) && $user->avatar) background-image: url('{{Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg()}}') @else background-image: url('{{Helper::getThemeImg('user.png')}}');background-repeat: no-repeat; background-size: cover; @endif">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="name-profile color2"><p>@if(isset($user)){{$user->full_name}} @endif</p>
                                    </div>
                                    <?php
                                        $canEditMood = isset($owner) && $owner && $owner->id==$user->id;
                                    ?>
                                    <div @if($canEditMood)class="mood-profile"@endif>
                                        @if($canEditMood)<i class="fa fa-pencil edit-status" aria-hidden="true"></i>@endif<i data-mood="1"
                                                                                                      class="truncate">{{$user->slogan?$user->slogan:'no slogan'}}</i>

                                        @if($canEditMood)<input class="mood-input"
                                                                                                    style="display: none;width: 100%"
                                                                                                    type="text"
                                                                                                    data-id="{{$user->id}}">@endif
                                    </div>
                                    <div class="row col-md-12">
                                        <div class="birthday-profile"><i class="fa fa-birthday-cake"
                                                                         aria-hidden="true"></i> @if(isset($user)){{$user->birth_date}} @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row content-bottom-info">
                    <div class="col-md-8 col-md-offset-2 content-profile--thumb">
                        <div class="content-profile">
                            <!--ABOUT ME-->
                            @if(isset($guide) && $guide->introduce)
                                <div class="about-me">
                                    <div class="border-profile-item">
                                        <div class="title-aboutme color2 font-size1 title-aboutme-pad">About me</div>
                                        <div class="content-aboutme">
                                            {{$guide->introduce}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <!--MY HOBBY-->
                            @if(isset($user) && $user->hobby)
                                <div class="my-experience">
                                    <div class="border-profile-item">
                                        <div class="title-aboutme color2 font-size1">My hobby</div>
                                        <div class="experience-content">
                                            {{$user->hobby}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script type="text/javascript">
        $(window).on('load resize', function(){
            var heightCover = $(window).width() / 5.48 ;
            if ($(window).width()<1370) { heightCover = $(window).width() / 4 ; }
            if ($(window).width()<991) { heightCover = $(window).width() / 3.4 ; }
            if ($(window).width()<=640) { heightCover = $(window).width() / 2.5 ; }
            if ($(window).width()<=414) { heightCover = $(window).width() / 2 ; }
            $('.cover-images-abc .img-cover').css('height',heightCover);
        });
    </script>
@stop

