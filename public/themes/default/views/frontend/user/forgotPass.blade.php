@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-sitelogin content--minheight-full">
        <div id="header-logo">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a class="logo-login" title="Lifgoo" href="{{Helper::url('')}}"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="100" height="40" viewBox="0 0 4990 2047.06"><defs><style>.cls-1{fill:#1fd8f8;}.cls-2{fill:#ff6800;}</style></defs><title>Lifgoo-SVG</title><path class="cls-1" d="M0,116.45q0-17,14.31-32T45,69.5H210.51q16.33,0,30.66,14.94t14.31,32V1516.26q0,19.2-14.31,33.08t-30.66,13.87H45q-16.38,0-30.66-13.87T0,1516.26Z"/><path class="cls-1" d="M603.78,408.78q-70.32,0-116.83-50.15T440.42,238.08q0-70.42,46.53-119.49T603.78,69.5q70.27,0,117.86,49.08T769.2,238.08q0,70.42-47.56,120.56T603.78,408.78ZM477.64,624.31q0-21.31,14.47-34.14t31-12.81H690.62q16.53,0,31,12.81t14.47,34.14v892q0,19.2-14.47,33.08t-31,13.87H523.13q-16.57,0-31-13.87t-14.47-33.08Z"/><path class="cls-1" d="M1050.71,807.6H952q-20.58,0-32.91-15.11T906.72,760.1V613.3q0-47.46,45.25-47.5h98.74V447.06q0-200.77,97.71-308.72t287-107.95q94.59,0,170.73,33.46t109,63.69q12.34,8.65,15.42,25.91t-5.14,30.22l-72,131.69a38.05,38.05,0,0,1-24.68,19.43q-16.49,4.35-32.91-6.48-22.66-17.26-65.82-34.54t-82.28-17.27q-45.27,0-72,11.87t-42.17,34.54q-15.43,22.66-20.57,54t-5.14,70.16V565.8H1567q45.23,0,45.26,47.5V760.1q0,17.3-12.35,32.38T1567,807.6H1307.83v708.11a46.87,46.87,0,0,1-13.37,33.46q-13.4,14.07-31.88,14H1096q-18.51,0-31.88-14a46.73,46.73,0,0,1-13.37-33.46Z"/><path class="cls-1" d="M2688,1549.38q0,118.32-37,207.1t-101.83,147.93q-64.8,59.14-151.21,88.76t-183.09,29.59q-59.69,0-113.15-11.62a632.87,632.87,0,0,1-98.75-29.59q-45.28-18-79.21-37t-50.4-31.7a57.7,57.7,0,0,1-14.4-26.42q-4.15-15.85,4.11-28.53l61.72-137.36q6.17-14.81,22.63-19t32.92,6.34q12.35,8.42,36,21.13a489.43,489.43,0,0,0,53.49,24.31,509.38,509.38,0,0,0,64.8,20.07,306.3,306.3,0,0,0,72,8.46q55.55,0,96.69-10.57t69.94-37q28.79-26.45,43.2-71.85t14.4-113.06V1521.9q-32.93,12.68-94.63,26.42t-133.72,13.73q-107,0-193.38-39.1T1861,1415.18q-61.71-68.65-95.66-161.67t-33.94-200.76q0-109.86,35-203.93T1862,686.09a426.57,426.57,0,0,1,144-106.72q83.32-38,180-38,76.09,0,132.69,23.25t97.72,50.72a444.41,444.41,0,0,1,80.23,71.85L2521.36,609q14.36-46.46,45.26-46.5h76.12q18.51,0,31.88,13.74A45.41,45.41,0,0,1,2688,609ZM2430.84,917.5q-8.24-21.1-24.69-44.38T2364,829.8q-25.75-20.06-61.72-33.81T2221,782.25q-55.55,0-97.72,23.25a212.45,212.45,0,0,0-69.94,61.29q-27.78,38-42.17,86.64a347.46,347.46,0,0,0-14.4,99.33,326.29,326.29,0,0,0,15.43,99.32q15.43,48.64,44.23,86.64t73,60.23q44.21,22.19,103.89,22.19,45.22,0,81.26-8.46t62.75-19q30.85-10.55,53.49-27.47Z"/><path class="cls-1" d="M3968.77,1049.09q0-103.64,39.94-195.7t109-161.85q69.07-69.82,161.93-110t198.63-40.2q105.76,0,198.63,40.2t163,110a516.6,516.6,0,0,1,110.12,161.85q39.92,92,39.94,195.7,0,105.82-39.94,198.87t-110.12,162.9q-70.18,69.82-163,111.07t-198.63,41.25q-105.81,0-198.63-41.25t-161.93-111.07q-69.13-69.82-109-162.9T3968.77,1049.09Zm250.45,0q0,57.13,20.51,106.84t55.05,86.74a249.31,249.31,0,0,0,82,58.18q47.47,21.17,101.48,21.16t101.47-21.16a262.07,262.07,0,0,0,83.12-58.18q35.62-37,56.14-86.74t20.51-106.84q0-55-20.51-103.67T4662.9,860.8a271.68,271.68,0,0,0-83.12-57.12q-47.52-21.12-101.47-21.16t-101.48,21.16q-47.51,21.17-82,57.12t-55.05,84.63Q4219.18,994.12,4219.22,1049.09Z"/><path class="cls-2" d="M3018.43,1463.27l132.07-228.76a254.65,254.65,0,0,1-76.5-182.24c0-141.09,114.38-255.47,255.47-255.47s255.47,114.38,255.47,255.47a254.65,254.65,0,0,1-76.5,182.24l132.07,228.76c106.74-107.1,199.89-247.39,199.89-411,0-282.18-228.75-510.93-510.93-510.93s-510.93,228.75-510.93,510.93C2818.53,1215.88,2911.68,1356.17,3018.43,1463.27Z"/><path class="cls-2" d="M3397.83,1298.41a255.25,255.25,0,0,1-136.73,0L3115.73,1550.2c112.4,90,213.73,140.74,213.73,140.74s101.33-50.7,213.73-140.74Z"/></svg></a>
                    </div>
                </div>
            </div>
        </div> <!-- end header-logo -->

        <div class="content-left bg-content"></div> <!-- end content-left -->

        <div class="content-right forgot-form aligncenter flex-center content--minheight-page">
           <div class="bg-form-login content-signin">
                <div class="title-login title-forgot fnt-weight font-size-24 color0084fb">Forgot Password</div>

                @if (count($errors) > 0)
                    <div class="text-error">
                        <ul class="errors">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif                          
                <div class="">
                    <div class="col-md-12">
                        <div class="content-login aligncenter">

                            <span class="tt-forgot font-size-14 color263038">Please enter your email address and we'll send you instructions on how to reset your password</span>
                            <div class="form-group form-error-message clearfix"></div>
                            {{ Form::open(array('url' => Helper::url('forgotPassword'), 'id' => 'formforgot'))}}
                            <div class="input-group inp-subcribe--thumb inp-forgot--thumb">
                                <input type="email" class="form-control font-size-14 borderRadius input-emailllogin" id="email" placeholder="Email" name="email" required><span class="color4e5d6b-op"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            </div>
                            <div class="btn-submit-forgot">
                                <button type="submit" class="btn btn-primary btn-success font-size-14 bgBlue">Submit</button>
                            </div>
                            {{ Form::close() }}
                            <div class="back-login">
                                <a href="{{Helper::url('signin')}}" class="color263038 font-size-12"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Go back to login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end content-right -->

        <div id="footer-login">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="font-size-12 colore4ebf0">Coppyright 2016 @ Lifgoo. All right reserved. <br class="br1">Privacy Policy.<br class="br2">Site Published by ViTPR</p>
                    </div>
                </div>
            </div>
        </div> <!-- end footer-login -->
    </div> <!-- end content-sitelogin -->

<!-- code old -->
@if(false)
    <div class="content-site">
        <div class="cnt-site-login">
            <div class="bg-SingupLogin bg-login bg-forgot content--minheight" style="background: url('{{Helper::getThemeImg('bg-login3.jpg')}}');background-size: cover; background-position: center;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-lg-8 col-md-10 col-sm-10 col-xs-10 pad-left pad-right content-forgot" style="position: relative">
                                <div class="bg-form-forgot">
                                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                                        <div class="title-login title-forgot fnt-weight font-size-24 color333">Forgot Password</div>
                                        @if (count($errors) > 0)
                                            <div class="text-error">
                                                <ul class="errors">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <span class="tt-forgot font-size-14 color999">Please enter your email address and we'll send you instructions on how to reset your password</span>
                                        <div class="form-group form-error-message clearfix"></div>
                                        {{ Form::open(array('url' => Helper::url('forgotPassword'), 'id' => 'formforgot'))}}
                                        <div class="input-group inp-subcribe--thumb inp-forgot--thumb">
                                            <input type="email" class="form-control font-size-14 borderRadius input-emailllogin" id="email" placeholder="Email" name="email" required>
                                            <!-- <div class="input-group-addon icon-mail-forgot"> --><span class="color999"><i class="fa fa-envelope-o" aria-hidden="true"></i></span><!-- </div> -->
                                        </div>
                                        <div class="btn-submit-forgot">
                                            <button type="submit" class="btn btn-primary btn-success font-size-14 bgBlue">Submit</button>
                                        </div>
                                        {{ Form::close() }}
                                        <div class="back-login">
                                            <a href="{{Helper::url('signin')}}" class="color999 font-size-12"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Go back to login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>

@endif

@stop