@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content--minheight">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">Advance Search</h3>
        </div>
        <div class="bg-editguide">
            <div class="container">
                <div class="title-edit-guide fnt-weight font-size-30"></div>

                <div class="row">
                    <div class="col-md-3 tabs-left-edit-guide"> <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left responsive tab-guide">
                            <li class="active" value="Edit your profile"><a class="color999 font-size-14" href="#edit-profile" data-toggle="tab">{{trans('site.edit_profile')}}</a></li>
                            <li value="Edit password"><a class="color999 font-size-14" href="#edit-password" data-toggle="tab">{{trans('site.edit_password')}}</a></li>
                            <li value="Your notification"><a class="color999 font-size-14" href="#notification" data-toggle="tab">{{trans('site.edit_notifications')}}</a></li>
                            <li value="History Booking"><a class="color999 font-size-14" href="#booking" data-toggle="tab">{{trans('site.edit_booking')}}</a></li>
                        </ul>
                    </div>

                    <div class="col-md-9 tabs-notify-edit-guide">
                        <!-- Tab panes -->
                        <div class="tab-content responsive">
                            @include('default::frontend.user.editProfile')
                            @include('default::frontend.user.editPassword')
                            @include('default::frontend.notifications.editNotification')
                            {{--EDIT BOOKING--}}
                            <div class="tab-pane edit-guide" id="booking">
                                {{--FILTER BY HISTORY--}}
                                <div class="filter-history">
                                    <div class="color10 fnt-weight" for="filter-history">Filter by</div>
                                    <select id="filter-history" class="selectpicker form-control" data-style="btn-primary">
                                        <option value="all">All</option>
                                        <option value="cancelled">Cancelled</option>
                                        <option value="travelled">Travelled</option>
                                    </select>
                                </div>
                                <div class="col-md-12 cnt-edit-traveller">
                                    <div class="row panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">
                                        <!--Begin Panel BOOKING-->
                                        <div class="panel">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    <div class="col-md-4 list-left">
                                                        <div class="col-md-4 avatar_traveller">
                                                            <img src="{{Helper::getThemeImg('pro2.jpg')}}">
                                                        </div>
                                                        <div class="col-md-8 font-size2">
                                                            <div class="name-history-booking color2">Nguyen Hoang
                                                                <div class="rating-star">
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 color10 list-right">
                                                        <div class="list-deactive">You booked with Thuy Linh total 20 hours in 4 day for 5 people, total $200.00</div>
                                                        <div class="list-active" style="display: none;">
                                                            Booking with Hoang Thuy Linh
                                                            <button type="button" data-toggle="modal" class="btn-cancel-booking">Cancel this booking</button>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    <div class="col-md-4 cnt-list-left">
                                                        <button type="button" class="btn btn-primary btn-chat">CHAT</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row cnt-list-right color10">
                                                            <div class="col-md-6">
                                                                <div class="booking-date">Booking dates</div>
                                                                <ul class="list-booking-date">
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                </ul>
                                                                <div class="total-book-date">
                                                                    Total: 20 hours in 4 days
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-right-statistical">
                                                                    <li>Number of people: 5</li>
                                                                    <li>Expected cost: $200.00</li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                        <div class="chat-cnt-edit-traveller">
                                                            <div class="row">
                                                                <div class="trevaller_inbox">
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
                                                                </div>
                                                                <div class="guide_inbox">
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of panel -->

                                        <!--Begin Panel BOOKING-->
                                        <div class="panel">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne">
                                                    <div class="col-md-4 list-left">
                                                        <div class="col-md-4 avatar_traveller">
                                                            <img src="{{Helper::getThemeImg('pro2.jpg')}}">
                                                        </div>
                                                        <div class="col-md-8 font-size2">
                                                            <div class="name-history-booking color2">Nguyen Hoang
                                                                <div class="rating-star">
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 color10 list-right">
                                                        <div class="list-deactive">You booked with Thuy Linh total 20 hours in 4 day for 5 people, total $200.00</div>
                                                        <div class="list-active" style="display: none;">
                                                            Booking with Hoang Thuy Linh
                                                            <button type="button" data-toggle="modal" class="btn-cancel-booking">Cancel this booking</button>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                    <div class="col-md-4 cnt-list-left">
                                                        <button type="button" class="btn btn-primary btn-chat">CHAT</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row cnt-list-right color10">
                                                            <div class="col-md-6">
                                                                <div class="booking-date">Booking dates</div>
                                                                <ul class="list-booking-date">
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                </ul>
                                                                <div class="total-book-date">
                                                                    Total: 20 hours in 4 days
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-right-statistical">
                                                                    <li>Number of people: 5</li>
                                                                    <li>Expected cost: $200.00</li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                        <div class="chat-cnt-edit-traveller">
                                                            <div class="row">
                                                                <div class="trevaller_inbox">
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
                                                                </div>
                                                                <div class="guide_inbox">
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of panel -->

                                        <!--begin panel CANCELlER-->
                                        <div class="panel">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        <div class="col-md-4 list-left">
                                                            <div class="col-md-4 avatar_traveller">
                                                                <img src="{{Helper::getThemeImg('pro2.jpg')}}">
                                                            </div>
                                                            <div class="col-md-8 font-size2">
                                                                <div class="name-history-booking color2">Hoàng Thùy Linh
                                                                    <div class="rating-star">
                                                                        <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                        <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                        <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                        <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 color10 list-right">Canceller</div>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                    <div class="col-md-4 cnt-list-left">
                                                        <button type="button" data-toggle="modal" class="btn btn-primary btn-write-review">Write Review</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row cnt-list-right color10">
                                                            <div class="col-md-6">
                                                                <div class="booking-date">Booking dates</div>
                                                                <ul class="list-booking-date">
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                    <li>January 15 - 6 hours </li>
                                                                </ul>
                                                                <div class="total-book-date">
                                                                    Total: 20 hours in 4 days
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-right-statistical">
                                                                    <li>Number of people: 5</li>
                                                                    <li>Expected cost: $200.00</li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                        <div class="chat-cnt-edit-traveller">
                                                            <div class="row">
                                                                <div class="trevaller_inbox">
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
                                                                </div>
                                                                <div class="guide_inbox">
                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end of panel -->
                                    </div>
                                    <!--div id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="history-booking row">
                                            <div class="col-md-4">
                                                <div class="col-md-4 avatar_traveller">
                                                    <img src="{{Helper::getThemeImg('pro2.jpg')}}">
                                                </div>
                                                <div class="col-md-8 font-size2">
                                                    <div class="name-history-booking">Nguyen Hoang
                                                        <div class="rating-star">
                                                            <i class="fa fa-star " aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 excerpt_traveller">
                                                You booked with Thuy Linh total 20 hours in 4 day for 5 people, total $200.00

                                                <div class="content-traveller">
                                                    <div class="col-md-6">
                                                        <div>Booking dates</div>
                                                        <ul>
                                                            <li>January 15 - 6 hours </li>
                                                            <li>January 15 - 6 hours </li>
                                                            <li>January 15 - 6 hours </li>
                                                            <li>January 15 - 6 hours </li>
                                                            <li>January 15 - 6 hours </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li>Number of people: 5</li>
                                                            <li>Expected cost: $200.00</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Modal Popup Write Review-->
        <div class="modal fade" id="write-review" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="ratings-star-title">Write Reviews</div>
                        <div id="stars" class="starrr"></div>
                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                        <button type="button" class="btn btn-secondary sbt-btn" data-dismiss="modal">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

