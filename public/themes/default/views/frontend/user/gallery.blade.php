<div class="col-md-3 col-sm-4 col-xs-4 item-allNews--thumb">
    <div class="item-allNews">
        <div class="img-guide div-wrapper-allNews img-galleryGuide">
            <div class="div-pad-allNews div-pad-allgallery"></div>
            <figure style="background-image: url('{{Helper::getThemeGallery('gallery' . $user->id.'/'.$gallery->url)}}')" class="bg-property div-inner-allNews"></figure>
            <div class="hover_profile-guide">
                <div class="item-cnt-infoGuide">
                    <div class="item-thumb-cnt-infoGuide">
                        {{--<a class="view_profile_guide font-size-14 del-gallery" href="{{Helper::url('gallery/delete/'.$gallery->id)}}" title="Delete"><i class="fa fa-close" aria-hidden="true"></i></a>
                        <a href="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$gallery->url)}}" class="viewGallery" title="" onclick="openModal();currentSlide({{$i}})"><i class="fa fa-search" aria-hidden="true"></i></a>--}}

                        <button class="view_profile_guide font-size-14 del-gallery" onclick="window.location.href='{{Helper::url('gallery/delete/'.$gallery->id)}}'" title="Delete"><i class="fa fa-close" aria-hidden="true"></i></button>
                        <a href="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$gallery->url)}}" class="viewGallery" title=""><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>