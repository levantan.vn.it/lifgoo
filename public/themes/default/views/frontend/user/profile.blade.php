@extends('default::frontend.layouts.master')
@section('content')
    <?php
    $isGuide = session('users') && session('users.guide');
    ?>
    @if(($empty_user==1))
        <div class="container content--minheight">
            <p><h4>This user is invalid! Please check and try again!</h4></p>
        </div>
    @else

    <div class="content-site" data-id="{{$user->id}}">
        <div class="bg-profile">
            <div class="cover-images-abc">
                <div class="@if(isset($owner) && $owner && $owner->id == $user->id) cropme @endif img-cover transition bg-property"
                     style="width: 100%; background-image: url(@if($guide && $guide->cover)'{{Helper::getThemeAvatar($guide->cover).'?id='.Helper::versionImg()}}') @else'{{Helper::getThemeImg('cover.jpg')}}')@endif">
                    <a href="javascript:void(0)" title="" class="change-coverImage"><i class="fa fa-camera"
                                                                              aria-hidden="true"></i>Change cover</a>
                </div>
                <div class="content-top-info">
                    <div class="cover-images">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 info-profile">
                                    <div class="thumb-avataProfile">
                                        <div class="thumbsmall-avataProfile">
                                            <div class="@if(isset($owner) && $owner && $owner->id==$user->id) cropme @endif avatar-profile transition bg-property"
                                             style="width: 150px; max-width: 150px; height: 150px; @if(isset($user) && $user->avatar) background-image: url('{{Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg()}}') @else background-image: url('{{Helper::getThemeImg('user.png')}}');background-repeat: no-repeat; background-size: cover; @endif">
                                                <a href="javascript:void(0)" title="Change avatar" class="change-avatar font-size-14 fnt-weight color4"><i class="fa fa-camera" aria-hidden="true"></i></a>
                                            </div> <!-- end avatar -->
                                        </div> <!-- end thumbsmall-avataProfile -->
                                        <div class="button-profile">
                                            @if(!$isGuide)
                                                <div class="btn-book-info">
                                                    <a href="{{Helper::url('booking/'.$user->id)}}" title="" class="btn btn-secondary btn-profile font-size color4 fnt-weight bgBlue">BOOK<span><i class="fa fa-usd" aria-hidden="true"></i>@if(isset($guide)){{$guide->hourly_rate}} @endif/h</span>
                                                    </a>
                                                </div>
                                            @else
                                                <div class="btn-book-info">
                                                    <div class="btn btn-secondary btn-profile font-size color4 fnt-weight bgBlue"><span><i class="fa fa-usd" aria-hidden="true"></i>@if(isset($guide)){{$guide->hourly_rate}} @endif/h</span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(0)
                                                <div class="btn-chat-info">
                                                    <button type="button" class="btn btn-secondary btn-profile"
                                                            data-dismiss="modal"><i class="fa fa-comment"
                                                                                    aria-hidden="true"></i>CHAT
                                                    </button>
                                                </div>
                                            @endif
                                        </div> <!-- end button BOOK -->                                      
                                        
                                    </div> <!-- end thumb-avataProfile - info guide -->
                                    <div class="status--thumb">
                                        <div class="name-profile color4 font-size-18 fnt-weight">
                                            <p>@if(isset($user)){{$user->full_name}} @endif</p>
                                        </div>
                                        <div class="rating-star rating-star--thumb">
                                            @for($i=1; $i<= $guide->star; $i++)
                                                <i class="fa fa-star yellow" aria-hidden="true"></i>
                                            @endfor
                                            @for($i=4; $i >= $guide->star; $i--)
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            @endfor
                                        </div>
                                        <?php
                                        $canEditMood = isset($owner) && $owner && $owner->id == $user->id;
                                        ?>
                                        <div id="Status" @if($canEditMood)class="mood-profile"@endif>
                                            @if($canEditMood)<i class="fa fa-pencil edit-status" aria-hidden="true"></i>@endif<i data-mood="1" class="truncate font-size-14 color4" title="{{$user->slogan}}">{{$user->slogan?$user->slogan:'no slogan'}}</i>

                                            @if($canEditMood)<input class="mood-input font-size-14" style="display: none;width: 100%" type="text" data-id="{{$user->id}}">@endif
                                        </div>

                                    </div> <!-- end status thumb -->
                                    <div class="col-lg-2 btn-coverImage">
                                        @if($isGuide && $canEditMood)
                                        <a href="javascript:void(0)" title="Change cover" class="btn-changecoverImg"><i class="fa fa-camera" aria-hidden="true"></i></a>
                                        @endif
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end content-top-info -->
            </div> <!-- end cover-images-abc -->
            <div class="container">
                <div class="row content-bottom-info">
                    <!-- ABOUT ME -->
                    <div class="col-lg-8 content-profile--thumb content-profile--left">
                        <!-- <div class="content-profile"> -->
                        <div>
                            <div class="contentprofile-item boxShadow15">
                                <!--ABOUT ME-->
                                @if(isset($guide) && $guide->introduce)
                                    <div class="about-me">
                                        <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">
                                            About me</h5>
                                        <div class="content-aboutme font-size-14 color4e5d6b">
                                            {{$guide->introduce}}
                                        </div>
                                    </div>
                                @endif
                                <!--MY EXPERIENCE-->
                                @if(isset($guide) && $guide->experience)
                                    <div class="about-me">
                                        <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">My experience</h5>
                                        <div class="content-aboutme font-size-14 color2c353d">
                                            {{$guide->experience}}
                                        </div>
                                    </div>
                                @endif
                                <!--MY Diploma-->
                                @if(isset($guide) && $guide->diploma_info)
                                    <div class="about-me">
                                        <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">My diploma</h5>
                                        <div class="content-aboutme font-size-14 color2c353d">
                                            {{$guide->diploma_info}}
                                        </div>
                                    </div>
                                @endif
                                <!--MY HOBBY-->
                                @if(isset($user) && $user->hobby)
                                    <div class="about-me">
                                        <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">My hobby</h5>
                                        <div class="content-aboutme font-size-14 color2c353d">
                                            {{$user->hobby}}
                                        </div>
                                    </div>
                                @endif
                                <!--MY LANGUAGE-->
                                @if(isset($knownLanguage) && count($knownLanguage))
                                    <div class="about-me aboutlanguage">
                                        <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">Language</h5>
                                        <?php $i=0; ?>
                                        <div class="content-aboutme font-size-14 color2c353d">
                                            @foreach($knownLanguage as $langua)
                                                @if($i==0)
                                                    <span class="font-size-14 color2c353d" style="float: left;">@if($langWorld = $langua->language()->first()){{$langWorld->name}} @endif</span>
                                                @else
                                                    <span class="font-size-14 color2c353d" style="float: left;">, @if($langWorld = $langua->language()->first()){{$langWorld->name}} @endif</span>
                                                @endif
                                                <?php $i++; ?>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div> <!-- end contentprofile-item -->
                            
                            <div class="contentprofile-item boxShadow15 contentlocation">
                                <!--LOCATION ACTIVE-->
                                @if(isset($guide_locations) && count($guide_locations))
                                    <div class="location-active">
                                        <h5 class="title-aboutme titleprofile-mar color4e5d6b font-size-14 fnt-weight">Location active</h5>
                                        <!--Plane-->
                                        <div class="content-profile-item">
                                            @foreach($guide_locations as $location)
                                                <?php
                                                $city = $location->cities()->first();
                                                $country = $location->countries()->first();
                                                ?>
                                                <div class="col-md-6 col-sm-6 col-xs-6 item-services font-size-14 color4e5d6b content-location">
                                                    {{$country->country_name}} - {{$city->city_name}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div> <!-- end contentprofile-item -->

                            <!--PROMOTION-->
                            <div class="contentprofile-item boxShadow15">
                                <h5 class="title-aboutme titleprofile-mar color4e5d6b font-size-14 fnt-weight">Promotion</h5>
                                <div class="price-profile">
                                    <table id="mytable" class="table table-striped table-bordered">
                                        <thead class="thead-edit-price font-size-14">
                                        <tr>
                                            <th class="font-size-12 color4e5d6b countuser">Number of passengers(person)</th>
                                            <th class="font-size-12 color4e5d6b">Time (h)</th>
                                            <th class="font-size-12 color4e5d6b">Money (usd)</th>
                                        </tr>
                                        </thead>

                                        <tbody class="edit-price font-size-14 color4e5d6b">
                                        <tr class="no-price">
                                            <td colspan="3">No price</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end contentprofile-item -->
                        </div> <!-- end content profile -->
                    </div> <!-- end content-profile-left -->

                    <!-- SERVICE -->
                    <div class="col-lg-4 content-profile--thumb end content-profile-right col-right">
                        <!--GUIDE CARD INFORMATION-->
                        @if(isset($certification) && $certification)
                        <div class="contentprofile-item boxShadow15 contentcard">
                            <div class="tour-guide">
                                <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">Guide card information</h5>
                                <div class="content-profile-item content-profile-card font-size-14">
                                    <div class="col-md-12 color4e5d6b item-services">
                                        ID card: <span class="color263038-1">{{$certification->idcard}}</span>
                                    </div>
                                    <div class="col-md-12 color4e5d6b item-services">
                                        Card type: <span class="color263038-1">{{$certification->type}}</span>
                                    </div>
                                    @if($certification->type=="location")
                                    <div class="col-md-12 color4e5d6b item-services">
                                        At location: <span class="color263038-1">{{$certification->location_allowed}}</span>
                                    </div>
                                    @endif
                                    <div class="col-md-12 color4e5d6b item-services">
                                        Place of issue: <span
                                                class="color263038-1">{{$certification->place_of_issue}}</span>
                                    </div>
                                    <div class="col-md-12 color4e5d6b item-services">
                                        Expiration date: <span
                                                class="color263038-1">{{$certification->expiry_date}}</span>
                                    </div>
                                    <?php
                                    if ($certification->cert_lang)
                                        $lang_id = explode(",", $certification->cert_lang);
                                    ?>
                                    <div class="col-md-12 color4e5d6b item-services lang-serv">
                                        <span>Language:&nbsp;</span>
                                        @if(isset($lang_id) && count($lang_id))
                                            <?php $i = 0; ?>
                                            @foreach($lang_id as $lang_id)

                                                <?php $lang_name = $certification->nameLanguage($lang_id)->name;?>
                                                @if($i==0)
                                                    <span class="color263038-1"> {{$lang_name}}</span>
                                                @else
                                                    <span class="color263038-1">, {{$lang_name}}</span>
                                                @endif
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </div>
                                    
                                </div>
                            </div>
                        </div> <!-- end contentprofile-item -->
                        @endif

                        <!--MY SERVICES-->
                        @if(isset($guideServices) && count($guideServices))
                        <div class="contentprofile-item boxShadow15">
                            <div class="my-services">
                                <h5 class="title-aboutme titleprofile-mar color4e5d6b font-size-14 fnt-weight">My services</h5>
                                <!--Plane-->
                                <div class="content-profile-item">
                                    @foreach($guideServices as $service)
                                        <?php
                                        $serviceJoin = $service->service()->first();
                                        ?>
                                        <div class="col-md-6 col-sm-6 col-xs-6 item-services">
                                            <i class="{{$serviceJoin->icon}} color4e5d6b" aria-hidden="true"></i><span class="font-size-14 color263038-1">@if($serviceJoin && $slang = $serviceJoin->serviceLang()->where('language_code','=','en')->first()){{$slang->service_name}}@endif</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div> <!-- end contentprofile-item -->
                        @endif
                        
                        <!--TRANSPORTATION-->
                        @if(isset($guidetransportations) && count($guidetransportations))
                        <div class="contentprofile-item boxShadow15 contenttransportation">
                            <div class="transportation">
                                <h5 class="title-aboutme color4e5d6b font-size-14 fnt-weight">Transportation</h5>
                                <!--Plane-->
                                <div class="content-profile-item">

                                    @foreach($guidetransportations as $service)
                                        <?php
                                        $serviceJoin = $service->service()->first();
                                        ?>
                                        <div class="col-md-6 col-sm-6 col-xs-6 item-services">
                                            <i class="{{$serviceJoin->icon}} color4e5d6b" aria-hidden="true"></i><span class="font-size-14 color263038-1">@if($serviceJoin && $slang = $serviceJoin->serviceLang()->where('language_code','=','en')->first()){{$slang->service_name}}@endif</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div> <!-- end contentprofile-item -->
                        @endif
                    </div> <!-- end content-profile-right -->

                    <!-- REVIEW + PROFILE -->
                    <div class="col-lg-8 content-profile--thumb content-profile--left">
                        

                        @if(isset($ratings) && count($ratings))
                        <div class="contentprofile-item boxShadow15 contentreivew">
                            <!-- code html Review guide -->                                
                            <h5 class="title-aboutme titleprofile-mar color4e5d6b font-size-14 fnt-weight title-gallery--review">Reviews guide({{count($ratings)}})</h5>
                            <div class="list-review list-review--border">
                                @foreach($ratings as $rating)
                                    @include('default::frontend.guide.detailReview')
                                @endforeach
                            </div>
                            <!-- end list-review -->
                        </div> <!-- end contentprofile-item -->
                        @endif

                        <!--My Gallery NEWS-->
                        <?php
                            $galeries = $guide->galleries()->orderBy('created_at', 'desc')->get();
                        ?>
                        @if(isset($galeries) && count($galeries))
                        <div class="contentprofile-item boxShadow15 contentgallery">
                            <div class="list-gallery">
                                <h5 class="title-aboutme titleprofile-mar color4e5d6b font-size-14 fnt-weight">Gallery</h5>
                                <div class="content-gallery">
                                    <div class="content-gallery--thumb">
                                        <div class="" id="custom-layout-demo">
                                            <?php $i = 1 ?>
                                            @foreach($galeries as $galery)
                                                <div class="col-md-3 col-sm-4 col-xs-4 item-gallery column-itemGallery">
                                                    <div class="div-wrapper-itemGallery">
                                                        <div class="div-pad-profileGallery"></div>
                                                        <a title="" href="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}" target="_blank" class="bg-hover-img div-inner-itemGallery bg-property"
                                                             style="background-image: url('{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}');">
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php $i++; ?>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end contentprofile-item -->
                        @endif
                    </div> <!-- end content profile -->
                </div> <!-- end content-profile-left -->
                    

            </div>

        </div>
    </div>





@if(false)
    <!-- code old -->
    <div class="content-site" data-id="{{$user->id}}">
        <div class="bg-profile">
            <div class="cover-images-abc">
                <div class="@if(isset($owner) && $owner && $owner->id == $user->id) cropme @endif img-cover transition bg-property"
                     style="width: 100%; background-image: url(@if($guide && $guide->cover)'{{Helper::getThemeAvatar($guide->cover).'?id='.Helper::versionImg()}}') @else'{{Helper::getThemeImg('cover.jpg')}}')@endif">
                    <a href="javascript:void(0)" class="change-coverImage"><i class="fa fa-camera"
                                                                              aria-hidden="true"></i>Change
                        cover</a>
                </div>
                {{--<div class="abcsss"></div>--}}
            </div>
            <div class="container">
                <div class="content-top-info">
                    <div class="cover-images">

                        <div class="row">
                            <div class="col-lg-12 info-profile">
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 thumb-avataProfile">
                                    <div class="@if(isset($owner) && $owner && $owner->id==$user->id) cropme @endif avatar-profile transition bg-property"
                                         style="width: 200px; max-width: 200px; height: 200px; margin-right: 0 ; @if(isset($user) && $user->avatar) background-image: url('{{Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg()}}') @else background-image: url('{{Helper::getThemeImg('user.png')}}');background-repeat: no-repeat; background-size: cover; @endif">
                                        <a href="javascript:void(0)" title="Change avatar" class="change-avatar font-size-14 fnt-weight color4"><i class="fa fa-camera"
                                                                                                   aria-hidden="true"></i>Change
                                            avatar</a>
                                    </div>

                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-9 status--thumb">
                                    <div class="name-profile color333 font-size-18 fnt-weight">
                                        <p>@if(isset($user)){{$user->full_name}} @endif</p>
                                    </div>
                                    <div class="rating-star rating-star--thumb">
                                        @for($i=1; $i<= $guide->star; $i++)
                                            <i class="fa fa-star yellow" aria-hidden="true"></i>
                                        @endfor
                                        @for($i=4; $i >= $guide->star; $i--)
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        @endfor
                                    </div>
                                    <?php
                                    $canEditMood = isset($owner) && $owner && $owner->id == $user->id;
                                    ?>
                                    <div id="Status" @if($canEditMood)class="mood-profile"@endif>
                                        @if($canEditMood)<i class="fa fa-pencil edit-status"
                                                            aria-hidden="true"></i>@endif<i data-mood="1"
                                                                                            class="truncate font-size-14 color666"
                                                                                            title="{{$user->slogan}}">{{$user->slogan?$user->slogan:'no slogan'}}</i>

                                        @if($canEditMood)<input class="mood-input font-size-14"
                                                                style="display: none;width: 100%" type="text"
                                                                data-id="{{$user->id}}">@endif
                                    </div>

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 button-profile">
                                    <div class="profilePrice--thumb">
                                        {{--<div class="birthday-profile font-size-14">
                                            <i class="fa fa-birthday-cake" aria-hidden="true"></i> @if(isset($user)){{$user->birth_date}} @endif
                                        </div>
                                        <div class="phone-profile">
                                            <i class="fa fa-phone" aria-hidden="true"></i>@if(isset($user)){{$user->phone_number}} @endif
                                        </div>--}}
                                        <div class="price-profile-info font-size-24 fnt-weight colorBlue"><i
                                                    class="fa fa-usd"
                                                    aria-hidden="true"></i>@if(isset($guide)){{$guide->hourly_rate}} @endif
                                            /h
                                        </div>
                                    </div>
                                    @if(!$isGuide)
                                        <div class="btn-book-info">
                                            <a href="{{Helper::url('booking/'.$user->id)}}"
                                               class="btn btn-secondary btn-profile font-size-14 color4 fnt-weight bgBlue">
                                                <!-- <button type="button" class="btn btn-secondary btn-profile font-size-14 color4 fnt-weight bgBlue" data-dismiss="modal"><i class="fa fa-calendar-o font-size-16 fnt-weight1" aria-hidden="true"></i>BOOK ME</button> -->
                                                <i class="fa fa-calendar-o font-size-16 fnt-weight1"
                                                   aria-hidden="true"></i>BOOK ME
                                            </a>
                                        </div>
                                    @endif
                                    @if(0)
                                        <div class="btn-chat-info">
                                            <button type="button" class="btn btn-secondary btn-profile"
                                                    data-dismiss="modal"><i class="fa fa-comment"
                                                                            aria-hidden="true"></i>CHAT
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row content-bottom-info">
                    <div class="col-lg-12 content-profile--thumb">
                        <div class="content-profile">
                            <!--ABOUT ME-->
                            @if(isset($guide) && $guide->introduce)
                                <div class="about-me">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight title-aboutme-pad">
                                            About me</h5>
                                        <div class="content-aboutme font-size-14 color666">
                                            {{$guide->introduce}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($guideServices) && count($guideServices))
                            <!--MY SERVICES-->
                                <div class="my-services">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">My Services</h5>
                                        <!--Plane-->
                                        <div class="content-profile-item">
                                            @foreach($guideServices as $service)
                                                <?php
                                                $serviceJoin = $service->service()->first();
                                                ?>
                                                <div class="col-md-2 col-sm-3 col-xs-4 item-services">
                                                    <i class="{{$serviceJoin->icon}}"
                                                       aria-hidden="true"></i><span
                                                            class="font-size-14 color666">@if($serviceJoin && $slang = $serviceJoin->serviceLang()->where('language_code','=','en')->first()){{$slang->service_name}}@endif</span>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($guidetransportations) && count($guidetransportations))
                            <!--TRANSPORTATION-->
                                <div class="transportation">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">Transportation</h5>
                                        <!--Plane-->
                                        <div class="content-profile-item">

                                            @foreach($guidetransportations as $service)
                                                <?php
                                                $serviceJoin = $service->service()->first();
                                                ?>
                                                <div class="col-md-2 col-sm-3 col-xs-4 item-services">
                                                    <i class="{{$serviceJoin->icon}}"
                                                       aria-hidden="true"></i><span
                                                            class="font-size-14 color666">@if($serviceJoin && $slang = $serviceJoin->serviceLang()->where('language_code','=','en')->first()){{$slang->service_name}}@endif</span>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(isset($guide_locations) && count($guide_locations))
                            <!--LOCATION ACTIVE-->
                                <div class="location-active">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">Location Active</h5>
                                        <!--Plane-->
                                        <div class="content-profile-item">
                                            @foreach($guide_locations as $location)
                                                <?php
                                                $city = $location->cities()->first();
                                                $country = $location->countries()->first();
                                                ?>
                                                <div class="col-md-3 col-sm-4 col-xs-6 item-services font-size-14 color666 content-location">
                                                    {{$country->country_name}} - {{$city->city_name}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        <!--MY EXPERIENCE-->
                            @if(isset($guide) && $guide->experience)
                                <div class="my-experience">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">My experience</h5>
                                        <div class="experience-content font-size-14 color666">
                                            {{$guide->experience}}
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <!--MY Diploma-->
                            @if(isset($guide) && $guide->diploma_info)
                                <div class="my-experience">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">My diploma</h5>
                                        <div class="experience-content font-size-14 color666">
                                            {{$guide->diploma_info}}
                                        </div>
                                    </div>
                                </div>
                            @endif

                        <!--MY HOBBY-->
                            @if(isset($user) && $user->hobby)
                                <div class="my-experience">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">My hobby</h5>
                                        <div class="experience-content font-size-14 color666">
                                            {{$user->hobby}}
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(isset($certification) && $certification)
                            <!--TOUR GUIDE ID CARD-->
                                <div class="tour-guide">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">Guide Card
                                            Information</h5>
                                        <div class="content-profile-item content-profile-card font-size-14">
                                            <div class="col-md-6 col-sm-6 col-xs-12 color666 item-services">
                                                ID card: <span class="color333">{{$certification->idcard}}</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 color666 item-services">
                                                Expiration date: <span
                                                        class="color333">{{$certification->expiry_date}}</span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 color666 item-services">
                                                Card type: <span class="color333">{{$certification->type}}</span>
                                            </div>
                                            {{--<div class="col-md-6 col-sm-6 col-xs-12 color666 item-services">--}}
                                                {{--Status: <span class="color333">{{$certification->status}}</span>--}}
                                            {{--</div>--}}
                                            <?php
                                            if ($certification->cert_lang)
                                                $lang_id = explode(",", $certification->cert_lang);
                                            ?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 color666 item-services lang-serv">
                                                <span>Language:&nbsp;</span>
                                                @if(isset($lang_id) && count($lang_id))
                                                    <?php $i = 0; ?>
                                                    @foreach($lang_id as $lang_id)

                                                        <?php $lang_name = $certification->nameLanguage($lang_id)->name;?>
                                                        @if($i==0)
                                                            <span class="color333"> {{$lang_name}}</span>
                                                        @else
                                                            <span class="color333">, {{$lang_name}}</span>
                                                        @endif
                                                        <?php $i++; ?>
                                                    @endforeach
                                                @endif
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 color666 item-services">
                                                Place of issue: <span
                                                        class="color333">{{$certification->place_of_issue}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        <!--MY LANGUAGE-->
                            @if(isset($knownLanguage) && count($knownLanguage))
                                <div class="my-language">
                                    <div class="border-profile-item">
                                        <h5 class="title-aboutme color666 font-size-14 fnt-weight">Language</h5>
                                        @foreach($knownLanguage as $langua)
                                            <div class="col-md-2 col-sm-3 col-xs-4 thumb-myLanguage item-services font-size-14 color666">@if($langWorld = $langua->language()->first()){{$langWorld->name}} @endif</div>
                                        @endforeach
                                    </div>
                                </div>
                        @endif

                        <!--Price-->
                            <div class="price-profile">
                                <h5 class="title-aboutme color666 font-size-14 fnt-weight">Promotion</h5>
                                <table id="mytable" class="table table-striped table-bordered">
                                    <thead class="thead-edit-price font-size-14">
                                    <tr>
                                        <th class="font-size-14 color666">Number of passengers(person)</th>
                                        <th class="font-size-14 color666">Time (h)</th>
                                        <th class="font-size-14 color666">Money (usd)</th>
                                    </tr>
                                    </thead>

                                    <tbody class="edit-price font-size-14">
                                    <tr class="no-price">
                                        <td colspan="3">No price</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 sidebar-profile--thumb">
                        <!-- code html Review guide -->
                        @if(isset($ratings) && count($ratings))
                            <h5 class="title-gallery color333 font-size fnt-weight title-gallery--review">Reviews of
                                this guide</h5>
                            <div class="list-review list-review--border">
                                @foreach($ratings as $rating)
                                    @include('default::frontend.guide.detailReview')
                                @endforeach
                            </div>
                        @endif

                        <!-- end list-review -->

                        <!--My Gallery OLD-->
                        @if(false)
                            @if(isset($galeries) && count($galeries))
                                <div class="list-gallery">
                                    <h5 class="title-gallery color333 font-size fnt-weight">Gallery</h5>
                                    <div class="content-gallery">
                                        <div class="content-gallery--thumb">
                                            <?php $i = 1 ?>
                                            @foreach($galeries as $galery)
                                                <div class="col-md-2 col-sm-3 col-xs-4 item-gallery column-itemGallery">
                                                    <div class="div-wrapper-itemGallery">
                                                        <div class="div-pad-itemGallery"></div>
                                                        <div class="bg-hover-img div-inner-itemGallery bg-property"
                                                             style="background-image: url('{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}');"
                                                             onclick="openModal();currentSlide({{$i}})">
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $i++; ?>
                                            @endforeach
                                        </div>
                                    {{--<ul>
                                        @foreach($galeries as $galery)
                                            <li class="col-md-4 item-gallery">
                                                <img src="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}"
                                                     height="200" alt="">
                                            </li>
                                        @endforeach
                                    </ul>--}}

                                    <!-- SLIDE SHOW GALLERY -->
                                        <div id="myModalGallery" class="modal modalgallery">
                                            <span class="close cursor" onclick="closeModal()">&times;</span>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-12 modal-content">
                                                        @foreach($galeries as $galery)
                                                            <div class="mySlides col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                                                                <div class="numbertext"></div>
                                                                <img src="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}"
                                                                     style="width:100%" alt="{{$galery->url}}">
                                                            </div>
                                                        @endforeach
                                                        <a class="prev" onclick="plusSlides(-1)"><i class="fa fa-angle-left"
                                                                                                    aria-hidden="true"></i></a>
                                                        <a class="next" onclick="plusSlides(1)"><i class="fa fa-angle-right"
                                                                                                   aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif

                        <!--My Gallery NEWS-->
                        <?php
                            $galeries = $guide->galleries()->orderBy('created_at', 'desc')->get();
                        ?>
                        @if(isset($galeries) && count($galeries))
                            <div class="list-gallery">
                                <h5 class="title-gallery color333 font-size fnt-weight">Gallery</h5>
                                <div class="content-gallery">
                                    <div class="content-gallery--thumb">
                                        <div class="" id="custom-layout-demo">
                                            <?php $i = 1 ?>
                                            @foreach($galeries as $galery)
                                                <div class="col-md-2 col-sm-3 col-xs-4 item-gallery column-itemGallery">
                                                    <div class="div-wrapper-itemGallery">
                                                        <div class="div-pad-itemGallery"></div>
                                                        <a href="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}" target="_blank" class="bg-hover-img div-inner-itemGallery bg-property"
                                                             style="background-image: url('{{Helper::getThemeGallery('gallery' . $user->id.'/'.$galery->url)}}');">
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php $i++; ?>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endif
@endif

@stop
@section('js')
    <script type="text/javascript">
        @if(isset($discounts))
            valuePrice = JSON.parse('<?php echo $discounts;?>');
        if (valuePrice) {
            table.insertIntoTable();
            $('.btn-delete-price').remove();
            $('.btn-edit-price').remove();
            $('.no-price').remove();
        }
        $(window).on('load resize', function () {
            var heightCover = $(window).width() / 3.036;
            if ($(window).width() < 1370) {
                heightCover = $(window).width() / 3.036;
            }
            if ($(window).width() < 991) {
                heightCover = $(window).width() / 2.9;
            }
            if ($(window).width() <= 640) {
                heightCover = $(window).width() / 2.5;
            }
            if ($(window).width() < 480) {
                heightCover = $(window).width() / 2;
            }
            $('.cover-images-abc').css('min-height', heightCover);
            $('.cover-images-abc .img-cover').css('height', heightCover);
        });

        @endif
    </script>
@stop

