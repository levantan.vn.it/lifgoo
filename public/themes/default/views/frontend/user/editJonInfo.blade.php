<div class="tab-pane edit-guide" id="edit-job-info">
    <div class="row">
        {!!Form::open(['url'=>Helper::url('editJobInfo'),'method'=>'post','class'=>'edit-user']) !!}
        {{--Local activities--}}
        <div class="col-md-12 frm_edit frm_editJobLocal">
            <label class="color666 fnt-weight font-size-14 lable-marL" for="local_activities">Local activities <span
                        class="color666">*</span></label>
            <?php
            $i = 0;
            ?>
            @if(isset($guide_locations) && count($guide_locations))
                @foreach($guide_locations as $location)
                    <div class="div-add-price">
                        <div>
                            
                            <div class="frm_edit frm_editJobCountry">
                                <select placeholder="{{trans('site.please_select_country')}}" class="selectpicker form-control select-country" data-style="btn-primary"
                                        name="country_id[]" data-url="{{Helper::url('admin/cities')}}" required>
                                    @if($countries)
                                        @foreach($countries as $country)
                                            <option value="{{$country->country_id}}"
                                                    @if($location && $location->countries()->first()->country_id == $country->country_id )selected @endif>{{$country->country_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="frm_edit frm_editJobCity">
                                <select class="selectpicker form-control select-city" data-style="btn-primary"
                                        name="city_id[]" required>
                                    <?php $country = $location->countries()->first();
                                    $cities = $country->cities()->get();
                                    ?>
                                    @if($location)
                                        @foreach($cities as $city)
                                            <option value="{{$city->city_id}}"
                                                    @if($location->city_id ==$city->city_id)selected @endif>
                                                {{$city->city_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="icon_add_remove">
                                @if($i==0)
                                    <i class="fa fa-plus click-add font-size-18" aria-hidden="true"></i>
                                    <?php $i++;?>
                                @else
                                    <i class="fa fa-minus click-remove font-size-18" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>
                    </div>

                @endforeach
            @else
                <div class="div-add-price">
                    <div>
                        
                        <div class="frm_edit frm_editJobCountry">
                            <select class="selectpicker form-control select-country"
                                    data-style="btn-primary" name="country_id[]"
                                    data-url="{{Helper::url('admin/cities')}}" required>
                                @if(true)
                                    <option value=""
                                            @if(!$user)selected @endif disabled selected>{{trans('site.please_select_country')}}
                                    </option>
                                @endif
                                @if($countries)
                                    @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                    @endforeach
                                @endif
                            </select>

                        </div>

                        <div class="frm_edit frm_editJobCity">
                            <select class="selectpicker form-control select-city"
                                    data-style="btn-primary" name="city_id[]" required>
                                <?php
                                    $cities = \Modules\User\Entities\CityEntity::where('country_id',$countries[0]->country_id)->get();
                                ?>
<!--                                     @if(count($cities))
                                        @foreach($cities as $city)
                                            <option value="{{$city->city_id}}">
                                                {{$city->city_name}}</option>
                                        @endforeach
                                    @endif -->
                            </select>
                        </div>

                        <div class="icon_add_remove">
                            <i class="fa fa-plus click-add font-size-18" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        {{--Language--}}
        <div class="col-md-12 frm_edit frm_editJobLanguage">
            <div class="editJobLanguage--thumb">
                <label class="color666 fnt-weight font-size-14 lable-marL" for="old_password">Language<span
                            class="color666">*</span></label>
                <select class="selectpicker" multiple name="languages[]" required>
                    @if(isset($languagesWorld) && $languagesWorld)
                        @foreach($languagesWorld as $langs)
                            <option value="{{$langs->id}}"
                                    @if(isset($knownLanguage) && in_array($langs->id,$knownLanguage)) selected @endif>{{$langs->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="editJobHour--thumb">
                <label class="color666 fnt-weight font-size-14 lable-marL" for="hourly_rate">Hourly Rate<span class="color666">*</span></label>
                <div class="frm_editJobHour--thumb">
                        <span class="hourlyRate-icon font-size">$</span>
                        <input type="number" class="form-control"
                               @if(isset($guide)) value="{{$guide->hourly_rate}}" @else placeholder
                               ="usd/h" @endif required

                               name="hourly_rate" min="0" max="100" maxlength="3" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)"/>

                </div>
                <span class="carretUp"></span>
                <span class="carretDown"></span>
            </div>
        </div>

        {{--<div class="col-md-12 frm_edit">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <select class="selectpicker" multiple name="languages[]" required>
                    @if(isset($languagesWorld) && $languagesWorld)
                        @foreach($languagesWorld as $langs)
                            <option value="{{$langs->id}}"
                                    @if(isset($knownLanguage) && in_array($langs->id,$knownLanguage)) selected @endif>{{$langs->name}}</option>
                        @endforeach
                    @endif
                </select>

            </div>
        </div>

        <div class="col-md-12 frm_edit frm_editJobHour">
            <label class="color666 fnt-weight font-size lable-marL" for="guide_card_info">Hourly Rate<span
                        class="color666">*</span></label>
            <div class="col-md-10 col-md-offset-1 frm_editJobHour--thumb">
                    <!-- <span style="border-radius: 0;">$</span> -->
                    <input type="number" class="form-control"
                           @if(isset($guide)) value="{{$guide->hourly_rate}}" @else placeholder
                           ="usd/h" @endif required
                           name="hourly_rate" min="0" max="100"/>
            </div>
        </div>--}}

        {{--Guide Card Info--}}
        <div class="col-md-12 frm_edit frm_editJobCard">
            <label class="color666 fnt-weight font-size-14 lable-marL" for="guide_card_info">Guide Card Information</label>
            <div class="cardinfo-item">
                <div class="frm_edit">
                    <label class="color999 fnt-weight font-size-12" for="idcard">{{trans('site.id_card')}}</label>
                    <div class="inputError--thumb">
                        <input type="text" id="id-card" class="form-control font-size-14" placeholder="ID Card" name="idcard"
                           @if(isset($certification)) value="{{$certification->idcard}}" @endif requireds maxlength="50">
                    </div>
                </div>
                <div class="frm_edit">
                    <label class="color999 fnt-weight font-size-12" for="expiry_date">{{trans('site.date_card')}}</label>
                    <input type="text" id="expiry_date" class="form-control datefilter-mindate font-size-14"
                           placeholder="Date Card"
                           @if(isset($certification)) value="{{$certification->expiry_date}}" @endif required name="expiry_date">
                </div>
                <div class="frm_edit">
                    <label class="color999 fnt-weight font-size-12" for="place_of_issue">{{trans('site.place_of_issue')}}</label>
                    <div class="inputError--thumb">
                        <input type="text" id="location-card" class="form-control font-size-14" placeholder="Place of issue"
                           name="place_of_issue"
                           @if(isset($certification)) value="{{$certification->place_of_issue}}" @endif maxlength="200">
                    </div>
                </div>
                <div class="frm_edit">
                    {{--<input type="text" id="city-card" class="form-control" placeholder="City Card" required>--}}
                    <label class="color999 fnt-weight font-size-12" for="type">{{trans('site.International')}}</label>
                    <select id="type" class="selectpicker form-control"
                            data-style="btn-primary" name="type" required>
                        <option value="international"
                                @if(isset($certification) && $certification->type =='internation') selected @endif>
                            International
                        </option>
                        <option value="domestic"
                                @if(isset($certification) && $certification->type =='domestic') selected @endif>
                            Domestic
                        </option>
                        <option value="location"
                                @if(isset($certification) && $certification->type =='location') selected @endif>
                            Location
                        </option>
                    </select>
                </div>


                {{--<div class="frm_edit">--}}
                    {{--<label class="color999 fnt-weight font-size-12" for="status">{{trans('site.active')}}</label>--}}
                    {{--<select id="status" class="selectpicker form-control" data-style="btn-primary" required name="status">--}}
                        {{--<option @if(isset($certification) && $certification->status == 'active') selected @endif>Active--}}
                        {{--</option>--}}
                        {{--<option @if(isset($certification) && $certification->status == 'inactive') selected @endif>--}}
                            {{--Inactive--}}
                        {{--</option>--}}
                    {{--</select>--}}
                {{--</div>--}}

                <div class="frm_edit">
                    <label class="color999 fnt-weight font-size-12" for="cert_lang[]">{{trans('site.language')}}</label>
                    <select class="selectpicker" multiple name="cert_lang[]">
                        @if(isset($languagesWorld) && $languagesWorld)
                            @foreach($languagesWorld as $langs)
                                <option value="{{$langs->id}}"
                                        @if(isset($certification) && in_array($langs->id,explode(",",$certification->cert_lang))) selected @endif>{{$langs->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="location_allowed_container col-md-12">
                    <label class="color999 fnt-weight font-size-12" for="location_allowed">{{trans('site.location')}}</label>
                    <textarea id="location_allowed" name="location_allowed" maxlength="200">@if(isset($certification)){{$certification->location_allowed}}@endif</textarea>
                </div>
            </div>
        </div>

        <div class="col-md-12 experience--thumb">
            <div class="experience--content">
                {{--Experience--}}
                <div class="col-md-6 frm_edit">
                    <label class="color666 fnt-weight font-size-14 lable-marR" for="experience">Experience <span class="color666">*</span></label>
                    <textarea name="experience" maxLength="10000" class="form-control font-size-14" id="experience" rows="6"
                              required>@if(isset($guide)){{$guide->experience}}@endif</textarea>
                </div>

                {{--Diploma--}}
                <div class="col-md-6 frm_edit">
                    <label class="color666 fnt-weight font-size-14 lable-marR" for="diploma_info">Diploma <span
                                class="color666">*</span></label>
                    <textarea name="diploma_info" class="form-control font-size-14" id="diploma_info" rows="6"
                              required maxlength="200">@if(isset($guide)){{$guide->diploma_info}}@endif</textarea>
                </div>
            </div>
        </div>

        {{--Service--}}
        <div class="col-md-12 frm_edit frm_editService">
            <label class="color666 fnt-weight font-size-14 lable-marL" for="old_password">Service</label>
            <div class="edit-service">
                @if(isset($services) && $services)
                    @foreach($services as $service)
                        <div class="form-check-label frm-check col-md-4 col-sm-6 col-xs-6">
                            <input class="form-check-input checkHelp" type="checkbox" name="service_id[]" value="{{$service->id}}" id="{{$service->id}}" @if(isset($guideServices)){{Helper::checkInArray($guideServices,$service->id,'service_id')}}@endif><label class="checkbox-inline font-size-14" for="{{$service->id}}"><span></span><span class="helpyou-item">@if($service_lang = $service->serviceLang()->whereLanguage_code(\LaravelLocalization::getCurrentLocale())->first()) {{$service_lang->service_name}}@endif</span></label>


                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        {{--Transportion--}}
        <div class="col-md-12 frm_edit frm_editTran">
            <label class="color666 fnt-weight font-size-14 lable-marL" for="old_password">Transportion</label>
            <div class="edit-transportion">
                @if(isset($transportations))
                    @foreach($transportations as $transportation)
                        <div class="form-check-label frm-check col-md-4 col-sm-6 col-xs-6">
                            <input class="form-check-input checkHelp" type="checkbox" name="transportation_id[]" value="{{$transportation->id}}" id="{{$transportation->id}}" @if(isset($guidetransportations)){{Helper::checkInArray($guidetransportations,$transportation->id,'service_id')}}@endif><label class="checkbox-inline font-size-14" for="{{$transportation->id}}"><span></span>@if($transportation_lang = $transportation->serviceLang()->whereLanguage_code(\LaravelLocalization::getCurrentLocale())->first()) {{$transportation_lang->service_name}}@endif</label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        {{--Button Update--}}
        <div class="col-md-12 btnUpdate--thumb">
            <button type="submit" class="btn btn_update btn-edit-submit font-size-14 fnt-weight color4 bgBlue">Update</button>
        </div>
        {!!Form::close() !!}
    </div>
</div>