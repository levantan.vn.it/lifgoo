
@extends('default::frontend.layouts.master')
@section('css')
    <style>
        .datepicker{
            display: block!important;
        }
    </style>
@stop
@section('content')
    <div class="content-site content--minheight">
        <div class="cnt-site-booking">
            <div class="bg-booking-guide">
                <div class="container">
                    <div class="row">
                        {!!Form::open(['url'=>Helper::url('guide/booking/add'),'method'=>'post','id'=>'msform']) !!}
                        {{--<form id="msform">--}}
                        <div class="col-md-12 progressbar-thumb">                 
                            <!-- progressbar -->
                            <ul id="progressbar" class="col-md-10 col-md-offset-1">
                                <li class="active"><span class="font-size-14 color4e5d6b-op">1. Trip Plan</span></li>
                                <li class=""><span class="font-size-14 color4e5d6b-op">2. Contact details</span></li>
                                <li class=""><span class="font-size-14 color4e5d6b-op">3. Finish</span></li>
                            </ul>
                        </div>
                        <div class="col-md-12">       
                            <!-- fieldsets 1 -->
                            <fieldset class="item-infoBooking item-infoBooking--1">
                                <h2 class="title-book-with font-size color263038-1 fnt-weight">Booking with @if(isset($userGuide)){{$userGuide->full_name}}@endif</h2>
                                <div class="col-sm-12 fieldset1-thumb">
                                    <div class="col-md-8 fieldset1thumb-left">
                                        <div class="fieldset1-left">
                                            <ul class="list-infoBooking">
                                                <li class="info-country">
                                                    <!-- Country -->
                                                    <div class="form-group frm_edit">
                                                        <label for="country" class="font-size-14 color263038 fnt-weight">Country <span class="important-star">*</span></label>
                                                        <select class="form-control selectpicker" id="country" required name="country">
                                                            @foreach($guide->getAvailableCountries() as $country)
                                                                <option @if(isset($search_city)&&($country->country_id==$search_city->country_id)) selected @endif value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </li>
                                                <li class="info-city">
                                                    <!-- City -->
                                                    <div class="form-group frm_edit">
                                                        <label for="city" class="font-size-14 color263038 fnt-weight">City <span class="important-star">*</span></label>
                                                        <select class="form-control selectpicker" id="city" required name="city">
                                                            @foreach($guide->getAvailableCities() as $city)
                                                                @if(isset($search_city))
                                                                    @if($city->country_id==$search_city->country_id)
                                                                    <option @if($search_city->city_id==$city->city_id) selected @endif value="{{$city->city_id}}">{{$city->city_name}}</option>
                                                                    @endif
                                                                @else
                                                                    <option value="{{$city->city_id}}">{{$city->city_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </li>
                                                <li class="info-pickup">
                                                    {{--Pickup placement--}}
                                                    <div class="form-group frm_edit">
                                                        <label for="pickup-placement" class="font-size-14 color263038 fnt-weight">Pickup placement <span class="important-star">*</span></label>
                                                        <input type="text" class="form-control" id="pickup-placement" placeholder="" required maxlength="500" name="pickup_place">
                                                    </div>
                                                </li> 
                                                <li class="info-destination">
                                                    {{--Destination--}}
                                                    <div class="form-group frm_edit">
                                                        <label for="destination" class="font-size-14 color263038 fnt-weight">Destination <span class="important-star">*</span></label>
                                                        <input type="text" class="form-control" id="destination" placeholder="" required maxlength="500" name="destination">
                                                    </div>
                                                </li>
                                                <li class="info-people">
                                                    {{--Number of people--}}
                                                    <div class="cnt-number-of-people">
                                                        <div class="hours-book color10">
                                                            <span class="font-size-14"></span>
                                                        </div>

                                                        <div class="content-number-people">
                                                            <div class="form-group input-number-people">
                                                                <label for="numberPeople" class="font-size-14 color263038 fnt-weight">People <span class="important-star">*</span></label>
                                                                {{--<input id="numberPeople" type="number" class="form-control font-size-14 input-numberPeople" placeholder="" name="capacity" min="1" max="1000" maxlength="4" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)">--}}

                                                                <input id="numberPeople" type="number" class="form-control font-size-14 input-numberPeople" placeholder="" name="capacity" min="1" max="1000" maxlength="4"  oninput="maxLengthCheck(this)" value="1">
                                                                <span class="carretUp"></span>
                                                                <span class="carretDown"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="col-md-6 col-sm-6 col-xs-6 step-one calendarBooking--thumb">
                                                <div class="bg-form-booking">
                                                    <div class="calendar--thumb">
                                                        <div class="trip-dates">
                                                            <span class="font-size-14 color263038 fnt-weight stepOne-span">Trip dates</span>
                                                        </div>
                                                        <div class="table-bordered" id="sandbox-container">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> <!-- end calendarBooking--thumb -->
                                            <div class="col-md-6 col-sm-6 col-xs-6 BookingInfo--thumb">
                                                <div class="selecthour--thumb">
                                                    <!-- Number of hours -->
                                                    <div class="cnt-number-hours">
                                                    </div>
                                                    <div class="hours-book">
                                                        <span class="font-size-14 color263038 fnt-weight stepOne-span">Number of hours </span><span class="important-star color263038 fnt-weight font-size-14">&nbsp;*</span>
                                                    </div>
                                                    <div class="content-hours-book">
                                                    </div>
                                                </div>
                                            </div> <!-- end BookingInfo--thumb -->
                                        </div> <!-- end fieldset1-left -->
                                    </div> <!-- end col-md-8 -->
                                    <div class="col-md-4 fieldset1thumb-right">
                                        <div class="fieldset1-right">
                                            <ul class="content-booking-info">
                                                <li class="total-hrs-dates font-size-14 color263038-1">
                                                    <p><span class="color263038">Hour</span> 0</p>
                                                    <p><span class="color263038">Day</span> 0</p>
                                                    <p><span class="color263038">Person</span> 1</p>
                                                </li>
                                                <li class="total-booking font-size-24 fnt-weight color0084fb"><span class="color263038 font-size-14">Total price: </span><span class="price-icon">0.00</span></li>
                                            </ul>
                                        </div>                                           
                                    </div> <!-- end col-md-4 -->                                       
                                </div> <!-- end fieldset1-thumb -->
                                <input type="button" name="next" class="next next-step-one action-button font-size-12 color4 fnt-weight bgBlue" value="Next" />
                            </fieldset>

                            <!-- 2 -->
                            <fieldset class="item-infoBooking item-infoBooking--2">
                                <div class="col-sm-12 step-two content-stepTwo">
                                    <div class="col-md-8 col-sm-12 col-xs-12 aligncenter bg-form-booking bg-form-bookingTwo color10">
                                        <!-- <div class=""> -->
                                            <div class="col-sm-6 frm_edit frm-email">
                                                <label for="email" class="color4e5d6b-op font-size-14 fnt-weight">Email <span class="important-star">*</span></label>
                                                <input type="email" name="email" class="form-control color4e5d6b" placeholder="Email" required value="{{$user->email}}">
                                            </div>
                                            <div class="col-sm-6 frm_edit frm-phone">
                                                <label for="phone_number" class="color4e5d6b-op font-size-14 fnt-weight">Phone number <span class="important-star">*</span></label>
                                                <input type="text" name="phone_number" class="form-control color4e5d6b" placeholder="Phone Number" required maxlength="20" value="{{$user->phone_number}}">
                                            </div>
                                            <div class="col-md-12 frm_edit frm-message">
                                                <label class="color4e5d6b-op font-size14 fnt-weight" for="message">Message</label>
                                                <textarea class="font-size-14 color4e5d6b" id="message" name="message" rows="4" maxlength="10000" placeholder="Message"></textarea>
                                            </div>
                                        <!-- </div> -->
                                    </div>
                                    
                                </div>
                                <input type="button" class="font-size-12 previous action-button bgGreen1" value="Previous" />
                                <input type="button" class="font-size-12 next next-second-step action-button bgBlue" value="Next" />
                            </fieldset>

                            <!--3-->
                            <fieldset class="item-infoBooking item-infoBooking--3">
                                <div class="col-sm-12 step-three content-stepThree">
                                    <div class="col-md-8 fieldset3thumb-left">
                                        <div class="fieldset3-left">
                                            <!-- name -->
                                            <div class="name-history-booking guideName font-size-14">
                                                <i class="color4e5d6b-op">Guide: </i><span class="font-size-14 color4e5d6b spaninfo">@if(isset($userGuide)){{$userGuide->full_name}}@endif</span>
                                            </div>

                                            <!-- book date -->
                                            <div class="booking-date bookingDate-thumbs font-size-14">
                                                <i class="color4e5d6b-op">Booking dates:</i>
                                            </div>

                                            <div class="booking-location">
                                                <ul class="list-locationInfo">
                                                    <li class="font-size-14 title-booking-margin-bottom title-country"><i class="color4e5d6b-op">Country: </i><span class="spaninfo font-size-14 color4e5d6b"></span></li>
                                                    <li class="font-size-14 title-booking-margin-bottom title-city"><i class="color4e5d6b-op">City: </i><span class="spaninfo font-size-14 color4e5d6b"></span></li>
                                                    <li class="font-size-14 title-booking-margin-bottom title-place"><i class="color4e5d6b-op">Pickup placement: </i><span class="spaninfo font-size-14 color4e5d6b">23/33 Nguyen truong to</span></li>
                                                    <li class="font-size-14 title-destination title-booking-margin-bottom"><i class="color4e5d6b-op">Destination: </i><span class="font-size-14 color4e5d6b">Lăng tự đức</span></li>
                                                    <li class="font-size-14 title-booking-margin-bottom title-email"><i class="color4e5d6b-op">Contact email: </i><span class="spaninfo font-size-14 color4e5d6b"></span></li>
                                                    <li class="spaninfo font-size-14 title-booking-margin-bottom title-phone"><i class="color4e5d6b-op">Phone number: </i><span class="font-size-14 color4e5d6b"></span></li>
                                                    <li class="spaninfo font-size-14 title-message"><i class="color4e5d6b-op">Message: </i><br> <span class="font-size-14 color4e5d6b">Lăng tự đức</span></li>
                                                </ul>
                                            </div>
                                        </div> <!-- end fieldset3-left -->
                                    </div> <!-- end col-md-8 fieldset3thumb-left -->
                                    <div class="col-md-4 fieldset3thumb-right">
                                        <div class="fieldset3-right">
                                            <!-- booking info -->
                                            <div class="booking-stt">
                                                <ul class="list-right-statistical">
                                                    <li class="total-hrs-dates font-size-14 color263038-1">
                                                        <p><span class="color263038">Hour</span><span class="data-append"></span>0</p>
                                                        <p><span class="color263038">Day</span><span class="data-append"></span>0</p>
                                                        <p><span class="color263038">Person</span><span class="data-append"></span>1</p>
                                                    </li>
                                                    <li class="wrwt3 statusbook font-size-14 color263038-1">
                                                        <p><span class="color263038" style="float: left;">Status</span><span class="data-append itemStt-pending">Pending</span></p>
                                                    </li>
                                                    <li class="booking-price font-size-24 fnt-weight color0084fb"><span class="color263038 font-size-14">Total price: </span><span class="price-icon">0.00</span></li>
                                                </ul>
                                            </div>
                                        </div> <!-- end fieldset3-right -->
                                        <!-- button cancel -->
                                        <div class="step-right-top step-btnCancle stepCancle">
                                            <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-canceled font-size-12 color4" data-type="refresh_page">Cancel this booking</button>
                                        </div>
                                    </div> <!-- end col-md-4 fieldset3thumb-right -->
                                </div>
                                
                                <input type="button" class="font-size-12 previous action-button bgGreen1" value="Previous" />
                                <input type="submit" class="font-size-12 submit done-stepThree action-button bgBlue" value="Finish" data-guideid="{{$userGuide->id}}"/>
                            </fieldset>
                            
                        </div>
                        {!!Form::close() !!}
                    </div> <!-- end row -->
                </div>
            </div> <!-- end bg-booking-guide -->
        </div>
    </div>
    <div class="modal fade modal-custom" id="cancel-booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">Warning</div>
                <div class="modal-body">
                    <div class="cancel-request font-size-14">Are you sure you want to cancel this booking request?</div>
                    <div>
                        <div class="thumb-btnyn">
                            <button type="button" class="btn btn-secondary btn-request-yes">Yes</button>
                        </div>
                        <div class="thumb-btnyn">
                            <button type="button" class="btn btn-request-no" data-dismiss="modal">No</button>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        
        @if(isset($schedule))
            booking.setGuideschedules(JSON.parse('<?php echo $schedule;?>'));
        @endif
        @if(isset($bookedHours))
            booking.setBookedHours(JSON.parse('<?php echo $bookedHours;?>'));
        @endif
        @if(isset($discounts))
            valuePrice = JSON.parse('<?php echo $discounts;?>');
        @endif
        @if(isset($guide))
            booking.setHourlyRate('{{$guide->hourly_rate}}');
        @endif
        $(document).ready(function(){
            $('#city').change(function(){
                $('#sandbox-container').datepicker('update');
                $('div.content-hours-book').empty();
                booking.setDateBooking('');
                booking.setDateHoursBooking('','');
                booking.removeHoursBooking();
                booking.countPrice();
                $.ajax({
                    type: 'POST',
                    url: '{{Helper::url('getguidebusyhours')}}',
                    data:{guide_id:'{{$guide->id}}',city_id:$(this).val()},
                    dataType:'JSON',
                    success: function (data) {
                        booking.setBookedHours(data.data);
                    }
                });
            });
            $('#country').on('change',function(){
                $.ajax({
                    type: 'POST',
                    url: '{{Helper::url('getguidecities')}}',
                    data:{id:'{{$guide->id}}',country_id:$(this).val()},
                    dataType:'JSON',
                    success: function (data) {
                        $('#city').selectpicker('destroy');
                        $("#city").empty();
                        var cities=data.data;
                        cities.map(function(city){
                            var temp='<option value="'+city.city_id+'" data-timezone="'+city.timezone+'">'+city.city_name+'</option>'
                            $('#city').append(temp);
                        });
                        $('#city').selectpicker('render');

                        $('#city').trigger("change");
                    }
                });
            });
            @if(!isset($search_city))
                $('#country').trigger("change");
            @else
                $('#city').trigger("change");
            @endif
        });
    </script>
@stop