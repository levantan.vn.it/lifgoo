@extends('default::frontend.layouts.master')
@section('content')
    <?php
    $isGuide = isset($user) && $user->hasRole('guide');
    ?>
    <div class="content-site">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="title-edit-guide color4 fnt-weight font-size-24"></h3>
        </div>
        <div class="bg-editguide">
            <div class="container">
                @if(session('flash_success'))
                    {{--<div class="callout callout-success">

                    </div>--}}
                @endif

                <!-- <div class="title-edit-guide fnt-weight font-size-30"></div> -->
                <div class="row tabs-left--thumb">
                    <div class="col-md-3 col-sm-3 tabs-left-edit-guide content--minheight"> <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left responsive tab-guide">
                            <li class="editProfile active" value="Edit your profile"><a
                                        class="color999 font-size-14 tabEdit"
                                        href="javascript:void(0)"
                                        data-href="edit-profile">{{trans('site.edit_profile')}}@if(isset($missingProfile) && $missingProfile)
                                        <i class="fa fa-exclamation-triangle icon-warning" aria-hidden="true"></i>@endif
                                </a>
                            </li>
                            <li value="Edit password" class="edit-password">
                                <a class="color999 font-size-14 tabEdit"
                                   href="javascript:void(0)"
                                   data-href="edit-password">{{trans('site.edit_password')}}</a>
                            </li>
                            @if($isGuide)
                                <li value="Edit job information" class="edit-job-info">
                                    <a class="color999 font-size-14 tabEdit"
                                       href="javascript:void(0)"
                                       data-href="edit-job-info">{{trans('site.edit_job_information')}}@if($missingJobInfo)
                                            <i class="fa fa-exclamation-triangle icon-warning"
                                               aria-hidden="true"></i>@endif</a>
                                </li>
                                <li value="Edit promotion" class="edit-price">
                                    <a class="color999 font-size-14 tabEdit"
                                       href="javascript:void(0)"
                                       data-href="edit-price">{{trans('site.edit_promotion')}}</a>
                                </li>
                            @endif
                            
                            <li value="Booking" class="booking">
                                <a class="color999 font-size-14" href="javascript:void(0)"
                                   data-href="booking">{{trans('site.booking')}}</a></li>
                            <li value="notification" class="notification">
                                <a class="color999 font-size-14" href="javascript:void(0)"
                                   data-href="notification">{{trans('site.notification')}}</a>
                            </li>
                            @if($isGuide)
                                <li value="Setup Time" class="setup-time">
                                    <a class="color999 font-size-14 tabEdit" href="javascript:void(0)"
                                       data-href="setup-time">{{trans('site.setup_time')}}</a></li>
                                <li value="Edit your gallery" class="gallery">
                                    <a class="color999 font-size-14 tabEdit"
                                       href="javascript:void(0)"
                                       data-href="gallery">{{trans('site.gallery')}}</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-9 tabs-notify-edit-guide">
                        <!-- Tab panes -->
                        <div class="tab-content responsive">
                            @include('default::frontend.user.editProfile')
                            @include('default::frontend.user.editPassword')

                            @if($isGuide)
                                {{--EDIT JOB INFO--}}
                                @include('default::frontend.user.editJonInfo')
                                {{--EDIT PRICE--}}
                                @include('default::frontend.user.editPrice')
                            @endif

                            @include('default::frontend.notifications.editNotification')

                            {{--EDIT SETUP TIME--}}
                            @include('default::frontend.user.editSetupTime')
                            @include('default::frontend.booking.mainBooking')

                                @if($isGuide)
                                <?php
                                $galleries = $guide->galleries()->orderBy('created_at', 'desc')->get();
                                ?>

                                {{--EDIT GALLERY--}}
                                <div class="tab-pane edit-guide" id="gallery">
                                {!!Form::open(['url'=>Helper::url('gallery'),'method'=>'post','class'=>'upload-gallery','enctype'=>"multipart/form-data"]) !!}
                                    <div class="row upload-Newgallery">
                                        <input type="file" name="files">
                                        <div class="btnUpdate--thumb">
                                            <button type="submit" class="btn btn-upload-gallery bgBlue color4 font-size-14 fnt-weight">Update</button>
                                        </div>
                                    </div>
                                {!!Form::close()!!}
                                <div class="row">
                                        <div class="" id="custom-layout-demo">
                                            <?php $i=1 ?>
                                            @if(count($galleries))
                                                @foreach($galleries as $gallery)
                                                    @include('default::frontend.user.gallery')
                                                    <?php $i++; ?>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <!-- SLIDE SHOW GALLERY -->
                                <div id="myModalGallery" class="modal modalgallery">
                                    <span class="close cursor" onclick="closeModal()">&times;</span>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 modal-content">
                                            @foreach($galleries as $gallery)
                                                <div class="mySlides col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                                                    <div class="numbertext"></div>
                                                    <img src="{{Helper::getThemeGallery('gallery' . $user->id.'/'.$gallery->url)}}" style="width:100%">
                                                </div>
                                                @endforeach
                                                <a class="prev" onclick="plusSlides(-1)"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                                <a class="next" onclick="plusSlides(1)"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script type="text/javascript">

        @if($isGuide && isset($discounts))
            valuePrice = JSON.parse('<?php echo $discounts;?>');
        if (valuePrice)
            table.insertIntoTable();
                @endif
        @if(isset($filter) && $filter)
            var array = window.location.href.split('/');
            var tab = $('.tab-guide li');
            tab.removeClass('active');
            if ($.inArray('booking', array) != -1) {
                var li = tab.find('[data-href="booking"]').parent();
                li.addClass('active');
                $('.tab-pane.edit-guide').removeClass('active');
                $('#booking').addClass('active');
                $('#filter-history').find('[value="' + '{{$filter}}' + '"]').attr('selected', 'selected');
            }
        @endif

        // active tab edit profile
        /*$(window).on('load resize', function(){
         var leng = $('.editProfile.active').length;
         if (leng > 0) {
         $('#edit-profile').addClass('active');
         }
         });

         $('.tabEdit').click(function (e) {
         if ($(window).width()>720) {
         e.preventDefault();
         e.stopImmediatePropagation();
         $(this).tab('show');
         }

         });*/

    </script>
@stop
