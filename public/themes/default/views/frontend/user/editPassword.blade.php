{{--EDIT PASSWORD--}}
<div class="tab-pane edit-guide" id="edit-password">
    <div class="callout callout-error"></div>
    <div class="row">
        {!!Form::open(['url'=>Helper::url('password'),'method'=>'post','class'=>'edit-user']) !!}
        {{--Old password--}}
        <div class="frm_edit">
            <label class="color666 fnt-weight font-size-14" for="old_password">Old password</label>
            <input type="password" id="old_password" class="form-control font-size-14" placeholder="Old password" name="old_password" @if(isset($user) && ($user->facebook_id || $user->google_id || $user->twitter_id) && !$user->password)@endif>
        </div>

        {{--New password--}}
        <div class="frm_edit">
            <label class="color666 fnt-weight font-size-14" for="new_password">New password</label>
            <input type="password" id="new_password" class="form-control font-size-14" placeholder="New password" name="new_password" required minlength="6">
        </div>

        {{--Confirm new password--}}
        <div class="frm_edit">
            <label class="color666 fnt-weight font-size-14" for="cfr_password">Confirm new password</label>
            <input type="password" id="cfr_password" class="form-control font-size-14" placeholder="Confirm new password" name="cfr_password" required minlength="6">
        </div>
        {{--Button Update--}}
        <div class="frm_edit btnUpdate--thumb">
            <button type="submit" class="btn btn_update btn-edit-submit font-size-14 color4 bgBlue fnt-weight" data-type="update_password">Update</button>
        </div>
        {!!Form::close() !!}
    </div>
</div>