{{--EDIT PROFILE--}}
<div class="tab-pane edit-guide" id="edit-profile">
    <div class="row">
        {{--Avatar--}}
        <div class="col-md-12 sidebarEditpr--thumb">
            <div class="editavatar--thumb">
                <label class="color666 fnt-weight font-size-14 title-editavatar" for="cropme">Avatar <span class="require">*</span></label>
                <div id="cropme" class="cropme avatar-editProfile">
                    <div class="contain-editavatar">
                        @if($user && $user->avatar) <img src="{{Helper::getThemeAvatar($user->avatar).'?id='.Helper::versionImg()}}" alt="" data-change="0">@endif
                    </div>
                    <a href="javascript:void(0)" class="change-avatar change-avatar-uploadnew font-size-12 color4 bgBlue fnt-weight"><i class="fa fa-upload font-size-14" aria-hidden="true"></i>Upload a new profile photo</a>
                </div>
                <a href="javascript:void(0)" class="change-avatar change-avatar-abs font-size-12 color4 bgBlue fnt-weight"><i class="fa fa-upload font-size-14" aria-hidden="true"></i>Upload a new profile photo</a>
            </div>
            
        </div>
        <div class="col-md-12 contentEditpr--thumb">
            {!!Form::open(['url'=>Helper::url('editProfile'),'method'=>'post','class'=>'edit-profile']) !!}
            @if(isset($user) && empty($user->email))
                {{--email--}}
                <div class="col-md-6 col-xs-6 frm_edit">
                    <label class="color666 fnt-weight font-size-14"
                           for="email">{{trans('site.email')}} <span class="require">*</span></label>
                    <input type="email" id="email" name="email"
                           class="form-control font-size-14 color999"
                           placeholder="Email" required
                           value="{{$user->email}}" maxlength="100">
                </div>
            @endif
            @if(isset($user) && empty($user->username))
                {{--username--}}
                <div class="col-md-6 col-xs-6 frm_edit">
                    <label class="color666 fnt-weight font-size-14"
                           for="username">{{trans('site.user_name')}} <span class="require">*</span></label>
                    <input type="text" id="username" name="username"
                           class="form-control font-size-14 color999"
                           placeholder="User name" required
                           value="{{$user->username}}" maxlength="100">
                </div>
            @endif
            {{--full_name--}}
            <div class="col-md-6 col-xs-6 frm_edit">
                <label class="color666 fnt-weight font-size-14"
                       for="full_name">{{trans('site.full_name')}} <span class="require">*</span></label>
                <input type="text" id="full_name" name="full_name"
                       class="form-control font-size-14 color999"
                       placeholder="Full name" required maxlength="100"
                       @if($user) value="{{trim($user->full_name)}}"@endif>
            </div>

            {{--Phone number--}}
            <div class="col-md-6 col-xs-6 frm_edit">
                <label class="color666 fnt-weight font-size-14"
                       for="phone_number">{{trans('site.phone_number')}} <span class="require">*</span></label>
                <input type="text" id="phone_number" name="phone_number"
                       class="form-control font-size-14 color999"
                       placeholder="Phone number" required
                       @if(isset($user)) value="{{$user->phone_number}}"@endif maxlength="20" onkeyup="this.value=removeSpaces(this.value);">
            </div>
            {{--Gender--}}
            <div class="col-md-6 col-xs-6 frm_edit frm_edit-gender">
                <label class="color666 fnt-weight font-size-14"
                       for="gender">{{trans('site.gender')}} @if(session('users') && session('users.guide') )<span class="require">*</span>@endif</label>
                <select id="gender" name="gender" class="selectpicker form-control font-size-14 color999"
                        data-style="btn-primary" required>
                    <option value="male"
                            @if(isset($user) && $user->gender == 'male') selected @endif>{{trans('site.male')}}</option>
                    <option value="female"
                            @if(isset($user) && $user->gender == 'female') selected @endif>{{trans('site.female')}}</option>
                </select>
            </div>

            @if(session('users') && session('users.guide') )
            {{--Passport number/ ID number--}}
            <div class="col-md-6 col-xs-6 frm_edit">
                <label class="color666 fnt-weight font-size-14"
                       for="passport_id_number">{{trans('site.passport_number')}}/{{trans('site.id_number')}} <span class="require">*</span></label>
                <input type="text" id="passport_id_number" class="form-control font-size-14 color999"
                       placeholder="Passport number/ ID number"
                       name="passport_id_number" required maxlength="20"
                       @if(isset($guide)) value="{{$guide->idnumber}}"@endif>
            </div>
            @endif
            
            {{--Birthday--}}
            <div class="col-md-6 col-xs-6 frm_edit">
                <label class="color666 fnt-weight font-size-14"
                       for="birthday">{{trans('site.birthday')}} @if(session('users') && session('users.guide') )<span class="require">*</span>@endif</label>
                <input type="text" class="form-control datefilter-maxdate font-size-14 color999"
                       id="birth_date" name="birth_date" placeholder="yyyy-mm-dd"
                       @if(isset($user) && $user->birth_date ) value="{{$user->birth_date}}"@endif required/>
                <span id="inputSuccess2Status3" class="sr-only"
                      required>(success)</span>
            </div>
            {{--Address--}}
            <div class="col-md-6 col-xs-6 frm_edit">
                <label class="color666 fnt-weight font-size-14"
                       for="address">{{trans('site.address')}} <span class="require">*</span></label>
                <input type="text" id="address" class="form-control font-size-14 color999"
                       placeholder="Address" name="address" required
                       maxlength="200"
                       @if(isset($user)) value="{{$user->address}}"@endif>
            </div>
            {{--Slogan--}}
            <div class="col-md-12 frm_edit frm_edit-slogan">
                <label class="color666 fnt-weight font-size-14"
                       for="slogan">{{trans('site.slogan')}}</label>
                {{--<input type="text" id="slogan" class="form-control font-size-14"
                       placeholder="Slogan" name="slogan" @if(isset($user)) value="{{trim($user->slogan)}}"@endif>--}}
                <textarea maxLength="10000" name="slogan" class="form-control editor-wrapper font-size-14 color999" id="slogan" rows="2">@if(isset($user)){{trim($user->slogan)}}@endif</textarea>
            </div>
            {{--Hobby--}}
            <div class="col-md-12 frm_edit">
                <label class="color666 fnt-weight font-size-14" for="hobby">{{trans('site.hobby')}}</label>
                <textarea maxLength="10000" name="hobby" class="form-control editor-wrapper font-size-14 color999" id="hobby" rows="6">@if(isset($user)){{trim($user->hobby)}}@endif</textarea>
            </div>

            {{--About me--}}
            <div class="col-md-12 frm_edit">
                <label class="color666 fnt-weight font-size-14" for="introduce">{{trans('site.about_me')}} @if(session('users') && session('users.guide') )<span class="require">*</span>@endif</label>
                <textarea maxLength="10000" name="introduce" class="form-control editor-wrapper font-size-14 color999" id="introduce" rows="6" @if(session('users') && session('users.guide') ) required @endif>@if(isset($guide)){{trim($guide->introduce)}}@endif</textarea>
            </div>
            {{--Button Update--}}
            <div class="col-md-12 btnUpdate--thumb">
                <button type="submit" class="btn btn_update font-size-14 fnt-weight color4 bgBlue">Update</button>
            </div>

            {!!Form::close() !!}
        </div>
    </div>
</div>
