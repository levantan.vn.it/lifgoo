
<script type="text/javascript">
    /*Jquery Validate*/
    $(document).ready(function () {
        $('#create-group-form').validate({
            ignore: [],
            rules: {
                title: "required"
            },
            messages: {
                title: "Please enter group title"
            },
            submitHandler: function (form) {
                var submit_url = $('#create-group-form').attr('action');
                $.ajax({
                    type: "POST",
                    url: submit_url,
                    data: $(form).serialize(),
                    dataType: "JSON",
                    success: function (result) {
                        if (result.success == false) {
                            modalDisplay("{{ trans('site.warning') }}", 'warning', result.message);
                        }
                        else
                            window.location.href = 'group';
                    },
                });
                return false;
            }
        });
    })
</script>