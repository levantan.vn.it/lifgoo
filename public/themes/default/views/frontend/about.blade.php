@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">About us</h3>
        </div>
        <div class="cnt-site-login">
            <div class="bg-about-us content--minheight">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-10 col-xs-10 aboutUs--thumb aligncenter">
                            <div class="content-about color666 font-size-14 text-justify">
                                <p class="first-p"><b>Lifgoo</b> is the application that enables registered tourist guides to show information about the services of his tourist guide helps visitors, visitors can search and select travel guides for the trip depending on the individual needs of each person. Lifgoo also supports travel guides show the time can make its services, the services themselves, information about experience, qualifications, licenses related to help visitors spot the better for your trip.</p>
                                <p><b>Lifgoo</b> is the only application that supports the connection between the parties, no fee and no responsibility about how tour operators of the parties. The two parties commit themselves to bear the legal responsibility to each other.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
