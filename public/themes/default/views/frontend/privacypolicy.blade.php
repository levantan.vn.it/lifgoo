@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-cus content-site-privacy">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">Privacy Policy</h3>
        </div>
        <div class="cnt-site-login">
            <div class="bg-about-us content--minheight">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-10 col-xs-10 aboutUs--thumb aligncenter privacy_thumb">
                            <div class="content-about color666 font-size-14 text-justify">
                                <h3 class="text-center"><b>The following is the safety and security policy, lifgoo promised to friends, please carefully read your content before joining lifgoo.</b></h3>
                                <ol class="list-number">
                                    <li>
                                        <h5 class="color0084fb">Share information</h5>
                                        <ul class="list-item">
                                            <li>All personal information, you can register the secure storage and lifgoo we do not sell, or to any person of your personal information, unless the legal institutions, have the right to request the investigation What's your trouble or legal problems.</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h5 class="color0084fb">The use of personal information</h5>
                                        <ul class="list-item">
                                            <li>The information of you that we store when registration or web access will we use to evaluate the internal system of Lifgoo well as aggregate for sorting, improved Web site, service services to serve you better, in addition to notification of system messages, updates, edit relating to Lifgoo.</li>
                                            <li>Your personal information will be provided at the request of law enforcement agencies are conducting the competent investigating a particular problem.</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h5 class="color0084fb">General policy</h5>
                                        <ul class="list-item">
                                            <li>Privacy Policy will be revised or updated continuously at any time and will notify you.</li>
                                            <li>We will not guarantee your safety when clicking any link does not belong to the system Lifgoo.<br>Example: links users to send you visitors during the exchange through Lifgoo service.</li>
                                            <li>Lifgoo will have links advertising pages 3rd party websites and is not under the control of us and also not governed by these policies, so Lifgoo encourages you to review the policies security is posted on the Web site there.</li>
                                        </ul>
                                    </li>
                                <ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
