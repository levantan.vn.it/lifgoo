@extends('default::frontend.layouts.master')
@section('content')
    <?php
    $iNews = 0;
    $isGuide = session('users') && session('users.guide');
    ?>
    <div class="content-site content-site-home">
        <div class="slider-site">
            <!--SLIDER REVOLUTION-->
            <div id="SlideHeader">
                <div class="slideHeader-item">
                    <img src="{{Helper::getThemeImg('1920x610slide1c.jpg')}}" alt="slidebg1" title="slidebg1">
                </div>
                <div class="slideHeader-item">
                    <img src="{{Helper::getThemeImg('1920x610slide2c.jpg')}}" alt="slidebg2" title="slidebg2">
                </div>
            </div>
            <!--Search Slider-->
            <div class="capHeader">
                <h1>Lifgoo! Make Travel Easier</h1>
                @if(!$isGuide)
                <h3>Find Your Guide At Here</h3>
                @endif
            </div>
            @if(!$isGuide)
                <div class="search-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['url'=>Helper::url('search'),'id'=>'search','method'=>'GET']) !!}
                                <div class="col-md-12 search-form txt-align search-header--thumb boxShadow">
                                    <div class="frm-search--thumb">
                                        <!-- CHOOSE DATE (new) -->
                                        <div class="frm-search frm-choosedate">
                                            <label class="color263038 font-size-14 fnt-weight" for="choose-datehome">Choose date</label>
                                            <div class="thumb-inputDate thumb-inputdatehome">
                                                <input id="choose-datehome" class="form-control thumb-datefilter font-size-14" type="text" name="daterange" value="" readonly><span class="icon-inputDate"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        <div class="frm-search frm-searchSelect frm-searchCountry">
                                            <label class="color263038 font-size-14 fnt-weight" for="country">Country</label>
                                            <select class="form-control" id="country" name="country_id">
                                                @if(isset($countries) && count($countries))
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->country_id}}" @if($country->country_id==140) selected @endif>{{$country->country_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        {{--<div class="frm-search frm-searchSelect">
                                            <label class="color263038 font-size-14 fnt-weight" for="city">Cities</label>
                                            <select class="form-control" id="city" name="city_id">
                                                @if(isset($cities) && count($cities))
                                                    @foreach($cities as $city)
                                                        <option value="{{$city->city_id}}">{{$city->city_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>--}}
                                        <div class="frm-search frm-searchSelect">
                                            <label class="color263038 font-size-14 fnt-weight" for="Language">Language</label>
                                            <select class="form-control" id="Language" name="language_id">
                                                @if(isset($langWorlds) && count($langWorlds))
                                                    @foreach($langWorlds as $lang)
                                                        <option value="{{$lang->id}}">{{$lang->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="frm-search frm-findguide">
                                            <!-- <a class="search-btn bgBlue fnt-weight font-size-14 color4"
                                               href="javascript:void(0)"
                                               onclick="document.getElementById('search').submit();">FIND GUIDE</a> -->
                                            <button type="submit" class="search-btn bgBlue fnt-weight font-size-14 color4">FIND GUIDE</button>
                                        </div>
                                    </div> <!-- end frm-search-thumb -->
                                </div>
                                {!! Form::close() !!}
                            </div>                            
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!--Best Guide-->
        <div class="best-guide">
            <div class="bg-best-guide">
                <div class="container">
                    <div class="title-site txt-align">
                        <h1 class="color263038-1 fnt-weight text-tr font-size-24">Best Guide</h1>
                        <h4 class="color263038 font-size">Find best guide , take best trip for you</h4>
                    </div>
                    <div class="row content-best-guide">
                        <div id="best-guide-slider" class="guide-slide">
                            <!-- slider new -->
                            @if(isset($guide) && count($guide))
                                @foreach($guide as $set)
                                    <?php
                                    $location = $set->locationTour()->first();
                                    $locationmore = $set->locationTour()->get();

                                    $userGuide = $set->user()->select('avatar', 'id')->first();

                                    $avatar = $userGuide->avatar ? Helper::getThemeAvatar($userGuide->avatar) : Helper::getThemeImg('user.png');
                                    ?>
                                    {{--<div class="text-center slide col-md-3" data-url="{{Helper::url('profile/'.$userGuide->id)}}">--}}
                                    <div class="guide-slide-item text-center slide col-md-3" data-id="{{$userGuide->id}}">
                                        <div class="img-round">
                                            <img src="{{$avatar.'?id='.Helper::versionImg()}}" title=""
                                                 alt="{{$set->user()->first()->full_name}}">
                                        </div>
                                        <div class="item-profile-guide">
                                            <div class="">
                                                <!-- name -->
                                                <h4 class="item-name-profile color0084fb font-size-14 fnt-weight">{{$set->user()->first()->full_name}}</h4>
                                                <!-- price -->
                                                <div class="price-profile-info font-size-24 fnt-weight color263038-1">
                                                    <div>
                                                        <i class="fa fa-usd" aria-hidden="true"></i>{{$set->hourly_rate}}/h
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="rating-star">
                                                @for($i=1; $i<= $set->star; $i++)
                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                @endfor
                                                @for($i=4; $i >= $set->star; $i--)
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="dropdown no-hover">
                                            @if(count($location))
                                                <?php
                                                    $city = $location->cities()->first();
                                                    $country = $location->countries()->first();
                                                    $j = 1;
                                                ?>
                                                <span class="font-size-12 fnt-weight color4e5d6b" style="width: 100%; display: inline-block; margin-bottom: 5px;">Guide locations:</span>
                                                <span class="item-adress-profile color4e5d6b font-size-12">{{$city->city_name}}
                                                    - {{$country->country_name}}</span>
                                                <?php $j = 1;?>
                                                @foreach($locationmore as $lc)
                                                    <?php $j++; ?>
                                                @endforeach
                                                @if($j>2)
                                                    <div class="more-location">
                                                        <span class="font-size-12 fnt-weight color4e5d6b hover-link">&nbsp;(More)</span>
                                                        <div class="dropdown-content dropdown-content-location">
                                                            <p class="font-size-12 color4e5d6b">
                                                            <?php $i = 1; ?>
                                                                @foreach($locationmore as $lc)
                                                                <span class="location-itemlist">
                                                                    @if($i<2)
                                                                        {{$lc->cities()->first()->city_name}} - {{$lc->countries()->first()->country_name}}
                                                                    @else
                                                                        , {{$lc->cities()->first()->city_name}} - {{$lc->countries()->first()->country_name}}
                                                                    @endif
                                                                    <?php  $i++;?>
                                                                </span>
                                                                @endforeach
                                                            </p>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                        <!-- LANGUAGE -->
                                        <div class="dropdown no-hover">
                                            <span class="font-size-12 fnt-weight color4e5d6b" style="width: 100%; display: inline-block; margin-bottom: 5px;">Language:</span>
                                            <?php 
                                                $knownlanguages=$set->known_languages()->get(); 
                                                $count=count($knownlanguages);
                                                $j=1; $k=1;
                                            ?>
                                            @foreach($knownlanguages as $language)
                                                @if($j<3)
                                                    <span class="item-adress-profile color4e5d6b font-size-12">
                                                
                                                    @if($k<2)                                                    
                                                        {{$language->language()->first()->name}}
                                                    @else
                                                        , {{$language->language()->first()->name}}
                                                    @endif
                                                    </span>
                                                @endif
                                                <?php $j++; $k++; ?>                                                
                                            @endforeach
                                            @if($j>3)
                                                <div class="more-language">
                                                    <span class="font-size-12 fnt-weight color4e5d6b hover-link">&nbsp;(More)</span>
                                                    <div class="dropdown-content dropdown-content-language font-size-12 color4e5d6b">
                                                        <p class="font-size-12 color4e5d6b">
                                                            <?php $i = 1; ?>
                                                            @foreach($knownlanguages as $language)
                                                                <span class="location-itemlist">
                                                                    @if($i<2)
                                                                        {{$language->language()->first()->name}}
                                                                    @else
                                                                        , {{$language->language()->first()->name}}
                                                                    @endif
                                                                    <?php  $i++;?>
                                                                </span>
                                                            @endforeach
                                                        </p>
                                                    </div>
                                                </div> 
                                            @endif                                               
                                        </div>
                                        <div class="hover-y">
                                        @if(!$isGuide)
                                            {{--<a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="{{Helper::url('booking/'.$userGuide->id)}}" target="_blank" title="">BOOK GUIDE</a>--}}
                                            <a data-id="{{$userGuide->id}}" class="book-profile-btn color4 fnt-weight bgBlue font-size-12 hover-link" href="javascript:void(0)" title="">BOOK GUIDE</a>
                                        @endif
                                        @if($isGuide)
                                            {{--<a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="{{Helper::url('profile/'.$userGuide->id)}}" target="_blank" title="">BOOK GUIDE</a>--}}
                                            <a data-id="{{$userGuide->id}}" class="profile-book-btn color4 fnt-weight bgBlue font-size-12 hover-link" href="javascript:void(0)" title="">BOOK GUIDE</a>
                                        @endif
                                        </div>                                        
                                    </div>
                                @endforeach
                            @endif
                            <!--slider old-->
@if(false)                            
                            @if(isset($guide) && count($guide))
                                @foreach($guide as $set)
                                    <?php
                                    $location = $set->locationTour()->first();
                                    $userGuide = $set->user()->select('avatar', 'id')->first();
                                    $avatar = $userGuide->avatar ? Helper::getThemeAvatar($userGuide->avatar) : Helper::getThemeImg('user.png');
                                    ?>
                                    <div class="text-center slide col-md-3">
                                        <div class="img-round">
                                            <img src="{{$avatar.'?id='.Helper::versionImg()}}" title=""
                                                 alt="{{$set->user()->first()->full_name}}">
                                        </div>
                                        <div class="item-profile-guide">
                                            <div class="no-hover">
                                                <h4 class="item-name-profile color333 font-size-14 fnt-weight">{{$set->user()->first()->full_name}}</h4>
                                                @if(count($location))
                                                    <?php
                                                    $city = $location->cities()->first();
                                                    $country = $location->countries()->first();
                                                    ?>
                                                    <span class="item-adress-profile color999 font-size-12">{{$city->city_name}}
                                                        - {{$country->country_name}}</span>
                                                @endif
                                            </div>
                                            <div class="hover-y" style="display: none;">
                                                <a class="color4 fnt-weight bgBlue font-size-14"
                                                   href="{{Helper::url('profile/'.$userGuide->id)}}">View Profile</a>
                                            </div>
                                            <div class="rating-star">
                                                @for($i=1; $i<= $set->star; $i++)
                                                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                                                @endfor
                                                @for($i=4; $i >= $set->star; $i--)
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="excerpt-item-profile font-size-14 color2">
                                            {{$set->introduce}}
                                        </div>
                                    </div>
                                @endforeach
                            @endif
@endif
                        </div>
                        <a style="display: none"
                           class="show-more show-moreBestguide font-size-14 fnt-weight color4 bgBlue"
                           href="{{Helper::url('guides')}}">VIEW MORE</a>
                    </div>
                </div>
            </div>
        </div>
        <!--Note-->
        <div class="bg-fix"
             style="background: url({{Helper::getThemeImg('Layer-29-optimize.jpg')}}); background-attachment: fixed;background-size: cover; background-repeat: no-repeat;">
            <div class="col-md-10 col-md-offset-1">
                <!--COUNT GUIDER-->
                <div class="col-md-3 col-sm-3 col-xs-3 item-count-guider txt-align">
                    <img src="{{Helper::getThemeImg('Shape-1-optimize.png')}}" alt="">
                    <div class="count-guide color4 counter">{{$countGuides}}</div>
                    <div class="color4 fnt-weight1 font-size-18 count-size">Guiders</div>
                </div>
                <!--COUNT TOUR-->
                <div class="col-md-6 col-sm-6 col-xs-6 item-count-tour txt-align">
                    <img src="{{Helper::getThemeImg('earth-paradise1-optimize.png')}}" alt="">
                    <div class="count-guide color4 counter">{{$countNews}}</div>
                    <div class="color4 fnt-weight1 font-size-18 count-size">News</div>
                </div>
                <!--COUNT TRAVELER-->
                <div class="col-md-3 col-sm-3 col-xs-3 item-count-traveler txt-align">
                    <img src="{{Helper::getThemeImg('traveler-with-a-suitcase-optimize.png')}}" alt="">
                    <div class="count-guide color4 counter">{{$countTraveler}}</div>
                    <div class="color4 fnt-weight1 font-size-18 count-size">Travelers</div>
                </div>
            </div>
        </div>
        <!--BEST TOUR-->
        <div class="best-tour">
            <div class="bg-best-tour">
                <div class="container">
                    <div class="title-site txt-align">
                        <h1 class="text-tr color263038-1 font-size-24">NEW TOUR</h1>
                        <h4 class="color263038 font-size">Choose location, find informations and discover!</h4>
                    </div>
                    <!--ROW-->
                    <div class="row content-best-tour">
                        @if(isset($news) && count($news))
                            @foreach($news as $ne)
                                <?php
                                $newsLang = $ne->news_language()->where('language_code', '=', 'en')->first();
                                $cities = $ne->cities()->first();
                                $countries = $ne->countries()->first();
                                ?>
                                @if($newsLang['title'] && $newsLang['content'])
                                <!-- BEST TOUR item -->
                                <div class="col-md-4 col-sm-6 col-xs-6 bestNews-item">
                                    <div class="lg-item-best-tour boxShadow">
                                        <div class="img-best-tour div-wrapper-bestTour">
                                            <div class="div-pad-bestTour"></div>
                                            <div class="bg-hover-img div-inner-bestTour"
                                                 style="background-image: url({{Helper::getThemeNewsUpload($ne->url).'?id='.Helper::versionImg()}});">
                                                <a href="{{Helper::url('news/'.$ne->id)}}" class="view_detailNews font-size-14" title=""></a>
                                                <div class="content-info">
                                                    <a href="{{Helper::url('news/'.$ne->id)}}" class="item-title-tour item-title-tour-2 color4 font-size fnt-weight" title="{{$newsLang->title}}">{{$newsLang->title}}</a>
                                                    <p>
                                                        <i class="fa fa-map-marker font-size colore4ebf0" aria-hidden="true"></i>
                                                        <span class="font-size-12 colore4ebf0">{{$cities->city_name}} - {{$countries->country_name}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<!-- code old -->
@if(false)
                                    @if($iNews == 0)
                                        <div class="col-md-9 col-sm-4 col-xs-6 first-best-tour bestNews-item">
                                            <div class="lg-item-best-tour">
                                                <div class="col-md-8 col-sm-12 col-xs-12 img-best-tour pad-left pad-right">
                                                    <div class="div-pad-bestTour-first"></div>
                                                    <div class="bg-hover-img div-inner-bestTour div-inner-bestTour--first"
                                                         style="background-image: url({{Helper::getThemeNewsUpload($ne->url).'?id='.Helper::versionImg()}});">
                                                         <a href="{{Helper::url('news/'.$ne->id)}}" class="view_detailNews font-size-14"></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12 col-xs-12 cnt-item-tour cnt-firstItem-tour">
                                                    <a href="{{Helper::url('news/'.$ne->id)}}"
                                                       class="item-title-tour color2 font-size fnt-weight"
                                                       title="{{$newsLang->title}}">{{$newsLang->title}}</a>
                                                    <span class="color999 text-ellipsis font-size-14">{{$cities->city_name}}
                                                        - {{$countries->country_name}}</span>
                                                    <div class="color2 excerpt-item-best-tour first-excerpt-item-best-tour font-size-14">
                                                        {!! $newsLang->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-3 col-sm-4 col-xs-6 bestNews-item">
                                            <div class="lg-item-best-tour">
                                                <div class="img-best-tour div-wrapper-bestTour">
                                                    <div class="div-pad-bestTour"></div>
                                                    <div class="bg-hover-img div-inner-bestTour"
                                                         style="background-image: url({{Helper::getThemeNewsUpload($ne->url).'?id='.Helper::versionImg()}});">
                                                         <a href="{{Helper::url('news/'.$ne->id)}}" class="view_detailNews font-size-14"></a>
                                                    </div>
                                                </div>
                                                <div class="cnt-item-tour cnt-item-tour-line">
                                                    <a href="{{Helper::url('news/'.$ne->id)}}"
                                                       class="item-title-tour item-title-tour-2 color2 font-size fnt-weight"
                                                       title="{{$newsLang->title}}">{{$newsLang->title}}</a>
                                                    <span class="color999 text-ellipsis font-size-14">{{$cities->city_name}}
                                                        - {{$countries->country_name}}</span>
                                                    <div class="color2 excerpt-item-best-tour font-size-14">
                                                        {!! $newsLang->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
@endif
                                @endif
                            <?php $iNews++;?>
                            @endforeach
                        @endif
                    </div>
                    <a class="show-more show-moreNewTour font-size-14 fnt-weight color4 bgBlue"
                       href="{{Helper::url('news')}}">VIEW MORE</a>
                </div>
            </div>
        </div>
        <!--SUBSCRIBE (GET IN TOUCH)-->
        <div class="get-in-touch">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-sm-12 aligncenter bg-get-in-touch">
                        <div class="col-md-7 col-sm-12">
                            <div class="color4 font-size-24 fnt-weight">SUBSCRIBE</div>
                            <div class="colore4ebf0 font-size cap-subscribe">
                                Enter your email to subscribe about Lifgoo
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-6 input-sub">
                            {!! Form::open(['url'=>Helper::url('subcribe'),'class'=>'form-inline','id'=>'subcribe','method'=>'GET']) !!}
                            <div class="form-group">
                                <div class="input-group inp-subcribe--thumb">
                                    <input type="email" class="form-control get-email font-size-14" id="email"
                                           placeholder="Type in Your Email Address" name="email" required>
                                    <div class="input-group-addon btn-subscribe--thumbslide">
                                        <a class="btn-subscribe" href="javascript:void(0)" onclick="checkValid()"><i class="fa fa-paper-plane font-size-18" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js_wrapper')
<script type="text/javascript">jQuery(document).ready(function () {$('.guide-slide-item').click(function(){var thisId = $(this).attr('data-id');window.open('{{Helper::url("profile")."/"}}'+thisId,'_blank');});$('.book-profile-btn').click(function(e){var thisId = $(this).attr('data-id');window.open('{{Helper::url("booking")."/"}}'+thisId,'_blank');if (window.event) {window.event.cancelBubble=true;}else {if (e.cancelable ) {e.stopPropagation();}}});$('.profile-book-btn').click(function(e){var thisId = $(this).attr('data-id');window.open('{{Helper::url("profile")."/"}}'+thisId,'_blank');if (window.event) {window.event.cancelBubble=true;}else {if (e.cancelable ) {e.stopPropagation();}}});});</script>

<!-- <script type="text/javascript">
    jQuery(document).ready(function () {
        
        $('.guide-slide-item').click(function(){
            var thisId = $(this).attr('data-id');
            window.open('{{Helper::url("profile")."/"}}'+thisId,'_blank');
        });

        $('.book-profile-btn').click(function(e){
            var thisId = $(this).attr('data-id');
            window.open('{{Helper::url("booking")."/"}}'+thisId,'_blank');
            if (window.event) {
                window.event.cancelBubble=true;
                }
            else {
                if (e.cancelable ) {e.stopPropagation();}
                }
        });
        $('.profile-book-btn').click(function(e){
            var thisId = $(this).attr('data-id');
            window.open('{{Helper::url("profile")."/"}}'+thisId,'_blank');
            if (window.event) {
                window.event.cancelBubble=true;
                }
            else {
                if (e.cancelable ) {e.stopPropagation();}
                }
        });
    });
</script> -->
@stop
