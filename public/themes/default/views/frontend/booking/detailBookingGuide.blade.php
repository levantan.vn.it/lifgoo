<div class="bg-1">
    <div class="col-md-8">
        <div class="list_booking">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Booking dates</th>
                    <th>Booking time</th>
                </tr>
                </thead>
                <tbody class="cnt-body-list-booking">
                @if ($dates)
                    @foreach ($dates as $date)
                        <?php
                        $hours = explode(',', $date->hours);
                        $total_count += count($hours);
                        $total_dates++;
                        ?>
                        <tr>
                            <td style="width: 200px;">{{$date->date}} - {{Helper::addPlural(count($hours), 'hour')}}</td>
                            <td>
                                <ul>
                                    @foreach($hours as $h)
                                        <li>{{Helper::addzero($h)}}:00</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <h3 class="title_total fnt-weight">
            Total
        </h3>
        <div class="body_total">
            <div class="date_time_total">
                Date time: {{$htmlDates}}
            </div>
            <div class="number_people_total">
                Number of people: {{$booking->capacity}}
            </div>
            <div class="expected_total">
                Expected cost: ${{$booking->price}}
            </div>
        </div>
    </div>
</div>
