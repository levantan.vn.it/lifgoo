<?php
$total_count = 0;
$total_dates = 0;
$li = '';
$guideArray = [];
?>
<div class="tab-pane edit-guide" id="booking">
    {{--FILTER BY HISTORY--}}
    <div class="filter-history">
        <div class="fnt-weight font-size-12 color999">Filter by</div>
        <select id="filter-history" class="selectpicker form-control font-size-14 btn-filterHistory" data-style="btn-primary" name="filter_history">
            <option value="all">All</option>
            <option value="pending">Pending</option>
            <option value="accepted">Accepted</option>
            <option value="declined">Declined</option>
            <option value="canceled">Canceled</option>
            <option value="finished">Finished</option>
        </select>
    </div>
    <div class="col-md-12 cnt-edit-traveller">
        <div class="row panel-group wrap font-size-14" id="accordion" role="tablist" aria-multiselectable="true">
            @if(isset($bookings) && count($bookings))
                @foreach($bookings as $booking)
                    @include('default::frontend.booking.detailBooking')
                @endforeach
                @if($flag_show)
                    <a class="show-more show-more-booking" href="javascript:void(0)" data-page="1"
                       data-url="{{Helper::url('moreBooking')}}">VIEW MORE</a>
                @endif
            @else
                No booking available
            @endif
        </div>
    </div>
</div>
