<?php
$guide_id = $isGuide ? $booking->traveler_id : $booking->guide_id;
$type = $isGuide ? 'traveler_id' : 'guide_id';
$userGlobal = session('users.user')?session('users.user'):session('users.guide');
if (!array_key_exists($guide_id, $guideArray))
    $guideArray[$guide_id] = $booking->guide($type)->select(['id', 'full_name', 'avatar'])->first();
$guideJoin = $guideArray[$guide_id];
if (!$isGuide)
    $guide_info = $guideArray[$guide_id]->guide()->select(['star'])->first();
?>
@if($booking->status != 'canceled')
    <?php
    $messages = $booking->messageBooking()->orderBy('created_at', 'desc')->limit(100)->get();
    $dates = $booking->datesBooking()->get();
    if ($dates) {
        foreach ($dates as $date) {
            $count = count(explode(',', $date->hours));
            $li .= '<li>' . Helper::formatDate($date->date) . ' - ' . Helper::addPlural($count, 'hour') . '</li>';
            $total_count += $count;
        }
    }
    $total_dates=count($booking->getConvertHours());
    $htmlDates = Helper::addPlural($total_count, 'hour') . ' in ' . Helper::addPlural($total_dates, 'day');
    $typeUser = [];
    ?>
    <!--Begin Panel BOOKING-->
    <div class="panel booking-panel" data-bkid="{{$booking->id}}">
        <div class="panel-heading @if($isGuide && !$booking->is_read)mark-as-read @endif" role="tab" data-cartid="{{$booking->id}}" data-url="{{Helper::url('markRead')}}" data-type="cart">
            <div class="list-left">
                <div class="avatar_traveller">
                    <a href="@if($isGuide) javascript:void(0) @else {{Helper::url('profile/'.$booking->guide_id)}} @endif">
                        <img src="{{$guideJoin->avatar?Helper::getThemeAvatar($guideJoin->avatar):Helper::getThemeImg('user.png')}}?id={{Helper::versionImg()}}">
                    </a>
                </div>
            </div>
    
            <div class="list-right">
                @if($booking->status == 'canceled')
                    <span class="font-size-14 fnt-weight itemStt-canceled">Canceled</span>
                @else
                    <div class="name-historyBooking">
                        <div class="name-history-booking">
                            <a href="@if($isGuide) javascript:void(0) @else {{Helper::url('profile/'.$booking->guide_id)}} @endif"><span class="colorBlue1 font-size-14 fnt-weight">{{$guideJoin->full_name}}</span></a>
                        </div>
                        <div class="rating-message">
                            @if(!$isGuide)
                                <div class="rating-star">
                                    @for($i=1; $i<= $guide_info->star; $i++)
                                        <i class="fa fa-star yellow" aria-hidden="true"></i>
                                    @endfor
                                    @for($i=4; $i >= $guide_info->star; $i--)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @endfor
                                </div>
                            @endif
                             <?php $countMessage = $booking->countMessage($booking->id,$userGlobal->id)?>
                            @if($countMessage)
                                <span class="font-size-12 fnt-weight notify-message">New messages</span>
                            @endif
                        </div>
                       
                    </div>
                    <div class="list-deactive">
                        <div class="list-deactive-item">
                            <div class="list-deactive-item--thumb">
                                
                                    <span class="font-size-14 color666">You booked
                                                with @if($guideJoin)<b>{{$guideJoin->full_name}}</b>@endif
                                        total {{$htmlDates}}
                                        for {{$booking->capacity<2?$booking->capacity.' person':$booking->capacity.' people'}}
                                        ,
                                                total <b>${{$booking->price}}</b>
                                            </span>
                                <span class="time-creatBooking font-size-14 color666 convert-time-js">{{$booking->created_at}}</span>
                            </div>

                        </div>
                    </div>

                    @if(!$isGuide && ($booking->status == 'pending' || $booking->status == 'accepted'))
                        <div class="list-active" style="display: none;">
                            <button type="button" class="btn-cancel-booking"
                                    data-url="{{Helper::url('statusBooking')}}"
                                    data-value="canceled" data-bkid="{{$booking->id}}">
                                Cancel
                                this booking
                            </button>
                        </div>
                    @endif
                    @if($isGuide)
                        <div class="list-active" style="display: none;">
                            @if($booking->status == 'pending')
                                <button type="button" class="btn-cancel-booking"
                                        data-url="{{Helper::url('statusBooking')}}"
                                        data-value="accepted" data-bkid="{{$booking->id}}">
                                    Accept
                                </button>
                            @endif
                            @if($booking->status == 'pending')
                                <button type="button" class="btn-cancel-booking font-size-14"
                                        data-url="{{Helper::url('statusBooking')}}"
                                        data-value="declined" data-bkid="{{$booking->id}}">
                                    Decline
                                </button>
                            @endif
                        </div>
                    @endif
                @endif
            </div>
            <a role="button" data-toggle="collapse" data-parent="#accordion"
               href=""
               aria-expanded="false" aria-controls="collapse{{$booking->id}}">
                <div class="status-booking"><span
                            class="font-size-14 fnt-weight itemStt-{{$booking->status}}">{{$booking->status}}</span></div>
            </a>
        </div>
        
        {{--<div id="collapse{{$booking->id}}" class="panel-collapse collapse" role="tabpanel"
             aria-labelledby="headingOne">
            <div class="panel-body">
                @if(!$isGuide)
                    <div class="col-md-12">
                        @include('default::frontend.booking.detailBookingTraveler')
                        @include('default::frontend.booking.chatBooking')
                    </div>
                @else
                    <div class="color10 font-size">
                        @include('default::frontend.booking.detailBookingGuide')
                        @include('default::frontend.booking.chatBooking')
                    </div>
                @endif
            </div>
        </div>--}}
    </div>
    <!-- end of panel -->
@else
    @include('default::frontend.booking.canceledBooking')
@endif