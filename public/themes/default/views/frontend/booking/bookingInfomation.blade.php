@extends('default::frontend.layouts.master')
@section('content')
    <?php
    $isGuide = $user->hasRole('guide');
    $partner = $isGuide ? $detail->guide('traveler_id')->first() : $detail->guide('guide_id')->first();
    $dates = $detail->datesBooking()->get();
    $city=$detail->getCity()->first();
    $bookingTimezone=new DateTimeZone($city->timezone);
    $total_hours = 0;
    $total_dates = 0;
    $userGlobal = session('users.user') ? session('users.user') : session('users.guide');
    $typeUser = [];
    $bookingDateHours=[];
    foreach($dates as $date){
        foreach(explode(',',$date->hours) as $hour){
            $time=new DateTime(($date->date.' '.$hour));
            $time->setTimezone($bookingTimezone);
            if(!isset($bookingDateHours[$time->format('Y-m-d')])||!is_array($bookingDateHours[$time->format('Y-m-d')])){
                $bookingDateHours[$time->format('Y-m-d')]=[];
            }
            array_push($bookingDateHours[$time->format('Y-m-d')],$time->format('H'));
        }
    }
    ?>
    <div class="content-bookingInfo content--minheight">
        <div class="container">
            <h1 class="title-bookingInfo font-size-24 color4e5d6b fnt-weight">Booking Information</h1>
                            
            <!-- info book left -->
            <div class="col-md-8 step-three content-stepThree fieldset3thumb-left">
                <div class="fieldset3-left">
                    <!-- name -->
                    <div class="name-history-booking guideName font-size-14">
                        <i class="color4e5d6b-op">@if($isGuide) Traveler @else Guide @endif: </i><span class="font-size-14 color4e5d6b spaninfo">{{$partner->full_name}}</span>
                    </div>

                    <!-- book date -->
                    <div class="booking-date bookingDate-thumbs font-size-14">
                        <i class="color4e5d6b-op">Booking dates:</i>
                        @if($dates)
                            @foreach ($bookingDateHours as $date=>$hours)
                                <?php
                                $count = count($hours);
                                $total_hours += count($hours);
                                $total_dates++;
                                ?>
                                <div class="itemInfo-date">
                                    <p class="font-size-14 fnt-weight1 color4e5d6b">{{Helper::formatDate($date) . ' - ' . Helper::addPlural($count, 'hour')}}</p>
                                    <ul class="list-booking-date">
                                        @foreach($hours as $h)
                                            <li>{{$h}}:00</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    <div class="booking-location">
                        <ul class="list-locationInfo">
                            <li class="font-size-14 title-booking-margin-bottom title-country"><i class="color4e5d6b-op">Country: </i><span class="spaninfo font-size-14 color4e5d6b">{{$city->country()->first()->country_name}}</span></li>
                            <li class="font-size-14 title-booking-margin-bottom title-city"><i class="color4e5d6b-op">City: </i><span class="spaninfo font-size-14 color4e5d6b">{{$city->city_name}}</span></li>
                            <li class="font-size-14 title-booking-margin-bottom title-place"><i class="color4e5d6b-op">Pickup placement: </i><span class="spaninfo font-size-14 color4e5d6b">{{$detail->pickup_place}}</span></li>
                            <li class="font-size-14 title-destination title-booking-margin-bottom"><i class="color4e5d6b-op">Destination: </i><span class="font-size-14 color4e5d6b">{{$detail->destination}}</span></li>
                            <li class="font-size-14 title-booking-margin-bottom title-email"><i class="color4e5d6b-op">Contact email: </i><span class="spaninfo font-size-14 color4e5d6b">{{$detail->email}}</span></li>
                            <li class="spaninfo font-size-14 title-booking-margin-bottom title-phone"><i class="color4e5d6b-op">Phone number: </i><span class="font-size-14 color4e5d6b">{{$detail->phone_number}}</span></li>
                        </ul>
                    </div>
                </div> <!-- end fieldset3-left -->
                
            </div><!-- end info book left -->

            <!-- info book right -->
            <div class="col-md-4 fieldset3thumb-right col-right-8 step-three content-stepThree">
                <div class="fieldset3-right">
                    <!-- booking info -->
                    <div class="booking-stt">
                        <ul class="list-right-statistical">
                            <li class="total-hrs-dates font-size-14 color263038-1">
                                <p><span class="color263038">{{$total_hours>1?'Hours':'Hour'}}</span>{{$total_hours}}</p>
                                <p><span class="color263038">{{$total_dates>1?'Days':'Day'}}</span>{{$total_dates}}</p>
                                <p><span class="color263038">{{$detail->capacity>1?'People':'Person'}}</span>{{$detail->capacity}}</p>
                            </li>
                            <li class="wrwt3 statusbook font-size-14 color263038-1">
                                <p><span class="color263038" style="float: left;">Status</span><span class="itemStt-{{$detail->status}}" style="display: inline-block;">{{$detail->status}}</span></p>
                            </li>
                            <li class="booking-price font-size-24 fnt-weight color0084fb"><span class="color263038 font-size-14">Total price: </span><span class="price-icon">{{$detail->price}}</span></li>
                        </ul>
                    </div>
                </div> <!-- end fieldset3-right -->
                <!-- button cancel -->
                <div class="step-right-top step-btnCancle stepCancle" data-url="" data-bkid="">
                    @if(!$isGuide && $detail->status == 'finished' && empty($detail->review))
                        <!-- BUTTON REVIEW GUIDE -->
                        <div class="step-right-top btn-review-Guide step-btnCancle">
                            <button type="button" class="btn-reviewGuide bgBtn-reviewguide font-size-12 color4" data-url="" data-value=""
                                    data-bkid="" data-toggle="modal" data-target="#reviewGuide">Review guide
                            </button>
                        </div>
                    @endif
                    @if($isGuide)
                        @if($detail->status == 'pending')
                            
                            <!-- BUTTON DECLINE  -->
                            <div class="step-right-top step-btnCancle">
                                <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-declined font-size-12 color4"
                                        data-url="{{Helper::url('statusBooking')}}"
                                        data-value="declined" data-bkid="{{$detail->id}}">Decline
                                </button>
                            </div>

                            <!-- BUTTON ACCEPT  -->
                            <div class="step-right-top step-btnCancle">
                                <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-accepted font-size-12 color4"
                                        data-url="{{Helper::url('statusBooking')}}"
                                        data-value="accepted" data-bkid="{{$detail->id}}">Accept
                                </button>
                            </div>

                        @endif
                    @elseif($detail->status == 'pending' || $detail->status == 'accepted')
                        <!-- BUTTON CANCLE  -->
                        <div class="step-right-top step-btnCancle">
                            <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-canceled font-size-12 color4"
                                    data-url="{{Helper::url('statusBooking')}}"
                                    data-value="canceled" data-bkid="{{$detail->id}}">Cancel
                            </button>
                        </div>
                    @endif

                   <!--  <button type="button" data-toggle="modal" class="btn-cancel-booking font-size-12 color4" data-type="refresh_page">Cancel this booking</button> -->
                </div>
            </div> <!-- end info book left -->

            <!-- message -->
            <div class="col-md-8 fieldset3thumb-left message--thumb">
                <!-- REVIEW GUIDE -->
                @if(!$isGuide && $detail->status == 'finished' && empty($detail->review))
                <div class="review--thumb">
                    <div class="title-bookingMessage--thumb">
                        <h3 class="title-bookingMessage font-size-14 color4e5d6b fnt-weight">Review</h3>
                    </div>
                    <div class="item-review contentReview--thumb">
                        <div class="contentReview">
                            <div class="avatar_aboutme">
                                <?php
                                $review = $detail->guide('traveler_id')->select('full_name', 'avatar')->first();
                                $avatar_review = $review->avatar ? Helper::getThemeAvatar($review->avatar) : Helper::getThemeImg('user.png');
                                ?>
                                <img src="{{$avatar_review}}" data-pin-nopin="true">
                            </div>
                            <div class="font-size-14 color4e5d6b content-listreview">
                                <div class="name-history-booking fnt-weight">{{$review->full_name}}
                                    <div class="rating-star">
                                        @for($i=1; $i<=$detail->ratings; $i++)
                                            <i class="fa fa-star yellow" aria-hidden="true"></i>
                                        @endfor
                                        @for($i=5; $i > $detail->ratings; $i--)
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        @endfor
                                    </div>
                                </div>
                                <div class="excerpt-list-review font-size-14 color666">
                                    <p class="content-review show-review">
                                        <span class="">{{$detail->review}}</span>
                                    </p>
                                    <p class="font-size-12 color999 convert-time-js">{{$detail->updated_at}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="message--thumb">
                    
                    @include('default::frontend.booking.chatBooking')
                </div>
            </div> <!-- end message -->

            @if(false)
            <div class="col-md-10 col-md-offset-1 step-three content-stepThree">
                <div class="panel-heading bg-form-bookingThree" role="tab" id="headingOne">
                    <div class="name-history-booking guideName color666 font-size-14 fnt-weight">
                        @if($isGuide) Traveler @else Guide @endif: <span
                                class="font-size-14">{{$partner->full_name}}</span>
                    </div>
                    <div class="booking-date bookingDate-thumbs color666">
                        Booking dates
                        @if($dates)
                            @foreach ($bookingDateHours as $date=>$hours)
                                <?php
                                $count = count($hours);
                                $total_hours += count($hours);
                                $total_dates++;
                                ?>
                                <div class="itemInfo-date">
                                    <p class="font-size-14 fnt-weight1 color666">{{Helper::formatDate($date) . ' - ' . Helper::addPlural($count, 'hour')}}</p>
                                    <ul class="list-booking-date">
                                        @foreach($hours as $h)
                                            <li>{{$h}}:00</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="booking-stt">
                        <ul class="list-right-statistical">
                            <li>Number of people: <span class="font-size-14 fnt-weight1">{{$detail->capacity}}</span>
                            </li>
                            <li>Total: <span class="font-size-14 fnt-weight1 total-book-date">{{Helper::addPlural($total_hours, 'hour')}}
                                    in {{Helper::addPlural($total_dates,'day')}}</span>
                            </li>
                            <li>Status: <span
                                        class="font-size-14 fnt-weight1 itemStt-{{$detail->status}}">{{$detail->status}}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="booking-location">
                        <ul class="list-locationInfo">
                            <li class="fnt-weight font-size-14 color666 title-booking-margin-bottom title-place">Country: <span
                                        class="font-size-14 fnt-weight1">{{$city->country()->first()->country_name}}</span></li>
                            <li class="fnt-weight font-size-14 color666 title-booking-margin-bottom title-place">City: <span
                                        class="font-size-14 fnt-weight1">{{$city->city_name}}</span></li>
                            <li class="fnt-weight font-size-14 color666 title-booking-margin-bottom title-place">Pickup placement: <span
                                        class="font-size-14 fnt-weight1">{{$detail->pickup_place}}</span></li>
                            <li class="fnt-weight font-size-14 color666 title-booking-margin-bottom">Destination: <span
                                        class="font-size-14 fnt-weight1">{{$detail->destination}}</span></li>
                        </ul>
                    </div>
                    <div class="booking-price font-size-24 fnt-weight colorBlue">
                        PRICE: ${{$detail->price}}
                    </div>
                </div>
            </div>

            <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 btnBookingdetail--thumb"
                 data-url="" data-bkid="">
                @if(!$isGuide && $detail->status == 'finished' && empty($detail->review))
                    <!-- BUTTON REVIEW GUIDE -->
                        <div class="step-right-top btn-review-Guide step-btnCancle">
                            <button type="button" class="btn-reviewGuide bgBtn-reviewguide" data-url="" data-value=""
                                    data-bkid="" data-toggle="modal" data-target="#reviewGuide">REVIEW GUIDE
                            </button>
                        </div>
                @endif
                @if($isGuide)
                    @if($detail->status == 'pending')
                        <!-- BUTTON ACCEPT  -->
                            <div class="step-right-top step-btnCancle">
                                <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-accepted"
                                        data-url="{{Helper::url('statusBooking')}}"
                                        data-value="accepted" data-bkid="{{$detail->id}}">Accept
                                </button>
                            </div>

                            <!-- BUTTON DECLINE  -->
                            <div class="step-right-top step-btnCancle">
                                <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-declined"
                                        data-url="{{Helper::url('statusBooking')}}"
                                        data-value="declined" data-bkid="{{$detail->id}}">decline
                                </button>
                            </div>
                    @endif
                @elseif($detail->status == 'pending' || $detail->status == 'accepted')
                    <!-- BUTTON CANCLE  -->
                        <div class="step-right-top step-btnCancle">
                            <button type="button" data-toggle="modal" class="btn-cancel-booking bgBtn-canceled"
                                    data-url="{{Helper::url('statusBooking')}}"
                                    data-value="canceled" data-bkid="{{$detail->id}}">Cancel
                            </button>
                        </div>
                    @endif
            </div>

            <!-- REVIEW GUIDE -->
            @if(!$isGuide && $detail->status == 'finished' && empty($detail->review))
                <div class="row review--thumb">
                    <div class="col-md-10 col-md-offset-1 title-bookingMessage--thumb">
                        <h3 class="title-bookingMessage font-size color333 fnt-weight">REVIEW</h3>
                    </div>
                    <div class="col-md-10 col-md-offset-1 item-review contentReview--thumb">
                        <div class="contentReview">
                            <div class="avatar_aboutme">
                                <?php
                                $review = $detail->guide('traveler_id')->select('full_name', 'avatar')->first();
                                $avatar_review = $review->avatar ? Helper::getThemeAvatar($review->avatar) : Helper::getThemeImg('user.png');
                                ?>
                                <img src="{{$avatar_review}}" data-pin-nopin="true">
                            </div>
                            <div class="font-size-14 color666 content-listreview">
                                <div class="name-history-booking fnt-weight">{{$review->full_name}}
                                    <div class="rating-star">
                                        @for($i=1; $i<=$detail->ratings; $i++)
                                            <i class="fa fa-star yellow" aria-hidden="true"></i>
                                        @endfor
                                        @for($i=5; $i > $detail->ratings; $i--)
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        @endfor
                                    </div>
                                </div>
                                <div class="excerpt-list-review font-size-14 color666">
                                    <p class="content-review show-review">
                                        <span class="">{{$detail->review}}</span>
                                    </p>
                                    <p class="font-size-12 color999 convert-time-js">{{$detail->updated_at}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row message--thumb">
                
                @include('default::frontend.booking.chatBooking')
            </div>
            @endif
        </div>

        <!-- POP UP REVIEW GUIDE -->
        @if(!$isGuide && $detail->status == 'finished')
        <div class="modal fade" id="reviewGuide" tabindex="-1" role="dialog" style="padding-right: 17px!important">
            <div class="container">

                <div class="modal-dialog col-md-12" role="document">
                    <div class="modal-content">
                        <div class="modal-header col-md-1 col-md-offset-9 col-sm-1 col-sm-offset-10 col-xs-1 col-xs-offset-11">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                            <h3 class="popupReview-title color333 fnt-weight font-size">REVIEW GUIDE</h3>

                            {{--@if(session()->has('users') && isset($checkBooking) && $checkBooking)--}}
                            {!! Form::open(['url'=>Helper::url('review'),'class'=>'traveler-review']) !!}
                            <div class="write-review-aboutme">
                                <div id="stars" class="starrr txt-align rating-star"></div>
                                <textarea class="font-size-14 form-control color999" id="traveler-review" rows="5"
                                          name="review"
                                          required placeholder="Write review" maxlength="300"></textarea>
                                <div class="submit-reviewGuide">
                                    <button type="submit"
                                            class="btn btn-secondary sbt-btn btn-review font-size-14 color4 fnt-weight bgBlue"
                                            data-booking_id="{{$detail->id}}">Submit
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            {{--@endif--}}
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        </div><!-- /.modal -->
        @endif
    </div>

    <div class="modal fade modal-custom" id="cancel-booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">Warning</div>
                <div class="modal-body">
                    <div class="cancel-request font-size-14">Are you sure you want to cancel this booking request?</div>
                    <div>
                        <div class="thumb-btnyn">
                            <button type="button" class="btn btn-secondary btn-request-yes">Yes</button>
                        </div>
                        <div class="thumb-btnyn">
                            <button type="button" class="btn btn-request-no" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
