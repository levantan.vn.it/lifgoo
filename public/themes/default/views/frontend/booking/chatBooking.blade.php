<!-- CHAT BOOKING -->
<div class="title-bookingMessage--thumb">
    @if($detail->status == 'pending' || $detail->status == 'accepted' || count($messages))
        <h3 class="title-bookingMessage font-size-14 color4e5d6b fnt-weight">Message</h3>
    @endif
</div>
<div class="chatbox--thumb">
    <div class="chat-bookingInfo--thumb">
        @if($detail->status == 'pending' || $detail->status == 'accepted')
            <div class="col-md-12 col-sm-12 col-xs-12 textarea-chat">
                <textarea class="font-size-14 form-control color4e5d6b" id="text" name="text" placeholder="Message" rows="5"  maxlength="10000" data-url="" data-type="message" data-cartid=""></textarea>
                <button class="btn btn-send message-booking font-size-12 bgBlue fnt-weight" type="submit"
                        data-url="{{Helper::url('messageBooking')}}" data-owner="{{$userGlobal->id}}"
                        data-bkid="{{$detail->id}}">Send
                </button>
            </div>
        @endif
        <div class="col-md-12 col-sm-12 col-xs-12 chat-cnt-edit-traveller">
            <div class="content-chat">
            @if(count($messages))
                @foreach($messages as $msg)
                    <?php
                    if (!array_key_exists($msg->user_id, $typeUser)) {
                        $typeUser[$msg->user_id] = $msg->user()->first();
                    }
                    $type = $typeUser[$msg->user_id]->type;
                    $avatar = $typeUser[$msg->user_id]->avatar;
                    $avatar = $avatar ? Helper::getThemeAvatar($avatar) : Helper::getThemeImg('user.png');
                    ?>
                    @if($msg->content)
                        <!-- trevaller_inbox -->
                        <div class="content-mesage avatar-bookinfo">
                            <div class="name-chat color263038 font-size-12">
                                <span class="fnt-weight">{{ $type=='guide'? $detail->guide('guide_id')->first()->full_name : $detail->guide('traveler_id')->first()->full_name }}</span>&nbsp<b>{{$type=='guide'?'(guide)':''}}</b>
                            </div>
                            <div class="avatar_traveller">
                                <img class="avatar-chatbox" src="{{$avatar}}">
                            </div>
                            <div class="item-message-chat" data-user_id="">
                                <div class="col-md-12 font-size-14 color4e5d6b {{$type=='guide'?'guide_inbox':'trevaller_inbox'}} message-chat"
                                     data-user_id="{{$msg->user_id}}">
                                     <span>{{$msg->content}}</span>            
                                </div>
                                <div class="col-md-12" style="padding-right: 0;">
                                    <time datetime="2009-11-13T20:00" class="font-size-12 color4e5d6b-op convert-time-js">{{$msg->created_at}}</time>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                @endif
            </div>
        @if(false)
            <!-- SHOW MORE MESSAGE -->
                <div class="show-earlier-messages">
                    <a class="color-pendding color999 font-size-12 fnt-weight" href="javascript:void(0)">Show earlier
                        messages</a>
                </div>
            @endif

        </div>
    </div>
</div>