<!--begin panel CANCELlED-->
<div class="panel booking-panel" data-bkid="{{$booking->id}}">
    <div class="panel-heading @if($isGuide && !$booking->is_read)mark-as-read @endif" role="tab" data-cartid="{{$booking->id}}" data-url="{{Helper::url('markRead')}}" data-type="cart">
        <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse"
               data-parent="#accordion"
               href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <div class="list-left">
                    <div class="avatar_traveller">
                        <img src="{{$guideJoin->avatar?Helper::getThemeAvatar($guideJoin->avatar):Helper::getThemeImg('user.png')}}">
                    </div>
                </div>
                <div class="list-right">
                    <div class="name-historyBooking">
                        <div class="name-history-booking">
                            <span class="colorBlue1 font-size-14 fnt-weight">{{$guideJoin->full_name}}</span>
                            @if(!$isGuide)
                                <div class="rating-star">
                                    @for($i=1; $i<= $guide_info->star; $i++)
                                        <i class="fa fa-star yellow"
                                           aria-hidden="true"></i>
                                    @endfor
                                    @for($i=4; $i >= $guide_info->star; $i--)
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    @endfor
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="list-deactive">
                        <div class="list-deactive-item">
                            <span class="font-size-14">Canceled</span>
                        </div>
                    </div>
                </div>
                <div class="status-booking" style="font-size: 14px">
                    <span class="font-size-14 fnt-weight itemStt-cancled">{{$booking->status}}</span>
                </div>
            </a>
        </h4>
    </div>
</div>
<!-- end of panel -->