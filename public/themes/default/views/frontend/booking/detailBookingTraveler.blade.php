<div class="row cnt-list-right color10">
    <div class="col-md-6">
        <div class="booking-date">Booking dates</div>
        <ul class="list-booking-date">
            {!!$li!!}
        </ul>
        <div class="total-book-date">
            Total: {{Helper::addPlural($total_count,'hour')}}
            in {{Helper::addPlural($total_dates,'day')}}
        </div>
    </div>
    <div class="col-md-6">
        <ul class="list-right-statistical">
            <li>Number of people: {{$booking->capacity}}</li>
            <li>Expected cost: ${{$booking->price}}</li>
        </ul>
    </div>
</div>