@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-news">
        <div class="slider-site">
            <!--SLIDER REVOLUTION-->
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul class="slide-list">    <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-title="Intro Slide">
                            <img src="{{Helper::getThemeImg('slider1.jpg')}}" alt="slidebg1" title=""
                                 data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        </li>
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-title="Intro Slide">
                            <img src="{{Helper::getThemeImg('slider2.jpg')}}" alt="slidebg1" title=""
                                 data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </div>

        <div class="bg-allnews">
            <div class="container">
                <div class="content-item-allNews">
                    <div class="advance_search ">
                        <h3 class="color2 font-size-24">All Tour</h3>
                    </div>
                    <div class="content-news row">
                        <div class="col-md-4">
                            <div class="item-allNews">
                                <div class="img-guide">
                                    <figure style="background-image: url('{{Helper::getThemeImg('Layer-6.jpg')}}')"></figure>
                                    <div class="info-allNews">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>Quảng Bình - Việt Nam</span>
                                    </div>
                                    <div class="hover_profile-guide">
                                        <div class="item-cnt-infoGuide">
                                            <div class="item-thumb-cnt-infoGuide">
                                                <a class="view_profile_guide font-size-14" href="#" title="">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-profile-news">
                                    <h4 class="item-name-profile color2">Thien Mu pagoda- Phong Nha cave</h4>
                                    <div class="post text-ellipsis">
                                        <span class="post-by">Post by</span><span class="post-name color2">Huong Tra Nguyen</span>
                                    </div>
                                    <div class="news-time-vehicles">
                                        <div class="item-allnews-left">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>3 Ngày 2 Đêm</span>
                                        </div>
                                        <div class="item-allnews-right">
                                            <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-allNews">
                                <div class="img-guide">
                                    <figure style="background-image: url('{{Helper::getThemeImg('Layer-6.jpg')}}')"></figure>
                                    <div class="info-allNews">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>Quảng Bình - Việt Nam</span>
                                    </div>
                                    <div class="hover_profile-guide">
                                        <div class="item-cnt-infoGuide">
                                            <div class="item-thumb-cnt-infoGuide">
                                                <a class="view_profile_guide" href="#" title="">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-profile-news">
                                    <h4 class="item-name-profile color2">Thien Mu pagoda- Phong Nha cave</h4>
                                    <div class="post text-ellipsis">
                                        <span class="post-by">Post by</span><span class="post-name color2">Huong Tra Nguyen</span>
                                    </div>
                                    <div class="news-time-vehicles">
                                        <div class="item-allnews-left">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>3 Ngày 2 Đêm</span>
                                        </div>
                                        <div class="item-allnews-right">
                                            <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-allNews">
                                <div class="img-guide">
                                    <figure style="background-image: url('{{Helper::getThemeImg('Layer-6.jpg')}}')"></figure>
                                    <div class="info-allNews">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>Quảng Bình - Việt Nam</span>
                                    </div>
                                    <div class="hover_profile-guide">
                                        <div class="item-cnt-infoGuide">
                                            <div class="item-thumb-cnt-infoGuide">
                                                <a class="view_profile_guide" href="#" title="">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-profile-news">
                                    <h4 class="item-name-profile color2">Thien Mu pagoda- Phong Nha cave</h4>
                                    <div class="post text-ellipsis">
                                        <span class="post-by">Post by</span><span class="post-name color2">Huong Tra Nguyen</span>
                                    </div>
                                    <div class="news-time-vehicles">
                                        <div class="item-allnews-left">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>3 Ngày 2 Đêm</span>
                                        </div>
                                        <div class="item-allnews-right">
                                            <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-allNews">
                                <div class="img-guide">
                                    <figure style="background-image: url('{{Helper::getThemeImg('Layer-6.jpg')}}')"></figure>
                                    <div class="info-allNews">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>Quảng Bình - Việt Nam</span>
                                    </div>
                                    <div class="hover_profile-guide">
                                        <div class="item-cnt-infoGuide">
                                            <div class="item-thumb-cnt-infoGuide">
                                                <a class="view_profile_guide" href="#" title="">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-profile-news">
                                    <h4 class="item-name-profile color2">Thien Mu pagoda- Phong Nha cave</h4>
                                    <div class="post text-ellipsis">
                                        <span class="post-by">Post by</span><span class="post-name color2">Huong Tra Nguyen</span>
                                    </div>
                                    <div class="news-time-vehicles">
                                        <div class="item-allnews-left">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>3 Ngày 2 Đêm</span>
                                        </div>
                                        <div class="item-allnews-right">
                                            <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-allNews">
                                <div class="img-guide">
                                    <figure style="background-image: url('{{Helper::getThemeImg('Layer-6.jpg')}}')"></figure>
                                    <div class="info-allNews">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>Quảng Bình - Việt Nam</span>
                                    </div>
                                    <div class="hover_profile-guide">
                                        <div class="item-cnt-infoGuide">
                                            <div class="item-thumb-cnt-infoGuide">
                                                <a class="view_profile_guide" href="#" title="">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-profile-news">
                                    <h4 class="item-name-profile color2">Thien Mu pagoda- Phong Nha cave</h4>
                                    <div class="post text-ellipsis">
                                        <span class="post-by">Post by</span><span class="post-name color2">Huong Tra Nguyen</span>
                                    </div>
                                    <div class="news-time-vehicles">
                                        <div class="item-allnews-left">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>3 Ngày 2 Đêm</span>
                                        </div>
                                        <div class="item-allnews-right">
                                            <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-allNews">
                                <div class="img-guide">
                                    <figure style="background-image: url('{{Helper::getThemeImg('Layer-6.jpg')}}')"></figure>
                                    <div class="info-allNews">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>Quảng Bình - Việt Nam</span>
                                    </div>
                                    <div class="hover_profile-guide">
                                        <div class="item-cnt-infoGuide">
                                            <div class="item-thumb-cnt-infoGuide">
                                                <a class="view_profile_guide" href="#" title="">View More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-profile-news">
                                    <h4 class="item-name-profile color2">Thien Mu pagoda- Phong Nha cave</h4>
                                    <div class="post text-ellipsis">
                                        <span class="post-by">Post by</span><span class="post-name color2">Huong Tra Nguyen</span>
                                    </div>
                                    <div class="news-time-vehicles">
                                        <div class="item-allnews-left">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>3 Ngày 2 Đêm</span>
                                        </div>
                                        <div class="item-allnews-right">
                                            <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                                            <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="pagination-form">
                        <nav aria-label="...">
                            <ul class="pagination pagination-sm">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" title="">Previous</a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#" title="">1</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">2</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">3</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">4</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">5</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">6</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">7</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">8</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">9</a></li>
                                <li class="page-item"><a class="page-link" href="#" title="">10</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" title="">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>--}}
                    {{--@if($flag_show)--}}
                        <div class="wraper-show-more">
                            <a class="show-more find-guide bgBlue color4 fnt-weight" href="javascript:void(0)" data-url="">VIEW MORE</a>
                        </div>
                        {{--@endif--}}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js_wrapper')
    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('.tp-banner').show().revolution(
                {
                    dottedOverlay: "none",
                    delay: 16000,
                    startwidth: 1170,
                    startheight: 600,
                    touchenabled: "on",
                    onHoverStop: "off",
                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,
                    parallax: "mouse",
                    parallaxBgFreeze: "on",
                    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                    keyboardNavigation: "off",
                    navigationHAlign: "center",
                    navigationVAlign: "bottom",
                    navigationHOffset: 0,
                    navigationVOffset: 20,
                    shadow: 0,
                    fullWidth: "off",
                    fullScreen: "off",
                    spinner: "spinner4",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "on",
                    forceFullWidth: "off",
                    hideThumbsOnMobile: "off",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "off",
                    hideArrowsOnMobile: "off",
                    hideThumbsUnderResolution: 0,
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    fullScreenOffsetContainer: ".header"
                });
        });

    </script>
@stop