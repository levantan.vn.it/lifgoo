@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-cus content-site-term">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">Terms of Service</h3>
        </div>
        <div class="cnt-site-login">
            <div class="bg-about-us content--minheight">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-10 col-xs-10 aboutUs--thumb aligncenter privacy_thumb">
                            <div class="content-about color666 font-size-14 text-justify">
                                <h3 class="text-center"><b>The terms of service of Lifgoo here are all very important, hope you read very carefully because it is a commitment by you to us and the commitment we with you when using the services of Lifgoo.</b></h3>
                                <ol class="list-upper-roman">
                                    <li>
                                        <h5>Description of service</h5>
                                        <p class="color666">Lifgoo provides members of the accessibility of information service, including: lifgoo allowed to hire a guide service, travel information location, your tour guide information, public image and content. Lifgoo and other services. The customer account can access the information, image information and guidance, have the right to participate in the employment service.<br>
                                        We will always endeavor to ensure stable operation of the Website and services, however Lifgoo will not be fully committed to the system will be providing continuous, stable or no technical errors (Website, services), or the intrusion of viruses. In the case of system failure or Website, Lifgoo team will make every effort to problems that do not affect to experience as well as your personal interests.<br>
                                        In addition, the Lifgoo service will have certain restrictions for accessing the resources on Lifgoo, so users must meet specific conditions to be able to access this resource.</p>
                                    </li> <!-- end I -->
                                    <li>
                                        <h5>Public information and sharing</h5>
                                        <p class="color666">Lifgoo provide services such as: Provides rental service tour guide, providing information of tourist attractions and other services to users.</p>
                                        <ol class="list-number color666">
                                            <li>When posting content must match Lifgoo services.</li>
                                            <li>Services Lifgoo allows users to upload images, information services, personal information of his Web site with forms available, except the content prohibited and not allowed by Lifgoo and legal documents State laws regarding the offer.</li>
                                            <li>All images, services, information you provide Lifgoo will not be responsible when it violates the rights of an individual, the system will be deleted if the proposal of the people are violated in case information sufficient evidence to prove.</li>
                                            <li>Users participating in service Lifgoo will bear full responsibility for these acts, information you actively posting on services Lifgoo, you may be responsible for Law, compensation for civil damages, vi administratively or prosecuted if they commit illegal acts. <br>
                                            Example: Using information Lifgoo create guides, fraud, appropriation of assets to meet customer directly through the introduction of information on Lifgoo, etc ...</li>
                                            <li>When has accepted to join the service Lifgoo means you have agreed to allow us to use the images, information (for the Guide) that you have shared through the system for publicly posted up the Website as well as storage rooms the case should data recovery information for you or for a Website, you have no rights to complain or claim any expenses for the pilot a information that you have provided to us.</li>
                                            <li>In any case, we have to handle the information that you have posted to suit the habits and customs as well as ensuring Law if information of a sensitive nature, we can allow or not allows information, your image appears on Lifgoo system.</li>
                                            <li>In the process of using and participating in the system Lifgoo, you may come across some link to websites not belonging to affiliate services Lifgoo by others posting or during the exchange of information with you (Guide or Traveler), you need careful consideration when clicking on these links, we will not responsible for this problem.</li>
                                            <li>The users need to read the instructions clearly, clearly defined before joining Lifgoo.</li>
                                        </ol>
                                    </li> <!-- end II -->
                                    <li>
                                        <h5>The rights and responsibilities of lifgoo</h5>
                                        <ol class="list-number color666">
                                            <li>Lifgoo respecting copyright information, images that members posted to the system, however we reserve the right to edit the information, images accordingly or canceled to ensure compliance with service Lifgoo well as provisions of the Law.</li>
                                            <li>Lifgoo have the right to block temporarily or permanently accounts of users who commit acts harmful to the Website Lifgoo in any form or intentional violation of the terms that we specified without compensation of any damage to users.</li>
                                            <li>Lifgoo may refuse the right to register as a member of the service if the member had been canceled on Lifgoo membership.</li>
                                            <li>Lifgoo have the right to provide information, pictures of members to third parties or State agencies competent in the case of a member violating the regulations on Lifgoo or at the request of law enforcement agencies in Vietnam.</li>
                                            <li>Lifgoo have the right to send electronic mail for the purpose of advertising, supplying information in the address which the member has registered on Lifgoo, in case you do not want to receive emails from us, please contact us at email: <a href="mailto:contact@vietprojectgroup.com<">contact@vietprojectgroup.com</a></li>
                                        </ol>
                                    </li> <!-- end III -->
                                    <li>
                                        <h5>Rights and responsibilities of member</h5>
                                        <ol class="list-number color666">
                                            <li>All members join Lifgoo are posting permissions information, photos or to exchange information with each other but must ensure the contents match the provisions of Lifgoo as well as the law was enacted, if the violation of this rule, but members may be responsible for complete responsibility for the damage caused.</li>
                                            <li>The user is committed to providing full information to the Lifgoo according to the form that we give out and totally responsible for this information.</li>
                                            <li>When users register Lifgoo, have a responsibility to protect your account and not to reveal your password to login, if lost password or suspect someone is logged illegally, you must immediately notify Lifgoo system to timely support and the change, however we will not be liable to indemnify the losses relating to lost members or expose the password in all cases.</li>
                                            <li>Users not using any tools, programs for the purpose of sabotage, unauthorized intrusion Lifgoo service.</li>
                                            <li>When discovered the fault of the system, please contact us by email: <a href="mailto:contact@vietprojectgroup.com<">contact@vietprojectgroup.com</a></li>
                                            <li>Members joining and posting information on the services Lifgoo must conform to the regulations of the site as well as the regulations of the State, members will be fully responsible to third parties or to the law if the offense copyright of an individual, certain enterprises.</li>
                                            <li>With guides (Guide), all the information that you register on the form of Lifgoo launched as well as photos will be public for tourists (Traveler) view and choice, we are not responsible for compensation damage to you if you cancel the contract or any other reason, these problems the guides and tourists need to review and sign the written agreement.</li>
                                            <li>With guides (Guide), if you find any information of other guides fake you or enterprise or organization you are working, contact your support team Lifgoo, where information and documentation full every account we will delete it from the system tampering or give you if you ask the prosecution authorities and was accepted.</li>
                                            <li>For tourists (Traveler), you can assess if you feel the quality of tour guides (guides) are not as you desire, or bad press.</li>
                                            <li>For tourists (Traveler) or guides (Guide), all activities related to travel Lifgoo will not interfere, so encouraged between two parties make a contract after the exchange, if any problems during the trip, the Lifgoo will not be liable to pay damages for you.</li>
                                            <li>Members who have participated have transparent information, clear to tourists (Traveler) or guides (Guide) reference, Lifgoo the right to delete accounts or banned permanently if the team we found information that is false or deceptive intention, not transparent as we had requested.</li>
                                        </ol>
                                    </li> <!-- end IV -->
                                    <li>
                                        <h5>Administrative penalties for violation of</h5>
                                        <ol class="list-number color666">
                                            <li>When joined Lifgoo, if members violate the rules which we set out, depending seriousness of the violation will be sanctioned corresponding way.
                                                <ul class="list-item">
                                                    <li>Delete content, images if the first violation and send a reminder notice accompanying error messages.</li>
                                                    <li>Lock account within 7 days if you continue to violate and sends reminder 2nd and accompanying error message.</li>
                                                    <li>Delete account permanently if you continue to violate.</li>
                                                </ul>
                                            </li>
                                            <li>In case of membership account by another person accused of forgery, fraud and information clear evidence, we will remove your account immediately or provide information to prosecution if the authorities competent the right to ask.</li>
                                            <li>Spread, fabricating information is not true about Lifgoo on social networking sites, media without evidence, Lifgoo will remind or inform the competent authorities to intervene.</li>
                                            <li>Prosecution, investigation and clarify with members taking advantage of loopholes, errors Lifgoo system to undermine or exploit information.</li>
                                            <li>Lifgoo team posing for personal purposes illegal appropriation, fraud without our permission.</li>
                                        </ol>
                                    </li> <!-- end V -->
                                    <li>
                                        <h5>Personal information</h5>
                                        <ol class="list-number color666">
                                            <li>The personal information of registered users as a member on our Lifgoo will be stored to be used for the following cases:
                                                <ul class="list-item">
                                                    <li>Recover your account Lifgoo.</li>
                                                    <li>Provide services to members Lifgoo.</li>
                                                    <li>Send information to your ads or notifications by e-mail members.</li>
                                                    <li>Contact, deal with members in case of need.</li>
                                                    <li>Use the information in these cases required state agencies to investigate the violations.</li>
                                                </ul>
                                            </li>
                                            <li>The members should be responsible for the security of login account Lifgoo your personal information or other sensitive nature that Lifgoo not mandatory declaration.</li>
                                            <li>The personal information of members will be stored on your system Lifgoo and will not be canceled unless otherwise requested cancellation or request the competent authority.</li>
                                        </ol>
                                    </li>  <!-- end VI -->
                                    <li>
                                        <h5>Intellectual property rights</h5>
                                        <ol class="list-number color666">
                                            <li>All activities copy entire Web site and services Lifgoo, from photographs and information on the Web site to use for unauthorized purposes, not through the permission of Lifgoo will be prosecuted through the competent state authority.</li>
                                            <li>Our image allows you to share your information, from the web site, other social vehicles to consult with questions or advertise their services, need not through our license.</li>
                                            <li>After registering and using the services of Lifgoo, meaning that you have committed to that information, that image is true and you full intellectual property information and pictures you posted.</li>
                                            <li>When posting information, photos publicly on services Lifgoo, other users can view the information, photographs of your service as well as the use of public information, which for other purposes, such as: - response service, used to compare other services, etc ...</li>
                                            <li>We will be committed to protecting the intellectual property of you, so if you see the use case information, publicity photos of you the purpose of fraud, impersonation, please contact the support team of Lifgoo at the contact information for timely solutions offline.</li>
                                        </ol>
                                    </li>   <!-- end VII -->
                                    <li>
                                        <h5>Report copyright infringement</h5>
                                        <ol class="list-number color666">
                                            <li>In the process of using the service, if you encounter the case of using an image, your information or the business that you know or are working on services Lifgoo without through allows your aim fraud, impersonation, please immediately notify us of violations so we treat the procedure as follows:
                                                <ul class="list-item">
                                                    <li>Information and images of your service.</li>
                                                    <li>Image, the personal information of you: as ID card, social security, driver's licenses or other qualifications to prove the information matches the information papers, photographs on Lifgoo.</li>
                                                    <li>Pictures and information of your business that you know or are working as well as the relevant documents to prove objects unauthorized use, piracy.</li>
                                                    <li>Service descriptions copyright infringement that you discovered OK.</li>
                                                    <li>Your contact information to our business or related certification.</li>
                                                    <li>The report your business or if possible.</li>
                                                </ul>
                                            </li>
                                            <li>When we report to you the commitment means that users falsify information, personal or corporate image, you know is right, you must be responsible for your punishment, if this information Perjury is not real or deliberate destruction of the user.</li>
                                        </ol>
                                    </li>  <!-- end VIII -->
                                    <li>
                                        <h5>General agreement</h5>
                                        <ol class="list-number color666">
                                            <li>The foregoing is considered as an agreement between the user and Lifgoo, during operation there will be some provisions to be changed or added, we will notify you by text message on the system and Email to you if there is a change.</li>
                                            <li>In case one or several provisions of Lifgoo conflict with the provisions of the law of Vietnam, that provision will be modified to conform with the provisions of the current law.</li>
                                            <li>Deal worth signing up service from Lifgoo and you've read.</li>
                                            <li>In case you skipped content agreement and do not read to also mean you agree with the service, we will always have the notice aims recommend reading through the agreement on, so please you take some time to go through.</li>
                                        </ol>
                                    </li>  <!-- end IX -->
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
