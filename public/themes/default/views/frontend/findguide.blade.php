@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-aboutus">
        <div class="banner-findguide bg-property"
             style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">Advance Search</h3>
        </div>
        <div class="cnt_find_guide">
            <div class="infoFind--thumb">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-infoFind--thumb aligncenter">
                            <!--Enter the info-->
                            {{Form::open(['url'=>Helper::url('findguide'),'method'=>'post','id'=>'findGuide'])}}
                            <div class="enter_the_info">

                                <div class="form_enter_info row">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12 text-label text-label-left">
                                            <label class="col-md-12 color666 font-size-14 fnt-weight" for="countryid_from">From</label>
                                            <div class="col-md-12 col-sm-12 col-xs-6 col-fromTo col-from--thumb col-from1--thumb frm_edit">
                                                <div class="frm-search">
                                                    <select data-url="{{Helper::url('admin/cities')}}" class="form-control font-size-14 selectpicker select-country" data-live-search="true" name="country_id_from" id="countryid_from">
                                                        @if(isset($countries) && count($countries))
                                                            @foreach($countries as $country)
                                                                <option value="{{$country->country_id}}" @if($country->country_id==140) selected @endif>{{$country->country_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-6 col-fromTo col-from--thumb col-from2--thumb frm_edit">
                                                <div class="frm-search">
                                                    <select class="form-control font-size-14 selectpicker select-city"
                                                            data-live-search="true" id="country_id_from">
                                                        @if(isset($cities) && count($cities))
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->city_id}}">{{$city->city_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        {{--TO--}}
                                        <div class="col-md-6 col-sm-6 col-xs-12 text-label text-label-right">
                                            <label class="color666 col-md-12 font-size-14 fnt-weight" for="countryid_to">To
                                                <span class="require">*</span></label>
                                            <div class="col-md-12 col-sm-12 col-xs-6 col-fromTo col-to--thumb col-to1--thumb frm_edit">
                                                <div class="frm-search">
                                                    <div class="frm-search">
                                                        <select data-url="{{Helper::url('admin/cities')}}" class="form-control font-size-14 selectpicker select-country"
                                                                data-live-search="true" name="country_id_to"
                                                                id="countryid_to">
                                                            @if(isset($countries) && count($countries))
                                                                @foreach($countries as $country)
                                                                    <option value="{{$country->country_id}}" @if($country->country_id==140) selected @endif>{{$country->country_name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-6 col-fromTo col-to--thumb col-to2--thumb frm_edit">
                                                <div class="frm-search">
                                                    <select class="form-control font-size-14 selectpicker select-city city-search"
                                                            data-live-search="true" name="city_id_to">
                                                        @if(isset($cities) && count($cities))
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->city_id}}">{{$city->city_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-dateLanguage--thumb">
                                        <!-- code choose date (OLD) -->
                                        {{--<div class="col-md-4 col-sm-4 col-xs-4 col-item">
                                            <div class="frm-search">
                                                <label class="color666 font-size fnt-weight" for="begin_date">Begin date
                                                    <span class="require">*</span></label>
                                                <div class="thumb-inputDate">
                                                    <input type="text"
                                                           class="form-control datefilter thumb-datefilter font-size-14"
                                                           id="begin_date"
                                                           placeholder="yyyy-mm-dd" name="begin_date"><span
                                                            class="icon-inputDate"><i class="fa fa-calendar"
                                                                                      aria-hidden="true"></i></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-item">
                                            <div class="frm-search">
                                                <label class="color666 font-size fnt-weight" for="end_date">End date
                                                    <span class="require">*</span></label>
                                                <div class="thumb-inputDate">
                                                    <input class="form-control datefilter thumb-datefilter font-size-14"
                                                           type="text" id="end_date" value=""
                                                           placeholder="yyyy-mm-dd" readonly name="end_date">
                                                    <span class="icon-inputDate"><i class="fa fa-calendar"
                                                                                    aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>--}}
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-fromTo col-item col-choosedate">
                                            <div class="frm-search">
                                                <label class="color666 font-size-14 fnt-weight" for="choose-date">Choose date<span class="require">*</span></label>
                                                <div class="thumb-inputDate">
                                                    <input id="choose-date" class="form-control thumb-datefilter font-size-14" type="text" name="daterange" value="" readonly><span class="icon-inputDate"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 col-fromTo col-item col-langague">
                                            <div class="frm-search">
                                                <label class="color666 font-size-14 fnt-weight" for="Language">Language
                                                    <span class="require">*</span></label>
                                                <select class="form-control font-size-14 selectpicker"
                                                        data-live-search="true" id="Language" name="language">
                                                    @if(isset($languagesWorld) && $languagesWorld)

                                                        @foreach($languagesWorld as $langs)
                                                            <option value="{{$langs->id}}">{{$langs->name}}</option>
                                                        @endforeach
                                                        <option value="" style="display: none;">&nbsp;</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!--Price range-->
                            <div class="price_range price_range--thumb">
                                <div class="price_range_title color666 fnt-weight font-size-14">Hourly rate</div>
                                <input type="text" id="range" value="" name="hourly_rate"/>
                            </div>
                            <!--To help you with-->
                            <div class="to_help_you">
                                <div class="to_help_you_title color666 fnt-weight font-size-14">To help you with</div>

                                @if(isset($services) && $services)
                                    @foreach($services as $ser)
                                        <?php
                                        $serLang = $ser->serviceLang()->first();
                                        ?>
                                        @if($serLang)
                                            <div class="col-md-3 col-sm-6 col-xs-6 col-help-item">
                                                <input type="checkbox" value="{{$ser->id}}" id="{{$ser->id}}"
                                                       class="checkHelp" name="services[]" checked="checked">
                                                <label class="checkbox-inline font-size-14"
                                                       for="{{$ser->id}}"><span></span><span
                                                            class="helpyou-item">{{$serLang->service_name}}</span>
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <!--And take you around by-->
                            <div class="take_you_around">
                                <div class="take_you_around_title color666 fnt-weight font-size-14">And take you around by</div>
                                @foreach($transportations as $ser)
                                    <?php
                                    $serLang = $ser->serviceLang()->first();
                                    ?>
                                    @if($serLang)
                                        <div class="col-md-3 col-sm-4 col-xs-6 col-takearound-item">
                                            <input type="checkbox" value="{{$ser->id}}" id="{{$ser->id}}"
                                                   class="checkHelp"
                                                   name="transportations[]" checked="checked">
                                            <label class="checkbox-inline font-size-14"
                                                   for="{{$ser->id}}"><span></span>{{$serLang->service_name}}</label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <input type="hidden" value="0" name="page">
                            <div class="row">
                                <div class="col-md-12 btnSearchGuide-thumb">
                                    <button type="submit"
                                            class="btn btn_update btn-searchGuide bgBlue fnt-weight font-size-14 color4"
                                            data-url="{{Helper::url('findGuide')}}">
                                        Search
                                    </button>
                                </div>
                            </div>

                            {!!Form::close()!!}
                        </div> <!-- end col-infoFind-thumb -->
                    </div> <!-- end row -->
                </div> <!-- end container -->
            </div> <!-- end infoFind-thumb -->

            <div class="listFind--thumb resultGuide-hide">
                <div class="container">

                    <!--Guide selected for you-->
                    <div class="advance_search">
                        <h3 class="font-size-24 color263038-1 fnt-weight" style="margin-top: 35px; padding-bottom: 5px;">Some tour guide for you to choose</h3>
                    </div>
                    <div class="row guide_selected">
                        {{--@include('default::frontend.guide.detailGuide')--}}
                    </div>
                    {{--<div class="pagination-form">
                        <nav aria-label="...">
                            <ul class="pagination pagination-sm">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">6</a></li>
                                <li class="page-item"><a class="page-link" href="#">7</a></li>
                                <li class="page-item"><a class="page-link" href="#">8</a></li>
                                <li class="page-item"><a class="page-link" href="#">9</a></li>
                                <li class="page-item"><a class="page-link" href="#">10</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>--}}
                    {{--@if($flag_show)--}}
                    <div class="wraper-show-more">
                    </div>
                    {{--@endif--}}
                </div> <!-- end container -->
            </div> <!-- end listFind-thumb -->
        </div> <!-- end cnt_find_guide -->
    </div>
@stop
@section('js_wrapper')
    <script type="text/javascript">

        jQuery(document).ready(function () {
            // jQuery('#end_date').change(function () {
            //     if ($(this).val() < $("#begin_date").val()) {
            //         alert("Please enter a valid begin date and end date!");
            //         $(this).val($("#begin_date").val());
            //         $(this).data('daterangepicker').setStartDate($(this).val());
            //         $(this).data('daterangepicker').setEndDate($(this).val());
            //     }
            // });
            // jQuery('#begin_date').change(function () {
            //     $('#end_date').val($(this).val());
            //     $('#end_date').data('daterangepicker').setStartDate($(this).val());
            //     $('#end_date').data('daterangepicker').setEndDate($(this).val());
            // });
            $('select.select-country').trigger("change");


            jQuery('.tp-banner').show().revolution(
            {
                dottedOverlay: "none",
                delay: 16000,
                startwidth: 1170,
                startheight: 600,
                touchenabled: "on",
                onHoverStop: "off",
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
                parallax: "mouse",
                parallaxBgFreeze: "on",
                parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                keyboardNavigation: "off",
                navigationHAlign: "center",
                navigationVAlign: "bottom",
                navigationHOffset: 0,
                navigationVOffset: 20,
                shadow: 0,
                fullWidth: "off",
                fullScreen: "off",
                spinner: "spinner4",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "on",
                forceFullWidth: "off",
                hideThumbsOnMobile: "off",
                hideNavDelayOnMobile: 1500,
                hideBulletsOnMobile: "off",
                hideArrowsOnMobile: "off",
                hideThumbsUnderResolution: 0,
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ".header"
            });

            $(document).on('click', '.guide-slide-item', function () {
                var thisId = $(this).attr('data-id');
                window.open('{{Helper::url("profile")."/"}}'+thisId,'_blank');
                console.log("sqqq");
            });

            $(document).on('click', '.book-profile-btn', function (e) {
                var thisId = $(this).attr('data-id');
                window.open('{{Helper::url("booking")."/"}}'+thisId,'_blank');
                if (window.event) {
                    window.event.cancelBubble=true;
                    }
                else {
                    if (e.cancelable ) {e.stopPropagation();}
                    }
            });
            
        });

    </script>
@stop