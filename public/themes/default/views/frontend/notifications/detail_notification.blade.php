<?php
if (!isset($user_notify[$notify->sender_id]))
    $user_notify[$notify->sender_id] = $notify->users('sender_id')->select('id', 'full_name', 'avatar')->first();
$content = $notify->content($notify->type,null, $user_notify[$notify->sender_id],$notify->booking_id);
$avatar = $user_notify[$notify->sender_id]->avatar ? Helper::getThemeAvatar($user_notify[$notify->sender_id]->avatar) : Helper::getThemeImg('user.png');
?>

<li @if(!$notify->is_read) class="mark-as-read" data-url="{{Helper::url('markRead')}}" @endif data-noid="{{$notify->id}}" data-type="bell">
    <input type="checkbox" value="{{$notify->id}}" name="item-delete-notify" id="{{$notify->id}}-single" class="checkHelp">
    <label class="checkbox-inline font-size-14 color999 fnt-weight1" for="{{$notify->id}}-single"><span></span></label>
    <div class="avatar_traveller">
        <img src="{{$avatar.'?id='.Helper::versionImg()}}">
    </div>
    <div class="excerpt-notification font-size-14 color666">
        {!!$content!!}
    </div>
    <div class="date-notification font-size-14 convert-time-js">{{$notify->created_at}}</div>

</li>
