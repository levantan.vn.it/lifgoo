<?php
if (!isset($userGlobal_notify[$no->sender_id]))
    $userGlobal_notify[$no->sender_id] = $no->users('sender_id')->select('id', 'full_name', 'avatar')->first();
$content = $no->content($no->type, null, $userGlobal_notify[$no->sender_id], $no->booking_id);
$avatar = $userGlobal_notify[$no->sender_id]->avatar ? Helper::getThemeAvatar($userGlobal_notify[$no->sender_id]->avatar) : Helper::getThemeImg('user.png');
$isGuide = session('users') && session('users.guide');
?>
<li @if(!$no->is_read)class="mark-as-read" data-url="{{Helper::url('markRead')}}" @endif data-noid="{{$no->id}}"
    data-type="bell">
    <div class="media">
        <div class="pull-left">
            @if(!$isGuide)
                <a class="notify-item" href="{{Helper::url('profile/'.$no->sender_id)}}">
                    <img class="notify-img-sm"
                         src="{{$avatar.'?id='.Helper::versionImg()}}" alt="" title="">
                </a>
            @else
                <img class="notify-img-sm"
                     src="{{$avatar.'?id='.Helper::versionImg()}}" alt="" title="">
            @endif
        </div>
        <div class="media-body">
            <small class="notify-small">
                {!!$content!!}
            </small>
            <p class="timeup-notify convert-time-js">{{$no->created_at}}</p>
        </div>
    </div>

</li>
