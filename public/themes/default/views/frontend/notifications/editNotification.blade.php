{{--EDIT NOTIFICATION--}}
<div class="tab-pane edit-guide" id="notification">
    {!!Form::open(['url'=>Helper::url('editNotify'),'method'=>'post','class'=>'form-notify']) !!}
    <div class="checkall-delete col-md-12">
        <input id="select_all" type="checkbox" class="checkHelp"><label class="checkbox-inline font-size-14 color999 fnt-weight1" for="select_all"><span></span>Select All</label>
        <i class="fa fa-trash-o delete-notification color999 fnt-weight" aria-hidden="true"><span class="font-size-14 color999 fnt-weight1">Delete</span></i>
    </div>
    <ul class="edit-notification color10">
        <?php
            $user_notify = [];
        ?>
        @if(isset($notifications) && count($notifications))
            @foreach($notifications as $notify)
                @include('default::frontend.notifications.detail_notification')
            @endforeach
        @endif
        
    </ul>
    <div class="wraper-show-more">
        @if(isset($flag_show_notify) && $flag_show_notify)
            <a class="show-more show-more-notify" href="javascript:void(0)" data-url="{{Helper::url('moreNotify')}}" data-page="1" data-type="notifications">VIEW MORE</a>
        @endif
    </div>
    {{ Form::close() }}
</div>