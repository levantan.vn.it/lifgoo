@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-aboutus">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">@if(isset($title)){{$title}}@endif</h3>
        </div>
        <div class="cnt_find_guide list-bestGuide content--minheight">
            <div class="container">

                {{--<div class="advance_search">
                    <h3 class="font-size-24">@if(isset($title)){{$title}}@endif</h3>
                </div>--}}
                <div class="row guide_selected flex-row show-more-append">
                    @if(isset($guides) && count($guides))
                        @foreach($guides as $guide)
                            @include('default::frontend.guide.detailBestGuide')
                        @endforeach
                    @else
                        <div class="col-md-3 col-sm-4 col-xs-4 item_vp_guide">
                            <p>Guide not found</p>
                        </div>
                    @endif
                </div>
                {{--@if($flag_show)--}}
                <div class="wraper-show-more" @if(!$flag_show) style="display: none;" @endif>
                    <a class="show-more best-guide bgBlue color4 fnt-weight" href="javascript:void(0)" data-url="" data-page="1">VIEW MORE</a>
                </div>
                {{--@endif--}}
            </div>
        </div>
    </div>
@stop
@section('js_wrapper')
    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('.tp-banner').show().revolution(
                {
                    dottedOverlay: "none",
                    delay: 16000,
                    startwidth: 1170,
                    startheight: 600,
                    touchenabled: "on",
                    onHoverStop: "off",
                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,
                    parallax: "mouse",
                    parallaxBgFreeze: "on",
                    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                    keyboardNavigation: "off",
                    navigationHAlign: "center",
                    navigationVAlign: "bottom",
                    navigationHOffset: 0,
                    navigationVOffset: 20,
                    shadow: 0,
                    fullWidth: "off",
                    fullScreen: "off",
                    spinner: "spinner4",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "on",
                    forceFullWidth: "off",
                    hideThumbsOnMobile: "off",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "off",
                    hideArrowsOnMobile: "off",
                    hideThumbsUnderResolution: 0,
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    fullScreenOffsetContainer: ".header"
                });
        });

    </script>
@stop