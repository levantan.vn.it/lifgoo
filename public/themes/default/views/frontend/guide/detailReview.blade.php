<?php
$traveler = $rating->guide('traveler_id')->select('avatar', 'full_name')->first();
?>

<div class="col-md-12 item-review">
    <div class="avatar_aboutme">
        <img src=" @if($traveler->avatar){{Helper::getThemeAvatar($traveler->avatar).'?id='.Helper::versionImg()}}@else {{Helper::getThemeImg('user.png')}} @endif" alt="user.png" title=""
             data-pin-nopin="true">
        <div class="name-history-booking fnt-weight color4e5d6b font-size-14">{{$traveler->full_name}}
            <div class="rating-star">
                @for($i=1; $i<= $rating->ratings; $i++)
                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                @endfor
                @for($i=4; $i >= $rating->ratings; $i--)
                    <i class="fa fa-star" aria-hidden="true"></i>
                @endfor
            </div>
        </div>
    </div>
    <div class="font-size-14 color4e5d6b content-listreview">
        <div class="excerpt-list-review font-size-14 color4e5d6b">
            <p class="content-review show-review">
                <span class="short-content-review">{{$rating->review}}</span>
                <span class="icon-hide-shortContent color4e5d6b"><i
                            class="fa fa-plus-square" aria-hidden="true"></i></span>
            </p>
            <p class="content-review hidden-review">
                    <span class="font-size-14">{{$rating->review}}</span>
                    <span><i class="fa fa-minus-square" aria-hidden="true"></i></span>
                </p>
            <p class="convert-time-js font-size-12 color4e5d6b" style="opacity: 0.6; text-align: right;">{{$rating->updated_at}}</p>
        </div>
    </div>
</div>


@if(false)
<div class="col-md-12 item-review">
    <div class="col-md-2 col-sm-2 col-xs-2 avatar_aboutme">
        <img src=" @if($traveler->avatar){{Helper::getThemeAvatar($traveler->avatar).'?id='.Helper::versionImg()}}@else {{Helper::getThemeImg('user.png')}} @endif" alt="user.png" title=""
             data-pin-nopin="true">
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 font-size-14 color666 content-listreview">
        <div class="name-history-booking fnt-weight">{{$traveler->full_name}}
            <div class="rating-star">
                @for($i=1; $i<= $rating->ratings; $i++)
                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                @endfor
                @for($i=4; $i >= $rating->ratings; $i--)
                    <i class="fa fa-star" aria-hidden="true"></i>
                @endfor
            </div>
        </div>
        <div class="excerpt-list-review font-size-14 color666">
            <p class="content-review show-review">
                <span class="short-content-review">{{$rating->review}}</span>
                <span class="icon-hide-shortContent color666"><i
                            class="fa fa-plus-square" aria-hidden="true"></i></span>
            </p>
            <p class="content-review hidden-review">
                    <span class="font-size-14">{{$rating->review}}</span>
                    <span><i class="fa fa-minus-square" aria-hidden="true"></i></span>
                </p>
            <p class="font-size-12 color999">{{$rating->updated_at}}</p>
        </div>
    </div>
</div>
@endif