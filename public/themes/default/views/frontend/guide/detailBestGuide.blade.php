<!-- GUIDE 1 -->
<?php
$user = $guide->user()->select('id', 'full_name', 'avatar')->first();
$avatar = $user->avatar ? Helper::getThemeAvatar($user->avatar) : Helper::getThemeImg('user.png');
$location = $guide->locationTour()->first();
$locationmore = $guide->locationTour()->get();
?>
@if(false)
<div class="col-md-3 col-sm-4 col-xs-4 item_vp_guide flex-direction">
    <div class="item-Findguide--thumb">
        <div class="img-guide div-wrapper-itemGallery">
            <div class="div-pad-itemGallery"></div>
            <figure style="background-image: url('{{$avatar}}?id={{Helper::versionImg()}}')"
                    class="bg-property div-inner-itemGallery"></figure>
            {{--<div class="hover_profile-guide">
                <div class="item-cnt-infoGuide">
                    <span class="from-price color4 font-size-14">From <i class="font-size1 font-size-18">${{$guide->hourly_rate}}</i></span>
                    <div class="item-thumb-cnt-infoGuide">
                        <a class="view_profile_guide font-size-14" href="{{Helper::url('profile/'.$user->id)}}" target="_blank">View
                            Profile</a>
                    </div>
                </div>
            </div>--}}
        </div>

        <div class="text-center item-profile-guide flex-thumb detailsGuide">
            <div class="no-hover">
                <h4 class="item-name-profile color333 fnt-weight txt-align font-size-14">{{$user->full_name}}</h4>
                @if(count($location))
                    <?php
                    $city = $location->cities()->first();
                    $country = $location->countries()->first();
                    $count_city = $location->cities()->count();
                    ?>
                    <span class="item-adress-profile color999 font-size-12">{{$city->city_name}}
                        - {{$country->country_name}} @if($count_city >1)and more @endif</span>
                @endif
            </div>
            <div class="hover-y" style="display: none;">
                <a class="color4 fnt-weight font-size-14 bgBlue" href="{{Helper::url('profile/'.$user->id)}}"
                   target="_blank">View Profile</a>
            </div>

            <div class="rating-star txt-align">
                @for($i=1; $i<= $guide->star; $i++)
                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                @endfor
                @for($i=4; $i >= $guide->star; $i--)
                    <i class="fa fa-star" aria-hidden="true"></i>
                @endfor
            </div>
            <span class="from-price font-size fnt-weight">From <i
                        class="font-size1 font-size-18">${{$guide->hourly_rate}}</i></span>
        </div>
    </div>
</div> <!-- end GUIDE ITEM -->
@endif

<div class="text-center slide col-lg-3 col-md-4 col-sm-6 col-xs-6">
    {{--<div class="slide-thumb" data-url="{{Helper::url('profile/'.$user->id)}}">--}}
    <div class="slide-thumb" onClick="window.open('{{Helper::url('profile/'.$user->id)}}','_blank')">
        <div class="img-round">
            <img src="{{$avatar}}?id={{Helper::versionImg()}}" title="" alt="{{$user->full_name}}">
        </div>

        <div class="item-profile-guide">
            <div class="">
                <!-- name -->
                <h4 class="item-name-profile color0084fb font-size-14 fnt-weight">{{$user->full_name}}</h4>
                <!-- price -->
                <div class="price-profile-info font-size-24 fnt-weight color263038-1">
                    <div>
                        <i class="fa fa-usd" aria-hidden="true"></i>{{$guide->hourly_rate}}/h
                    </div>
                </div>
            </div>
            <!-- star -->
            <div class="rating-star">
                @for($i=1; $i<= $guide->star; $i++)
                    <i class="fa fa-star yellow" aria-hidden="true"></i>
                @endfor
                @for($i=4; $i >= $guide->star; $i--)
                    <i class="fa fa-star" aria-hidden="true"></i>
                @endfor
            </div>
        </div>
        <div class="dropdown no-hover">
            <!-- location active -->
            @if(count($location))
                <?php
                $city = $location->cities()->first();
                $country = $location->countries()->first();
                $count_city = $location->cities()->count();
                ?>
                <span class="font-size-12 fnt-weight color4e5d6b" style="width: 100%; display: inline-block; margin-bottom: 5px;">Guide locations:</span>
                <span class="item-adress-profile color4e5d6b font-size-12">{{$city->city_name}} - {{$country->country_name}}</span>
                <?php $j = 1;?>
                    @foreach($locationmore as $lc)
                        <?php $j++; ?>
                    @endforeach
                    @if($j>2)
                    <div class="more-location">
                        <span class="font-size-12 fnt-weight color4e5d6b hover-link">&nbsp(More)</span>
                        <div class="dropdown-content dropdown-content-location">
                            <p class="font-size-12 color4e5d6b">
                                <?php $i = 1; ?>
                                @foreach($locationmore as $lc)
                                <span class="location-itemlist">
                                    @if($i<2)
                                        {{$lc->cities()->first()->city_name}} - {{$lc->countries()->first()->country_name}}
                                    @else
                                        , {{$lc->cities()->first()->city_name}} - {{$lc->countries()->first()->country_name}}
                                    @endif
                                    <?php  $i++;?>
                                </span>
                                @endforeach
                            </p>
                        </div>
                    </div>
                @endif
            @endif

        </div>
        <!-- LANGUAGE -->
        <div class="dropdown no-hover">
            <span class="font-size-12 fnt-weight color4e5d6b" style="width: 100%; display: inline-block; margin-bottom: 5px;">Language:</span>
            <?php 
                $knownlanguages=$guide->known_languages()->get(); 
                $count=count($knownlanguages);
                $j=1; $k=1;
            ?>
            @foreach($knownlanguages as $language)
                @if($j<3)
                    <span class="item-adress-profile color4e5d6b font-size-12">
                    @if($k<2)                                                    
                        {{$language->language()->first()->name}}
                    @else
                        , {{$language->language()->first()->name}}
                    @endif
                    </span>
                @endif
                <?php $j++; $k++; ?>
            @endforeach
            @if($j>3)
                <div class="more-language">
                    <span class="font-size-12 fnt-weight color4e5d6b hover-link">&nbsp(More)</span>
                    <div class="dropdown-content dropdown-content-language">
                        <p class="font-size-12 color4e5d6b">
                            <?php $i = 1; ?>
                            @foreach($knownlanguages as $language)
                                <span class="location-itemlist">
                                    @if($i<2)
                                        {{$language->language()->first()->name}}
                                    @else
                                        , {{$language->language()->first()->name}}
                                    @endif
                                    <?php  $i++;?>
                                </span>
                            @endforeach
                        </p>
                    </div>
                </div> 
            @endif
        </div>
        <div class="hover-y">
            {{--<a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="{{Helper::url('booking/'.$user->id)}}" title="">BOOK GUIDE</a>--}}
            <a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="javascript:void(0)" onClick="window.open('{{Helper::url('booking/'.$user->id)}}','_blank')" title="">BOOK GUIDE</a>
        </div>
    </div>   
</div>
