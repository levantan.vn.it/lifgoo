@extends('default::frontend.layouts.master')

@section('content')

@if(isset($news))
        <?php
        $isGuide = session('users') && session('users.guide');
        $newsLang = $news->news_language()->where('language_code', '=', 'en')->first();
        $cities = $news->cities()->first();
        $countries = $news->countries()->first();
        $user = $news->users()->select('full_name', 'type')->first();
        $info_newsLang = [
            $news->id => $newsLang
        ];
        ?>

    <!-- breadcrumb -->
    <div id="breadcrumb-detailnew">
        <div class="container">
            <div class="row">
                <div class="col-md-12 breadcrumb--thumb">
                    <ul class="breadcrumb-News">
                        <li class="color4e5d6b-op"><a href="{{Helper::url('')}}" title="" class="font-size-12 color4e5d6b">Home</a>&nbsp;/&nbsp;</li>
                        <li class="color4e5d6b-op"><a href="{{Helper::url('news')}}" title="" class="font-size-12 color4e5d6b">News</a>&nbsp;/&nbsp;</li>
                        <li><span class="font-size-12 fnt-weight color4e5d6b-op">{{$newsLang->title}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>    

    <!-- CONTENT DETAIL NEW -->
    <div id="content-detailNews">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 col-sm-12 aligncenter content-detailnews">
                    <h3 class="font-size-24 color263038-1 fnt-weight">{{$newsLang->title}}</h3>
                    <div class="img-detailNews-thumbnail color263038 font-size-12">
                        <span class="post-name">{{date_format($news->created_at,"d/m/Y")}}</span>
                        <span style="font-style: italic;">by </span><span>&nbsp; {{$user->full_name}}</span>
                    </div>
                    <div class="img-detailNews-banner">
                        @if(isset($news) && $news->url)
                            <div class="div-wrapper-detailNew">
                                <div class="div-pad-detailNew"></div>
                                <div class="thumbImg-detailNews-banner div-inner-detailNew"
                                     style="background-image: url('{{Helper::getThemeNewsUpload($news->url).'?id='.Helper::versionImg()}}');background-position: center center; background-size: cover; background-repeat: no-repeat;">
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="bg-text-detailNews color333 font-size">
                        {!!$newsLang->content!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SUGGEST GUIDE -->
    <div id="suggest-guide">
        <div class="container">
            <div class="row content-best-guide">
                <div class="suggest-guide">
                    <h3 class="font-size-24 color263038-1 fnt-weight">Suggest guide</h3>

                    @if(isset($suggest) && count($suggest))
                    <div id="suggest-guide-slider" class="guide-slide">
                        <!-- slider new -->
                        @foreach($suggest as $su)
                            <?php
                                $us = $su->user()->select('full_name', 'id', 'avatar')->first();
                                $location = $su->locationTour()->first();
                                $locationmore = $su->locationTour()->get();
                            ?>
                            {{--<div class="text-center slide col-md-3" data-url="{{Helper::url('profile/'.$us->id)}}">--}}
                            <div class="text-center slide col-md-3" onClick="window.open('{{Helper::url('profile/'.$us->id)}}','_blank')"">
                                <div class="img-round">
                                    <img src="{{$us->avatar?Helper::getThemeAvatar($us->avatar):Helper::getThemeImg('user.png')}}" title="" alt="{{$us->full_name}}">
                                </div>

                                <div class="item-profile-guide">
                                    <div class="">
                                        <!-- name -->
                                        <h4 class="item-name-profile color0084fb font-size-14 fnt-weight">{{$us->full_name}}</h4>
                                        <!-- price -->
                                        <div class="price-profile-info font-size-24 fnt-weight color263038-1">
                                            <div>
                                                <i class="fa fa-usd" aria-hidden="true"></i>{{$su->hourly_rate}}/h
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rating-star">
                                        @for($i=1; $i<= $su->star; $i++)
                                            <i class="fa fa-star yellow" aria-hidden="true"></i>
                                        @endfor
                                        @for($i=4; $i >= $su->star; $i--)
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        @endfor
                                    </div>
                                </div>
                                <div class="dropdown no-hover">
                                    @if(count($location))
                                        <?php
                                            $city = $location->cities()->first();
                                            $country = $location->countries()->first();
                                            $j = 1;
                                        ?>

                                        <span class="font-size-12 fnt-weight color4e5d6b" style="width: 100%; display: inline-block; margin-bottom: 5px;">Guide locations:</span>
                                        <span class="item-adress-profile color4e5d6b font-size-12">{{$city->city_name}}
                                            - {{$country->country_name}}</span>
                                        <?php $j = 1;?>
                                        @foreach($locationmore as $lc)
                                            <?php $j++; ?>
                                        @endforeach
                                        @if($j>2)
                                            <div class="more-location">
                                                <span class="font-size-12 fnt-weight color4e5d6b hover-link">&nbsp(More)</span>
                                                <div class="dropdown-content dropdown-content-location">
                                                    <p class="font-size-12 color4e5d6b">
                                                    <?php $i = 1; ?>
                                                        @foreach($locationmore as $lc)
                                                        <span class="location-itemlist">
                                                            @if($i<2)
                                                                {{$lc->cities()->first()->city_name}} - {{$lc->countries()->first()->country_name}}
                                                            @else
                                                                , {{$lc->cities()->first()->city_name}} - {{$lc->countries()->first()->country_name}}
                                                            @endif
                                                            <?php  $i++;?>
                                                        </span>
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                                <!-- LANGUAGE -->
                                <div class="dropdown no-hover">
                                    <span class="font-size-12 fnt-weight color4e5d6b" style="width: 100%; display: inline-block; margin-bottom: 5px;">Language:</span>
                                    <?php 
                                        $knownlanguages=$su->known_languages()->get(); 
                                        $count=count($knownlanguages);
                                        $j=1; $k=1;
                                    ?>
                                    @foreach($knownlanguages as $language)
                                        @if($j<3)
                                            <span class="item-adress-profile color4e5d6b font-size-12">
                                        
                                            @if($k<2)                                                    
                                                {{$language->language()->first()->name}}
                                            @else
                                                , {{$language->language()->first()->name}}
                                            @endif
                                            </span>
                                        @endif
                                        <?php $j++; $k++; ?>
                                        
                                    @endforeach
                                    @if($j>3)
                                        <div class="more-language">
                                            <span class="font-size-12 fnt-weight color4e5d6b hover-link">&nbsp(More)</span>
                                            <div class="dropdown-content dropdown-content-language">
                                                <p class="font-size-12 color4e5d6b">
                                                    <?php $i = 1; ?>
                                                    @foreach($knownlanguages as $language)
                                                        <span class="location-itemlist">
                                                            @if($i<2)
                                                                {{$language->language()->first()->name}}
                                                            @else
                                                                , {{$language->language()->first()->name}}
                                                            @endif
                                                            <?php  $i++;?>
                                                        </span>
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div> 
                                    @endif
                                </div>
                                <div class="hover-y">
                                    @if(!$isGuide)
                                        {{--<a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="{{Helper::url('booking/'.$us->id)}}" target="_blank" title="">BOOK GUIDE</a>--}}
                                        <a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="javascript:void(0)" onClick="window.open('{{Helper::url('booking/'.$us->id)}}','_blank')" title="">BOOK GUIDE</a>
                                    @endif
                                    @if($isGuide)
                                        {{--<a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="{{Helper::url('profile/'.$us->id)}}" target="_blank" title="">BOOK GUIDE</a>--}}
                                        <a class="color4 fnt-weight bgBlue font-size-12 hover-link" href="javascript:void(0)" onClick="window.open('{{Helper::url('profile/'.$us->id)}}','_blank')" title="">BOOK GUIDE</a>
                                    @endif
                                </div>
                                
                            </div>
                        @endforeach
                    
                    </div>
                    @else 
                        <div class="col-md-12" style="text-align: center; margin-bottom: 55px;">
                            <p class="font-size">No suggest guide!</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- POPULAR NEWS -->
    <div id="content-popNews">
        <div class="container">
            <div class="row">
                <div class="col-md-12 content-popNews">
                    <h3 class="font-size-24 color263038-1 fnt-weight">POPULAR NEWS</h3>
                    @if(isset($populars) && count($populars))
                        @foreach($populars as $popular)
                            <?php
                            if (!isset($info_newsLang[$popular->id]))
                                $info_newsLang[$popular->id] = $popular->news_language()->where('language_code', '=', 'en')->first();
                                $cities = $popular->cities()->first();
                                $countries = $popular->countries()->first();
                            ?>
                            
                            <div class="col-md-4 col-sm-6 col-xs-6 bestNews-item">
                                <div class="lg-item-best-tour boxShadow">
                                    <div class="img-best-tour div-wrapper-bestTour">
                                        <div class="div-pad-bestTour"></div>
                                        <div class="bg-hover-img div-inner-bestTour" style="background-image: url({{Helper::getThemeNewsUpload($popular->url).'?id='.Helper::versionImg()}});">
                                            <a href="{{Helper::url('news/'.$popular->id)}}" class="view_detailNews font-size-14" title=""></a>
                                            <div class="content-info">
                                                <a href="{{Helper::url('news/'.$popular->id)}}" class="item-title-tour item-title-tour-2 color4 font-size fnt-weight" title="{{$info_newsLang[$popular->id]->title}}">{{$info_newsLang[$popular->id]->title}}</a>
                                                <p>
                                                    <i class="fa fa-map-marker font-size colore4ebf0" aria-hidden="true"></i>
                                                    <span class="font-size-12 colore4ebf0">{{$cities->city_name}} - {{$countries->country_name}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- code old -->
    @if(false)
    <div class="container">

        <div class="row bg-detail-News">
            <div class="col-md-12 content-detailNews">
                <div class="col-md-12 col-sm-12 contentPost--thumb">
                    <h3 class="font-size-24 color333 fnt-weight">{{$newsLang->title}}</h3>
                    <div class="img-detailNews-banner">
                        @if(isset($news) && $news->url)
                            <div class="div-wrapper-detailNew">
                                <div class="div-pad-detailNew"></div>
                                <div class="thumbImg-detailNews-banner div-inner-detailNew"
                                     style="background-image: url('{{Helper::getThemeNewsUpload($news->url).'?id='.Helper::versionImg()}}');background-position: center center; background-size: cover; background-repeat: no-repeat;">
                                </div>
                            </div>
                        @endif
                        {{--<div class="info-allNews">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <span class="font-size-14">{{$cities->city_name}} - {{$countries->country_name}}</span>
                        </div>--}}
                    </div>

                    <div class="img-detailNews-thumbnail">
                        <div class="col-md-12">
                            <div class="author-detailNews color666 font-size-12"><span>Post by </span><b class="font-size-12 text-ellipsis">&nbsp; {{$user->full_name}}</b>
                            </div>
                        </div>
                        @if(false)
                        <div class="col-md-8 col-sm-8 col-xs-8 list-left">
                            <div class="pad-left pad-right avatar_traveller avatar_traveller_News">
                                <img src="{{Helper::getThemeImg('user.png')}}">
                            </div>
                            <div class="font-size2 pad-nameNews">
                                <div class="name-history-booking color2">
                                    <span class="font-size text-ellipsis">{{$user->full_name}}</span>
                                </div>
                                
                                    <div class="rating-star">
                                        <i class="fa fa-star yellow" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                
                            </div>
                        </div>
                        @endif
                        @if(false)
                        <div class="col-md-4 col-sm-4 col-xs-4 detailNews-time-vehicles">
                            
                                <div class="item-allnews-left">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <span>3 Ngày 2 Đêm</span>
                                </div>
                            
                            <div class="item-allnews-right">
                                <img src="https://lifgoo.on/themes/default/assets/img/sailboat.png" alt="">
                                <img src="https://lifgoo.on/themes/default/assets/img/sneaker.png" alt="">
                                <img src="https://lifgoo.on/themes/default/assets/img/bicycle.png" alt="">
                            </div>
                        </div>
                        @endif
                        @if(false)
                            <div class="col-md-4 pad-right">
                                <div class="icon-social icon-social--hover">
                                    <a href="javascript:void (0)">
                                        <i class="fa fa-skype" aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void (0)">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void (0)">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void (0)">
                                        <i class="fa fa-pinterest" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="bg-text-detailNews color2 font-size-14">
                        {!!$newsLang->content!!}
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 sidebar--thumb">
                    <div class="recent-tour">
                        <h3 class="lineAfter-text font-size color333 fnt-weight">Recent News</h3>
                        <?php
                        $isSession = false;
                        $i = 0;
                        if (session('news') && count(session('news')) > 1) {
                            $recents = session('news');
                            $isSession = true;
                        }
                        ?>
                        @if($recents && count($recents))
                            @foreach($recents as $recent)
                                @if($recent->id != $news->id)
                                    <?php
                                        $post = \Modules\User\Entities\NewsEntity::where('id',$recent->id)->first();
                                        if($post!=NULL){
                                            if (!isset($info_newsLang[$recent->id]))
                                            $info_newsLang[$recent->id] = $recent->news_language()->where('language_code', '=', 'en')->first();
                                    ?>
                                    <div class="content-sidebar-detailNews titleGeneral-hover">
                                        <div class="img-sidebar-detailNews">
                                            <a href="{{Helper::url('news/'.$recent->id)}}" class="transition">
                                                <img src="{{$recent->url?Helper::getThemeNewsUpload($recent->url):Helper::getThemeImg('shutterstock_57318268.jpg')}}"
                                                     alt="" style="width: 100%">
                                            </a>
                                        </div>
                                        <div class="info-sidebar-detailNews">
                                            <h5>
                                                <a href="{{Helper::url('news/'.$recent->id)}}" title="{{$info_newsLang[$recent->id]->title}}" class="title-detailNews--thumb titleGeneral-item font-size-14">{{$info_newsLang[$recent->id]->title}}</a>
                                            </h5>
                                            <div class="author-">
                                                @if(false)
                                                    <span>by Hong</span>
                                                    <span>Apr 12, 2016 </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                @endif
                            @endforeach
                        @endif
                    </div>

                    <div class="popular-tour">
                        <h3 class="lineAfter-text font-size color333 fnt-weight">popular news</h3>
                        <div class="content-popular">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    @if(isset($populars) && count($populars))
                                        @foreach($populars as $popular)
                                            <?php
                                            if (!isset($info_newsLang[$popular->id]))
                                                $info_newsLang[$popular->id] = $popular->news_language()->where('language_code', '=', 'en')->first();
                                            $i++;
                                            ?>
                                            <div class="item titleGeneral-hover @if($i==1) active @endif">
                                                <div class="div-wrapper-slidePopular">
                                                    <div class="div-pad-slidePopular"></div>
                                                    <a href="{{Helper::url('news/'.$popular->id)}}" class="img-popularItem--thumb div-inner-slidePopular"><img src="{{Helper::getThemeNewsUpload($popular->url)}}" alt="1"></a>
                                                </div>
                                                <h5>
                                                    <a href="{{Helper::url('news/'.$popular->id)}}" title="{{$info_newsLang[$popular->id]->title}}" class="title-itemPopularNews titleGeneral-item font-size-14 color666 fnt-weight">{{$info_newsLang[$popular->id]->title}}</a>
                                                </h5>
                                            </div>
                                            
                                        @endforeach
                                    @endif
                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control btn-Control-popularSlide btn-leftControl" href="#myCarousel" data-slide="prev">
                                    <span class="icon-popularSlide icon-leftControl"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control btn-Control-popularSlide btn-rightControl" href="#myCarousel" data-slide="next">
                                    <span class="icon-popularSlide icon-rightControl"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="suggest-guide">
                        <h3 class="lineAfter-text font-size color333 fnt-weight">suggest guide</h3>
                        @if(isset($suggest) && count($suggest))
                        <div class="col-md-12 item-suggestGuide--thumb">
                            @foreach($suggest as $su)
                                <?php
                                $us = $su->user()->select('full_name', 'id', 'avatar')->first();
                                ?>
                                
                                <div class="item-suggest-guide">
                                    <div class="avatar_traveller">
                                        <a href="{{Helper::url('profile/'.$us->id)}}">
                                            <img src="{{$us->avatar?Helper::getThemeAvatar($us->avatar):Helper::getThemeImg('user.png')}}" title="" alt="{{$us->full_name}}">
                                        </a>
                                    </div>
                                    <div class="suggest-info">
                                        <div class="name-history-booking color2 font-size2 titleGeneral-hover">
                                            <a href="{{Helper::url('profile/'.$us->id)}}" class="name-suggestGuide titleGeneral-item font-size-14 color666 fnt-weight">{{$us->full_name}}</a>
                                        </div>
                                        <div class="rating-star">
                                            @for($i=1; $i<= $su->star; $i++)
                                                <i class="fa fa-star yellow" aria-hidden="true"></i>
                                            @endfor
                                            @for($i=4; $i >= $su->star; $i--)
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            @endfor
                                        </div>
                                    </div>  
                                </div>
                                
                            @endforeach
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    @endif

    @section('facebook')
    <meta property="og:title" content="{{$newsLang->title}}"/>
    @endsection

@else
<div id="no-detailnew" class="content--minheight">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p><h4>This new is invalid! Please try later!</h4></p>
            </div>
        </div>
    </div>
</div>
    
@endif
    

@stop
@section('js_wrapper')
    <script type="text/javascript">


    </script>
@stop