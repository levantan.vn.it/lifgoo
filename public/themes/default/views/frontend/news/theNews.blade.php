<?php
$newsLang = $news->news_language()->where('language_code', '=', 'en')->first();
$cities = $news->cities()->first();
$countries = $news->countries()->first();
$user = $news->users()->select('full_name', 'type')->first();
?>
<!-- code old -->
@if(false)
<div class="col-md-4 col-sm-6 col-xs-6 item-allNews--thumb">
    <div class="item-allNews item-detailNew">

        <div class="img-guide div-wrapper-allNews">
            <div class="div-pad-allNews"></div>
            <figure style="background-image: url('{{Helper::getThemeNewsUpload($news->url).'?id='.Helper::versionImg()}}')"
                    class="bg-property div-inner-allNews img--detailNew">
                <a href="{{Helper::url('news/'.$news->id)}}" class="view_detailNews font-size-14"></a>
            </figure>
            <div class="info-allNews">
                <i class="fa fa-map-marker font-size" aria-hidden="true"></i>
                <span class="font-size-12 color4">{{$cities->city_name}} - {{$countries->country_name}}</span>
            </div>
        </div>

        <div class="item-profile-news">
            @if($newsLang)
                <a href="{{Helper::url('news/'.$news->id)}}" title="{{$newsLang->title}}"
                   class="item-name-profile fnt-weight font-size color666">{{$newsLang->title}}</a>
            @endif
            <div class="post text-ellipsis">
                {{--<span class="post-by">Post by</span><span class="post-name color666">{{$user->full_name}}</span>--}}
                <span class="post-by font-size-12 color999">Post date</span><span
                        class="post-name color2 font-size-12">{{date_format($news->created_at,"Y/m/d")}}</span>
            </div>
            <div class="news-time-vehicles">
                <div class="item-allnews-left">
                    @if(false)
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span>3 Ngày 2 Đêm</span>
                    @endif
                </div>
                <div class="item-allnews-right">
                    <img src="{{Helper::getThemeImg('sailboat.png')}}" alt="" title="">
                    <img src="{{Helper::getThemeImg('sneaker.png')}}" alt="" title="">
                    <img src="{{Helper::getThemeImg('bicycle.png')}}" alt="" title="">
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<!-- code new -->
<div class="col-md-4 col-sm-6 col-xs-6 bestNews-item">
    <div class="lg-item-best-tour boxShadow">
        <div class="img-best-tour div-wrapper-bestTour">
            <div class="div-pad-bestTour"></div>
            <div class="bg-hover-img div-inner-bestTour"
                 style="background-image: url({{Helper::getThemeNewsUpload($news->url).'?id='.Helper::versionImg()}});">
                <a href="{{Helper::url('news/'.$news->id)}}" class="view_detailNews font-size-14" target="_blank"></a>
                <div class="content-info">
                    <a href="{{Helper::url('news/'.$news->id)}}" class="item-title-tour item-title-tour-2 color4 font-size fnt-weight" title="{{$newsLang->title}}">{{$newsLang->title}}</a>
                    <p>
                        <i class="fa fa-map-marker font-size colore4ebf0" aria-hidden="true"></i>
                        <span class="font-size-12 colore4ebf0">{{$cities->city_name}} - {{$countries->country_name}}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>