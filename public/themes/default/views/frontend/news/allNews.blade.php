@extends('default::frontend.layouts.master')
@section('content')
    <div class="content-site content-site-news">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">News</h3>
        </div>

        <div class="bg-allnews">
            <div class="container">
                <div class="content-item-allNews">
                    <div class="content-news row show-more-append">
                        @if(isset($news) && count($news))
                            @foreach($news as $news)
                                @include('default::frontend.news.theNews')
                            @endforeach
                        @endif

                    </div>
                    <div class="wraper-show-more">
                        @if($flag_show)
                            <a class="show-more best-guide bgBlue color4 fnt-weight font-size-14" href="javascript:void(0)" data-page="1" data-type="news">VIEW MORE</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js_wrapper')
    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('.tp-banner').show().revolution(
                {
                    dottedOverlay: "none",
                    delay: 16000,
                    startwidth: 1170,
                    startheight: 600,
                    touchenabled: "on",
                    onHoverStop: "off",
                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,
                    parallax: "mouse",
                    parallaxBgFreeze: "on",
                    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
                    keyboardNavigation: "off",
                    navigationHAlign: "center",
                    navigationVAlign: "bottom",
                    navigationHOffset: 0,
                    navigationVOffset: 20,
                    shadow: 0,
                    fullWidth: "off",
                    fullScreen: "off",
                    spinner: "spinner4",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "on",
                    forceFullWidth: "off",
                    hideThumbsOnMobile: "off",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "off",
                    hideArrowsOnMobile: "off",
                    hideThumbsUnderResolution: 0,
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    fullScreenOffsetContainer: ".header"
                });
        });

    </script>
@stop