@extends('default::frontend.layouts.master')
@section('css')
    <style>
        label.error {
            width: 100%;
        }
    </style>
@stop
@section('content')
    <div class="content-site">
        <div class="banner-findguide bg-property" style="background-image: url('{{Helper::getThemeImg('bg-Findguidec.jpg')}}')">
            <h3 class="color4 fnt-weight font-size-24">CONTACT</h3>
        </div>
        <div class="cnt-site-login">
            <div class="bg-form-contact content--minheight">
                <div class="container">
                    <div class="row">
                        {{--@if (count($errors) > 0)
                            <div class="text-error">
                                <ul class="errors">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif--}}
                        <div class="col-md-6 col-sm-8 col-xs-10 frm-contact aligncenter">
                            {!!  Form::open(['url'=>Helper::url('contact'),'method'=>'post','id'=>'contactform']) !!}
                            <div class="input-group">
                                <input type="text" class="form-control font-size-14 borderRadius color666" id="full_name" placeholder="Full name" name="full_name" maxlength="50">
                                <input type="text" class="form-control font-size-14 borderRadius color666" id="phone_number" placeholder="Phone number" name="phone_number" maxlength="15">
                                <input type="email" class="form-control font-size-14 borderRadius color666" id="email" placeholder="Email" name="email">
                                <textarea placeholder="Message" class="form-control font-size-14 borderRadius color666" name="message" id="message" rows="10" required></textarea>
                            </div>
                            <div class="btn-submit btn-submit-contact">
                                <button type="submit" class="btn btn-primary bgBlue font-size-14 color4 fnt-weight">Submit</button>
                            </div>
                            {!!Form::close() !!}
                        </div>
                        
                        {{--<div class="col-md-6 col-md-offset-1 col-md-pull-4 col-sm-6 col-sm-offset-0 col-sm-pull-6 col-xs-12 map-site col-fixed--height">
                            <div class="div-wrapper">
                                <div class="div-pad"></div>
                                <div class="div-inner">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.409436743302!2d107.58613631435443!3d16.454792988643728!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a1465d3d3b7d%3A0x5b9cda8123c1155f!2zMjMvMzMgTmd1eeG7hW4gVHLGsOG7nW5nIFThu5kgdOG7lSA0LCBQaMaw4bubYyBWxKluaCwgVHAuIEh14bq_LCBUaOG7q2EgVGhpw6puIEh14bq_LCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1490235212703" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>                                
                        </div>--}}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop