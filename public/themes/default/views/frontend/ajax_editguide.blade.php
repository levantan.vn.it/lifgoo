
<div id="frm-add">
    <div class="col-md-5 frm_edit">
        <input type="password" id="new_password" class="form-control" placeholder="New password">
    </div>

    <div class="col-md-5 frm_edit">
        <input type="password" id="cfr_password" class="form-control" placeholder="Confirm new password">
    </div>

    <div class="col-md-2 icon_add_remove">
        <i id="click-remove" class="fa fa-minus-circle fa-3x" aria-hidden="true"></i>
        <i id="click-add" class="fa fa-plus-circle fa-3x" aria-hidden="true"></i>
    </div>
</div>