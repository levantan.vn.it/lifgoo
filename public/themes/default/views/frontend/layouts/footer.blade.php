
<div id="footer" class="footer">
    <div class="back-to-top" id="back-to-top">
        <i class="fa fa-long-arrow-up" aria-hidden="true" title="BACK TO TOP"></i><span class=" color4e5d6b" title="BACK TO TOP">BACK TO TOP</span>
    </div>
    <div class="bg-footer">
        <div class="container container--thumb">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 copyright-text colorRgba font-size-12">

                Coppyright 2016 @ Lifgoo. All right reserved. <br class="br-first">Privacy Policy. <br class="br-last">Site Published by <a class="colorRgba" href="http://vietprojectgroup.com/" target="_blank">ViTPR</a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 social-thumb">
                <div class="icon-contact icon-social--hover">
                    <a href="skype:lmkhoi.it?chat" title="skype" target="_blank">
                        <i class="fa fa-skype" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.facebook.com/Lifgoo-1713603265625908/" title="facebook" target="_blank">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/LifgooProject" title="twitter" target="_blank">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=com.vitpr.android.lifgoo" title="Google Play" target="_blank">
                        <i class="fa fa-play" aria-hidden="true"></i>
                    </a>
                    {{--<a href="javascript:void (0)" title=""><i class="fa fa-skype" aria-hidden="true"></i></a>
                    <a href="javascript:void (0)"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="javascript:void (0)"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="javascript:void (0)"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    <a href="javascript:void (0)"><i class="fa fa-youtube-play social-last" aria-hidden="true"></i></a>--}}
                </div>
            </div>
        </div>
    </div>
</div>
{{--<div id="loading" style="display:none">
    <div class="cssload-dots">
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
        <div class="cssload-dot"></div>
    </div>

    <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
        <defs>
            <filter id="goo">
                <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12"></feGaussianBlur>
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0    0 1 0 0 0   0 0 1 0 0   0 0 0 18 -7"
                               result="goo"></feColorMatrix>
                <feBlend in2="goo" in="SourceGraphic" result="mix"></feBlend>
            </filter>
        </defs>
    </svg>
</div>--}}
<div id="loading" style="display:none">
    <div class="cssload-dots">
        <div id="circleG_1" class="circleG"></div>
        <div id="circleG_2" class="circleG"></div>
        <div id="circleG_3" class="circleG"></div>
    </div>
</div>

<div id="successModal" class="modal fade notify" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content panel-success" style="color: #3c763d;border-color: #b2dba1;">
            <div class="modal-body panel-heading text-center">
                <p></p>
            </div>
        </div>
    </div>
</div>
<div id="errorModal" class="modal fade notify" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content panel-danger" style="color: #a94442;border-color: #ebccd1;">
            <div class="modal-body panel-heading text-center">
                <p></p>
            </div>
        </div>
    </div>
</div>
