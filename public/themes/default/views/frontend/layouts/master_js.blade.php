<script>
    var trans = (function () {
        return {
            previous: '{{trans('admin.previous')}}',
            next: '{{trans('admin.next')}}',
            show: '{{trans('admin.show')}}',
            entries: '{{trans('admin.entries')}}',
            search: '{{trans('admin.search')}}',
            yes: '{{trans('site.yes')}}',
            no: '{{trans('site.no')}}',
            warning: '{{trans('site.warning')}}',
            error_occured: '{{trans('site.error_occured')}}',
            please_choose_image: '{{trans('site.please_choose_image')}}',
            no_image_change: '{{trans('site.please_choose_image')}}',
        }
    })();

    var config = (function (url) {
        return {
            width: 270,
            height: 270,
            langUrl: "{{Helper::url()}}",
            previous: "{{session('_previous')['url']}}",
            themeAvatar : "{{Helper::getThemeAvatar()}}",
            themeImg : "{{Helper::getThemeImg()}}"
        }
    })();

    function dayOfWeek() {
        return JSON.parse('<?php echo json_encode(Helper::daysOfWeek());?>');
    }
    var langUrl = "{{Helper::url()}}";
    @if(session()->has('users'))
        avatarGlobal = "{{session('users.user') ? session('users.user')->avatar : session('users.guide')->avatar}}";
    @endif
</script>