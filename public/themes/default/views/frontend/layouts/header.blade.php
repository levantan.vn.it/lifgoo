@if($isGuide && session()->has('missingInfor') && session('missingInfor.missing_profile') && session('missingInfor.missing_jobInfo') )
    <div class="update-profile color4e5d6b">
        You must update the full information to activate your account <br><a class="color4e5d6b" href="{{Helper::url('edit/edit-profile')}}" title="">update
            now</a>
    </div>
@endif
<?php
session('users') && session('users.guide') && $session_user = session('users.guide');
?>
<div id="header" class="header">
    <!-- Start Atribute Navigation -->
    <div class="attr-nav attr-nav-responsive">
        <div class="container">       
    
            <!-- languge -->
            <div class="header-language">
                <div class="form-group">
                    <select class="selectpicker" id="exampleSelect1">
                        <option>English</option>
                        <option>Viet Nam</option>
                        <option>French</option>
                        <option>Japanese</option>
                    </select>
                </div>
            </div>

            <!-- sign in - sign up -->
            @if(!session()->has('users'))
                <ul class="sign-responsive">
                    <li>
                        <a href="{{Helper::url('signin')}}"><span class="color4 font-size-12 fnt-weight">Login</span></a>
                    </li>
                    <li>
                        <a href="{{Helper::url('signup')}}"><span class="color4 font-size-12 fnt-weight">Register</span></a>
                    </li>
                </ul>
            @else
                <?php
                $userGlobal = session('users.guide') ? session('users.guide') : session('users.user');
                $box_notifis = $userGlobal->notifications();
                $count = $userGlobal->notifications()->where('is_click', '=', 0)->count();
                $box_notifis = $box_notifis->limit(50)->orderBy('created_at', 'desc')->get();
                $userGlobal_notify = [];

                if ($userGlobal->hasRole('guide')) {
                    $countBooking = $userGlobal->bookings()->where('is_read', '=', 0)->count();
                }
                $countMessage = $userGlobal->countMessage($userGlobal->id);
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="navnotify-right" role="presentation">
                        <a href="{{Helper::url('edit/booking')}}" class="info-number message-booking-notify"
                           aria-expanded="false" data-type="message">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <span class="badge bg-green">@if(count($countMessage)){{count($countMessage)}}@endif</span>
                        </a>
                    </li>
                    <li class="navnotify-right" role="presentation">
                        <a href="{{Helper::url('edit/booking')}}" class="info-number cart-booking"
                           aria-expanded="false" data-type="cart">
                            <i class="fa fa-cart-plus" aria-hidden="true"></i>
                            <span class="badge bg-green">@if(isset($countBooking) && $countBooking){{$countBooking}}@endif</span>
                        </a>
                    </li>
                    <li role="presentation" class="navnotify-right notification">
                        <a href="javascript:void(0);"
                           class="info-number info-notify dropdown-toggle @if($count) bell-notify @endif"
                           aria-expanded="false"
                           data-toggle="dropdown" data-url="{{Helper::url('updateClick')}}" data-type="bell">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                            <span class="badge bg-green">@if($count){{$count}}@endif</span>
                        </a>
                        <ul class="dropdown-menu input-dropdown-menu dropdown-notification"
                            role="menu">
                            <li class="header-notify font-size-12 colorRgba fnt-weight">NOTIFICATIONS</li>
                            <li class="body-notify">
                                <div class="slimScrollDiv">
                                    <ul class="menu content-notify notify-scroll">
                                        @if(count($box_notifis))
                                            @foreach($box_notifis as $no)
                                                @include('default::frontend.notifications.box_notifcation')
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </li>
                            <li class="footer-notify">
                                <a class="viewAll-notify color999 font-size-12" href="{{Helper::url('edit/notification')}}">View All
                                    Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <li class="username-right">
                        <a href="javascript:void (0);" class="user-profile dropdown-toggle" data-toggle="dropdown"
                           id="drop_profile"
                           aria-expanded="false"><span
                                    class="text-ellipsis username-header font-size-12 fnt-weight" title='@if(session('users')){{session('users.user')?session('users.user')->full_name:session('users.guide')->full_name}}@endif'>@if(session('users')){{session('users.user')?session('users.user')->full_name:session('users.guide')->full_name}}@endif</span><span
                                    class="user-dropdown fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu drop-profile" aria-labelledby="drop_profile">
                            @if(isset($session_user))
                                <li class="myProfile"><a href="{{Helper::url('profile/'.$session_user->id)}}"
                                       class="font-size-14 color999">{{trans('site.my_profile')}}</a></li>@endif
                            <li class="myAccount"><a href="{{Helper::url('edit/edit-profile')}}"
                                   class="font-size-14 color999">{{trans('site.my_account')}}</a></li>
                            <li><a href="{{Helper::url('logout')}}"
                                   class="font-size-14 color999">{{trans('site.logout')}}</a></li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
    </div>
    <!-- End Atribute Navigation -->

    <!-- Start Navigation -->
    <nav class="navbar navbar-default bootsnav" style="position: inherit;">

        <div class="container">

            <!-- Start Header Navigation -->
            <div class="navbar-header col-md-3">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{Helper::url('')}}"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="100" height="40" viewBox="0 0 4990 2047.06"><defs><style>.cls-1{fill:#1fd8f8;}.cls-2{fill:#ff6800;}</style></defs><title>Lifgoo-SVG</title><path class="cls-1" d="M0,116.45q0-17,14.31-32T45,69.5H210.51q16.33,0,30.66,14.94t14.31,32V1516.26q0,19.2-14.31,33.08t-30.66,13.87H45q-16.38,0-30.66-13.87T0,1516.26Z"/><path class="cls-1" d="M603.78,408.78q-70.32,0-116.83-50.15T440.42,238.08q0-70.42,46.53-119.49T603.78,69.5q70.27,0,117.86,49.08T769.2,238.08q0,70.42-47.56,120.56T603.78,408.78ZM477.64,624.31q0-21.31,14.47-34.14t31-12.81H690.62q16.53,0,31,12.81t14.47,34.14v892q0,19.2-14.47,33.08t-31,13.87H523.13q-16.57,0-31-13.87t-14.47-33.08Z"/><path class="cls-1" d="M1050.71,807.6H952q-20.58,0-32.91-15.11T906.72,760.1V613.3q0-47.46,45.25-47.5h98.74V447.06q0-200.77,97.71-308.72t287-107.95q94.59,0,170.73,33.46t109,63.69q12.34,8.65,15.42,25.91t-5.14,30.22l-72,131.69a38.05,38.05,0,0,1-24.68,19.43q-16.49,4.35-32.91-6.48-22.66-17.26-65.82-34.54t-82.28-17.27q-45.27,0-72,11.87t-42.17,34.54q-15.43,22.66-20.57,54t-5.14,70.16V565.8H1567q45.23,0,45.26,47.5V760.1q0,17.3-12.35,32.38T1567,807.6H1307.83v708.11a46.87,46.87,0,0,1-13.37,33.46q-13.4,14.07-31.88,14H1096q-18.51,0-31.88-14a46.73,46.73,0,0,1-13.37-33.46Z"/><path class="cls-1" d="M2688,1549.38q0,118.32-37,207.1t-101.83,147.93q-64.8,59.14-151.21,88.76t-183.09,29.59q-59.69,0-113.15-11.62a632.87,632.87,0,0,1-98.75-29.59q-45.28-18-79.21-37t-50.4-31.7a57.7,57.7,0,0,1-14.4-26.42q-4.15-15.85,4.11-28.53l61.72-137.36q6.17-14.81,22.63-19t32.92,6.34q12.35,8.42,36,21.13a489.43,489.43,0,0,0,53.49,24.31,509.38,509.38,0,0,0,64.8,20.07,306.3,306.3,0,0,0,72,8.46q55.55,0,96.69-10.57t69.94-37q28.79-26.45,43.2-71.85t14.4-113.06V1521.9q-32.93,12.68-94.63,26.42t-133.72,13.73q-107,0-193.38-39.1T1861,1415.18q-61.71-68.65-95.66-161.67t-33.94-200.76q0-109.86,35-203.93T1862,686.09a426.57,426.57,0,0,1,144-106.72q83.32-38,180-38,76.09,0,132.69,23.25t97.72,50.72a444.41,444.41,0,0,1,80.23,71.85L2521.36,609q14.36-46.46,45.26-46.5h76.12q18.51,0,31.88,13.74A45.41,45.41,0,0,1,2688,609ZM2430.84,917.5q-8.24-21.1-24.69-44.38T2364,829.8q-25.75-20.06-61.72-33.81T2221,782.25q-55.55,0-97.72,23.25a212.45,212.45,0,0,0-69.94,61.29q-27.78,38-42.17,86.64a347.46,347.46,0,0,0-14.4,99.33,326.29,326.29,0,0,0,15.43,99.32q15.43,48.64,44.23,86.64t73,60.23q44.21,22.19,103.89,22.19,45.22,0,81.26-8.46t62.75-19q30.85-10.55,53.49-27.47Z"/><path class="cls-1" d="M3968.77,1049.09q0-103.64,39.94-195.7t109-161.85q69.07-69.82,161.93-110t198.63-40.2q105.76,0,198.63,40.2t163,110a516.6,516.6,0,0,1,110.12,161.85q39.92,92,39.94,195.7,0,105.82-39.94,198.87t-110.12,162.9q-70.18,69.82-163,111.07t-198.63,41.25q-105.81,0-198.63-41.25t-161.93-111.07q-69.13-69.82-109-162.9T3968.77,1049.09Zm250.45,0q0,57.13,20.51,106.84t55.05,86.74a249.31,249.31,0,0,0,82,58.18q47.47,21.17,101.48,21.16t101.47-21.16a262.07,262.07,0,0,0,83.12-58.18q35.62-37,56.14-86.74t20.51-106.84q0-55-20.51-103.67T4662.9,860.8a271.68,271.68,0,0,0-83.12-57.12q-47.52-21.12-101.47-21.16t-101.48,21.16q-47.51,21.17-82,57.12t-55.05,84.63Q4219.18,994.12,4219.22,1049.09Z"/><path class="cls-2" d="M3018.43,1463.27l132.07-228.76a254.65,254.65,0,0,1-76.5-182.24c0-141.09,114.38-255.47,255.47-255.47s255.47,114.38,255.47,255.47a254.65,254.65,0,0,1-76.5,182.24l132.07,228.76c106.74-107.1,199.89-247.39,199.89-411,0-282.18-228.75-510.93-510.93-510.93s-510.93,228.75-510.93,510.93C2818.53,1215.88,2911.68,1356.17,3018.43,1463.27Z"/><path class="cls-2" d="M3397.83,1298.41a255.25,255.25,0,0,1-136.73,0L3115.73,1550.2c112.4,90,213.73,140.74,213.73,140.74s101.33-50.7,213.73-140.74Z"/></svg></a>
                {{--<a class="navbar-brand" href="{{Helper::url('')}}"><img src="{{Helper::getThemeImg('Lifgoo-SVG-01.svg')}}"
                                                                        alt=""></a>--}}
                <!-- FIND GUIDE RESPONSIVE -->
                @if(!$isGuide)
                <div class="findguide--thumb">
                    <a href="{{Helper::url('findguide')}}" class="font-size-14 color4 fnt-weight" title="">Find Guide</a>
                </div>
                @endif
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="home"><a href="{{Helper::url('')}}" class="font-size-14" title="">Home</a></li>
                    <li class="about"><a href="{{Helper::url('about')}}" class="font-size-14" title="">About</a></li>
                    {{--<li class="guide"><a href="{{Helper::url('guides')}}" class="font-size-14" title="">Guides</a></li>--}}
                    <li class="news"><a href="{{Helper::url('news')}}" class="font-size-14" title="">News</a></li>
                    <li class="contact"><a href="{{Helper::url('contact')}}" class="font-size-14" title="">Contact</a></li>
                    @if(!$isGuide)
                        <li class="find-guide"><a href="{{Helper::url('findguide')}}" class="font-size-14 color4" title="">Find
                                Guide</a></li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <!-- End Navigation -->
</div>
