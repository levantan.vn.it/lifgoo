
<div id="loading" style="display:none">
    <div class="cssload-dots">
        <div id="circleG_1" class="circleG"></div>
        <div id="circleG_2" class="circleG"></div>
        <div id="circleG_3" class="circleG"></div>
    </div>
</div>

<div id="successModal" class="modal fade notify" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content panel-success" style="color: #3c763d;border-color: #b2dba1;">
            <div class="modal-body panel-heading text-center">
                <p></p>
            </div>
        </div>
    </div>
</div>
<div id="errorModal" class="modal fade notify" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content panel-danger" style="color: #a94442;border-color: #ebccd1;">
            <div class="modal-body panel-heading text-center">
                <p></p>
            </div>
        </div>
    </div>
</div>
