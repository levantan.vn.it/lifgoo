<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lifgoo</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include(Theme::getActive().'::frontend.meta')
    <link rel="shortcut icon" href="{{Helper::getThemeImg('favicon.ico')}}" type="image/x-icon"/>
    {!! Assets::css() !!}
    @yield('css')

    <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-32694432-8', 'auto');ga('send', 'pageview');</script>
    <!-- <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-32694432-8', 'auto');
      ga('send', 'pageview');

    </script> -->
    @yield('facebook')
    
</head>
<body>
<?php
$isGuide = session('users') && session('users.guide') && session('users.guide')->hasRole('guide');
$isLogin = session()->has('users');
?>
@if(!isset($islogin) || $islogin!=1)
    @include(Theme::getActive().'::frontend.layouts.header')
    @yield('content')
    @include(Theme::getActive().'::frontend.layouts.footer')
@else
    @yield('content')
    @include(Theme::getActive().'::frontend.layouts.footer-login')
@endif
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>--}}
@include('default::frontend.layouts.master_js')
{!! Assets::js() !!}
<script type="text/javascript">$.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});</script>
<!--Link Select2-->
@yield('js_wrapper')
@yield('js')
<script type="text/javascript">@if(session('flash_success'))modalCallback('#successModal','{{session('flash_success')}}','Success');@elseif(session('flash_error'))modalCallback('#errorModal','{{session('flash_error')}}','Error');@endif @if($isLogin)var intervalNotify = setInterval(function(){intervalCheckNotify();},300000);@endif</script>
<!-- <script type="text/javascript">
    @if(session('flash_success'))
        modalCallback('#successModal', '{{session('flash_success')}}', 'Success');
    @elseif(session('flash_error'))
        modalCallback('#errorModal', '{{session('flash_error')}}', 'Error');
    @endif
    @if($isLogin)
        var intervalNotify = setInterval(function(){
            intervalCheckNotify();
        }, 300000);
    @endif
</script> -->
</body>

</html>