$(document).ready(function () {

    // enable fileuploader plugin
    $('input[name="files"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
        changeInput: ' ',
        theme: 'thumbnails',
        enableApi: true,
        addMore: true,
        maxSize:100,
        thumbnails: {
            box: '<div class="fileuploader-items">' +
            '<ul class="fileuploader-items-list">' +
            '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><div class="icon-upload"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i><span>Upload Images</span></div></div></li>' +
            '</ul>' +
            '</div>',
            item: '<li class="fileuploader-item">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '<div class="progress-holder">${progressBar}</div>' +
            '</div>' +
            '</li>',
            item2: '<li class="fileuploader-item">' +
            '<div class="fileuploader-item-inner">' +
            '<div class="thumbnail-holder">${image}</div>' +
            '<div class="actions-holder">' +
            '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
            '</div>' +
            '</div>' +
            '</li>',
            startImageRenderer: true,
            canvasImage: false,
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                remove: '.fileuploader-action-remove'
            },
            onItemShow: function (item, listEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input');

                plusInput.insertAfter(item.html);

                if (item.format == 'image') {
                    item.html.find('.fileuploader-item-icon').hide();
                }
            }
        },
        afterRender: function (listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));

            plusInput.on('click', function () {
                api.open();
            });
        },
        onItemRemove: function(itemEl, listEl, parentEl, newInputEl, inputEl) {
            itemEl.children().animate({'opacity': 0}, 200, function() {
                setTimeout(function() {
                    itemEl.slideUp(200, function() {
                        itemEl.remove();
                    });
                }, 100);
            });
        }
        // while using upload option, please set
        // startImageRenderer: false
        // for a better effect
        /*upload: {
         url: '/gallery',
         data: null,
         type: 'POST',
         enctype: 'multipart/form-data',
         start: true,
         synchron: true,
         beforeSend: null,
         onSuccess: function(data, item) {
         setTimeout(function() {
         item.html.find('.progress-holder').hide();
         item.renderImage();
         }, 400);
         },
         onError: function(item) {
         /!*item.html.find('.progress-holder').hide();
         item.html.find('.fileuploader-item-icon i').text('Failed!');

         setTimeout(function() {
         item.remove();
         }, 1500);*!/
         },
         onProgress: function(data, item) {
         var progressBar = item.html.find('.progress-holder');

         if(progressBar.length > 0) {
         progressBar.show();
         progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
         }
         }
         },*/
        /*dragDrop: {
         container: '.fileuploader-thumbnails-input'
         }*/
    });
    $(".upload-gallery").submit(function (e) {
        var button = $(this).find('.btn-upload-gallery');
        var url = $(this).attr('action');
        e.preventDefault();
        var data = new FormData(this);
        // if(($('.item-allNews').length + $('.fileuploader-item').length)>50)
        //     return modalDisplay(trans.warning, 'warning', 'Your library is not only a maximum of 50 images. Please remove them before upload more');
        // if($('.fileuploader-item').length>10){
        //     return modalDisplay(trans.warning, 'warning', 'Only 10 files are allowed to be uploaded.');
        //     return false;
        // }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            contentType: false,
            processData: false,
            beforeSend: showLoader.bind(button)
        }).done(function (str) {
            if (str.success) {
                //modalCallback('#successModal',str.message,'success');
                location.reload();
            } else {
                //$('.form-error-message').html(str.message).show();
                return modalDisplay(trans.warning, 'warning', str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            $('.form-error-message').hide();
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
            hideLoader.call(button);
        });
    });
});