$(document).ready(function () {
    var button = $('.btn-submit');
    $("#formsignup").validate({
        onkeyup: false,
        ignore: [],
        rules: {
            full_name: {
                required: {
                    depends: function () {
                        this.value = this.value.trim();
                        return true;
                    }
                },
                maxlength: 100
            },
            username: {
                required: {
                    depends: function () {
                        this.value = this.value.trim();
                        return true;
                    }
                },
                maxlength: 100
            },
            email: {
                required: {
                    depends: function () {
                        this.value = this.value.trim();
                        return true;
                    }
                },
                maxlength: 100,
                email: true
            },
            password: {
                required: true,
                maxlength: 100,
                minlength: 6
            },
            confirmpassword: {required: true, equalTo: "#password", minlength: 6}
        },
        messages: {
            regex: 'Please enter a valid this field.'
        }
        , submitHandler: function (form, event) {
            var url = $(form).attr('action');
            event.preventDefault();
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: $(form).serialize(),
                beforeSend: showLoader.bind(button)
            }).done(function (str) {
                $('label.error').remove();
                if (str.success) {
                    /*location.reload();*/
                    window.location.href = '/signin';
                } else {
                    $('.form-error-message').html(str.message).show();
                }
            }).fail(function (e) {
                $('label.error').remove();
                var response = e.responseJSON;
                $('.form-error-message').hide();
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                Object.keys(response).forEach(function (i) {
                    var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                    $('#' + i).after(label);
                });
            }).always(function () {
                hideLoader.call(button);
            })
        }
    });

    $(".form-check-input").change(function () {
        if (this.checked) {
            button.prop('disabled', false);
        }
        else
            button.prop('disabled', true);
    });

    $("#formlogin").validate({
        rules: {
            email: {required: true, maxlength: 100},
            password: {required: true, minlength: 6, maxlength: 30}
        },
        messages: {
            title: "Please enter group title"
        },
        tooltip_options: {
            email: {placement: 'top', html: true},
            password: {placement: 'top', html: true}
        },
        submitHandler: function (form) {
            var url = $(form).attr('action');
            var button_login = $('button.btn-submit');
            $.ajax({
                type: "POST",
                url: url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend: showLoader.bind(button_login)
            }).done(function (str) {
                if (str.success) {
                    if (str.data.type == 'user')
                        window.location.href = config.previous
                    else
                        window.location.href = 'edit/edit-profile';
                }
                else {
                    //$('.form-error-message').html(str.message).show();
                    location.reload();
                }
            }).fail(function (e) {
                $('label.error').remove();
                var response = e.responseJSON;
                $('.form-error-message').hide();
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                Object.keys(response).forEach(function (i) {
                    var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                    $('#' + i).after(label);
                });
            }).always(function () {
                hideLoader.call(button_login);
            });
        }
    });

    $("#formforgot").validate({
        rules: {
            email: {
                required: {
                    depends: function () {
                        this.value = this.value.trim();
                        return true;
                    }
                },
                maxlength: 100,
            },
        },
        submitHandler: function (form,event) {
            event.preventDefault();
            var url = $(form).attr('action');
            var button = $('.btn-success');
            $.ajax({
                type: "POST",
                url: url,
                data: $(form).serialize(),
                dataType: "JSON",
                beforeSend: showLoader.bind(button)
            }).done(function (str) {
                if (str.success) {
                    $('.form-error-message').hide();
                    window.location.href = '/signin';
                } else {
                    $('.form-error-message').html(str.message).show();
                }
            }).fail(function (e) {
                var response = e.responseJSON;
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
            }).always(function () {
                hideLoader.call(button);
            })
        }
    });
    /*$(document).ready(function () {
        if (window.location.href.indexOf('#_=_') > 0) {
            window.location = window.location.href.replace(/#.*!/, '');
        }});*/
});
