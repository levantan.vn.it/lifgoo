$(document).ready(function () {

    check_type_cer();
    $("select#type").on("changed.bs.select",function(){
        if(this.value=="location"){
            $(".location_allowed_container").show();
        }else{
            $(".location_allowed_container").hide();
        }
    });
    function check_type_cer(){
        if($("select#type").val()=="location"){
            $(".location_allowed_container").show();
        }else{
            $(".location_allowed_container").hide();
        }
    }

    $('.convert-time-js').each(function(){
        var srcDate=$(this).text()+' UTC';
        srcDate = srcDate.replace(/-/g, ' ');
        var convertDate=new Date(srcDate);
        $(this).text(convertDate.toLocaleDateString()+" "+convertDate.toLocaleTimeString());
    });

    // trim space before form submit
    var button = $('.btn_update');
    button.click(function () {
        $('#passport_id_number').val($.trim($('#passport_id_number').val()));
        $('#id-card').val($.trim($('#id-card').val()));
        $('#location-card').val($.trim($('#location-card').val()));
        $('#experience').val($.trim($('#experience').val()));
        $('#diploma_info').val($.trim($('#diploma_info').val()));
        
    });
    $('#phone_number').val($.trim($('#phone_number').val()));

    $('.next-second-step').click(function(){
        $('#message').val($.trim($('#message').val()));
    });

    var btnlogin = $('.btn-success');
    btnlogin.click(function(){
        $('.input-emailllogin').val($.trim($('.input-emailllogin').val()));
    });
    $(".edit-profile").validate({
        onkeyup: false,
        ignore: [],
        rules: {
            password: {
                maxlength: 100,
                minlength: 6
            },
            confirmpassword: {equalTo: "#password", minlength: 6},
            phone_number: {
                digits: true
            },
            hobby: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return false;
                    }
                }
            }
        },
        messages: {}
        , submitHandler: function (form, event) {

            event.preventDefault();
            var url = $(form).attr('action');
            //var data = new FormData(form);
            var fd = new FormData(form);
            var check = checkEmptyImage();
            if (!avatarGlobal && check)
                return modalDisplay("warning", 'warning', 'You have to choose avatar to continue update profile.');
            var res = url.length ? url.split("/") : [];
            if (res && res[res.length - 1] == 'editProfile' && !check) {
                var imageData = $('.cropme').find('img').attr('src');
                var file = dataURItoBlob(imageData);
                fd.append('img', file);
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: fd,
                processData: false,
                contentType: false,
                beforeSend: showLoader.bind(button)
            }).done(function (str) {
                $('label.error').remove();
                if (str.success) {
                    $('.form-error-message').hide();
                    /*window.location.href = 'list';*/
                    location.reload();
                } else {
                    $('.form-error-message').html(str.message).show();
                }
            }).fail(function (e) {
                $('label.error').remove();
                var response = e.responseJSON;
                $('.form-error-message').hide();
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                Object.keys(response).forEach(function (i) {
                    var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                    $('#' + i).after(label);
                });
            }).always(function () {
                hideLoader.call(button);
            });
        }
        // ,
        // invalidHandler: function(form, validator) {
        //     $('.edit-profile').addClass('form-err');
        // }
    });

    $("#msform").validate({
        onkeyup: false,
        rules: {
            phone_number: {
                digits: true
            },
            capacity: {digits: true}
        },
        submitHandler: function (form, event) {
            event.preventDefault();
            var $form = $(form);
            var url = $form.attr('action');
            var button = $form.find('.action-button');
            var data = {
                email: $form.find('input[name="email"]').val(),
                phone_number: $form.find('input[name="phone_number"]').val(),
                message: $form.find('textarea[name="message"]').val(),
                hours_booking: booking.getDatesHoursBooking(),
                capacity: $form.find('input[name="capacity"]').val(),
                price: booking.getPrie(),
                guide_id: $form.find('input[type="submit"]').data('guideid'),
                pickup_place: $form.find('input[name="pickup_place"]').val(),
                destination: $form.find('input[name="destination"]').val(),
                city_id:$form.find('select[name="city"]').val(),
            };
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data,
                beforeSend: showLoader.bind(button)
            }).done(function (str) {
                $('label.error').remove();
                if (str.success) {
                    $('.form-error-message').hide();
                    window.location.href = '/detail/'+str.data;
                } else {
                    $('.form-error-message').html(str.message).show();
                }
            }).fail(function (e) {
                $('label.error').remove();
                var response = e.responseJSON;
                $('.form-error-message').hide();
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                Object.keys(response).forEach(function (i) {
                    var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                    $('#' + i).after(label);
                });
            }).always(function () {
                hideLoader.call(button);
            });
        }
        // ,
        // invalidHandler: function(form, validator) {
        //     $('#msform').addClass('form-err');
        // }
    });

    $("#contactform").validate({
        onkeyup: false,
        rules: {
            full_name: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                maxlength: 100,
            },
            email: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                maxlength: 100,
                email:true
            },
            phone_number: {
                digits: true,
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            message: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                maxlength: 10000,
            }
        },
        submitHandler: function (form, event) {
            event.preventDefault();
            var url = $(form).attr('action');
            var field_email = $(form).find('[name="email"]');
            if (!checkEmail(field_email.val()))
                return modalDisplay(trans.warning, 'warning', 'Please enter a valid email address');
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: $(form).serialize(),
                beforeSend: showLoader.bind(button)
            }).done(function (str) {
                $('label.error').remove();
                if (str.success) {
                    $('.form-error-message').hide();
                    /*window.location.href = 'list';*/
                    location.reload();
                } else {
                    /*$('.form-error-message').html(str.message).show();*/
                    notify.alertNotifyError(str.message);
                }
            }).fail(function (e) {
                $('label.error').remove();
                var response = e.responseJSON;
                $('.form-error-message').hide();
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                Object.keys(response).forEach(function (i) {
                    var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                    $('#' + i).after(label);
                });
            }).always(function () {
                hideLoader.call(button);
            })
        }
        // ,
        // invalidHandler: function(form, validator) {
        //     $('#contactform').addClass('form-err');
        // }
    });

    $(".traveler-review").validate({
        rules: {
            review: "required"
        },
        messages: {
            review: "This field is required."
        }
    });

    $("#findGuide").submit(function (event) {
        event.preventDefault();
        var url = $(form).attr('action');
        var form = $(this);
        form.valid();
        var button = form.find('.btn_update');

        htmlCity.ajaxFindGuide({form: form, url: url, button: button});

        var length = $('.listFind--thumb.resultGuide-hide').length;
        if (length > 0) {
            $('.listFind--thumb').removeClass('resultGuide-hide');
        }

    });

    $('input.datefilter').daterangepicker({
        singleDatePicker: true,
        showDropdowns: false,
        locale: {
            format: 'YYYY/MM/DD'
        },
        minDate: new Date(),
    });

    $('input.datefilter-maxdate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: false,
        locale: {
            format: 'YYYY/MM/DD'
        },
        maxDate: new Date(),
    });

    $('input.datefilter-mindate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: false,
        locale: {
            format: 'YYYY/MM/DD'
        }
    });

    $('.edit-user input, .bg-booking-guide input').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('.btn-edit-submit').click(function () {
        var $form = $(this).closest('.edit-user');
        var $rules = {};
        if ($(this).data('type') == 'update_password')
            $rules = {
                new_password: {
                    maxlength: 50,
                    minlength: 6,
                    required: {
                        depends: function () {
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                },
                cfr_password: {
                    equalTo: "#new_password",
                    minlength: 6,
                    required: {
                        depends: function () {
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    }
                }
            };
        $form.validate({
            onkeyup: false,
            ignore: [],
            rules: $rules,
            messages: {}
            , submitHandler: function (form, event) {
                event.preventDefault();
                var url = $(form).attr('action');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(form).serialize(),
                    beforeSend: showLoader.bind(button)
                }).done(function (str) {
                    $('label.error').remove();
                    if (str.success) {
                        $('.form-error-message').hide();
                        /*window.location.href = 'list';*/
                        location.reload();
                    } else {
                        /*$('.form-error-message').html(str.message).show();*/
                        notify.alertNotifyError(str.message);
                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                })
            }
            // ,
            // invalidHandler: function(form, validator) {
            //     $('.edit-user').addClass('form-err');
            // }
        });
    });
    // $('select.select-country').trigger("change");

    $(document).on('change', 'select.select-country', function () {
        var country_id = $(this).val();
        var url = $(this).attr('data-url');
        returnCity(country_id, url, $(this));
    });

    $(document).on('click', '.click-add', function () {
        var local = $('select.select-country').first();
        var localLast = $('.div-add-price').last();
        var htmlLocal = htmlCity.htmlLocal({
            option: local.html(),
            url: local.data('url'),
            option_city: $('select.select-city').first().html()
        });
        $(htmlLocal).insertAfter(localLast);
        $('.selectpicker').selectpicker('refresh');
    });

    $(document).on('click', '.show-more.find-guide', function (e) {
        e.preventDefault();
        var form = $("#findGuide");
        var url = $(form).attr('action');
        form.valid();
        var button = form.find('.btn_update');
        htmlCity.ajaxFindGuide({form: form, url: url, button: button, type: 'show_more'});
    });

    $(".traveler-review").submit(function (e) {
        var form = $(this);
        form.valid();
        e.preventDefault();
        var rating = $(".starrr").find('.fa-star').length;
        var review = $('#traveler-review').val();
        $('#traveler-review').val('');
        var button = $('.btn-review');
        var url = form.attr('action');
        var bkid = button.data('booking_id');
        if (review)
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {booking_id: bkid, review: review, ratings: rating},
                beforeSend: showLoader.bind(button)
            }).done(function (str) {
                if (str.success) {
                    location.reload();
                } else {
                    return modalDisplay(trans.warning, 'warning', str.message);
                }
            }).fail(function (e) {
                var response = e.responseJSON;
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
            }).always(function () {
                hideLoader.call(button);
            });
    });

    
    $(document).on('click', '.click-remove', function () {
        $(this).closest('.div-add-price').remove();
    });

    $(document).on('click', '.btn-cancel-booking', function () {
        /*$('#cancel-booking').modal('show');*/
        var button = $(this);
        var bkid = button.data('bkid');
        var url = button.data('url');
        var status = button.data('value');
        var type = button.data('type');
        //console.log(status);
        if (type == 'refresh_page') {
            $('#cancel-booking').modal({
                backdrop: 'static',
                keyboard: false
            }).one('click', '.btn-request-yes', function (e) {
                location.reload();
            });
            return false;
        }
        var $cancel_booking = $('#cancel-booking');
        var $title_booking = $cancel_booking.find('.cancel-request');
        $cancel_booking.find('textarea').remove();
        $cancel_booking.find('.error').remove();
        var button_yes = $cancel_booking.find('.btn-request-yes');
        button_yes.data('bkid', bkid);
        button_yes.data('url', url);
        button_yes.data('status', status);
        button_yes.data('type', type);
        if (status == 'accepted')
            $title_booking.text('Are you sure you want to accept this booking request?');
        if (status == 'declined') {
            var st = '<div class="form-group"><textarea class="font-size-14 form-control " name="text" placeholder="Message" maxlength="10000" rows="5" style="margin-bottom: 10px;"></textarea></div>';
            $title_booking.text('Are you sure you want to decline this booking request? Please enter the reason you do not accepted.');
            $title_booking.after(st);
        }
        $('#cancel-booking').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $(document).on('click', '.btn-request-yes', function (e) {
        var button = $(this);
        var bkid = button.data('bkid');
        var url = button.data('url');
        var status = button.data('status');
        var type = button.data('type');
        if  (typeof status == 'undefined')
            return false;
        var $cancel_booking = $('#cancel-booking');
        var textarea = $cancel_booking.find('textarea');
        if (status == 'declined' && !$.trim(textarea.val())) {
            $cancel_booking.find('#message-error').remove();
            '<label id="message-error" class="error" for="email">This field is required.</label>';
            textarea.after('<label id="message-error" class="error" for="message">This field is required.</label>');
            return false;
        }
        statusBooking(button, {booking_id: bkid, status: status, type: type, message: $.trim(textarea.val())}, url);
    });
    $(document).on('click', '.booking-panel', function () {
        var button = $(this);
        var bkid = button.data('bkid');
        window.location.href = config.langUrl + '/detail/' + bkid;
    });
    $(document).on('click', '.message-booking', function () {
        var button = $(this);
        var bkid = button.data('bkid');
        var url = button.data('url');
        var message = button.siblings('textarea');
        var owner = button.data('owner');
        var recei_tag = button.closest('.chat-cnt-edit-traveller').find('.item-message-chat');
        /*if(owner == recei_tag.data('user_id')){
         message.val('');
         return modalDisplay(trans.warning, 'warning', 'The message cannot send. You have to wait until the enemy reply message');
         }*/
        if (!$.trim(message.val())) {
            return modalDisplay(trans.warning, 'warning', 'The message is not empty');
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {booking_id: bkid, message: message.val()},
            beforeSend: showLoader.bind(button)
        }).done(function (str) {
            if (str.success) {
                var srcDate=str.data.created_at+' UTC';
                srcDate = srcDate.replace(/-/g, ' ');
                var date=new Date(srcDate);
                var created_at= date.toLocaleDateString()+" "+date.toLocaleTimeString();
                var avatar = str.data.avatar?config.themeAvatar+'/'+str.data.avatar:config.themeImg+'/user.png';
                var type = str.data.type == 'user' ? 'trevaller' : 'guide';
                var html = '<div class="content-mesage avatar-bookinfo">';
                html += '<div class="name-chat color263038 font-size-12">';
                if (type == 'guide'){
                    html += '<span class="fnt-weight">'+str.data.full_name+'</span>&nbsp<b>(guide)</b>';
                }
                else{
                    html += '<span class="fnt-weight">'+str.data.full_name+'</span>';
                }
                html += '</div>';
                html += '<div class="avatar_traveller">';
                html += '<img class="avatar-chatbox" src="'+avatar+'">';
                html += '</div>';
                html += '<div class="item-message-chat" data-user_id="">'
                html += '<div class="col-md-12 font-size-14 color4e5d6b ' + type + '_inbox message-chat" data-user_id="' + str.data.user_id + '">';
                html += '<span>' + str.data.content + '</span>';
                html += '</div>';
                html += '<div class="col-md-12">';
                html += '<time datetime="2009-11-13T20:00" class="font-size-12 color4e5d6b-op">' + created_at + '</time>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                $('.content-chat').prepend(html);
                message.val('');
            } else {
                return modalDisplay(trans.warning, 'warning', str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
            hideLoader.call(button);
        })
    });

    $(document).on('click', '.show-more-booking', function (e) {
        e.preventDefault();
        var button = $(this);
        var url = button.data('url');
        var page = button.data('page');
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {page: page},
            beforeSend: showLoader.bind(button)
        }).done(function (str) {
            if (str.success) {
                button.data('page', str.data.page);
                if (str.data.html)
                    button.before(str.data.html);
                if (!str.data.flag_show)
                    button.hide();
            } else {
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
            hideLoader.call(button);
        })
    });

    $(document).on('click', '.show-more-notify', function (e) {
        e.preventDefault();
        var button = $(this);
        var url = button.data('url');
        var page = button.data('page');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {page: page},
            beforeSend: showLoader.bind(button)
        }).done(function (str) {
            console.log(str);
            if (str.success) {
                button.data('page', str.data.page);
                if (str.data.html)
                    $('.edit-notification').append(str.data.html);
                if (!str.data.flag_show)
                    button.hide();
            } else {
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
            hideLoader.call(button);
        })
    });

    $('#filter-history').change(function () {
        var value = $(this).val();
        if (value == 'all')
            window.location.href = '/edit/booking';
        else
            window.location.href = '/edit/booking/' + value;
    });
    $('.viewAll-notify').click(function () {
        window.location.href = '/edit/notification';
        location.reload();
    });

    $('button.btn-edit-price').click(function () {
        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: valuePrice,
            beforeSend: showLoader.bind(button)
        }).done(function (str) {
            $('label.error').remove();
            if (str.success) {
                $('.form-error-message').hide();
                location.reload();
            } else {
                $('.form-error-message').html(str.message).show();
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            $('label.error').remove();
            var response = e.responseJSON;
            $('.form-error-message').hide();
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
            Object.keys(response).forEach(function (i) {
                var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                $('#' + i).after(label);
            });
        }).always(function () {
            hideLoader.call(button);
        });
    });

    $('.bell-notify').click(function () {
        var url = $(this).data('url');
        var $this = $(this);
        var type = $this.data('type');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
            data: {type: type},
        }).done(function (str) {
            if (str.success) {
                $this.removeClass('.' + type + '-notify');
                $this.find('.badge').text('');
            } else {
                $('.form-error-message').html(str.message).show();
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
        });
    });

    $('.mark-as-read').click(function () {
        var url = $(this).data('url');
        var $this = $(this);
        var type = $this.data('type');
        var id = type == 'bell' ? $this.data('noid') : $this.data('cartid');
        var data = {notify_id: id, type: type};

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
            data: data,
        }).done(function (str) {
            if (str.success) {
                if (type == 'bell')
                    $('li[data-noid="' + $this.data('noid') + '"]').removeClass('mark-as-read');
                else
                    $('.panel-heading[data-cartid="' + $this.data('cartid') + '"]').removeClass('mark-as-read');
                $this.removeClass('mark-as-read');
                notify.subOne(type, 1);
            } else {
                $('.form-error-message').html(str.message).show();
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            // if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
        });
    });

    $(document).on('click', '#preview .ok', function (e) {
        var avatar = $('.avatar-profile').find('img');
        var cover = $('.img-cover').find('img');
        var img = avatar;
        var type = 'avatar';
        if (cover.length) {
            img = cover;
            type = 'cover';
            console.log('b');
        }
        if (img.length) {
            var fd = new FormData();
            var imageData = img.attr('src');
            var file = dataURItoBlob(imageData);
            fd.append('img', file);
            var url = config.langUrl + '/avatar';
            fd.append('type', type);
            fd.append('user_id', img.closest('.content-site').data('id'));
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: fd,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#loading').show()
                },
            }).done(function (str) {
                if (str.success) {
                    location.reload();
                } else {
                    $('.form-error-message').html(str.message).show();
                }
            }).fail(function (e) {
                var response = e.responseJSON;
                if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
            }).always(function () {
                $('#loading').hide();
                img.remove();
            });
        }
    });

    $(document).click(function (e) {
        var target = $(e.target);
        var mood = $('.mood-profile');
        var slogan = $('.mood-input');
        if (mood.is(e.target) || $(e.target).is('i.truncate')) {
            slogan.show();
            mood.find('i').hide();
            slogan.val(mood.find('i').text());
        }
        if (!mood.is(e.target) && !mood.has(e.target).length && !$(e.target).hasClass('mood-input') && !$(e.target).is('i')) {
            $('.mood-input').hide();
            mood.find('i').show();
        }
        if ($(e.target).hasClass('best-guide') && $(e.target).hasClass('show-more')) {
            var page = target.data('page');
            if (target.data('type') == 'news')
                htmlCity.show_more(config.langUrl + '/news', page, target);
            else
                htmlCity.show_more(config.langUrl + '/bestguide', page, target);
        }
    });

    $(document).on('keyup', '.mood-input', function (e) {
        if (e.key === 'Enter') {
            var input = $(this);
            var user_id = input.data('id');
            var url = config.langUrl + '/slogan';
            if (input.val())
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {user: user_id, slogan: input.val()},
                    beforeSend: function () {
                        $('#loading').show()
                    },
                }).done(function (str) {
                    if (str.success) {
                        location.reload();
                    } else {
                        $('.form-error-message').html(str.message).show();
                    }
                }).fail(function (e) {
                    var response = e.responseJSON;
                    if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
                }).always(function () {
                    $('#loading').hide();
                });
        }
    });

});
function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = decodeURI(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
}

function checkEmptyImage() {
    var cropme = $('.cropme');
    if (!cropme.find('img').length)
        return trans.please_choose_image;
    if (cropme.find('img').data('change') == 0)
        return trans.no_image_change;
    return false;
}

function returnCity(country_id, url, $this) {
    $.ajax({
        url: url,
        data: {country_id: country_id},
    }).done(function (e) {
        var city = $this.closest('.frm_edit').siblings('.frm_edit').find('select.select-city');
        var ultag = city.siblings('.dropdown-menu').find('ul');
        if (e.success) {
            var st = '';
            var i = 0;
            var st1 = htmlCity.liSelectCity({city_name: 'Please select city'}, i);
            e.data.forEach(function (city) {
                st += htmlCity.optionCity(city);
                i++;
                st1 += htmlCity.liSelectCity(city, i);
            });
            city.html(st);
            ultag.html(st1);
            city.selectpicker('refresh');

        } else {
            city.html('');
            ultag.html('');
        }
    }).always()
}

function statusBooking(button, data, url) {
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        beforeSend: showLoader.bind(button)
    }).done(function (str) {
        if (str.success) {
            location.reload();
        } else {
            location.reload();
        }
    }).fail(function (e) {
        var response = e.responseJSON;
        if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
    }).always(function () {
        hideLoader.call(button);
    })
}

function removeSpaces(string) {
    return string.split(' ').join('');
}




