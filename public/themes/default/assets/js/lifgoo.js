/**
 * display messi modal
 * @param m_title
 * @param m_class
 * @param message
 */
function modalDisplay(m_title, m_class, message) {
    $('.messi-modal,.messi').remove();
    new Messi(
        message,
        {
            modal: true,
            modalOpacity: 0.5,
            title: m_title,
            titleClass: m_class
        }
    );
}
/**
 * display messi modal
 * @param element
 * @param message
 */
function modalCallback(element, message, status) {
    var jqu = $(element);
    jqu.find('p').html('<strong>' + status + '! </strong>' + message);
    jqu.modal('show').delay( 6000 ).fadeOut( 400 );
    setTimeout(function(){
        // $("body").removeClass('modal-open');
        // $("body").css('padding-right','0px');
        jqu.modal('hide');
    }, 6400);
    if (element == '#successModal')
        $('.modal-backdrop').removeClass("modal-backdrop");
}
/**
 * disable button submit of form
 * user sendjaxbefore
 * show css waiting
 */
function showLoader() {
    if ($(this).is('button')) {
        this.disabled = true;
        $(this).addClass('disabled');
        $(this).prop('disabled', true);
    }
    $('.callout-error').html('');
    $('#loading').show();
}
/**
 * hide css waiting and enalble button submit form
 */
function hideLoader() {
    if ($(this).is('button')) {
        this.disabled = false;
        $(this).removeClass('disabled');
        $(this).prop('disabled', false);
    }
    $('#loading').hide();
}
function checkEmail(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email)) {
        return false;
    }
    return true;
}

function intervalCheckNotify() {
    var url = config.langUrl + '/intervalNotify';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'JSON',
        data: notify.arrayCount(),
    }).done(function (str) {
        if (str.success) {
            var bell = str.data.countBell;
            var message = str.data.countMessage;
            var cart = str.data.countCart;
            if (str.data.htmlBell)
                $('.content-notify').html(str.data.htmlBell);
            var segment = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
            if (segment == 'edit/notification' && notify.is_different('bell', bell))
                location.reload();
            if (segment == 'edit/booking' && (notify.is_different('cart', cart) || notify.is_different('message', message)))
                location.reload();
            if (notify.is_different('cart', cart))
                notify.replaceCount('cart', cart);
            if (notify.is_different('bell', bell))
                notify.replaceCount('bell', bell);
            if (notify.is_different('message', message))
                notify.replaceCount('message', message);

        }
    }).fail(function (e) {
        var response = e.responseJSON;
    }).always(function () {
    });
}

$(document).ready(function () {
    $('.user-profile').dropdown();
});

var windowLocation = (function () {
    var self = {};

    self.redirectUrlEdit = function (element) {
        var segment = window.location.href.split('/');
        segment = segment[segment.length-1];
        segment = segment.replace('#_=_','');
        if(segment != 'pending' && segment != 'accepted' && segment !='declined' && segment != 'canceled' && segment != 'finished'){
            element.removeClass('active');
            var par = element.find('a[data-href="' + segment + '"]').parent();
            par.addClass('active');
            $('.edit-guide').removeClass('active');
            $('#' + segment + '').addClass('active');
        }
        return self;
    };
    self.addTitleEdit = function (element) {
        var tab_left = element.attr('value');
        var title = $('.title-edit-guide');
        title.empty();
        title.append(tab_left);
        return self;
    };
    return self;
})();
/**
 * system notification include cart, notification and message
 */
var notify = (function () {
    var self = {};
    var belltag = $('.notification').find('.info-number');
    var carttag = $('.cart-booking');
    var messagetag = $('.message-booking-notify');

    self.alertNotifyError = function (str) {
        var message = self.formatNotify(str);
        $('.callout-error').html(message);
        return self;
    };
    self.formatNotify = function (message) {
        var str = '<div class="alert alert-danger fade in">';
        str += '<a href="#" class="close" data-dismiss="alert">&times;</a>';
        str += '<strong>Error! </strong>' + message;
        str += '</div>';
        return str;
    };
    self.addOne = function (type, number) {
        var target = belltag;
        if (type == 'cart')
            target = carttag;
        if (type == 'message')
            target = messagetag;
        var counttag = target.find('.badge');
        var count = counttag.text() ? parseInt(counttag.text()) : 0;
        if (count + number > 0) {
            counttag.text(count + number);
            target.addClass(type + '-notify');
        }
    };
    self.replaceCount = function (type, number, html) {
        var target = belltag;
        if (type == 'cart')
            target = carttag;
        if (type == 'message')
            target = messagetag;
        var counttag = target.find('.badge');
        if (number > 0)
            counttag.text(number);
        if (type == 'bell' && number > 0) {
            target.addClass(type + '-notify');
        }

    };
    self.subOne = function (type, number) {
        var target = belltag;
        if (type == 'cart')
            target = carttag;
        if (type == 'message')
            target = messagetag;
        var counttag = target.find('.badge');
        var count = counttag.text() ? parseInt(counttag.text()) : 0;
        if (count - number > 0)
            counttag.text(count - number);
        else {
            counttag.text('');
            target.removeClass(type + '-notify');
        }
    };
    self.count = function (target) {
        target = target.find('.badge');
        return target.text() ? parseInt(target.text()) : 0;
    };
    self.arrayCount = function () {
        return {
            cart: self.count(carttag),
            bell: self.count(belltag),
            message: self.count(messagetag)
        };
    };
    self.is_different = function (type, number) {
        var target = belltag;
        if (type == 'cart')
            target = carttag;
        if (type == 'message')
            target = messagetag;
        var count = self.count(target);
        if (count != number)
            return true;
        return false;
    };
    return self;
})();
/**
 * value price global parameter
 * valuePrice = {1//capacity:{1//hour:2//price,2:3}}
 * @type {{}}
 */
var valuePrice = {};
var editPrice = false;

var table = (function () {
    var tbPrice = $('#mytable');
    var self = {};

    self.hasValue = function (capacity, hour, price) {
        if (valuePrice.hasOwnProperty(capacity)) {
            if (valuePrice[capacity].hasOwnProperty(hour)) {
                if (valuePrice[capacity][hour] == price)
                    return 'hasPrice';
                return 'hasHour';
            }
            return 'hasCapacity';
        }
        return false;
    };

    self.insertPriceGlobal = function (capacity, hour, price) {
        var inputPrice = $(".input-add-price");
        if(editPrice){
            delete valuePrice[editPrice[0]][editPrice[1]];
        }
        if (!self.hasValue(capacity, hour, price)) {
            valuePrice[capacity] = {};
        }
        valuePrice[capacity][hour] = price;
        valuePrice = self.sortObjectInObject(valuePrice);
        self.insertIntoTable();
        inputPrice.val('');
        editPrice = false;
        return self;
    };

    self.removePriceGlobal = function (capacity, hour, price) {
        if (self.hasValue(capacity, hour, price) == 'hasPrice') {
            delete valuePrice[capacity][hour];
            self.insertIntoTable();
        }
        else
            return modalDisplay(trans.warning, 'warning', 'Invalid value');

    };

    self.insertIntoTable = function () {
        var st = '';
        for (var obj in valuePrice) {
            var leng = Object.keys(valuePrice[obj]).length;
            if (leng)
                for (var j in valuePrice[obj]) {
                    st += htmlCell(obj, j, valuePrice[obj][j], leng);
                    leng = 0;
                }
        }
        tbPrice.find('.tr-edit-price').remove();
        tbPrice.find('.edit-price').prepend(st);
        return self;
    };

    self.sortObjectInObject = function (obj, order) {
        if (obj) {
            obj = self.sortObject(obj);
            for (var j in obj) {
                var value = self.sortObject(obj[j]);
                obj[j] = value;
            }
        }
        return obj;
    };

    self.sortObject = function (obj, order) {
        var key, tempArry = [], i, tempObj = {};
        for (key in obj) {
            tempArry.push(key);
        }
        tempArry.sort(
            function (a, b) {
                return a.toLowerCase().localeCompare(b.toLowerCase());
            }
        );
        if (order === 'desc') {
            for (i = tempArry.length - 1; i >= 0; i--) {
                tempObj[tempArry[i]] = obj[tempArry[i]];
            }
        } else {
            for (i = 0; i < tempArry.length; i++) {
                tempObj[tempArry[i]] = obj[tempArry[i]];
            }
        }
        return tempObj;
    };
    self.regexNumber = function (number) {
        if (!/^[0-9.,]+$/.test(number))
            return modalDisplay(trans.warning, 'warning', 'Please enter only digits.');
        if (number == 0)
            return modalDisplay(trans.warning, 'warning', 'You must enter a number greater than 0');
    };

    function htmlCell(capacity, hour, price, leng) {
        var st = '<tr class="tr-edit-price">';
        if (leng)
            st += '<td rowspan="' + leng + '" class="count_user">' + capacity + '</td>';
        st += '<td class="count_hour" data-capacity="' + capacity + '">' + hour + '</td>';
        st += '<td class="count_price">' + price + '</td>';
        st += '<td class="btn-delete-price"><a href="javascript:void(0)">Delete</a></td>';
        st += '<td class="btn-edit-price"><a href="javascript:void(0)">Edit</a></td>';
        st += '</tr>';
        return st;
    }

    return self;
})();

/*datesBooking = {};*/
/**
 * data of database: 1: 1,2,3,4,5,6,7 //1: sunday, 1,2,3,4,5,6,7: hours
 *           2: 1,3,6,8 // 2: monday, 3 tuesday, 4 wednesday, 5 thursday, 6 friday, 7 saturday
 */
var booking = (function () {
    var datesBooking = {};
    var guideschedules = {};
    var dateHoursBooking = {};
    var hourly_rate = 0;
    var self = {};
    var bookedHours=[];
    var priceBooking = 0.0;
    self.setDateBooking = function (dates) {
        datesBooking = dates;
    };

    self.setHourlyRate = function (price) {
        hourly_rate = price;
    };

    self.setGuideschedules = function (dates) {
        guideschedules = dates;
    };

    self.setBookedHours = function (dates) {
        bookedHours=[];
        $.map(dates, function(value, index) {
            bookedHours[index.toString()]=[value];
        });
    };

    self.setDateHoursBooking = function (date, hrs) {
        dateHoursBooking[date] = hrs;
        if (hrs == 0)
            delete dateHoursBooking[date];
    };
    self.removeHoursBooking = function () {
        var temp = {};
        for (var i in datesBooking) {
            var date = self.getDate(datesBooking[i]);
            if (typeof (dateHoursBooking[date]) != 'undefined')
                temp[date] = dateHoursBooking[date];
        }
        dateHoursBooking = temp;
    };

    self.getDatesBooking = function () {
        return datesBooking;
    };

    self.getSchedules = function () {

        return guideschedules;
    };

    self.getDatesHoursBooking = function () {
        return dateHoursBooking;
    };

    self.getPrie = function () {
        return priceBooking;
    };

    function addZero(num) {
        return num < 10 ? '0' + num : num;
    };

    self.getDate = function (times) {
        return times.getFullYear() + '/' + addZero(times.getMonth() + 1) + '/' + addZero(times.getDate());
    };

    self.genarateHours = function () {
        var day, date;
        var hours_tag = $('.content-hours-book');
        var st = '';
        datesBooking.forEach(function (value) {
            date = self.getDate(value);
            day = dayOfWeek()[value.getDay()];
            hrs = dateHoursBooking && dateHoursBooking[date];
            st += self.htmlHours(guideschedules[day], date, hrs);
        });

        hours_tag.html(st);
        hours_tag.css('min-height','200px');
    };

    self.htmlHours = function (dateBusy, date, hrs) {
        var leng_hrs = hrs ? hrs.length : 0;
        var st = '<div class="item-hours-book">';
        st += '<div class="date-book color263038-1 font-size-14">' + date + '</div>';
        st += '<div class="count-hour-book">';
        st += '<div class="form-group select-hour">';
        st += '<input type="text" class="form-control" value="' + leng_hrs + 'h" readonly>';
        st += '<div class="input-group-addon show-form-hour"><i class="fa fa-clock-o" aria-hidden="true"></i></div>';
        st += '</div>';
        st += '</div>';
        st += '<div class="choose-date" style="display: none;" data-date="' + date + '">';
        st += '<table class="table table-bordered">';
        st += '<tbody>';
        for (var i = 0; i <= 23; i++) {
            if ((i+1) % 6 == 1)
                st += '<tr>';
            if (!dateBusy || dateBusy.indexOf(i.toString()) == -1 || (bookedHours[date.toString()]!=undefined&&bookedHours[date.toString()][0].indexOf(i.toString())>-1))
                st += '<td disabled class="disable-hour" data-value="' + i + '">' + addZero(i) + ':00</td>';
            else if (hrs && hrs.indexOf(i) > -1)
                st += '<td data-value="' + i + '" class="active-hour">' + addZero(i) + ':00</td>';
            else
                st += '<td data-value="' + i + '">' + addZero(i) + ':00</td>';
            if ((i+1) % 6 == 0)
                st += '</tr>';
        }
        st += '</tbody>';
        st += '</table>';
        st += '</div>';
        st += '</div>';
        return st;
    };

    self.countPrice = function () {
        var count_d = datesBooking.length || 0;
        var count_h = 0;

        var pp = $('.cnt-number-of-people').find('input').val() || 0;
        var temp = {};
        var price = 0.0;
        var li_date = '';
        var st_bookingdate = '<i class="color4e5d6b-op">Booking dates:</i>';
        for (var i in dateHoursBooking) {
            if (typeof dateHoursBooking[i] != 'undefined') {
                var leng = dateHoursBooking[i].length;
                count_h += leng;
                li_date += '<li>' + i + ' - ' + leng + (leng > 1 ? ' hours' : ' hour') + '</li>';
                st_bookingdate += self.htmlDatetime({date:i,hour_leng:leng,hours:dateHoursBooking[i]});
            }
        }
        count_d=(Object.keys(dateHoursBooking).length);

        var st = '';
        st += count_h;
        st += count_h < 2 ? ' hour' : ' hours';
        st += ' in ' + count_d;
        st += count_d < 2 ? ' day' : ' days';
        $('.total-book-date').text(st);
        st += ' for ' + pp;
        st += pp < 2 ? ' person' : ' people';
        price = (table.hasValue(pp, count_h, -1) == 'hasHour') ? (valuePrice[pp][count_h]) : (hourly_rate * pp * count_h);
        price = parseFloat(price).toFixed(2);

        stPrice = "<span class='color263038 font-size-14'>Total price: </span><span class='price-icon'>" + price + "</span>";
        $('.total-booking').html(stPrice);
        // $('.total-booking').text('Total price: $' + price);
        // 
        stHour = count_h < 2 ? ' Hour' : ' Hours';
        stDay = count_d < 2 ? ' Day' : ' Days';
        stPeople = pp < 2 ? ' Person' : ' People';
        // 
        stDate = "<p><span class='color263038'>"+stHour+"</span> " + count_h + "</p><p><span class='color263038'>"+stDay+"</span> " + count_d + "</p><p><span class='color263038'>"+stPeople+"</span> " + pp + "</p>";
        $('.total-hrs-dates').html(stDate);
        // $('.total-hrs-dates').html(st);

        //$('.list-booking-date').html(li_date);
        var li_info = $('.list-right-statistical').find('li');
        li_info.first().find('span.data-append').text(pp);
        var booking_price = $('.booking-price');
        var fixedprice=parseFloat(price);
        booking_price.html('<span class="color263038 font-size-14">Total price: </span><span class="price-icon">' + fixedprice.toFixed(2) + '</span>');
        // booking_price.text('PRICE: $' + fixedprice.toFixed(2));
        // li_info.first().text('Number of people: ' + pp);
        // li_info.last().text('Expected cost: $' + price);
        priceBooking = fixedprice.toFixed(2);
        $('.booking-date').html(st_bookingdate);
    };

    self.htmlDatetime = function (data) {
        var st = '<div class="itemInfo-date">';
        st += '<p class="font-size-14 fnt-weight1 color4e5d6b">'+data['date']+' - '+self.addPlural(data['hour_leng'],'hour','s')+'</p>';
        st += '<ul class="list-bookingdate">'
        if(data['hours'].length){
            for (var i in data['hours']){
                st += '<li>'+addZero(data['hours'][i])+':00</li>';
            }
        }
        st += '</ul>'
        st += '</div>'
        return st;
    };
    self.addPlural = function($x,$value,$type){
        if ($x > 1)
            return $x+' '+$value+$type;
        return $x+' '+$value;
    }
    return self;
})();

var generateOptionTime = (function () {
    var self = {};

    function addZero(num) {
        num = String(num);
        return num < 10 ? '0' + num : String(num);
    }

    self.htmlTime = function (from, type) {
        var st = '';
        st += '<option value="" hidden>' + type + '</option>';
        for (var i = from; i < 24; i++) {
            st += '<option value="' + i + '">' + addZero(i) + ':00</option>';
        }
        return st;
    };

    self.htmlTime59 = function (from, type) {
        var st = '';
        st += '<option value="" hidden>' + type + '</option>';
        for (var i = from; i < 24; i++) {
            st += '<option value="' + i + '">' + addZero(i) + ':59</option>';
        }
        return st;
    };

    return self;
})();

var htmlCity = (function () {
    var self = {};
    self.optionCity = function (data) {
        return '<option value="' + data['city_id'] + '">' + data['city_name'] + '</option>';
    };

    self.liSelectCity = function (data, i) {
        var st = '<li data-original-index="' + i + '" class="">';
        st += '<a tabindex="' + i + '" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">' + data['city_name'] + '</span><span class="glyphicon glyphicon-ok check-mark"></span></a>';
        st += '</li>';
        return st;
    };

    self.htmlLocal = function (data) {
        var st = '<div class="div-add-price">';
        st += '<div>';
        st += '<div class="frm_edit frm_editJobCountry">';
        st += ' <select class="selectpicker form-control select-country" data-style="btn-primary" name="country_id[]" data-url="' + data['url'] + '" required>';
        st += data['option'];
        st += '</select>';
        /*st += data['select'];*/
        st += '</div>';
        st += '<div class="frm_edit frm_editJobCity">';
        st += '<select class="selectpicker form-control select-city" data-style="btn-primary" name="city_id[]" required>';
        st += data['option_city'];
        st += '</select>';
        st += '</div>';
        st += '<div class="icon_add_remove">';
        st += '<i class="fa fa-minus click-remove font-size-18" aria-hidden = "true"></i>';
        st += '</div>';
        st += '</div>';
        st += '</div>';
        return st;
    };

    self.ajaxFindGuide = function (data) {
        var url = data['url'];
        var form = data['form'];
        var type = data.type;
        var button = data['button'];
        if (type != 'show_more')
            form.find('input[name="page"]').val(0);
        else {
            if (form.find('input[name="page"]').val() == 0)
                form.find('input[name="page"]').val(1);
        }
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: form.serialize(),
            beforeSend: showLoader.bind(button)
        }).done(function (str) {
            if (str.success) {
                var guide = $('.guide_selected');
                if (type == 'show_more') {
                    form.find('input[name="page"]').val(str.data.page);
                    guide.append(str.data.html);
                }
                if (type != 'show_more')
                    guide.html(str.data.html);
                if (!str.data.html)
                    guide.html('<p style="text-align: center;">No guide is found</p>');
                if (str.data.flag_show)
                    $('.wraper-show-more').html('<a class="show-more find-guide font-size-14 fnt-weight color4 bgBlue" href="javascript:void(0)" data-url="' + config.langUrl + '/findGuide">VIEW MORE</a>');
                else
                    $('.wraper-show-more').html('');
            } else {
                /*$('.form-error-message').html(str.message).show();*/
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
            hideLoader.call(button);
        })
        return self;
    }

    self.show_more = function (url, page, target) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: {page: page},
            beforeSend: function () {
                $('#loading').show()
            },
        }).done(function (str) {
            console.log(str);
            if (str.success) {
                if (str.data.html) {
                    $('.show-more-append').append(str.data.html);
                    target.data('page', str.data.page);
                }
                if (!str.data.flag_show)
                    target.remove();
            } else {
                $('.form-error-message').html(str.message).show();
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
            $('#loading').hide();
        });
        return self;
    };

    return self;
})();

