

$(document).ready(function () {

    jQuery('.tp-banner').show().revolution(
    {
        dottedOverlay: "none",
        delay: 16000,
        startwidth: 1170,
        startheight: 600,
        touchenabled: "on",
        onHoverStop: "off",
        swipe_velocity: 0.7,
        swipe_min_touches: 1,
        swipe_max_touches: 1,
        drag_block_vertical: false,
        parallax: "mouse",
        parallaxBgFreeze: "on",
        parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
        keyboardNavigation: "off",
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 0,
        navigationVOffset: 20,
        shadow: 0,
        fullWidth: "off",
        fullScreen: "off",
        spinner: "spinner4",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "on",
        forceFullWidth: "off",
        hideThumbsOnMobile: "off",
        hideNavDelayOnMobile: 1500,
        hideBulletsOnMobile: "off",
        hideArrowsOnMobile: "off",
        hideThumbsUnderResolution: 0,
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        startWithSlide: 0,
        fullScreenOffsetContainer: ".header"
    });

    // ----------------------------

    $("#main-slider").slick({
     dots: false,
     infinite: true,
     autoplay: true,
     autoplaySpeed: 1000,
     fade: true,
     cssEase: 'linear',
     arrows: false,
     mobileFirst: true,
     speed: 700
     });

    // SLIDE HEADER
    $('#SlideHeader').slick({
         dots: false,
         arrows: false,
         fade: true,
         autoplay: true,
         speed: 1000
     });

    // $('#best-guide-slider').slick({
    //      dots: false,
    //      infinite: true,
    //      speed: 300,
    //      slidesToShow: 4,
    //      slidesToScroll: 1,
    //      autoplay: false,
    //      responsive: [
    //          {
    //              breakpoint: 1200,
    //              settings: {
    //                  slidesToShow: 3,
    //                  slidesToScroll: 1,
    //                  infinite: true,
    //                  dots: false
    //              }
    //          },
    //          {
    //              breakpoint: 801,
    //              settings: {
    //                  slidesToShow: 2,
    //                  slidesToScroll: 1,
    //                  infinite: true                 
    //              }
    //          },
    //          {
    //              breakpoint: 415,
    //              settings: {
    //                  slidesToShow: 1,
    //                  slidesToScroll: 1,
    //                  infinite: true
    //              }
    //          }
    //          // You can unslick at a given breakpoint now by adding:
    //          // settings: "unslick"
    //          // instead of a settings object
    //      ]
    //  });
    
    // SUGGEST GUIDE DETAIL NEWS
    $('.guide-slide').slick({
         dots: false,
         infinite: true,
         speed: 300,
         slidesToShow: 4,
         slidesToScroll: 1,
         autoplay: false,
         pauseOnHover: true,
         responsive: [
             {
                 breakpoint: 1200,
                 settings: {
                     slidesToShow: 3,
                     slidesToScroll: 1,
                     infinite: true,
                     dots: false
                 }
             },
             {
                 breakpoint: 801,
                 settings: {
                     slidesToShow: 2,
                     slidesToScroll: 1,
                     infinite: true                 
                 }
             },
             {
                 breakpoint: 415,
                 settings: {
                     slidesToShow: 1,
                     slidesToScroll: 1,
                     infinite: true
                 }
             }
             // You can unslick at a given breakpoint now by adding:
             // settings: "unslick"
             // instead of a settings object
         ]
     });

    // go to link profile when click slide item
    // $('.slide').click(function(e) {
    //     if(!$(e.target).hasClass('hover-link') )
    //     {
    //        var url = $(this).attr('data-url');
    //         window.open(url, '_blank');               
    //     }
    // });
    // go to link profile when click find guide item
    // $(document).on('click','.slide-thumb',function(e){
    //     if(!$(e.target).hasClass('hover-link') )
    //     {
    //        var url = $(this).attr('data-url');
    //         window.open(url, '_blank');     
    //     }
    // });

    // CHANGE COVER IMAGE
    $(document).on('click','.btn-changecoverImg',function(e){
        $('.cropme.img-cover').click();

    });

    /*BACK TO TOP*/
    $("#back-to-top").click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
    });
    $(window).on("scroll load", function(){
        var startBtntop = $(window).height();
        var docheight = $(document).innerHeight();
        if ($(window).width() >= 992) {
            var bottomheight = $('.footer').innerHeight() + $('.get-in-touch').innerHeight() + 18;
        }
        else{
            // var bottomheight = $('.footer').innerHeight();
            var bottomheight = 10;
        }
        var endBtntop = docheight - startBtntop - $('.footer').innerHeight() - $('.get-in-touch').innerHeight();
        var subscribe = $('.get-in-touch').length;
        if (startBtntop < endBtntop) {
            if ($(this).scrollTop() <= startBtntop) {
                if ($('#back-to-top').has('back-to-top-show')) {
                    $('#back-to-top').removeClass('back-to-top-show').addClass('back-to-top-hide');
                }
                else{
                    $('#back-to-top').addClass('back-to-top-hide');
                }        
            }else{            
                if (($(this).scrollTop() >= startBtntop) && ($(this).scrollTop() <= endBtntop)) {
                    if ($('#back-to-top').has('back-to-top-hide')) {
                        $('#back-to-top').removeClass('back-to-top-hide').addClass('back-to-top-show');
                    }
                        else{
                        $('#back-to-top').addClass('back-to-top-show');
                    }
                    if ($('#back-to-top').has('back-to-top-absolute')) {
                        $('#back-to-top').removeClass('back-to-top-absolute');
                    }
                }
                else{
                    $('#back-to-top').removeClass('back-to-top-show');
                    $('#back-to-top').removeClass('back-to-top-hide');
                    $('#back-to-top').addClass('back-to-top-absolute');
                }
            }
        }
        $('.back-to-top-show').css('bottom','18px');
        $('.back-to-top-absolute').css('bottom',bottomheight);
    });

    /*Jquery Datepicker*/
    /*$('#dp1').datepicker({
     format: 'yyyy-mm-dd',
     });
     $('#dp2').datepicker({
     format: 'yyyy-mm-dd',
     });*/

    /*Jquery click change tab for signup*/
    $("#watch-me").click(function () {
        $("#show-me:hidden").show('slow');
        $("#show-me-two").hide();
    });
    $("#watch-me").click(function () {
        if ($('watch-me').prop('checked') === false) {
            $('#show-me').hide();
        }
    });

    $("#see-me").click(function () {
        $("#show-me-two:hidden").show('slow');
        $("#show-me").hide();
    });
    $("#see-me").click(function () {
        if ($('see-me-two').prop('checked') === false) {
            $('#show-me-two').hide();
        }
    });

    /*Jquery add and remove input*/
    $(document).on("click", ".add-input", function () {
        var addjob = '<div class="ar-job-setting">' +
            '<div class="form-group col-md-5">' +
            '<label>Country</label>' +
            '<select class="form-control">' +
            '<option>1</option>' +
            '</select>' +
            '</div>' +
            '<div class="form-group col-md-5">' +
            '</div>' +
            '<div class="form-group col-md-2">' +
            '<label>Active</label>' +
            '<a href="javascript:void(0)" class="add-input">' +
            '<i class="fa fa-plus" aria-hidden="true">' +
            '</i>' +
            '</a>' +
            '<a href="javascript:void(0)" class="remove-input">' +
            '<i class="fa fa-minus" aria-hidden="true">' +
            '</i>' +
            '</a>' +
            '</div>' +
            '</div>';
        $("#job-setting").append(addjob);
    });

    $(document).on("click", ".remove-input", function () {
        var re_in = $(this).parents(".ar-job-setting");
        $(re_in).remove();
    });

    /*Jquery select2 language*/
    /*$(".js-example-basic-multiple").select2({
     placeholder: "Select a language"
     });
     */
    /*Jquery add & remove Price*/
    $(document).on("click", ".add-row", function () {
        //$(".table-price-guide").css("display","block");
        var i = 0;
        var number_man = $('#number-man').val();
        var times = $('#time').val();
        var prices = $('#price').val();
        var tr_guide = '<tr>' +
            '<th class="snm' + i + ' " scope="row">' + number_man +
            '</th>' +
            '<td>' + times +
            '</td>' +
            '<td>' + prices +
            '</td>' +
            '</tr>';
        $('.table-price-guide').append(tr_guide);
        var dem = 0;
        var first;
        $(".snm").each(function () {
            var num_val = $(this).text();
            if (num_val == number_man) {

            }
        });
    });

    /*Jquery call counter up*/
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });


    /*Jquery Validate*/
    /*Jquery Avatar*/
    jQuery('.cropme').simpleCropper();
    /*jQuery('.cropme-cover').simpleCropper();*/

    $("#click-add").click(function () {
        $.ajax({
            type: 'GET',
            url: 'addform',
            success: function (data) {
            }
        });
    });

    $(document).on("click", "#click-remove", function () {
        $(this).parent().parent().remove();
    });

    function MergeGridCells() {
        var dimension_cells = [];
        var dimension_col = null;

        var i = 1;
        // First, scan first row of headers for the "Dimensions" column.
        $("#mytable").find('th').each(function () {
            if ($(this).text() == 'Id') {
                dimension_col = i;
            }
            i++;
        });

        // first_instance holds the first instance of identical td
        var first_instance = null;
        var rowspan = 1;
        // iterate through rows
        $("#mytable").find('tr.parent-grid-row').each(function () {

            // find the td of the correct column (determined by the dimension_col set above)
            var dimension_td = $(this).find('td.parent-grid-column:nth-child(' + dimension_col + ')');

            if (first_instance == null) {
                // must be the first row
                first_instance = dimension_td;
            } else if (dimension_td.text() == first_instance.text()) {
                // the current td is identical to the previous
                // remove the current td
                dimension_td.remove();
                ++rowspan;
                // increment the rowspan attribute of the first instance
                first_instance.attr('rowspan', rowspan);
            } else {
                // this cell is different from the last
                first_instance = dimension_td;
                rowspan = 1;
            }
        });
    }

    /*jQuery Edit Price*/
    $(document).on("click", ".td_add", function () {

        var capacity = $("input[name=edit_count_user]").val();
        var hour = $("input[name=edit_hour]").val();
        var price = $("input[name=edit_price]").val();
        // console.log(capacity);
        // console.log(hour);
        // console.log(price);

        var reg = /^[0-9.,]+$/;
        if(!capacity || !hour || !price)
            return modalDisplay(trans.warning, 'warning', 'Please fill all with right number.');
        if (!reg.test(capacity) || !reg.test(hour) || !reg.test(price))
            return modalDisplay(trans.warning, 'warning', 'Please fill all with right number.');
        if (capacity == 0 || hour == 0 || price == 0)
            return modalDisplay(trans.warning, 'warning', 'You must enter a number greater than 0.');
        table.insertPriceGlobal(capacity, hour, price);
    });

    $(document).on("click", "td.btn-edit-price", function () {
        var $this = $(this);
        var sib = $this.siblings('.count_hour');
        var sib_price = $this.siblings('.count_price');
        var capacity = sib.data('capacity');
        var hour = sib.text();
        var price = sib_price.text();
        $("input[name='edit_count_user']").val(capacity);
        $("input[name='edit_hour']").val(hour);
        $("input[name='edit_price']").val(price);
        editPrice = [capacity,hour,price];
    });

    $(document).on("click", ".btn-delete-price", function () {
        var $this = $(this);
        var sib = $this.siblings('.count_hour');
        var sib_price = $this.siblings('.count_price');
        var capacity = sib.data('capacity');
        var hour = sib.text();
        var price = sib_price.text();
        table.removePriceGlobal(capacity, hour, price);
        /*$this.siblings().remove();
         $this.remove();*/
    });
    $(".input-add-price:not(.price)").change(function () {
        table.regexNumber($(this).val());
    });

    /*CHECK ALL*/
    $('#select_all').change(function () {
        var checkboxes = $(this).closest('form').find(':checkbox');
        if ($(this).is(':checked')) {
            checkboxes.prop('checked', true);
        } else {
            checkboxes.prop('checked', false);
        }
    });

    /*Jquery click delete remove notification*/
    $(document).on("click", ".delete-notification", function () {
        //$('form input:checked').parents('li').remove();
        var delete_notify = [];
        $('.form-notify input:checked[name="item-delete-notify"]').each(function() {
            delete_notify.push($(this).val());
        });
        if(delete_notify.length == 0)
            return modalDisplay(trans.warning, 'warning', 'You must select at least one notice to delete');
        $.ajax({
            url: config.langUrl+'/delete_notify',
            type: 'GET',
            dataType: 'JSON',
            data: {notify_ids: delete_notify},
        }).done(function (str) {
            if (str.success) {
                location.reload();
            } else {
                $('.form-error-message').html(str.message).show();
                notify.alertNotifyError(str.message);
            }
        }).fail(function (e) {
            var response = e.responseJSON;
            if (!response) return modalDisplay(trans.warning, 'warning', trans.error_occured);
        }).always(function () {
        });
    });

    /*Jquery click Marked read notification*/
    $(document).on("click", ".marked-read a", function () {
        $('form input:checked').parents('li').find('.marked-read').remove();
        $('form input:checked').parents('li').css('background-color', '#cbf5f1');
    });

    /*jQuery Modal Deatail Book Guide*/
    $(document).on("click", ".modal-detail-book", function () {
        $('#modal-detail-book').modal('show');
    });

    /*jQuery Modal Write Review*/
    $(document).on("click", ".btn-write-review", function () {
        $('#write-review').modal('show');
    });

    /*Jquery append title edit guide*/
    $(document).ready(function () {
        windowLocation.redirectUrlEdit($('.tab-guide li'));
        windowLocation.addTitleEdit($('.tab-guide .active'));
    });
    $(document).on("click", ".tab-guide li", function () {
        var $this = $(this);
        windowLocation.addTitleEdit($this);
        var segment = $this.find('a').data('href');
        window.history.pushState(null, null, config.langUrl+'/edit/'+segment);
        $('.tab-pane').removeClass('active');
        var tab = $('.tab-guide li');
        tab.removeClass('active');
        $('#'+segment).addClass('active');
        $('.'+segment).addClass('active');
        if (segment == 'edit-profile') {
            $('.editProfile').addClass('active');
        }
        if(segment == 'booking' || segment == 'notification')
            location.reload();
    });

    $(document).on("click", ".click-here", function () {
        $(".tab-guide").find('a[data-href="notification"]').click();
    });

    $('.cart-booking,.message-booking-notify').click(function () {
       $(".tab-guide").find('a[data-href="booking"]').click();
    });

    $(document).ready(function () {
        $("#e1").select2();
    });

    /*$(document).on("click",".history-booking",function () {
     $(this).find('.excerpt_traveller').find('.content-traveller').toggle();
     });*/

    /*Jquery content edit traveller*/
    $(document).ready(function () {
        $('.collapse.in').prev('.panel-heading').addClass('active');
        $('#accordion, #bs-collapse')
            .on('show.bs.collapse', function (a) {
                $(a.target).prev('.panel-heading').addClass('active');
            })
            .on('hide.bs.collapse', function (a) {
                $(a.target).prev('.panel-heading').removeClass('active');
            });
    });

    $(document).on("click", ".panel-heading", function () {
        $('.panel').css('background-color', 'transparent');
        $('.active').parents('.panel').css('background-color', '#f9f9f9');
        $('.list-deactive').css('display', 'block');
        $('.list-active').css('display', 'none');
        var hClass = $(this).hasClass('active');
        if (hClass) {
            $(this).find('.list-right').find('.list-deactive').css('display', 'none');
            $(this).find('.list-right').find('.list-active').css('display', 'block');
        }
        else {
            $(this).find('.list-right').find('.list-deactive').css('display', 'block');
            $(this).find('.list-right').find('.list-active').css('display', 'none');
        }
    });


    // Starrr plugin (https://github.com/dobtco/starrr)
    var __slice = [].slice;

    (function ($, window) {
        var Starrr;

        Starrr = (function () {
            Starrr.prototype.defaults = {
                rating: void 0,
                numStars: 5,
                change: function (e, value) {
                }
            };

            function Starrr($el, options) {
                var i, _, _ref,
                    _this = this;

                this.options = $.extend({}, this.defaults, options);
                this.$el = $el;
                _ref = this.defaults;
                for (i in _ref) {
                    _ = _ref[i];
                    if (this.$el.data(i) != null) {
                        this.options[i] = this.$el.data(i);
                    }
                }
                this.createStars();
                this.syncRating();
                this.$el.on('mouseover.starrr', 'i', function (e) {
                    return _this.syncRating(_this.$el.find('i').index(e.currentTarget) + 1);
                });
                this.$el.on('mouseout.starrr', function () {
                    return _this.syncRating();
                });
                this.$el.on('click.starrr', 'i', function (e) {
                    return _this.setRating(_this.$el.find('i').index(e.currentTarget) + 1);
                });
                this.$el.on('starrr:change', this.options.change);
            }

            Starrr.prototype.createStars = function () {
                var _i, _ref, _results;

                _results = [];
                for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                    _results.push(this.$el.append("<i class='fa fa-star-o'></i>"));
                }
                return _results;
            };

            Starrr.prototype.setRating = function (rating) {
                if (this.options.rating === rating) {
                    rating = void 0;
                }
                this.options.rating = rating;
                this.syncRating();
                return this.$el.trigger('starrr:change', rating);
            };

            Starrr.prototype.syncRating = function (rating) {
                var i, _i, _j, _ref;

                rating || (rating = this.options.rating);
                if (rating) {
                    for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                        this.$el.find('i').eq(i).removeClass('fa-star-o').addClass('fa-star');
                    }
                }
                if (rating && rating < 5) {
                    for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                        this.$el.find('i').eq(i).removeClass('fa-star').addClass('fa-star-o');
                    }
                }
                if (!rating) {
                    return this.$el.find('i').removeClass('fa-star').addClass('fa-star-o');
                }
            };

            return Starrr;

        })();
        return $.fn.extend({
            starrr: function () {
                var args, option;

                option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                return this.each(function () {
                    var data;

                    data = $(this).data('star-rating');
                    if (!data) {
                        $(this).data('star-rating', (data = new Starrr($(this), option)));
                    }
                    if (typeof option === 'string') {
                        return data[option].apply(data, args);
                    }
                });
            }
        });
    })(window.jQuery, window);

    $(function () {
        return $(".starrr").starrr();
    });

    $(document).ready(function () {

        $('#stars').on('starrr:change', function (e, value) {
            $('#count').html(value);
        });

        $('#stars-existing').on('starrr:change', function (e, value) {
            $('#count-existing').html(value);
        });

        /*****
         *
         *
         *
         * jQuery Step Booking Guiode*/
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $("input.next").click(function () {
            var $this = $(this);
            if ($this.hasClass('next-step-one')) {
                if (!Object.keys(booking.getDatesBooking()).length)
                    return modalDisplay(trans.warning, 'warning', 'Please select at least trip date.');
                if (!Object.keys(booking.getDatesHoursBooking()).length)
                    return modalDisplay(trans.warning, 'warning', 'Please select at least one hour.');
                var pickUp = $('#pickup-placement').val() || 0;
                if (pickUp < 1)
                    return modalDisplay(trans.warning, 'warning', 'Please type pickup placement.');

                var destination = $('#destination').val() || 0;
                if (destination < 1)
                    return modalDisplay(trans.warning, 'warning', 'Please type destination.');

                var pp = $('.cnt-number-of-people').find('input').val() || 0;
                if (pp < 1 || !/^[0-9]+$/.test(pp))
                    return modalDisplay(trans.warning, 'warning', 'Invalid number of people booking.');
            }
            if ($this.hasClass('next-second-step')) {
                if (!$('#msform').valid())
                    return false;
                var title_country = $('#country option:selected').text();
                var title_city = $('#city option:selected').text();
                var title_pickup = $('#pickup-placement').val();
                var title_destination = $('#destination').val();
                var title_message=$('#message').val();
                var title_email=$('input[name="email"]').val();
                var title_phone=$('input[name="phone_number"]').val();

                $('.title-country span').text(title_country);
                $('.title-city span').text(title_city);
                $('.title-place span').text(title_pickup);
                $('.title-destination span').text(title_destination);
                $('.title-message span').text(title_message);
                $('.title-email span').text(title_email);
                $('.title-phone span').text(title_phone);

            }


            if (animating) return false;
            animating = true;

            current_fs = $this.parent();
            next_fs = $this.parent().next();
            
            $("#progressbar li").each(function(){
                if($(this).hasClass("active")){
                    $(this).removeClass('active');
                }
            });
            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'absolute'                       
                    });
                    next_fs.css({'left': left, 'opacity': opacity, 'display': 'inline-block'});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $("input.previous").click(function () {
            if (animating) return false;
            animating = true;

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            $("#progressbar li").eq($("fieldset").index(previous_fs)).addClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });

            /*****
             *
             *
             *
             * End jQuery Step Booking Guiode*/
        });

        /*jQuery Select Language*/
        $('.selectpicker').selectpicker();

        /*jQuery get windowsize remove element*/
        var windowsize = $(window).width();
        if (windowsize <= 992) {
            $('.bootsnav').find('.navbar-header').removeClass('col-md-3');
        }

        if(windowsize <= 1199){
            $('.bg-login').find('.content-signin').removeClass('col-md-8 col-md-offset-2');
            $('.bg-login').find('.content-signin').addClass('col-md-10 col-md-offset-1');
        }


        /*
         *
         *Input tags tokenfield**
         */
        $('.tokenfield').tokenfield({});

    });

    $(window).resize(function () {
        var windowsize = $(window).width();
        if (windowsize <= 992) {
            $('.bootsnav').find('.navbar-header').removeClass('col-md-3');
        }
    });

    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    $('#sandbox-container').datepicker({
        multidate: true,
        todayHighlight: true,
        inline: true,
        sideBySide: true,
        format: 'YYYY-MM-DD',
        startDate: new Date()
    }).on('changeDate', function (event) {
        var dates = event.dates;
        booking.setDateBooking(dates);
        booking.genarateHours();
        booking.removeHoursBooking();
        booking.countPrice();
    });

    $('#city').change(function(){
        $('#sandbox-container').datepicker('update');
        $('div.content-hours-book').empty();
        booking.setDateBooking('');
        booking.setDateHoursBooking('','');
        booking.removeHoursBooking();
        booking.countPrice();
    });

    $('th.next').on('click', function () {
        $('.bg-form-booking .datepicker-days table.table-condensed thead th.prev').css({
            'visibility': 'visible!important',
            'pointer-events': 'visible',
            'color': '#fff !important',
        });

    });

    var $win = $(window); // or $box parent container
    var $box = $(".mood-profile");
    var $log = $(".log");
    var mood_i = $('.mood-profile i');
    var mood_input = $('.mood-profile input');



    /*$win.on("click.Bst", function(event){
        if ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
            && !$box.is(event.target) //checks if the $box itself was clicked
        ){
            var text_mood_in = $('.mood-profile input').val();
            $(mood_i).css('display','block');
            $(mood_input).css('display','none');
            $(mood_i).empty();
            $(mood_i).append(text_mood_in);
        } else {
            var text_mood = $('.mood-profile i').text();
            $(mood_i).css('display','none');
            $(mood_input).css('display','block');
            $(mood_input).val(text_mood);
        }
    });*/

    /*
     *Jquery active menu*
     *
     */
    /*$(document).on('click', '#navbar-menu ul li a', function () {
        var hasActive = $(this).parents('li');
        if(!hasActive.hasClass('active')){
            $("#navbar-menu ul li").removeClass('active');
            $(this).parents('li').addClass('active');
        }
    });*/



    var page = window.location.pathname.split('/')[2];
    $("#navbar-menu ul li").removeClass('active');
    $('.username-right ul li').removeClass('linkactive');
    if(page == undefined){
        $('li.home').addClass('active');
    }
    else  if(page == "about"){
        $('li.about').addClass('active');
    } else if (page == 'findguide'){
        $('li.find-guide').addClass('active');
        $('.findguide--thumb').addClass('active');
    } else if(page == 'news'){
        $('li.news').addClass('active');
    } else if (page == 'contact'){
        $('li.contact').addClass('active');
    }else if (page == 'guides'){
        $('li.guide').addClass('active');
    }else if (page == 'profile'){
        $('li.myProfile').addClass('linkactive');
    }else if (page == 'edit'){
        $('li.myAccount').addClass('linkactive');
    }


    $('input[checkbox]').click(function () {
        if ($(this).is(':checked')) {
        }
        else {
        }
    });
    /*
     *
     *
     *jQuery click icon clock show form choose hours*/

    // $(document).on('click', '.show-form-hour', function () {
    //     var hg = $(this).parents('.item-hours-book').find('.choose-date');
    //     hg.toggle();
    // });
    $(document).on('click', '.select-hour', function () {
        var hg = $(this).parents('.item-hours-book').find('.choose-date');
        hg.toggle();
    });
    /*
     *
     *
     *jQuery add class active td in table*/
    $(document).on('click', '.choose-date table tbody tr td', function () {
        var $this = $(this);
        $this.toggleClass('active-hour');
        var choose_date = $this.closest('.choose-date');
        var hours = choose_date.find('td.active-hour');
        var hrs = [];
        if (hours.length)
            hours.each(function (index) {
                hrs.push($(this).data('value'));
            });
        booking.setDateHoursBooking(choose_date.data('date'), hrs);
        choose_date.siblings('.count-hour-book').find('input').val(hrs.length + 'h');
        booking.countPrice();
    });

    $('body').click(function (e) {
        var target = $(e.target);
        if (!target.is('.choose-date') && !target.closest('.choose-date').length) {
            $('.choose-date').not(target).slideUp();
        }
    });

    $('.cnt-number-of-people input').change(function () {
        booking.countPrice();
    });

    /*jquery check checkbox checked*/
    $('.edit-setup-time input[type="radio"]').change(function () {
        var $this = $(this);
        /*var choose_time = $this.closest('.on-off-time');
        var parent = choose_time.parent();
        var row = parent.siblings();
        console.log(row);
        if($this.val() == 0){
            !row.hasClass('hidden') && row.addClass('hidden');
            propSelect(parent.find('select'),false,true);
            //parent.siblings().addClass('hidden');
            choose_time.siblings().not('.hasRemove').addClass('hidden');
        }
        else{
            parent.find('.hasRemove').removeClass('hidden hasRemove');
            propSelect(parent.find('select'),true,false);
            choose_time.siblings().not('.hasRemove').removeClass('hidden');
        }*/
        var setup_item = $this.closest('.setup-time-item');
        var setup_second = setup_item.find('.add-setup-time');
        var setup_first = setup_item.find('.setup-time-first');
        var choose_time = $this.closest('.on-off-time');
        if($this.val() == 0){
            setup_second.addClass('hidden');
            setup_first.addClass('hidden');
            setup_first.find('.btn-setupTime--thumb').not('.hasRemove').addClass('hidden');
            propSelect(setup_item.find('select'),false,true);
        }else{
            setup_first.removeClass('hidden');
            propSelect(setup_first.find('select'),true,false);
            setup_first.find('.btn-setupTime--thumb').removeClass('hidden hasRemove');
        }
        $('.edit-setup-time').find('select').selectpicker('refresh');
    });

    $('.edit-setup-time .choose-add-hour').click(function () {
        var $this = $(this);
        var sib = $this.closest('.row').siblings('.add-setup-time');
        var org = $this.closest('.row.setup-time-first');
        if(org.find(".setup-select-to.to select").val()==23){
            return;
        }else{
            if(!sib.find(".setup-select-from.from1 select").val()){
            sib.find(".setup-select-to.to1 select").html("<option value='' hidden>To</option>");
            sib.find(".setup-select-to.to1 select").selectpicker("refresh");
            }
            sib.removeClass('hidden');
            /*sib.find('select').prop('disabled',false);*/
            propSelect(sib.find('select'),true,false);
            sib.find('select').selectpicker('refresh');
            $this.parent().addClass('hidden');
        }  
    });

    $('.edit-setup-time .choose-remove-hour').click(function () {
        var $this = $(this);
        $this.closest('.row').siblings().find('.choose-add-hour').parent().removeClass('hidden');
        $this.closest('.row').addClass('hidden');
        /*$this.closest('.row').find('select').prop('disabled',false);*/
        propSelect($this.closest('.row').find('select'),false,true);
    });
    
    $('.btn-edit-submit').click(function () {
        $('.edit-setup-time').valid();
    });

    $('.setup-select-from select').change(function () {
        var $this = $(this);
        var html = generateOptionTime.htmlTime59(parseInt($this.val()),'to');
        var parent = $this.closest('.setup-select-from');
        var select = parent.siblings('.setup-select-to').find('select');
        select.html(html);
        select.selectpicker('refresh');
    });

    $('.setup-select-to select').change(function () {
        var $this = $(this);

        if($this.closest('.setup-select-to').hasClass('to')){
            var html = generateOptionTime.htmlTime(parseInt($this.val())+1,'from');
            var setup_time_item =$this.closest('.setup-time-item').find('.from1').find('select');
            setup_time_item.html(html);
            setup_time_item.selectpicker('refresh');
            if($this.val()==23){
                $this.closest('.setup-time-item').find(".add-setup-time").addClass("hidden");
                $this.closest('.setup-time-first').find(".btn-setupTime--thumb").addClass("hidden");
            }else{
                 // $this.closest('.setup-time-item').find(".add-setup-time").removeClass("hidden");
                $this.closest('.setup-time-first').find(".btn-setupTime--thumb").removeClass("hidden");
            }
            var abc =$this.closest('.setup-time-item').find(".add-setup-time").find(".to1").find("select");
            if($this.val()>abc.val()){
                abc.html("<option value='' hidden>To</option>");
                abc.selectpicker('refresh');
            }
        }
    });

    $("#range").ionRangeSlider({
        type: "double",
        min: 0,
        max: 100,
        from: 0,
        to: 100,
        keyboard: true,
        keyboard_step: 1,
        prefix: "$"
    });

    // SHORT TEXT PREVIEW
    $(window).load(function(){
        var maxlengthReview = 300;
        if ($(window).width()<1200) {
            maxlengthReview = 400;
        }
        if ($(window).width()<992) {
            maxlengthReview = 250;
        }
        if ($(window).width()<720) {
            maxlengthReview = 180;
        }
        if ($(window).width()<=640) {
            maxlengthReview = 150;
        }
        if ($(window).width()<=504) {
            maxlengthReview = 210;
        }
        if ($(window).width()<=414) {
            maxlengthReview = 90;
        }
        if ($(window).width()<360) {
            maxlengthReview = 80;
        }

        $('.short-content-review').each(function(){
            var str = $(this).text();
            if (str.length > maxlengthReview) {
                var shortened = str.substring(0,maxlengthReview) + ' ...';
                $(this).parent().find('.icon-hide-shortContent').addClass('icon-show-shortContent');
            }
            else{
                var shortened = str;
            }
            $(this).text(shortened);
        });
    });
    

    // SHOW FULL PREVIEW
    $('.show-review i').click(function(){
        $(this).parents().eq(1).hide();
        $(this).parents().eq(2).find('.hidden-review').show();
    });
    $('.hidden-review i').click(function(){
        $(this).parents().eq(1).hide();
        $(this).parents().eq(2).find('.show-review').show();
    });

    // SHORT TEXT BEST TOUR home page
    // $(window).load(function(){
    //     var i = 0;
    //     var mlength1 = 220;
    //     var mlength = 140;
    //     if ($(window).width() < 1200) {
    //         mlength1 = 180;
    //         mlength = 120;
    //     }
    //     if ($(window).width() < 992) {
    //         mlength1 = 125;
    //         mlength = 125;
    //     }
    //     if ($(window).width() < 768) {
    //         mlength1 = 110;
    //         mlength = 110;
    //     }
    //     if ($(window).width() < 570) {
    //         mlength1 = 130;
    //         mlength = 130;
    //     }
    //     if ($(window).width() < 505) {
    //         mlength1 = 130;
    //         mlength = 130;
    //     }
    //     if ($(window).width() < 461) {
    //         mlength1 = 150;
    //         mlength = 150;
    //     }
    //     if ($(window).width() < 330) {
    //         mlength1 = 140;
    //         mlength = 140;
    //     }
        
    //     $('.excerpt-item-best-tour').each(function(){
    //         var str = $(this).text();
    //         if (i==0) {
    //             if (str.length > mlength1) {
    //                 var shortened = str.substring(0,mlength1) + ' [...]';
    //             }
    //             else{
    //                 var shortened = str;
    //             }
    //         }else{
    //             if (str.length > mlength) {
    //                 var shortened = str.substring(0,mlength) + ' [...]';
    //             }
    //             else{
    //                 var shortened = str;
    //             }
    //         }
    //         i++;
    //         $(this).text(shortened);
    //     });
    // });

    // SHORT TITLE BEST NEWS - home page
    $(window).load(function(){
        var mlength = 100;
        if ($(window).width() < 1200) {
            mlength = 50;
        }
        if ($(window).width() < 992) {
            mlength = 60;
        }
        if ($(window).width() < 720) {
            mlength = 50;
        }
        if ($(window).width() < 541) {
            mlength = 100;
        }
        $('.item-title-tour').each(function(){
            var str = $(this).text();
            if (str.length > mlength) {
                var shortened = str.substring(0,mlength) + '...';
            }
            else{
                var shortened = str;
            }
            $(this).text(shortened);
        });
    });

    // SHORT TEXT BEST GUIDE
    $(window).load(function(){
        $('.excerpt-item-profile').each(function(){
            var str = $(this).text();
            if (str.length > 137) {
                var shortened = str.substring(0,137) + ' [...]';
            }
            else{
                var shortened = str;
            }
            $(this).text(shortened);
        });
        // FIXED HEIGHT SLIDE ITEM - slide best guide home page
        var heights = $("#best-guide-slider .slick-slide").map(function() { 
            return $(this).height(); 
        }).get(), maxHeight = Math.max.apply(null, heights); $("#best-guide-slider .slick-slide").height(maxHeight);
    });

    
    // SHORT TEXT RECENT NEWS
    $(window).load(function(){
        $('.title-detailNews--thumb').each(function(){
            var str = $(this).text();
            var mlength = 50;
            if ($(window).width() < 1200) {
                mlength = 40;
            }
            if ($(window).width() < 992) {
                mlength = 100;
            }
            if ($(window).width() < 360) {
                mlength = 70;
            }
            if (str.length > mlength) {
                var shortened = str.substring(0,mlength) + '...';
            }
            else{
                var shortened = str;
            }
            $(this).text(shortened);
        });
    });

    // SHORT TEXT SLOGAN - profile page
    $(window).load(function(){
        var slogan = $('#Status .truncate');
        var lengthSlogan = slogan.text(); 
        maxlenght = 200; 
        if ($(window).width() <= 1024) {
            maxlenght = 150;
        }
        if ($(window).width() <= 991) {
            maxlenght = 130;
        }
        if ($(window).width() <= 640) {
            maxlenght = 90;
        }
        if ($(window).width() <= 479) {
            maxlenght = 90;
        }
        if ($(window).width() <= 360) {
            maxlenght = 60;
        }
        if (lengthSlogan.length > maxlenght) {
            var shortened = lengthSlogan.substring(0,maxlenght) + '...';
        }  
        else {
            var shortened = lengthSlogan;
        }
        slogan.text(shortened);
    });

    // $('.carousel').carousel({
    //     interval: 2000
    // });

    // EFFECT hover popular slide - page news
    $('.btn-Control-popularSlide').mouseenter(function(){
        $(this).parent().find('.carousel-inner').addClass('carousel-active');
    });
    $('.btn-Control-popularSlide').mouseleave(function(){
        $(this).parent().find('.carousel-inner').removeClass('carousel-active');
    });

    // active radio REGISTER PAGE
    $('#watch-me').click(function(){
        if ($('.radio-guide').hasClass('active')) {
            $('.radio-travel').addClass('active');
            $('.radio-guide').removeClass('active');
        }
    });
    $('#see-me').click(function(){
        if ($('.radio-travel').hasClass('active')) {
            $('.radio-guide').addClass('active');
            $('.radio-travel').removeClass('active');
        }
    });

    // GALLERY - EDIT PROFILE GUIDE page
    $('.fileuploader-thumbnails-input-inner').click(function(){
        var length = $('#gallery .fileuploader-thumbnails-input:nth-child(1)').length;
        if (length > 0 ) {
            $('#gallery .btnUpdate--thumb').css({'display':'block'});
            $('.upload-Newgallery .fileuploader-theme-thumbnails .fileuploader-items-list').css({'text-align':'left'});
            $('.upload-Newgallery .fileuploader-theme-thumbnails .fileuploader-thumbnails-input').css({'padding-bottom':'20px','padding-right':'20px'});
        }
    });

    // choose date search guide
    $('input[name="daterange"]').daterangepicker(
    {
        locale: {
          format: 'MM/DD/YYYY'
        },
        startDate: new (Date),
        endDate: new (Date),
        minDate: new Date()
    });

    $('input').keyup(function () {

    });

    // validate input-add-price in edit promotion
    $(".input-add-price:not(.price)").on("input", function(){
        var value = $(this).val();

        $(this).val("");
        $(this).val(parseInt(value));

        return true;
    });
    // validate input-add-price in edit promotion
    $(".input-add-price").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#numberPeople").on("input", function(){
        var value = $(this).val();

        $(this).val("");
        $(this).val(parseInt(value));

        return true;
    });

    $("#numberPeople").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    // slick lightbox gallery in profile
    $('#custom-layout-demo').slickLightbox({
    layouts: {
        closeButton: '<span class="close cursor" onclick="closeModal()">×</span>'
        }
    });

    $('.change-avatar-abs').click(function(){
        $('.cropme').click();
    });

});

function checkValid() {
    var form = document.getElementById('subcribe');
    var str = $('.get-email').val().trim();
    if (str == "" ){
        $('.get-email').val("");
        $(form).valid();
    }
    else{
        if ($(form).valid()){
            form.submit();
        }
    }
}

// validate email
jQuery.validator.addMethod("email", function(value, element)
{
    var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    var result = pattern.test(value);
    return result;
});

// validate input number people
function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}
  
// function isNumeric (evt) {
//     var theEvent = evt || window.event;
//     var key = theEvent.keyCode || theEvent.which;
//     key = String.fromCharCode (key);
//     var regex = /[0-9]|\./;
//     if ( !regex.test(key) ) {
//       theEvent.returnValue = false;
//       if(theEvent.preventDefault) theEvent.preventDefault();
//     }
// }  
function isNumeric (evt) {
    if (navigator.userAgent.search("Firefox") >= 0) {
    } 
    else { 
        var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode (key);
            var regex = /[0-9]|\./;
            if ( !regex.test(key) ) {
              theEvent.returnValue = false;
              if(theEvent.preventDefault) theEvent.preventDefault(); 
        }
    }
}

// ACTIVE INPUT Hourlyrate
    $('.editJobHour--thumb input.form-control').focus(function(){
        $(this).parent().addClass('active');
    });
    $('.editJobHour--thumb input.form-control').blur(function(){
        $(this).parent().removeClass('active');
    });

function propSelect($this,tDis,tHidden) {
    $this.prop('required',tDis);
    $this.prop('disabled',tHidden);
}

// SLIDE SHOW GALLERY
function openModal() {
  document.getElementById('myModalGallery').style.display = "block";
}

function closeModal() {
    var length = $('#myModalGallery').length;
    if (length > 0) { document.getElementById('myModalGallery').style.display = "none"; }
  
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var lengthSlide = $('.mySlides').length;
    if (lengthSlide > 0) {
        var slides = document.getElementsByClassName("mySlides");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slides[slideIndex-1].style.display = "block";
    }
}
