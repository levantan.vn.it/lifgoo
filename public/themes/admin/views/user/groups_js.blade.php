
<script>
    function remove_groups(ids_value) {
        $.ajax({
            type: "POST",
            url: "delete",
            data: {ids: ids_value, _token: '{{csrf_token()}}'},
            dataType: "JSON",
            beforeSend: showLoader
        }).done(function (result) {
            if (result.success == false) {
                // open modal error message
                modalDisplay("{{trans('site.error') }}", 'error', result.message);
                return false;
            }
            // redirect to group list after delete
            sessionStorage[window.location.href] = 0;
            window.location.replace("list");
        }).always(hideLoader)
    }


    function delete_group_popup(ids, message) {
        new Messi(
                message,
                {
                    modal: true,
                    modalOpacity: 0.5,
                    title: '{{trans('site.confirm')}}',
                    titleClass: 'warning',
                    buttons: [
                        {id: 0, label:trans.yes, val: 'Y'},
                        {id: 1, label: trans.no, val: 'N'}
                    ],
                    callback: function (val) {
                        if (val == 'Y') {
                            // sent ajax to remove groups
                            remove_groups(ids);
                        }
                    }
                }
        );
    }

    $(document).ready(function () {
        table = $("#user-list").DataTable({
            ordering: false,
            "language": {
                "paginate": {
                    "previous": trans.previous,
                    "next": trans.next
                },
                "lengthMenu": trans.show + ' ' + '_MENU_' + ' ' + trans.entries,
                "search": trans.search,
                "pagingType": "full_numbers"
            }
        });

        if (sessionStorage[window.location.href] && sessionStorage[window.location.href] !== '0') {
            table.page(parseInt(sessionStorage[window.location.href])).draw(false);
            sessionStorage[window.location.href] = '0';
        }

        $(document).on('click', '.group-delete', function (event) {
            event.preventDefault();
            id = $(this).attr('id');
            delete_group_popup(id, '{{ trans('admin.delete_this_role') }}');
            $(this).blur();
        });

        $("#bulkDelete").on('ifClicked', function () {
            var status = this.checked;
            if (!status) {
                $(".deleteRow").iCheck('check');
            } else {
                $(".deleteRow").iCheck('uncheck');
            }
        });

        $('#delete-checked').click(function (event) {
            event.preventDefault();
            var rows = $('.deleteRow:checked');
            if (rows.length > 0) {
                var val = rows.map(function (_, el) {
                    return $(el).val();
                }).get();
                val = val.toString();
                delete_group_popup(val, '{{ trans('admin.delete_roles') }}');
            }
            else {
                modalDisplay("{{ trans('site.error') }}", 'error', '{{trans('site.no_roles_selected')}}');
            }
            $(this).blur();
        });

        $(document).on('click', '#create-new, .group-view, .group-edit', function (event) {
            var button = this;
            event.preventDefault();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: showLoader.bind(button)
            }).done(function (e) {
                if (e.success === false) {
                    modalDisplay("{{ trans('site.warning') }}", 'warning', e.message);
                    return false;
                }
                $('#pop-up .modal-content').html(e.message);
                $('#pop-up').modal('show');
            }).always(hideLoader.bind(button))
            $(this).blur();
        });

        $(document).on('input', '#display_name', function() {
            // fix bug _ in admin role list
            if (0) {$('#role_name').html(this.value.toSlug());}
            $('#role_name').html(this.value);
        });
    });
</script>