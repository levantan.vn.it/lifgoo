@extends('admin::layouts.master')
@section('css')
    @include('admin::user.groups_css')
@stop
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{trans('admin.users')}}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard" style="margin-right: 5px;"></i>{{trans('admin.homepage')}}</a></li>
                        <li class="active">{{trans('admin.roles')}}</li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{trans('admin.user_roles')}}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(session('action_succeeded'))
                            <div class="callout callout-success">
                                {{session('action_succeeded')}}
                            </div>
                        @endif
                        <?php $canAdd = $admin->can('add_new_group');
                        $canEdit = $admin->can('edit_group');
                        $canDelete = $admin->can('delete_group');
                        $admin_role = $admin->roles()->select('display_name')->first();
                        $adminIsSuperior = $admin_role === 'admin';
                        $name_default = ['admin', 'manager', 'guide', 'user'];
                        ?>

                        <table id="user-list" class="table table-striped table-bordered bulk_action">
                            <thead>
                            <tr>
                                <th width="15px"><input type="checkbox" id="bulkDelete" class="minimal"/></th>
                                <th>{{ trans('site.title') }}</th>
                                <th>{{ trans('site.action') }}</th>
                            </tr>
                            </thead>


                            <tbody>
                            @if(!empty($groups))
                                @foreach($groups as $group)
                                    <tr>
                                        <td>
                                            @if(!in_array($group->name, $name_default) && $canDelete)<input
                                                    class="deleteRow minimal" type="checkbox" name="group_id"
                                                    value="{{$group->id}}"/> @endif
                                        </td>
                                        <td>
                                        <span class="{{(in_array($group->name,['admin']))?'text-red':(($group->name=='manager')?'text-blue':"")}}">
                                            <strong>{{$group->display_name}}</strong>
                                        </span>
                                        </td>
                                        <td>
                                            {{--<a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View
                                            </a>--}}

                                            @if(!in_array($group->name, ['admin']))
                                                <a href="javascript:void(0)"
                                                   class="btn btn-info btn-xs bg-light-blue group-edit"
                                                   data-url="edit/{{$group->id}}"><i
                                                            class="fa fa-pencil"></i> {{ trans('site.edit') }}
                                                </a>
                                            @endif
                                            @if(!in_array($group->name, $name_default) && $canDelete)
                                                <a href="javascript:void(0)"
                                                   class="btn btn-danger btn-xs {{(!in_array($group->name, $name_default)) && $canDelete ? 'group-delete' : '' }}"
                                                   id="{{$group->id}}"><i class="fa fa-trash-o"></i>
                                                    {{ trans('site.delete') }} </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>

                        </table>
                    </div>
                    <div class="x_content">
                        @if($canDelete)
                            <a id="delete-checked"
                               data-url="{{Helper::url('admin/roles/create')}}" href="javascript:void(0)"
                               class="btn btn-danger">{{ trans('admin.delete_roles') }}</a>
                        @endif
                        @if($canAdd)
                            <a href="javascript:void(0)" class="btn btn-success"
                               data-url="{{Helper::url('admin/roles/create')}}"
                               id="create-new">{{ trans('admin.create_new_role') }}</a>
                        @endif
                    </div>

                </div>
            </div>
        </div>
            <div id="pop-up" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                </div>
            </div>
        </div>
        </div>
    </div>
@stop
@section('js')
    @include('admin::user.groups_js')
@stop