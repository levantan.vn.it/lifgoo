<style>
    table.dataTable thead > tr > th {padding-right: 8px;}
    table#user-list {position: relative;}
    button#delete-checked, button#create-new {
        position: absolute;
        bottom: -43px;
        left: 0;
    }
    button#create-new {left: 133px;}
    .dataTables_info {display: none;}
    .group-view, .group-edit, .group-delete {cursor: pointer;}
    .userlist-fullname {float: left; margin: 4px 0 0 6px; font-weight: bold;}
    .input-group {position: relative;}
    label.error {position: absolute!important; z-index: 2; top: 6px; right: 0; font-weight: normal; color: #FF0000;}
    input.error, select.error {border-color: #FF0000;}
    .input-group[class*=col-]{
        float:left;
    }
    .box-body div:nth-child(2).col-md-12{
        margin-top:10px;
    }
</style>