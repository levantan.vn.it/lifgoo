<div class="profile_details">
    <div class="well profile_view">
        <div class="col-sm-12">
            <div class="x_title">
                <h2>{{ trans('admin.guide_details') }}</h2>
                <div class="clearfix"></div>
            </div>
            {{--<h4 class="brief"><i>{{ trans('admin.guide_details') }}</i></h4>--}}
            <div class="left col-xs-7">
                <h2 style="word-break: break-all;">{{$user->username}}</h2>
                <ul class="nav nav-stacked">
                    <li><a>{{ trans('site.email') }}: <strong>{{$user->email}}</strong></a></li>
                    <li><a>{{ trans('site.full_name') }}: <strong>{{$user->full_name}}</strong></a></li>
                    <li><a>{{ trans('site.phone_number') }}: <strong>{{$user->phone_number}}</strong></a></li>
                    <li><a>{{ trans('site.address') }}: <strong class="line-break">{{$user->address}}</strong></a>
                    </li>
                    <li><a>{{ trans('site.hobby') }}: <strong>{{$user->hobby}}</strong></a></li>
                    <li><a>{{ trans('site.group') }}: <strong>{{$role->display_name}}</strong></a></li>
                </ul>
            </div>
            <div class="right col-xs-5 text-center">
                <img src="{{Helper::getThemeAvatar($user->avatar?$user->avatar:'user.png')}}" alt=""
                     class="img-circle img-responsive">
            </div>
        </div>

    </div>
</div>

