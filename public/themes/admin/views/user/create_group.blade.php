<link rel="stylesheet" href="{{Helper::getThemePlugins('simplecrop/css/style.css', 'admin')}}"/>
<style type="text/css">
    .clear {
        clear: both;
    }

    .input-group label.error {
        top: -23px;
    }
</style>

<div class="nav-tabs-custom">
    <div class="page-title title-modal">
        {{--<ul class="nav nav-tabs pull-right">
            <li class="pull-left header text-blue"><i
                        class="fa fa-asterisk"></i> {{empty($group) ? trans('admin.create_new_role') : trans('admin.update_user_role')}}
            </li>
        </ul>--}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h3><i class="fa fa-key"></i>{{empty($group) ? trans('admin.create_new_role') : trans('admin.update_user_role')}}</h3>
    </div>
    <div class="tab-content">
        <div class="tab-pane active" id="normal-user">
            {!! Form::open(['url' => empty($group) ? Helper::url('admin/roles/create') : Helper::url('admin/roles/edit'), 'id' => 'create-group-form']) !!}
            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="display_name">{{$display_name}}<i class="text-red">*</i></label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </div>
                            <?php $name_arr = ['admin', 'manager', 'guide', 'user'] ?>
                            <input id="display_name" value="{{$group ? $group->display_name : '' }}"  type="text" class="form-control" name="display_name"
                                   @if($group && in_array($group->name,$name_arr)) readonly @endif
                                   required/>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="name">{{$title}}:</label>
                        <span id="role_name">{{$group ? $group->name : '' }}</span>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h3>{{trans('admin.permissions')}}</h3>
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                @foreach($permissions as $perm)
                                    <div class="input-group col-xs-12 col-sm-6">
                                        <label>
                                            <input type="checkbox" class="flat minimal"
                                                   name="permission[{{$perm->name}}]"
                                                   {{!empty($group) && !empty($group->perms()->select('id')->whereName($perm->name)->first())?'checked' : ''}}
                                                   value="{{$perm->id}}"/>
                                            {{trans('permissions.'.$perm->name.'')}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="col-md-12 text-center">
                    @if(!empty($group))
                        <input type="hidden" value="{{$group->id}}" name="id"/>
                    @endif
                    <button type="submit" id="submit-form"
                            class="btn btn-success">{{$group ? trans('admin.update_role') : trans('admin.add_role')}}</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

        $('#create-group-form').validate({
            ignore: [],
            rules: {
//                display_name : {
//                    required: {
//                        depends: function () {
//                            this.value = this.value.trim();
//                            return true;
//                        }
//                    },
//                    maxlength: 30
//                }
//                "permission[]": {
//                    required: function (element) {
//                        var boxes = $('.checkbox');
//                        return boxes.filter(':checked').length == 0;
//                    },
//                    minlength: 1
//                }
            },
            messages: {
//                "permission[]": "Select at least one permission"
            },
            submitHandler: function (form) {
                var button = $('#submit-form');
                if (!button.hasClass('disabled')) {
                    button.addClass('disabled');
                    var submit_url = $('#create-group-form').attr('action');
                    $.ajax({
                        type: "POST",
                        url: submit_url,
                        data: $(form).serialize(),
                        dataType: "JSON",
                        success: function (result) {
                            if (result.success === false) {
                                modalDisplay("{{ trans('site.warning') }}", 'warning', result.message);
                            }
                            else {
                                sessionStorage[window.location.href] = table.page.info().page;
                                window.location.href = 'list';
                            }
                        }
                    }).fail(function (e) {
                        $('label.error').remove();
                        var response = e.responseJSON;
                        $('.form-error-message').hide();
                        if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                        Object.keys(response).forEach(function (i) {
                            var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                            $('#' + i).after(label);
                        });
                    }).always(function () {
                        button.removeClass('disabled');
                    });
                }
            }
        });
        $('input')
    });
</script>