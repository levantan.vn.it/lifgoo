<style>
    .form-group.form-error-message.clearfix {
        color: red;
        text-align: center;
    }

    .modal-dialog {
        width: 65%;
    }
    .cropme img{
        width: 100%;
    }
</style>
<link rel="stylesheet" href="{{Helper::getThemePlugins('simplecrop/css/style.css', 'admin')}}"/>
<div class="popup-model clearfix">
    <div class="x_panel">
        <div class="x_title">
            <h2>{{!$user? trans('admin.add_new_user'): trans('admin.edit_user')}} </h2>
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="col-md-6 col-sm-6" style="margin: 0 auto;float: none;text-align: center;">
                <div class="cropme" style="width: 270px; height: 270px;">@if($user && $user->avatar) <img
                            src="{{Helper::getThemeAvatar($user->avatar)}}" alt="" data-change="0">@endif</div>
                <span>{!! trans('site.click_upload_avatar') !!}</span>
            </div>
            {!! Form::open(['url' => Helper::url('admin/user/').$user?"edit" : 'new', 'id' => 'regForm','class'=>$user?'edit-user':'add-user', 'enctype'=>'multipart/form-data']) !!}
            <div class="form-group form-error-message clearfix"></div>
            @if($user) <input name="id" type="number" hidden value="{{$user->id}}"> @endif
            @if(!$is_guide)
                @if($user)
                    <?php $user_role = $user->roles()->first()->name;?>
                    @if($user_role !== 'admin')
                        <div class="form-group col-sm-6">
                            <label for="role_id">{{trans('admin.user_role') }}(*)</label>
                            <select id="role_id" class="form-control" name="role_id">
                                @foreach($roles as $role)
                                    @if($role->name !== 'admin' && $role->name !== 'guide')
                                        @if($is_admin)
                                            <option value="{{$role->id}}"
                                                    @if($user_role === $role->name) selected @endif>{{$role->display_name}}</option>
                                        @endif
                                        @if(!$is_admin && $role->name == 'user')
                                                <option value="{{$role->id}}"
                                                        @if($user_role === $role->name) selected @endif>{{$role->display_name}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    @endif
                @else
                    <div class="form-group col-sm-6">
                        <label for="role_id">{{trans('admin.user_role') }}(*)</label>
                        <select id='role_id' class="form-control" name="role_id">
                            @foreach($roles as $role)
                                @if($role->name !== 'admin' && $role->name !== 'guide' && $role->name !== 'user')
                                    <option value="{{$role->id}}"
                                            @if($role->name == 'user')selected @endif>{{$role->display_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                @endif
            @endif
            <div class="form-group col-sm-6">
                <label for="username">{{ trans('site.username') }} (*)</label>
                <input id="username" type="text" name="username" class="form-control"
                       @if($user && $user->username) readonly value="{{$user->username}}" @endif>
            </div>
            <div class="form-group col-sm-6">
                <label for="email">{{ trans('site.email') }} (*)</label>
                <input id="email" type="text" name="email" class="form-control"
                       @if($user && $user->email) readonly value="{{$user->email}}" @endif>
            </div>
            <div class="form-group col-sm-6">
                <label for="full_name">{{ trans('site.full_name') }}(*)</label>
                <input id="full_name" type="text" name="full_name" class="form-control"
                       @if($user) value="{{$user->full_name}}" @endif>
            </div>
            <div class="form-group col-sm-6">
                <label for="phone_number">{{ trans('site.phone_number') }} *</label>
                <input type="text" name="phone_number" class="form-control"
                       @if($user) value="{{$user->phone_number}}" @endif maxlength="15">
            </div>
            <div class="form-group col-sm-6 {{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address">{{ trans('site.address') }} (*)</label>
                <input id="address" type="text" name="address" class="form-control"
                       @if($user) value="{{$user->address}}" @endif>
                @if ($errors->has('address'))
                    <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                @endif
            </div>
            @if($user)
                @if($admin->id == $user->id)
                    <div class="form-group col-sm-6">
                        <label for="old_password">{{trans('site.old_password') }} (*)</label>
                        <input type="password" name="old_password" class="form-control" id="old_password"
                               placeholder="********">
                    </div>
                @endif
            @endif
            @if(!$user)
                <div class="form-group col-sm-6">
                    <label for="password">{{trans('site.password') }} (*)</label>
                    <input type="password" name="password" class="form-control" id="password"
                           placeholder="********">
                </div>
                <div class="form-group col-sm-6">
                    <label for="password_confirmation">{{trans('site.password_confirmation') }} (*)</label>
                    <input type="password" name="password-confirmation" class="form-control" id="password_confirmation"
                           placeholder="********">
                </div>
            @endif
            <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                <label for="birth_day">{{trans('admin.birth_date') }}</label>
                <input type="text" class="form-control has-feedback-left datefilter" name="birth_date" id="birth_date"
                       @if($user)value="{{$user->birth_date}}" @endif/>
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
            </div>
            <?php $traveller = false; if ($user) $traveller = $user->traveller()->first() ?>
            <div class="form-group col-sm-12">
                <label for="slogan">{{ trans('site.slogan') }}</label>
                <textarea id="slogan" name="slogan"
                          class="form-control">@if($user){{$user->slogan}}@endif</textarea>
            </div>
            <div class="form-group col-sm-12">
                <label for="hobby">{{ trans('site.hobby') }}</label>
                <textarea id="hobby" name="hobby" class="form-control">@if($user){{$user->hobby}}@endif</textarea>
            </div>

            <div class="col-sm-3 col-xs-6 pull-right">
                <div class="pad-top"></div>
                <button id="submit-button" type="submit"
                        class="btn btn-primary btn-block btn-flat">{{ trans('site.submit') }}</button>
                <div class="pad-bottom"></div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var button = $('#submit-button'); 

        $('.cropme').simpleCropper();
        var firstOpen = true;
        $('input.datefilter').daterangepicker({
            singleDatePicker: true,
            showDropdowns: false,
            locale: {
                format: 'YYYY/MM/DD'
            },
            maxDate: new Date()
        });
                @if($user)
        var url = 'edit';
                @else
        var url = 'new';
        @endif
        $('#regForm').validate({
            onkeyup: false,
            ignore: [],
            rules: {
                phone_number: {
                    required: true,
                    digits: true
                },
                birth_date: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 10
                },
                address: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 200
                },
                full_name: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 100
                },
                username: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 100
                },
                email: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 100,
                    email: true
                },
                slogan: {
                    maxlength: 200
                },
                hobby: {},
                password: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 50,
                    minlength: 6
                },
                'password-confirmation': {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    minlength: 6,
                    equalTo: "#password"
                },
                role_id: {
                    required: true
                }

            },
            messages: {
                regex: 'Please enter a valid this field.'
            }
            , submitHandler: function (form, event) {
                button.blur();
                var type = $('#regForm').attr('class');
                event.preventDefault();
                var fd = new FormData(form);
                if (type == 'add-user' && (message = checkEmptyImage())){
                    return modalDisplay("{{ trans('site.warning') }}", 'warning', message);
                    button.blur();

                }
                if (!checkEmptyImage()) {
                    var imageData = $('.cropme').find('img').attr('src');
                    var file = dataURItoBlob(imageData);
                    fd.append('img', file);
                    button.blur();
                    
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: fd,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: showLoader.bind(button)
                }).done(function (str) {
                    $('label.error').remove();
                    if (str.success) {
                        $('.form-error-message').hide();
                        $('#pop-up').modal('hide');
                        if (window.location.href) {
                            sessionStorage[window.location.href] = table.page.info().page;
                        }
                        window.location.href = 'list';
                    } else {
                        $('.form-error-message').html(str.message).show();

                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) {
                        return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                        button.blur();

                    }
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                })

            }
        });

        function checkEmptyImage() {
            var cropme = $('.cropme');
            if (!cropme.find('img').length){
                return '{{trans('site.please_choose_image')}}';
                button.blur(); 
            }
            if (cropme.find('img').data('change') == 0){
                return '{{trans('site.no_image_change')}}';
                button.blur();
            }
            return false;

        }

        function dataURItoBlob(dataURI) {
            // convert base64/URLEncoded data component to raw binary data held in a string
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(dataURI.split(',')[1]);
            else
                byteString = decodeURI(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to a typed array
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {type: mimeString});

        }

        $(document).on('click', '.close', function () {
            location.reload();
        });
    });
</script>