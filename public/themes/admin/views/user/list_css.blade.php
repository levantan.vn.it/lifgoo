<link rel="stylesheet" href="{{Helper::getThemePlugins('jvectormap/jquery-jvectormap-1.2.2.css')}}"/>
<style type="text/css">
	.description-header{
		font-size: 24px !important;
	}
	.description-text{
		font-size: 18px;
	}
	.pad canvas{
		width: 45px !important;
	}
	.user-view, .user-edit, .user-delete{
		cursor: pointer;
	}
    .row {
        margin:auto;
    }
    table.dataTable thead > tr > th {padding-right: 8px;}
	table#user-list {position: relative;}
	button#delete-checked, button#create-new {
		position: absolute;
		bottom: -43px;
		left: 0;
	}
	table.table-bordered th:last-child{
		text-align: center;
	}
	button#create-new {left: 120px;}
	.dataTables_info {display: none;}
	.group-view, .group-edit, .group-delete,.can_toggle {cursor: pointer;}
	.userlist-fullname {float: left; margin: 4px 0 0 6px; font-weight: bold;}
	.input-group {position: relative;}
	label.error {position: absolute; top: 58px; right: 8px; font-weight: normal; color: #FF0000;}
	input.error, select.error {border-color: #FF0000;}
</style>