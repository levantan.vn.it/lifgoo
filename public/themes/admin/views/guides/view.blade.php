<style type="text/css">
    .modal-dialog {
        width: 85%;
    }

    .x_panel {
        border: none;
    }
</style>
<?php
$avatar = $user->avatar ? Helper::getThemeAvatar($user->avatar) : Helper::getThemeImg('user.png');
$lackInfo = $user->lackOfInformation($user);
$isMissing = $lackInfo['missing_profile'] || $lackInfo['missing_jobInfo'];

$count_propfile = count(Helper::arrayCheckMissing()['profile_user']) + count(Helper::arrayCheckMissing()['profile_info']);
$count_jobInf = count(Helper::arrayCheckMissing()['jobInfo_certification']) + count(Helper::arrayCheckMissing()['jobInfo_info']) + count(Helper::arrayCheckMissing()['jobInfo_location']) + count(Helper::arrayCheckMissing()['jobInfo_language']);
$percen_profile = (100 - (!$lackInfo['missing_profile'] ? 0 : count($lackInfo['profile']) / $count_propfile) * 100);

$percen_jobInfo = (100 - (!$lackInfo['missing_jobInfo'] ? 0 : count($lackInfo['jobInfo']) / $count_jobInf) * 100);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Details information</h4>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                    <div class="profile_img">
                        <div id="crop-avatar" style="width: 100%;">
                            <!-- Current avatar -->
                            <img class="img-responsive avatar-view" src="{{$avatar}}" alt="Avatar"
                                 title="{{$user->full_name}}" style="width: 100%;">
                        </div>
                    </div>
                    <div class="rating-star rating-star--thumb">
                        @for($i=1; $i<= $guide->star; $i++)
                            <i class="fa fa-star yellow" aria-hidden="true"></i>
                        @endfor
                        @for($i=4; $i >= $guide->star; $i--)
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        @endfor
                    </div>
                    <h3>{{$user->full_name}}</h3>

                    <ul class="list-unstyled user_data">
                        @if($user->email)
                            <li><i class="fa fa-envelope user-profile-icon"></i>
                                {{$user->email}}
                            </li>@endif
                        @if($user->username)
                            <li><i class="fa fa-user user-profile-icon"></i>
                                {{$user->username}}
                            </li>@endif
                        @if($user->birth_date)
                            <li><i class="fa fa-birthday-cake user-profile-icon"></i>
                                {{$user->birth_date}}
                            </li>
                        @endif
                        @if($user->gender)
                            <li style="text-transform: capitalize;"><i class="fa fa-transgender user-profile-icon"></i>
                                {{$user->gender}}
                            </li>@endif
                        @if($user->address)
                            <li class="adress-admininfo"><i class="fa fa-map-marker user-profile-icon"></i>
                                {{$user->address}}
                            </li>
                        @endif
                        @if($user->phone_number)
                            <li>
                                <i class="fa fa-mobile-phone user-profile-icon"></i> {{$user->phone_number}}
                            </li>
                        @endif
                        @if($user->status)
                            <li style="text-transform: capitalize;">
                                <i class="fa fa-paw user-profile-icon"></i> status: {{$user->status}}
                            </li>
                        @endif
                    </ul>
                    <br/>

                    <!-- start skills -->
                    <h4>Lack of information</h4>
                    <ul class="list-unstyled user_data">
                        <li>
                            <p>Profile</p>
                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar"
                                     data-transitiongoal="{{$percen_profile}}"></div>
                            </div>
                            <small>{{round($percen_profile,2)}}% Complete</small>
                        </li>
                        <li>
                            <p>Job information</p>
                            <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar"
                                     data-transitiongoal="{{$percen_jobInfo}}"></div>
                            </div>
                            <small>{{round($percen_jobInfo,2)}}% Complete</small>
                        </li>
                    </ul>
                    <!-- end of skills -->

                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <!-- start of user-activity-graph -->
                    <div class="content-profile">
                        
                        <!--SLOGAN-->
                        <div class="about-me">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">Slogan</div>
                                <div class="content-aboutme text-justify" style="margin-bottom: 0;">
                                    @if(isset($guide) && $guide->introduce)
                                        {{$user->slogan?$user->slogan:'no slogan'}}
                                    @else
                                        No slogan
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--ABOUT ME-->
                        <div class="about-me">
                            <div class="border-profile-item">
                                <!-- <div class="title-aboutme color2 font-size1 title-aboutme-pad">About me</div> -->
                                <div class="title-aboutme color2 font-size1">About me</div>
                                <div class="content-aboutme text-justify" style="margin-bottom: 0;">
                                    @if(isset($guide) && $guide->introduce)
                                        {{$guide->introduce}}
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--MY SERVICES-->
                        <div class="my-services">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">My services</div>
                                <!--Plane-->
                                <div class="content-profile-item">
                                    @if(isset($guideServices) && count($guideServices))
                                        @foreach($guideServices as $service)
                                            <?php
                                            $serviceJoin = $service->service()->first();
                                            ?>
                                            <div class="col-md-4 item-services">
                                                <i class="{{$serviceJoin->icon}}"
                                                   aria-hidden="true"></i><span>@if($serviceJoin && $slang = $serviceJoin->serviceLang()->where('language_code','=','en')->first()){{$slang->service_name}}@endif</span>
                                            </div>
                                        @endforeach
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--TRANSPORTATION-->
                        <div class="transportation">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">Transportation</div>
                                <!--Plane-->
                                <div class="content-profile-item">
                                    @if(isset($guidetransportations) && count($guidetransportations))
                                        @foreach($guidetransportations as $service)
                                            <?php
                                            $serviceJoin = $service->service()->first();
                                            ?>
                                            <div class="col-md-4 item-services">
                                                <i class="{{$serviceJoin->icon}}"
                                                   aria-hidden="true"></i><span>@if($serviceJoin && $slang = $serviceJoin->serviceLang()->where('language_code','=','en')->first()){{$slang->service_name}}@endif</span>
                                            </div>
                                        @endforeach
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--LOCATION ACTIVE-->
                        <div class="location-active">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">Location active</div>
                                <!--Plane-->
                                <div class="content-profile-item">
                                    @if(isset($guide_locations) && count($guide_locations))
                                        @foreach($guide_locations as $location)
                                            <?php
                                            $city = $location->cities()->first();
                                            $country = $location->countries()->first();
                                            ?>
                                            <div class="col-md-4 item-services">
                                                {{$country->country_name}}- {{$city->city_name}}
                                            </div>
                                        @endforeach
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--MY EXPERIENCE-->
                        <div class="my-experience">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">My experience</div>
                                <div class="experience-content text-justify">
                                    @if(isset($guide) && $guide->experience)
                                        {!! $guide->experience!!}
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--MY Diploma-->
                        <div class="my-experience">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">My diploma</div>
                                <div class="experience-content text-justify">
                                    @if(isset($guide) && $guide->diploma_info)
                                        {!! $guide->diploma_info!!}
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--MY HOBBY-->
                    <div class="my-experience">
                        <div class="border-profile-item">
                            <div class="title-aboutme color2 font-size1">My hobby</div>
                            <div class="experience-content text-justify">
                                @if(isset($user) && $user->hobby)
                                    {!! $user->hobby!!}
                                @else
                                    No available
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--TOUR GUIDE ID CARD-->
                        <div class="tour-guide">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">Guide card information</div>
                                <div class="content-profile-item">
                                    @if(isset($certification) && $certification)
                                        <div class="col-md-12">
                                            ID card: <span style="word-break: break-word;">{{$certification->idcard}}</span>
                                        </div>
                                        <div class="col-md-12">
                                            Expiration date: <span>{{$certification->expiry_date}}</span>
                                        </div>
                                        <div class="col-md-12">
                                            Card type: <span>{{$certification->type}}</span>
                                        </div>
                                        @if($certification->type=="location")
                                        <div class="col-md-12">
                                            At location: <span>{{$certification->location_allowed}}</span>
                                        </div>
                                        @endif
                                        {{--<div class="col-md-12">--}}
                                            {{--Status: <span>{{$certification->status}}</span>--}}
                                        {{--</div>--}}
                                        <?php
                                        if($certification->cert_lang)
                                            $lang_id = explode(",", $certification->cert_lang);
                                        ?>
                                        <div class="col-md-12 lang-serv">
                                            <span>Language:&nbsp;</span>
                                            @if(isset($lang_id) && count($lang_id))
                                            <?php $i = 0; ?>
                                                @foreach($lang_id as $lang_id)
                                                    <?php $lang_name = $certification->nameLanguage($lang_id)->name;?>
                                                    @if($i==0)
                                                        <span class="color333"> {{$lang_name}}</span>
                                                    @else
                                                        <span class="color333">, {{$lang_name}}</span>
                                                    @endif
                                                    <?php $i++; ?>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="col-md-12">
                                            Place of issue: <span>{{$certification->place_of_issue}}</span>
                                        </div>
                                    @else
                                        No available
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--MY LANGUAGE-->

                        <div class="my-language">
                            <div class="border-profile-item">
                                <div class="title-aboutme color2 font-size1">My language</div>
                                @if(isset($knownLanguage) && count($knownLanguage))
                                    @foreach($knownLanguage as $langua)
                                        <div class="col-md-4 thumb-myLanguage">@if($langWorld = $langua->language()->first()){{$langWorld->name}} @endif</div>
                                    @endforeach
                                @else
                                    No available
                                @endif
                            </div>
                        </div>


                        <!--Price-->
                        <div class="price-profile">
                            <div class="title-aboutme color2 font-size1">Price</div>
                            <table id="mytable" class="table table-striped table-bordered">
                                <thead class="thead-edit-price">
                                <tr>
                                    <th>Number of passengers(person)</th>
                                    <th>Time (h)</th>
                                    <th>Money (usd)</th>
                                </tr>
                                </thead>

                                <tbody class="edit-price">
                                <tr class="no-price">
                                    <td colspan="3">No price</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end of user-activity-graph -->
                </div>
            </div>
            <div class="text-center mtop20">
                @if(!$isMissing && $guide->is_approved == 0)<a href="javascript:void(0)"
                                                               class="btn btn-primary guide-approve"
                                                               data-url="approve/{{$user->id}}">Approve</a>@endif
            </div>
        </div>
    </div>
</div>
@if(isset($discounts))
    @include('admin::guides.schedule_js')
@endif
<script type="text/javascript">
    @if(isset($discounts) && $discounts)
        valuePrice = JSON.parse('<?php echo $discounts;?>');
    if (valuePrice) {
        table.insertIntoTable();
        $('.btn-delete-price').remove();
        $('.btn-edit-price').remove();
        $('.no-price').remove();
    }
    @endif
</script>

