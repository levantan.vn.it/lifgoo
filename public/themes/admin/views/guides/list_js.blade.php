<script type="text/javascript">
    $(document).ready(function () {
        table = $("#user-list").DataTable({
            ordering: true,
            "language": {
                "paginate": {
                    "previous": trans.previous,
                    "next": trans.next
                },
                "lengthMenu": trans.show + ' ' + '_MENU_' + ' ' + trans.entries,
                "search": trans.search,
                "pagingType": "full_numbers"
            }
        });

        if (sessionStorage[window.location.href] && sessionStorage[window.location.href] !== '0') {
            table.page(parseInt(sessionStorage[window.location.href])).draw(false);
            sessionStorage[window.location.href] = '0';
        }



        $('#delete-checked').click(function (event) {
            event.preventDefault();
            if ($('.deleteRow:checked').length > 0) {
                var val = $('.deleteRow:checked').map(function (_, el) {
                    return $(el).val();
                }).get();
                val = val.toString();
                delete_user_popup(val, '{{ trans('admin.delete_user') }}');
            }
            else {
                new Messi('{{trans('admin.no_users_selected')}}',
                        {
                            modal: true,
                            modalOpacity: 0.5,
                            title: "{{ trans('site.error') }}",
                            titleClass: "error"
                        }
                );
            }
        });

        $(document).on('click', '.user-delete', function (event) {
            event.preventDefault();
            id = $(this).attr('id');
            delete_user_popup(id, '{{ trans('admin.block_user') }}');
        });

        $('#create-new').on('click', function (event) {
            event.preventDefault();
            $.ajax({
                url: '{{Helper::url('admin/guide/new')}}',
                data: {type: 'guide'},
                beforeSend: showLoader
            }).done(function (e) {
                if (e.success == false) {
                    modalDisplay("{{ trans('site.warning') }}", 'warning', e.message);
                    return false;
                }
                else {
                    $('#pop-up .modal-content').html(e.message);
                    $('#pop-up').modal('show');
                }
            }).always(hideLoader)

        });

        $(document).on('click', '.user-view,.user-edit', function (event) {
            event.preventDefault();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: showLoader
            }).done(function (e) {
                if (e.success == false) {
                    modalDisplay("{{trans('site.error') }}", 'error', e.message);
                    return false;
                }
                $('#pop-up .modal-content').html(e.message);
                $('#pop-up').modal('show');
//                tinyMce();
            }).always(hideLoader);
        });
        $(document).on('click', '.guide-approve', function (event) {
            event.preventDefault();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: showLoader
            }).done(function (e) {
                if (e.success == false) {
                    modalDisplay("{{trans('site.error') }}", 'error', e.message);
                    return false;
                }
                else {
                    sessionStorage[window.location.href] = '0';
                    window.location.href = 'approval';
                }
            }).always(hideLoader);
        });

        /*$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });*/

        $("#bulkDelete").on('ifClicked', function () {
            var status = this.checked;
            if (!status) {
                $(".deleteRow").iCheck('check');
            } else {
                $(".deleteRow").iCheck('uncheck');
            }
        });
    });

    function delete_user_popup(ids, message) {
        new Messi(message, {
                    modal: true,
                    modalOpacity: 0.5,
                    title: '{{ trans('site.confirm') }}',
                    titleClass: 'warning',
                    buttons: [
                        {id: 0, label: trans.yes, val: 'Y'},
                        {id: 1, label: trans.no, val: 'N'}
                    ],
                    callback: function (val) {
                        if (val == 'Y') {
                            $.ajax({
                                url: "{{Helper::url('admin/user/delete')}}",
                                type: 'POST',
                                dataType: 'JSON',
                                data: {ids: ids, _token: '{{csrf_token()}}'},
                                beforeSend: function () {
                                    $('#loading').show();
                                },
                                success: function (e) {
                                    $('#loading').hide();
                                    if (e.success == false) {
                                        modalDisplay("{{ trans('site.warning') }}", 'warning', e.message);
                                    }
                                    else {
                                        sessionStorage[window.location.href] = '0';
                                        window.location.href = 'list';
                                    }
                                }
                            });
                        }
                    }
                }
        );
    }
</script>
@if($canEdit)
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.can_toggle', function () {
                var status = $(this).text();
                var label = (status == 'pending' || status == 'inactive') ? 'label-warning' : ((status == 'deleted' || status == 'blocked') ? 'label-danger' : 'label-success');
                if (status == 'inactive') {
                    $(this).text('active');
                    $(this).removeClass(label).addClass('label-success');
                }
                else if (status == 'active') {
                    $(this).text('inactive');
                    $(this).removeClass(label).addClass('label-danger');
                }
                else {
                    $(this).text('active');
                    $(this).removeClass(label).addClass('label-success');
                 }
                toggleStatus($(this).attr('data-toggle'));
            });

            function toggleStatus(id) {
                $.ajax({
                    url: "{{Helper::url('admin/user/statustoggle')}}",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {id: id, _token: '{{csrf_token()}}'}
                });
            }
        });
    </script>
@endif