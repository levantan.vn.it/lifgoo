<div class="popup-model clearfix">
    <div class="popup-head">
        <a href="{{ url('admin') }}">{{!$info? trans('admin.add_new_user'): trans('admin.edit_user')}}</a>
    </div>
    <div class="popup-box-body">
        {!! Form::open(['url' => Helpers::url('admin/user/').$info?"edit" : 'new', 'id' => 'regForm','class'=>'add-user', 'enctype'=>'multipart/form-data']) !!}
        <div class="form-group form-error-message clearfix"></div>
        @if($info)<input name="id" type="number" hidden value="{{$info->id}}">@endif
        <input type="number" value="{{$user->id}}" name="user_id" hidden>
        <div class="form-group col-sm-6">
            <label for="guide_card_number">{{ trans('site.guide_card_number') }}</label>
            <input class="form-control" type="text" name="guide_card_number" id="guide_card_number"
                   @if($info)value="{{$info->guide_card_number}}"@endif>
        </div>
        <div class="form-group col-sm-6">
            <label for="expiry_date">{{ trans('site.expiry_date') }}</label>
            <input class="form-control" type="date" name="expiry_date" id="expiry_date"
                   @if($info)value="{{$info->expiry_date}}"@endif>
        </div>
        <div class="form-group col-sm-6">
            <label for="place_of_issue">{{ trans('site.place_of_issue') }}</label>
            <input class="form-control" type="text" name="place_of_issue" id="place_of_issue"
                   @if($info)value="{{$info->place_of_issue}}"@endif>
        </div>
        <div class="form-group col-sm-6">
            <label for="type">{{ trans('site.card_type') }}</label>
            <select name="type" id="type">
                <option value="" disabled hidden>{{ trans('site.select_card_type') }}</option>
                <option value="1" disabled hidden>{{ trans('site.international') }}</option>
                <option value="2" disabled hidden>{{ trans('site.national') }}</option>
            </select>
        </div>
        <div class="form-group col-sm-6">
            <label for="status">{{ trans('site.status') }}</label>
            <input class="form-control" type="text" name="status" id="status"
                   @if($info)value="{{$info->status}}"@endif>
        </div>
        <div class="form-group col-sm-6">
            <label for="cert_lang">{{ trans('site.cert_lang') }}</label>
            <textarea class="form-control" name="cert_lang"
                      id="cert_lang">@if($info){{$info->cert_lang}}@endif</textarea>
        </div>
        <div class="col-sm-3 col-xs-6 pull-right">
            <div class="pad-top"></div>
            <button id="submit-button" type="submit"
                    class="btn btn-primary btn-block btn-flat">{{ trans('site.submit') }}</button>
            <div class="pad-bottom"></div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $(document).ready(function () {
        var button = $('#submit-button');
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
                @if($guide)
        var url = 'edit';
                @else
        var url = 'new';
        @endif
        $('#regForm').validate({
            onkeyup: false,
            ignore: [],
            rules: {
                location: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 255
                },
                hourly_rate: {
                    required: true,
                    digits: true,
                    maxlength: 20
                },
                min_time: {
                    digits: true,
                    maxlength: 11
                },
                max_time: {
                    digits: true,
                    maxlength: 11,
                    min: function () {
                        return $('#min_time').val();
                    }
                },
                capacity: {
                    digits: true,
                    maxlength: 5
                },
                promotional_media: {
                    required: function () {
                        if (this.value) {
                            this.value = this.value.trim();
                        }
                        return false;
                    }
                },
                exprience: {
                    required: function () {
                        if (this.value) {
                            this.value = this.value.trim();
                        }
                        return false;
                    }
                },
                diploma_info: {
                    required: function () {
                        if (this.value) {
                            this.value = this.value.trim();
                        }
                        return false;
                    }
                }
            },
            messages: {}
            , submitHandler: function (form, event) {
                event.preventDefault();
                var data = new FormData(form);
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: showLoader.bind(button)
                }).done(function (str) {
                    $('label.error').remove();
                    if (str.success) {
                        $('.form-error-message').hide();
                        $('#pop-up').modal('hide');
                        if (window.location.href) {
                            sessionStorage[window.location.href] = table.page.info().page;
                        }
                        window.location.href = 'list';
                    } else {
                        $('.form-error-message').html(str.message).show();
                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                })
            }
        });

        $(document).on('change', '#birth_month, #birth_year', function () {
            var month = $('#birth_month').val();
            var year = $('#birth_year').val();
            if (!month || !year) return;
            var day = daysInMonth(+month, +year);
            var options = '<option selected disabled hidden></option>';
            for (var i = 1; i <= day; i++) {
                options += '<option value="' + i + '">' + i + '</option>';
            }
            $('#birth_day').html(options);
        })
    });
</script>