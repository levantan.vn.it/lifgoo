<link rel="stylesheet" href="{{Helper::getThemePlugins('jvectormap/jquery-jvectormap-1.2.2.css')}}"/>
<style type="text/css">
	.description-header{
		font-size: 24px !important;
	}
	.description-text{
		font-size: 18px;
	}
	.pad canvas{
		width: 45px !important;
	}
	.user-view, .user-edit, .user-delete{
		cursor: pointer;
	}
    .row {
        margin:auto;
    }
    table.dataTable thead > tr > th {padding-right: 8px;}
	table#user-list {position: relative;}
	button#delete-checked, button#create-new {
		position: absolute;
		bottom: -43px;
		left: 0;
	}
	table.table-bordered th:last-child{
		text-align: center;
	}
	button#create-new {left: 120px;}
	.dataTables_info {display: none;}
	.group-view, .group-edit, .group-delete,.can_toggle {cursor: pointer;}
	.userlist-fullname {float: left; margin: 4px 0 0 6px; font-weight: bold;}
	.input-group {position: relative;}
	label.error {position: absolute; bottom: -27px; right: 15px; font-weight: normal; color: #FF0000;}
	input.error, select.error {border-color: #FF0000;}

    .frm_edit {
        margin-bottom: 20px;
    }
    /*.tabs-left-edit-guide ul.tabs-left {
        padding-top: 0px;
    }

    .title-edit-guide {
        font-size: 30px;
        text-align: center;
        padding: 30px 0px;
        font-weight: bold;
        border-bottom: 1px solid #d3d3d3;
    }

    .tabs-left-edit-guide ul.tabs-left li {
        margin-bottom: 0px;
    }

    !*.tabs-left-edit-guide ul.tabs-left li.active a:hover{border-bottom: 0px;}*!
    .tabs-left-edit-guide ul.tabs-left li a {
        border: 1px solid #d3d3d3;
        border-bottom: 0px;
        border-radius: 0px;
        font-size: 18px;
        padding: 17px 20px;
    }

    .tabs-left-edit-guide ul.tabs-left li:first-child a {
        border-top: 0px;
    }

    .tabs-left-edit-guide ul.tabs-left li.active a {
        border-left-color: #cc6a35;
        background-color: #e9e9e9;
        color: #747474 !important;
    }

    !*#cropme{float: initial;}*!

    .edit-guide label {
        font-size: 14px;
    }

    .edit-guide input.form-control, .edit-guide select.form-control {
        border-radius: 0px;
        border-color: #b7b7b7;
        height: 38px;
    }

    .edit-guide textarea.form-control {
        border-radius: 0px;
        border-color: #b7b7b7;
    }

    .form-control:focus {
        outline: none;
        box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0.075);
    }

    .edit-guide label {
        margin-left: 10px;
    }



    .btn_update {
        background-color: #6b3c61;
        color: #fff;
        border-radius: 2px;
        padding: 9px 41px;
        float: right;
    }

    .icon_add_remove i {
        color: #b7b7b7;
        cursor: pointer;
    }

    .name-history-booking {
        text-decoration: underline;
    }

    .history-booking {
        word-spacing: 5px;
        border-bottom: 1px solid #d3d3d3;
        padding: 18px 0px;
        cursor: pointer;
    }

    #booking.edit-guide {
        padding: 0px;
    }

    .history-booking:last-child {
        border-bottom: 0px;
    }

    .filter-history {
        position: absolute;
        right: 15px;
        top: -65px;
    }

    .filter-history select {
        border-top-left-radius: 4px !important;
        border-top-right-radius: 4px !important;
    }

    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus {
        border-right-color: #d3d3d3;
        border-bottom: 1px solid #d3d3d3;
    }

    .tabs-left-edit-guide ul.tabs-left li:last-child a {
        border-bottom: 1px solid #d3d3d3;
    }

    .frm-check span {
        padding-left: 10px;
    }

    .frm-check {
        margin-bottom: 10px;
    }

    .edit-service, .edit-transportion {
        display: table;
        margin-left: 10%;
    }

    .frm-check input {
        float: left;
    }

    .frm-check span {
        display: table;
    }

    #form-add-local #frm-add .icon_add_remove #click-add {
        display: none;
    }

    #form-add-local #frm-add:last-child .icon_add_remove #click-remove {
        display: none;
    }

    #form-add-local #frm-add:last-child .icon_add_remove #click-add {
        display: block !important;
    }

    #preview .buttons .cancel, #preview .buttons .ok {
        border: 0px;
    }

    .edit-price tr input {
        text-align: center;
        border: 0px;
        width: 100%;
        background: transparent;
    }

    .edit-price td, .thead-edit-price tr th {
        text-align: center;
    }*/
</style>