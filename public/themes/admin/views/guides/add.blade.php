<link rel="stylesheet" href="{{Helper::getThemePlugins('simplecrop/css/style.css', 'admin')}}"/>
<div class="popup-model clearfix">
    {{--<div class="popup-head">
        <a href="{{ url('admin') }}">{{!$guide? trans('admin.add_new_user'): trans('admin.edit_user')}}</a>
    </div>--}}
    {{--<div class="popup-box-body">--}}
    <?php
    !empty($user) && $guide = $user->guide()->first();
    !empty($user) && $certification = $user->certification()->first();
    !empty($guide) && $location = $guide->locationTour()->first();
    !empty($guide) && $guideServices = $guide->services()->whereType('service')->get();
    !empty($guide) && $guidetransportations = $guide->services()->whereType('transportation')->get();
    !empty($location) && $country = $location->countries()->first();
    !empty($country) && $cities = $country->cities()->orderBy('city_name', 'ASC')->get();

    ?>
    <div class="x_panel">
        <div class="x_title">
            <h2>{{!$user? trans('admin.add_new_user'): trans('admin.edit_user')}} </h2>
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <div class="content-site">
                <div class="bg-editguide">
                    <div class="container">
                        <div class="title-edit-guide fnt-weight font-size-30">Edit Guide</div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Tab panes -->
                                <div class="tab-content responsive">
                                    {{--EDIT PROFILE--}}
                                    <div class="tab-pane active edit-guide" id="edit-profile">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6"
                                                 style="margin: 0 auto;float: none;text-align: center;">
                                                <div class="cropme"
                                                     style="width: 270px; height: 270px;">@if($user && $user->avatar)
                                                        <img
                                                                src="{{Helper::getThemeAvatar($user->avatar)}}" alt=""
                                                                data-change="0">@endif</div>
                                                <span>{!! trans('site.click_upload_thumbnail') !!}</span>
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::open(['url' => Helper::url('admin/guide/').($user?"/editProfile" : '/newProfile'), 'id' => 'regProfile','class'=>'add-user', 'enctype'=>'multipart/form-data']) !!}
                                               @if($user) <input type="hidden" value="{{$user->id}}" name="id"> @endif
                                                <div class="col-md-12">
                                                    <h3>Profile</h3>
                                                </div>
                                                {{--email--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="email">{{trans('site.email')}} *</label>
                                                    <input type="email" id="email" name="email"
                                                           class="form-control"
                                                           placeholder="Email" required maxlength="255"
                                                           @if($user) value="{{$user->email}}" readonly @endif>
                                                </div>
                                                {{--username--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="username">{{trans('site.username')}} *</label>
                                                    <input type="text" id="username" name="username"
                                                           class="form-control"
                                                           placeholder="Username" required maxlength="255"
                                                           @if($user) value="{{$user->username}}" readonly @endif>
                                                </div>
                                                {{--full_name--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="full_name">{{trans('site.full_name')}} *</label>
                                                    <input type="text" id="full_name" name="full_name"
                                                           class="form-control"
                                                           placeholder="Full name" required maxlength="100"
                                                           @if($user) value="{{$user->full_name}}"@endif>
                                                </div>
                                                {{--Passport number/ ID number--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="passport_id_number">{{trans('site.passport_number')}}
                                                        /{{trans('site.id_number')}} *</label>
                                                    <input type="text" id="passport_id_number" class="form-control"
                                                           placeholder="Passport number/ ID number"
                                                           name="passport_id_number" required maxlength="20"
                                                           @if(!empty($guide)) value="{{$guide->idnumber}}"@endif>
                                                </div>
                                                @if(!$user)
                                                    {{--password--}}
                                                    <div class="col-md-6 frm_edit">
                                                        <label class="color10 fnt-weight"
                                                               for="password">{{trans('site.password')}} *</label>
                                                        <input type="password" id="password" name="password"
                                                               class="form-control"
                                                               placeholder="Password" required maxlength="100">
                                                    </div>
                                                    {{--password confirm--}}
                                                    <div class="col-md-6 frm_edit">
                                                        <label class="color10 fnt-weight"
                                                               for="password_confirm">{{trans('site.password_confirm')}}
                                                            *</label>
                                                        <input type="password" id="password_confirm"
                                                               name="password_confirm"
                                                               class="form-control"
                                                               placeholder="Password confirm" maxlength="100">
                                                    </div>
                                                @endif
                                                {{--Phone number--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="phone_number">{{trans('site.phone_number')}} *</label>
                                                    <input type="text" id="phone_number" name="phone_number"
                                                           class="form-control"
                                                           placeholder="Phone number" required
                                                           @if(!empty($user)) value="{{$user->phone_number}}"@endif>
                                                </div>
                                                {{--Gender--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="gender">{{trans('site.gender')}} *</label>
                                                    <select id="gender" name="gender" class="selectpicker form-control"
                                                            data-style="btn-primary" required>
                                                        <option value="male"
                                                                @if(!empty($user) && $user->gender == 'male') selected @endif>{{trans('site.male')}}</option>
                                                        <option value="female"
                                                                @if(!empty($user) && $user->gender == 'female') selected @endif>{{trans('site.female')}}</option>
                                                    </select>
                                                </div>

                                                {{--Birthday--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="birthday">{{trans('site.birthday')}} *</label>
                                                    <input type="text" class="form-control datefilter"
                                                           id="birth_date" name="birth_date"
                                                           @if($user)value="{{$user->birth_date}}" @endif/>
                                                    <span id="inputSuccess2Status3" class="sr-only"
                                                          required>(success)</span>
                                                </div>
                                                {{--Address--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="address">{{trans('site.address')}} *</label>
                                                    <input type="text" id="address" class="form-control"
                                                           placeholder="Address" name="address" required
                                                           maxlength="200"
                                                           @if(!empty($user)) value="{{$user->address}}"@endif>
                                                </div>

                                                {{--Slogan--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="slogan">{{trans('site.slogan')}} *</label>
                                                    <input type="text" id="slogan" class="form-control"
                                                           placeholder="Slogan" name="slogan" required
                                                           @if(!empty($guide)) value="{{$guide->slogan}}"@endif>
                                                </div>
                                                {{--Slogan--}}
                                                <div class="col-md-6 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="hourly_rate">{{trans('site.hourly_rate')}} *</label>
                                                    <input type="number" id="hourly_rate" class="form-control"
                                                           placeholder="Hourly rate &#36;" name="hourly_rate" required
                                                           @if(!empty($guide)) value="{{$guide->hourly_rate}}"@endif>
                                                </div>
                                                {{--Hobby--}}
                                                <div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="hobby">{{trans('site.hobby')}} *</label>
                                                    <textarea name="hobby" class="form-control" id="hobby" required>@if(!empty($user))
                                                            {{$user->hobby}}@endif</textarea>
                                                </div>

                                                {{--About me--}}
                                                <div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="introduce">{{trans('site.about_me')}} *</label>
                                                    <textarea name="introduce" class="form-control editor-wrapper"
                                                              id="introduce"
                                                              rows="6" required>@if(!empty($guide))
                                                            {{$guide->introduce}}@endif</textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <h3>Certification</h3>
                                                </div>
                                                @if(!$user)
                                                {{--Local activities--}}
                                                <div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="old_password">{{trans('site.local_activities')}}
                                                        <span class="color1">*</span></label>
                                                </div>
                                                <div id="form-add-local" class="col-md-10 col-lg-offset-1">
                                                    <div id="frm-add">
                                                        <div class="col-md-5 frm_edit">
                                                            <select class="form-control select-country"
                                                                    name="country_id"
                                                                    data-url="{{Helper::url('admin/cities')}}" required>
                                                                <option value=""
                                                                        @if(!$user)selected @endif>{{trans('site.please_select_country')}}
                                                                    *
                                                                </option>
                                                                @if($countries)
                                                                    @foreach($countries as $country)
                                                                        <option class="option"
                                                                                value="{{$country->country_id}}"
                                                                                @if(!empty($location) && $location->country_id ==$country->country_id )selected @endif>{{$country->country_name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>

                                                        </div>

                                                        <div class="col-md-5 frm_edit">
                                                            <select class="form-control select-city" name="city_id">
                                                                <option value=""
                                                                        @if(!$user)selected @endif>{{trans('site.please_select_city')}}</option>

                                                                @if(!empty($location) && !empty($city))
                                                                    @foreach($cities as $city)
                                                                        <option value="{{$city->city_id}}"
                                                                                @if($location->city_id ==$city->city_id)selected @endif>
                                                                            {{$city->city_name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif

                                                {{--Language--}}
                                                {{--<div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight" for="old_password">Language<span
                                                                class="color1">*</span></label>
                                                </div>

                                                <div class="col-md-6 frm_edit">
                                                    <select id="language" class="selectpicker form-control"
                                                            data-style="btn-primary" name="language_code" required>
                                                        @if($languages)
                                                            @foreach($languages as $language)
                                                                <option value="{{$language->code}}">
                                                                    {{$language->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>--}}

                                                {{--Guide Card Info--}}
                                                <div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight" for="old_password">Guide Card
                                                        Info<span class="color1">*</span></label>
                                                </div>

                                                <div class="col-md-10 col-lg-offset-1">
                                                    <div class="col-md-6 frm_edit">
                                                        <input type="text" id="id-card" class="form-control"
                                                               placeholder="ID Card" name="idcard" required
                                                               @if(!empty($certification)) value="{{$certification->idcard}}"@endif>
                                                    </div>
                                                    <div class="col-md-6 frm_edit">
                                                        {{--<input type="date" id="date-card" class="form-control"
                                                               placeholder="Date Card *" name="expiry_date" required>--}}
                                                        <input type="text" class="form-control valid"
                                                               id="expiry_date" name="expiry_date" aria-invalid="false"
                                                               @if(!empty($certification)) value="{{$certification->expiry_date}}"@endif>
                                                    </div>
                                                    <div class="col-md-6 frm_edit">
                                                        <input type="text" id="location-card" class="form-control"
                                                               placeholder="Place of issue *" name="place_of_issue"
                                                               required
                                                               @if(!empty($certification)) value="{{$certification->place_of_issue}}"@endif>
                                                    </div>
                                                    <div class="col-md-6 frm_edit">
                                                        <select id="gender" class="selectpicker form-control"
                                                                data-style="btn-primary" name="type" required>
                                                            <option value="international"
                                                                    @if(!empty($certification) && $certification->type =='internation') selected @endif>
                                                                International
                                                            </option>
                                                            <option value="domestic"
                                                                    @if(!empty($certification) && $certification->type =='domestic') selected @endif>
                                                                Domestic
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 frm_edit">
                                                        <select id="status" class="selectpicker form-control"
                                                                data-style="btn-primary" name="status" required>
                                                            <option value="active"
                                                                    @if(!empty($certification) && $certification->status =='active') selected @endif>
                                                                Active
                                                            </option>
                                                            <option value="inactive"
                                                                    @if(!empty($certification) && $certification->status =='inactive') selected @endif>
                                                                Inactive
                                                            </option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6 frm_edit">
                                                        <input type="text" id="language_card" class="form-control"
                                                               placeholder="language card*" name="cert_lang" required
                                                               @if(!empty($certification)) value="{{$certification->cert_lang}}" @endif>
                                                    </div>
                                                </div>

                                                <div class="col-md-11">
                                                    {{--Experience--}}
                                                    <div class="col-md-6 frm_edit">
                                                        <label class="color10 fnt-weight" for="experience">Experience
                                                            <span class="color1">*</span></label>
                                                        <textarea required name="experience"
                                                                  class="editor-wrapper form-control" id="experience"
                                                                  rows="6"> @if(!empty($guide))
                                                                {{$guide->experience}} @endif</textarea>
                                                    </div>

                                                    {{--Diploma--}}
                                                    <div class="col-md-6 frm_edit">
                                                        <label class="color10 fnt-weight" for="diploma">Diploma<span
                                                                    class="color1">*</span></label>
                                                        <textarea name="diploma_info"
                                                                  class="form-control editor-wrapper"
                                                                  id="diploma"
                                                                  rows="6" required>@if(!empty($guide))
                                                                {{$guide->diploma_info}} @endif</textarea>
                                                    </div>
                                                </div>

                                                {{--Service--}}
                                                <div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight" for="old_password">Service<span
                                                                class="color1">*</span></label>
                                                </div>

                                                <div class="col-md-10 col-lg-offset-1">
                                                    <div class="edit-service">
                                                        @if($services)
                                                            @foreach($services as $service)
                                                                <div class="form-check-label frm-check col-md-4">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           name="service_id[]"
                                                                           value="{{$service->id}}" @if(!empty($guideServices)){{Helper::checkInArray($guideServices,$service->id,'service_id')}}@endif><span>@if($service_lang = $service->serviceLang()->whereLanguage_code(\LaravelLocalization::getCurrentLocale())->first()) {{$service_lang->service_name}}@endif</span>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                {{--Transportion--}}
                                                <div class="col-md-12 frm_edit">
                                                    <label class="color10 fnt-weight"
                                                           for="old_password">Transportation<span
                                                                class="color1">*</span></label>
                                                </div>

                                                <div class="col-md-10 col-lg-offset-1">
                                                    <div class="edit-transportion">
                                                        @if($transportations)
                                                            @foreach($transportations as $transportation)
                                                                <div class="form-check-label frm-check col-md-4">
                                                                    <input class="form-check-input" type="checkbox"
                                                                           name="transportation_id[]"
                                                                           value="{{$transportation->id}}" @if(!empty($guidetransportations)){{Helper::checkInArray($guidetransportations,$transportation->id,'service_id')}}@endif><span>@if($transportation_lang = $transportation->serviceLang()->whereLanguage_code(\LaravelLocalization::getCurrentLocale())->first()) {{$transportation_lang->service_name}}@endif</span>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                {{--Button Update--}}
                                                <div class="col-sm-3 col-xs-6 pull-right">
                                                    <div class="pad-top"></div>
                                                    <button id="submit-button" type="submit"
                                                            class="btn btn-primary btn-block btn-flat">Submit
                                                    </button>
                                                    <div class="pad-bottom"></div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}
</div>

<script>
    $(document).ready(function () {
        var button = $('#submit-button');
        $('.cropme').simpleCropper();
        var type = 'add-user';
        @if($user)
                type = 'edit-user';
        @endif
        $('input.datefilter').daterangepicker({
            singleDatePicker: true,
            showDropdowns: false,
            locale: {
                format: 'YYYY/MM/DD'
            },
            maxDate: new Date()
        });
        $('input#expiry_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: false,
            locale: {
                format: 'YYYY/MM/DD'
            },
        });
        if (typeof(tinyMCE) != "undefined") {
            tinymce.EditorManager.editors = [];
        }
        tinymce.init({
            selector: ".editor-wrapper", theme: "modern",
            plugins: [
                "advlist autolink link lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
        });

        $('.add-user').validate({
            onkeyup: false,
            ignore: [],
            rules: {
                phone_number: {
                    digits: true
                },
                password_confirm: {
                    equalTo: "#password"
                }
            },
            messages: {}
            , submitHandler: function (form, event) {
                event.preventDefault();
                var url = $(form).attr('action');
                //var data = new FormData(form);
                var fd = new FormData(form);
                message = checkEmptyImage();
                if (type == 'add-user' && message)
                    return modalDisplay("{{ trans('site.warning') }}", 'warning', message);
                if (!message) {
                    var imageData = $('.cropme').find('img').attr('src');
                    var file = dataURItoBlob(imageData);
                    fd.append('img', file);
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: fd,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: showLoader.bind(button)
                }).done(function (str) {
                    $('label.error').remove();
                    if (str.success) {
                        $('.form-error-message').hide();
                        $('#pop-up').modal('hide');
                        if (window.location.href) {
                            sessionStorage[window.location.href] = table.page.info().page;
                        }
                        window.location.href = 'list';
                    } else {
                        $('.form-error-message').html(str.message).show();
                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                })
            }
        });

        $(document).on('change', '#birth_month, #birth_year', function () {
            var month = $('#birth_month').val();
            var year = $('#birth_year').val();
            if (!month || !year) return;
            var day = daysInMonth(+month, +year);
            var options = '<option selected disabled hidden></option>';
            for (var i = 1; i <= day; i++) {
                options += '<option value="' + i + '">' + i + '</option>';
            }
            $('#birth_day').html(options);
        });
        function dataURItoBlob(dataURI) {
            // convert base64/URLEncoded data component to raw binary data held in a string
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(dataURI.split(',')[1]);
            else
                byteString = decodeURI(dataURI.split(',')[1]);

            // separate out the mime component
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            // write the bytes of the string to a typed array
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {type: mimeString});
        }

        function checkEmptyImage() {
            var cropme = $('.cropme');
            if (!cropme.find('img').length)
                return '{{trans('site.please_choose_image')}}';
            if (cropme.find('img').data('change') == 0)
                return '{{trans('site.no_image_change')}}';
            return false;
        }
    });
</script>