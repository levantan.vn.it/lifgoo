@extends('admin::layouts.master')
@section('css')
    @include('admin::guides.list_css')
@stop
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('admin.approval_list') }}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                            </a>
                        </li>
                        <li class="active"><a href="/admin/user/list">{{ trans('admin.guide_list') }}</a></li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{trans('admin.guide_list')}}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(session('action_succeeded'))
                                <div class="callout callout-success">
                                    {{session('action_succeeded')}}
                                </div>
                            @endif
                            <?php $canAdd = $admin->can('add_new_user');
                            $canEdit = $admin->can('edit_user');
                            $canDelete = $admin->can('delete_user');
                            $admin_role = $admin->roles()->select('display_name')->first();
                            $adminIsSuperior = $admin_role === 'admin';
                            ?>
                            <table id="user-list" class="table table-striped table-bordered bulk_action">
                                <thead>
                                <tr>
                                    {{--<th width="15px"><input type="checkbox" id="bulkDelete" class="minimal"/></th>--}}
                                    <th width="20px">ID</th>
                                    <th>{{ trans('site.avatar')}}</th>
                                    <th>{{ trans('site.email') }}</th>
                                    <th>Username</th>
                                    <th width="200px">{{ trans('site.full_name') }}</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>{{ trans('site.action') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($guides)>0)
                                    @foreach($guides as $guide)
                                        <?php
                                        $user = $guide->user()->first();
                                        if ($user->id === $admin->id) {
                                            $role = $admin_role;
                                        } else {
                                            $role = $user->roles()->select('display_name')->first();
                                        }
                                        $userIsAdmin = $role->name === 'admin';
                                        $lackInfo = $user->lackOfInformation($user);
                                        $isMissing = $lackInfo['missing_profile'] || $lackInfo['missing_jobInfo'];
                                        ?>

                                        <tr>
                                            <td>{{$user->id}}</td>
                                            <td>
                                                <img src="{{Helper::getThemeAvatar($user->avatar?$user->avatar:'user.png')}}"
                                                     class="avatar" alt="Avatar"></td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>{{$user->full_name}}</td>

                                            <td>{{date('Y-m-d',strtotime(urldecode($user->created_at)))}}</td>
                                            <td>
                                                <span class="badge {{$isMissing?'':'label-success'}}" style="color: #73879C; background: none;">{{$isMissing?'Incomplete':'Completed'}}</span>
                                            </td>
                                            <td>
                                                <?php
                                                $canView = $canEdit && !$userIsAdmin || $adminIsSuperior;
                                                $can_delete = $canDelete && $admin->id != $user->id && !$userIsAdmin;
                                                ?>
                                                @if($canView)
                                                    <a href="#" class="btn btn-primary btn-xs user-view"
                                                       data-url="details/{{$guide->id}}"><i class="fa fa-folder"></i>
                                                        View details
                                                    </a>
                                                @endif
                                                @if(!$isMissing)
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-xs guide-approve"
                                                       data-url="approve/{{$user->id}}"><i class="fa fa-folder"></i>
                                                    Approve
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                        <div class="x_content">
                            @if(false && $canAdd)
                                <a href="javascript:void(0)" class="btn btn-success"
                                   data-url="{{Helper::url('admin/guide/new')}}"
                                   id="create-new">{{ trans('admin.create_new_role') }}</a>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div id="pop-up" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    </div>
                </div>
            </div>

            <div id="guide-pop-up" class="face-pop modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-alignment-center">
                        <!-- Modal content-->
                        <div class="modal-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @include('admin::guides.list_js')
@stop

