<script type="text/javascript" src="{{Helper::getThemePlugins("bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
<script type="text/javascript">
    $('.progress .progress-bar').progressbar();
    var valuePrice = {};
    var table = (function () {
        var tbPrice = $('#mytable');
        var self = {};

        self.hasValue = function (capacity, hour, price) {
            if (valuePrice.hasOwnProperty(capacity)) {
                if (valuePrice[capacity].hasOwnProperty(hour)) {
                    if (valuePrice[capacity][hour] == price)
                        return 'hasPrice';
                    return 'hasHour';
                }
                return 'hasCapacity';
            }
            return false;
        };

        self.insertPriceGlobal = function (capacity, hour, price) {
            var inputPrice = $(".input-add-price");
            if (!self.hasValue(capacity, hour, price)) {
                valuePrice[capacity] = {};
            }
            valuePrice[capacity][hour] = price;
            valuePrice = self.sortObjectInObject(valuePrice);
            self.insertIntoTable();
            inputPrice.val('');
            return self;
        };

        self.removePriceGlobal = function (capacity, hour, price) {
            if (self.hasValue(capacity, hour, price) == 'hasPrice') {
                delete valuePrice[capacity][hour];
                self.insertIntoTable();
            }
            else
                return modalDisplay(trans.warning, 'warning', 'Invalid value');

        };

        self.insertIntoTable = function () {
            var st = '';
            for (var obj in valuePrice) {
                var leng = Object.keys(valuePrice[obj]).length;
                if (leng)
                    for (var j in valuePrice[obj]) {
                        st += htmlCell(obj, j, valuePrice[obj][j], leng);
                        leng = 0;
                    }
            }
            tbPrice.find('.tr-edit-price').remove();
            tbPrice.find('.edit-price').prepend(st);
            return self;
        };

        self.sortObjectInObject = function (obj, order) {
            if (obj) {
                obj = self.sortObject(obj);
                for (var j in obj) {
                    var value = self.sortObject(obj[j]);
                    obj[j] = value;
                }
            }
            return obj;
        };

        self.sortObject = function (obj, order) {
            var key, tempArry = [], i, tempObj = {};
            for (key in obj) {
                tempArry.push(key);
            }
            tempArry.sort(
                function (a, b) {
                    return a.toLowerCase().localeCompare(b.toLowerCase());
                }
            );
            if (order === 'desc') {
                for (i = tempArry.length - 1; i >= 0; i--) {
                    tempObj[tempArry[i]] = obj[tempArry[i]];
                }
            } else {
                for (i = 0; i < tempArry.length; i++) {
                    tempObj[tempArry[i]] = obj[tempArry[i]];
                }
            }
            return tempObj;
        };
        self.regexNumber = function (number) {
            if (!/^[0-9.,]+$/.test(number))
                return modalDisplay(trans.warning, 'warning', 'Please enter only digits.');
            if (number == 0)
                return modalDisplay(trans.warning, 'warning', 'You must enter a number greater than 0');
        };

        function htmlCell(capacity, hour, price, leng) {
            var st = '<tr class="tr-edit-price">';
            if (leng)
                st += '<td rowspan="' + leng + '" class="count_user">' + capacity + '</td>';
            st += '<td class="count_hour" data-capacity="' + capacity + '">' + hour + '</td>';
            st += '<td class="count_price">' + price + '</td>';
            st += '<td class="btn-delete-price"><a href="javascript:void(0)">Delete</a></td>';
            st += '<td class="btn-edit-price"><a href="javascript:void(0)">Edit</a></td>';
            st += '</tr>';
            return st;
        }

        return self;
    })();
</script>