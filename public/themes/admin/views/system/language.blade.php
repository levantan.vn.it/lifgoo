@extends('admin::layouts.master')
@section('css')
    {{--@include('admin::guides.list_css')--}}
@stop
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('admin.language_list') }}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                            </a>
                        </li>
                        <li class="active"><a href="/admin/language/list">{{ trans('admin.language_list') }}</a></li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{trans('admin.languages')}}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(session('action_succeeded'))
                                <div class="callout callout-success">
                                    {{session('action_succeeded')}}
                                </div>
                            @endif
                            <?php $canConfig = $admin->can('config_language');
                            $admin_role = $admin->roles()->select('display_name')->first();
                            $adminIsSuperior = $admin_role === 'admin';
                            ?>
                            <table id="user-list" class="table table-striped table-bordered bulk_action">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>{{ trans('site.language_name') }}</th>
                                    <th>{{ trans('site.code') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($languages)>0)
                                    @foreach($languages as $language)
                                        <tr>
                                            <td>{{$language->id}}</td>
                                            <td>{{$language->name}}</td>
                                            <td>{{$language->code}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="pop-up" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
     @include('admin::system.language_js')
@stop

