<?php
if (!array_key_exists($notify->user_id, $user_notify)) {
    $user_notify[$notify->sender_id] = $notify->users('sender_id')->first();
}
$name = $user_notify[$notify->sender_id]->full_name;
$avatar = $user_notify[$notify->sender_id]->avatar;
$avatar = $avatar ? Helper::getThemeAvatar($avatar) : Helper::getThemeImg('user.png');
$content = $notify->content($notify->type, null, $user_notify[$notify->sender_id]);
$is_read = $notify->is_read;
?>
<li @if(!$is_read)class="mark-as-read"@endif data-url="{{Helper::url('admin/markRead')}}" data-noid="{{$notify->id}}">
    <a href="{{Helper::url('admin/guide/approval')}}">
                                    <span class="image">
                                        <img src="{{$avatar}}" alt="Profile Image"/>
                                    </span>
        <span class="message">{!! $content !!}</span>
        <span class="time">{{$notify->created_at}}</span>
    </a>
</li>