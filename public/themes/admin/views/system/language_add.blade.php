<div class="popup-model clearfix">
    <div class="x_panel">
        <div class="x_title">
            <h2>{{empty($language)? trans('admin.add_new_language'): trans('admin.edit_language')}} </h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {!! Form::open(['url' => Helper::url('admin/language').(empty($language)?"/new" : '/edit'), 'id' => 'regForm','class'=>'add-language', 'enctype'=>'multipart/form-data']) !!}
            <div class="form-group form-error-message clearfix"></div>
            @if(!empty($language))<input name="id" type="number" hidden value="{{$language->id}}"> @endif
            <div class="form-group">
                <label for="name">{{ trans('site.language_name')}} (*) </label>
                <input class="form-control" type="text" name="name"
                       id="name"
                       @if(isset($language) && $language)value="{{$language->name}}"
                       @else placeholder="{{trans('site.language_name')}}"@endif>
            </div>
            <div class="form-group">
                <label for="code">{{ trans('site.language_code')}} (*) </label>
                <input class="form-control" type="text" name="code"
                       id="code"
                       @if(isset($language) && $language)value="{{$language->code}}"
                       @else placeholder="{{trans('site.language_code')}}"@endif>
            </div>
            <div class="col-sm-3 col-xs-6 pull-right">
                <div class="pad-top"></div>
                <button id="submit-button" type="submit"
                        class="btn btn-primary btn-block btn-flat">{{ trans('site.submit') }}</button>
                <div class="pad-bottom"></div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--</div>--}}
</div>

<script>
    $(document).ready(function () {
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        var button = $('#submit-button');
                @if(!empty($language))
        var url = 'edit';
                @else
        var url = 'new';
        @endif
        $('#regForm').validate({
            onkeyup: false,
            ignore: [],
            rules: {
                type: {
                    required: true
                },
                language_name_en: {
                    required: {
                        depends: function () {
                            if (this.value) {
                                this.value = this.value.trim();
                            }
                            return false;
                        }
                    },
                    maxlength: 100
                },
                language_name_vi: {
                    required: {
                        depends: function () {
                            if (this.value) {
                                this.value = this.value.trim();
                            }
                            return false;
                        }
                    },
                    maxlength: 100
                }/*,
                diploma_info: {
                    required: {
                        depends: function () {
                            if (this.value) {
                                this.value = this.value.trim();
                            }
                            return false;
                        }
                    }
                }*/
            },
            messages: {}
            , submitHandler: function (form, event) {
                event.preventDefault();
                var data = new FormData(form);
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: showLoader.bind(button)
                }).done(function (str) {
                    $('label.error').remove();
                    if (str.success) {
                        $('.form-error-message').hide();
                        $('#pop-up').modal('hide');
                        if (window.location.href) {
                            sessionStorage[window.location.href] = table.page.info().page;
                        }
                        window.location.href = 'list';
                    } else {
                        $('.form-error-message').html(str.message).show();
                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                })
            }
        });

        $(document).on('change', '#birth_month, #birth_year', function () {
            var month = $('#birth_month').val();
            var year = $('#birth_year').val();
            if (!month || !year) return;
            var day = daysInMonth(+month, +year);
            var options = '<option selected disabled hidden></option>';
            for (var i = 1; i <= day; i++) {
                options += '<option value="' + i + '">' + i + '</option>';
            }
            $('#birth_day').html(options);
        })
    });
</script>