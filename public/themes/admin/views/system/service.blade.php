@extends('admin::layouts.master')
@section('css')
    {{--@include('admin::guides.list_css')--}}
    <style type="text/css">
        .modal-content {
            background-color: #F2F5F7;
        }

        .well {
            border: none;
        }

        .profile_details .profile_view {
            width: 100%;
        }

        .timeline .block {
            margin: 0 0 0 145px;
        }

        .timeline .tags {
            width: 125px;
        }

        textarea {
            resize: none;
        }
    </style>
@stop
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('admin.service_list') }}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                            </a>
                        </li>
                        <li class="active"><a href="/admin/service/list">{{ trans('admin.service_list') }}</a></li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{trans('admin.services')}}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(session('action_succeeded'))
                                <div class="callout callout-success">
                                    {{session('action_succeeded')}}
                                </div>
                            @endif
                            <?php $canConfig = $admin->can('config_service');
                            $admin_role = $admin->roles()->select('display_name')->first();
                            $adminIsSuperior = $admin_role === 'admin';
                            ?>
                            <table id="user-list" class="table table-striped table-bordered bulk_action servicelist">
                                <thead>
                                <tr>
                                    <th width="15px"><input type="checkbox" id="bulkDelete" class="minimal"/></th>
                                    <th>{{ trans('site.icon')}}</th>
                                    <th width="200px">{{ trans('site.service_name') }}</th>
                                    <th>{{ trans('site.type') }}</th>
                                    <th>{{ trans('site.action') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($services)>0)
                                    @foreach($services as $service)
                                        <?php
                                        $service_lang = $service->serviceLang()->whereLanguage_code(\LaravelLocalization::getCurrentLocale())->first();
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <input class="deleteRow minimal" type="checkbox" name="ids"
                                                       value="{{$service->id}}"/>
                                            </td>
                                            <td><span class="{{$service->icon}}"></span></td>

                                            <td>{{$service_lang?$service_lang->service_name:""}}</td>
                                            <td>{{$service->type}}</td>
                                            <td>
                                                @if($canConfig)
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-info btn-xs bg-light-blue service-edit"
                                                       data-url="edit/{{$service->id}}"><i
                                                                class="fa fa-pencil"></i> {{ trans('site.edit') }}
                                                    </a>
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-danger btn-xs service-delete"
                                                       id="{{$service->id}}"><i class="fa fa-trash-o"></i>
                                                        {{ trans('site.delete') }} </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                        <div class="x_content">
                            @if($canConfig)
                                <a id="delete-checked"
                                   data-url="{{Helper::url('service/delete')}}" href="javascript:void(0)"
                                   class="btn btn-danger">{{ trans('admin.delete_services') }}</a>
                                <a href="javascript:void(0)" class="btn btn-success service-new"
                                   data-url="{{Helper::url('admin/service/edit')}}"
                                   id="create-new">{{ trans('admin.create_new_service') }}</a>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div id="pop-up" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @include('admin::system.service_js')
@stop

