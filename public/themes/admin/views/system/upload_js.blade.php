<script type="text/javascript">
    /* js year*/
    var file;
    var isChange = false;
    var linkAvata;
    /*js get city*/
    $(document).ready(function () {
        var input = document.getElementById('upload_avatar');
        var fileName;
        $('#file-upload').on('click', function (e) {
            e.preventDefault();
            /*input.click();*/
            $('#cModalChangeAvatar').modal({backdrop: 'static', keyboard: false});
        });

        $('#_pf-choose-image').on('click', function (e) {
            e.preventDefault();
            $('.hovereffect').click();
        });


        $(".pf-btn-button-save").click(function () {
            var form = document.createElement('form');
            form.appendChild(input);
            var data = new FormData(form);
            $.ajax({
                url: '{{Helper::url('profile/uploadAvatar')}}',
                type: 'POST',
                dataType: 'JSON',
                data: data,
                processData: false,
                contentType: false,
                enctype: "multipart/form-data",
                success: function (str) {
                    form = null;
                    isChange = false;
                }
            });
        });

        $(".pf-btn-button-cancel").click(function () {
            $('.hovereffect').css('background-image', 'url(' + linkAvata + ')');
        });

        $('.image-editor').cropit({
            imageBackground: true,
            smallImage: true,
            allowDragNDrop: true,
            width: '{{env('avatar')}}',
            height: '{{env('avatar')}}'
        });

        $('.cropit-image-input').change(function () {
            var file;
            if (file = this.files[0]) {
                fileName = file.name;
            }
        });

        $('.rotate-cw').click(function () {
            $('.image-editor').cropit('rotateCW');
        });

        $('.rotate-ccw').click(function () {
            $('.image-editor').cropit('rotateCCW');
        });

        $('.export').click(function () {
            var imageData = $('.image-editor').cropit('export');
            if (imageData) {
                var file = dataURItoBlob(imageData);
                var fd = new FormData();
                fd.append('fname', fileName);
                fd.append('data', file);
                $.ajax({
                    url: '{{Helper::url('profile/uploadAvatar')}}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: fd,
                    processData: false,
                    contentType: false,
                    enctype: "multipart/form-data",
                    success: function (str) {
                        form = null;
                        isChange = false;
                        if (str.success) {
                            var url = '{{Helper::getThemeAvatar()}}' + str.data['url'];
                            var hovereffect = $('.hovereffect');
                            hovereffect.css('background-image', 'url(' +
                                    url + ')');
                            hovereffect.find('i.fa-users').remove();
                            $('#cModalChangeAvatar').modal('hide');
                        }
                    }
                });
            } else {
                $('#cModalChangeAvatar').modal('hide');
            }
        });
    });

    /* Show preview for image*/
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (isChange == false) {
                    isChange = true;
                    var bg = $('.hovereffect').css('background-image');
                    linkAvata = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
                }
                $('.hovereffect').css('background-image', 'url(' + e.target.result + ')');
                $(".pf_button").fadeIn("slow");
            };
            return reader.readAsDataURL(input.files[0]);
        }
        return false
    }

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = decodeURI(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    }


</script>