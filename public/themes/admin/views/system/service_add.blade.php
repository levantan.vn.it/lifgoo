<div class="popup-model clearfix">
    <div class="x_panel">
        <div class="x_title">
            <h2>{{empty($service)? trans('admin.add_new_service'): trans('admin.edit_service')}} </h2>
            <div class="clearfix"> <button type="button" class="close close-editservice" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></div>
        </div>
        <div class="x_content">
            {!! Form::open(['url' => Helper::url('admin/service').(empty($service)?"/new" : '/edit'), 'id' => 'regForm','class'=>'add-service', 'enctype'=>'multipart/form-data']) !!}
            <div class="form-group form-error-message clearfix"></div>
            @if(!empty($service))<input name="id" type="number" hidden value="{{$service->id}}"> @endif
            <div class="form-group">
                <label for="type" style="width: 100%;">{{trans('admin.type')}}</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>
                        <input type="radio" class="minimal" checked name="type" value="service"> {{trans('site.service')}}
                    </label>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label>
                        <input type="radio" class="minimal" @if(!empty($service)) {{$service->type=='transportation'?'checked':''}} @endif name="type" value="transportation"> {{trans('site.transportation')}}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="icon" style="width: 100%;">icon</label>
                <input class="form-control" type="text" name="icon" id="icon"
                       @if(isset($service)) value="{{$service->icon}}"
                       @else placeholder="icon (fa fa-user)"@endif>
            </div>
            @if($languages)
                @foreach($languages as $lang)
                    <?php
                    !empty($service) && $service_lang = $service->serviceLang()->where('language_code', $lang->code)->first();
                    ?>
                    <div class="form-group">
                        <label for="location">{{ trans('site.service_name_'.$lang->code)}} (*) </label>
                        <input class="form-control" type="text" name="service_name_{{$lang->code}}"
                               id="service_name_{{$lang->code}}"
                               @if(isset($service_lang) && $service_lang)value="{{$service_lang->service_name}}"
                               @else placeholder="{{trans('site.service_name')}}"@endif>
                    </div>
                @endforeach
            @endif
            <div class="col-sm-3 col-xs-6 pull-right">
                <div class="pad-top"></div>
                <button id="submit-button" type="submit"
                        class="btn btn-primary btn-block btn-flat">{{ trans('site.submit') }}</button>
                <div class="pad-bottom"></div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--</div>--}}
</div>

<script>
    $(document).ready(function () {
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        var button = $('#submit-button');
                @if(!empty($service))
        var url = 'edit';
                @else
        var url = 'new';
        @endif
        $('#regForm').validate({
            onkeyup: false,
            ignore: [],
            rules: {
                type: {
                    required: true
                },
                service_name_en: {
                    required: {
                        depends: function () {
                            if (this.value) {
                                this.value = this.value.trim();
                            }
                            return false;
                        }
                    },
                    maxlength: 100
                },
                service_name_vi: {
                    required: {
                        depends: function () {
                            if (this.value) {
                                this.value = this.value.trim();
                            }
                            return false;
                        }
                    },
                    maxlength: 100
                }/*,
                diploma_info: {
                    required: {
                        depends: function () {
                            if (this.value) {
                                this.value = this.value.trim();
                            }
                            return false;
                        }
                    }
                }*/
            },
            messages: {}
            , submitHandler: function (form, event) {
                event.preventDefault();
                var data = new FormData(form);
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: showLoader.bind(button)
                }).done(function (str) {
                    $('label.error').remove();
                    if (str.success) {
                        $('.form-error-message').hide();
                        $('#pop-up').modal('hide');
                        if (window.location.href) {
                            sessionStorage[window.location.href] = table.page.info().page;
                        }
                        window.location.href = 'list';
                    } else {
                        $('.form-error-message').html(str.message).show();
                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                })
            }
        });

        $(document).on('change', '#birth_month, #birth_year', function () {
            var month = $('#birth_month').val();
            var year = $('#birth_year').val();
            if (!month || !year) return;
            var day = daysInMonth(+month, +year);
            var options = '<option selected disabled hidden></option>';
            for (var i = 1; i <= day; i++) {
                options += '<option value="' + i + '">' + i + '</option>';
            }
            $('#birth_day').html(options);
        })
    });
</script>