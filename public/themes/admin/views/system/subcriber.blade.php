@extends('admin::layouts.master')
@section('css')
    {{--@include('admin::guides.list_css')--}}
@stop
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('admin.subcribers_list') }}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                            </a>
                        </li>
                        <li class="active"><a href="/admin/subscriber/list">{{ trans('admin.subcribers_list') }}</a></li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{trans('admin.subcribers')}}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(session('action_succeeded'))
                                <div class="callout callout-success">
                                    {{session('action_succeeded')}}
                                </div>
                            @endif
                            <table id="user-list" class="table table-striped table-bordered bulk_action">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($subcribers)>0)
                                    @foreach($subcribers as $subcriber)
                                        <tr>
                                            <td>{{$subcriber->id}}</td>
                                            <td>{{$subcriber->email}}</td>
                                            <td>
                                                @if($subcriber->status==1)
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="pop-up" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
     @include('admin::system.language_js')
@stop

