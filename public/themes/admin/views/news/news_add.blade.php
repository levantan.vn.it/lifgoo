<link rel="stylesheet" href="{{Helper::getThemePlugins('simplecrop/css/style.css', 'admin')}}"/>
<style type="text/css">

    .modal-dialog {
        width: 50%;
    }

    #cModalChangeAvatar .modal-body {
        padding: 0;
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$news?trans('admin.news_edit'):trans('admin.news_new')}}</h2>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="message-return"></div>
                <div class="col-md-12 col-sm-12" style="margin: 0 auto;float: none;text-align: center;">
                    <div class="cropme" style="width: 600px; height: 400px;">@if($news && $news->url) <img
                                src="{{Helper::getThemeNewsImg($news->url).'?id='.Helper::versionImg()}}" alt=""
                                data-change="0">@endif</div>
                    <span>{!! trans('site.click_upload_thumbnail') !!}</span>
                </div>
                {!! Form::open(['url' => Helper::url('admin/news/').($news?"/edit" : '/new'), 'id' => 'regForm','class'=>$news?'edit-news':'add-news', 'enctype'=>'multipart/form-data','onsubmit'=>'event.preventDefault()']) !!}
                @if(!empty($news))<input name="id" type="number" hidden value="{{$news->id}}"> @endif
                <div class="form-group col-sm-6 frm_edit">
                    <label for="country_id">{{ trans('admin.countries')}}(*)</label>
                    <select class="form-control select-country" name="country_id"
                            data-url="{{Helper::url('admin/cities')}}">
                        @if($countries)
                            @foreach($countries as $country)
                                <option class="option" value="{{$country->country_id}}"
                                        @if($news)
                                            @if($news->country_id==$country->country_id) selected @endif
                                        @else
                                            @if($country->country_id==140)selected @endif                                                selected
                                        @endif
                                >{{$country->country_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group col-sm-6 frm_edit">
                    <label for="city_id">{{ trans('admin.cities')}}(*)</label>
                    <select class="form-control select-city" name="city_id">
                        <?php
                            if($news){
                                $cities = \Modules\User\Entities\CityEntity::where('country_id', '=', $news->country_id)->get();
                            }else{
                                $cities = \Modules\User\Entities\CityEntity::where('country_id', '=', 140)->get();
                            }
                        ?>
                        @if($cities)
                            @foreach($cities as $city)
                                <option class="option" value="{{$city->city_id}}"
                                        @if($news && $news->city_id ==$city->city_id)selected @endif>{{$city->city_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <?php
                $news && $news_en = $news->news_language()->whereLanguage_code('en')->first();
                //$news && $news_vi = $news->news_language()->whereLanguage_code('vi')->first();
                ?>
                <div class="form-group col-sm-12">
                    <label for="location">{{ trans('site.title')}}(*)</label>
                    <input class="form-control" type="text" name="title_en" id="title"
                           @if(!empty($news_en)) value="{{$news_en->title}}"@endif>
                </div>
                <div class="form-group col-sm-12">
                    <label for="content_summary_en">{{ trans('site.content_summary')}}(*)</label>
                    <textarea class="form-control" name="content_summary_en" id="content_summary_en"
                              maxlength="200">@if(!empty($news_en)) {!!$news_en->content_summary!!}@endif</textarea>
                </div>
                <div class="form-group col-sm-12">
                    <label for="content">{{ trans('site.content')}}(*)</label>
                    <textarea class="form-control editor-wrapper" name="content_en" id="content_en">@if(!empty($news_en))<?php echo $news_en->content ?>@endif</textarea>
                </div>
                <div class="col-sm-3 col-xs-6 pull-right">
                    <div class="pad-top"></div>
                    <button id="submit-button" type="submit"
                            class="btn btn-primary btn-block btn-flat">{{ trans('site.submit') }}</button>
                    <div class="pad-bottom"></div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@include('admin::system.upload_js')
<script>
    $(document).ready(function () {
        $('.cropme').simpleCropper();
        tinyMce();
        $('#regForm').validate({
            onkeyup: false,
            ignore: [],
            rules: {
                title_en: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 100
                },
                content_en: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    }
                },
                content_summary_en: {
                    required: {
                        depends: function () {
                            this.value = this.value.trim();
                            return true;
                        }
                    },
                    maxlength: 200
                },
                country_id: {
                    required: true
                },
                city_id: {
                    required: true
                }
            },
            submitHandler: function (form) {
                var type = $('#regForm').attr('class');
                var fd = new FormData(form);
                if (type == 'add-news' && (message = checkEmptyImage()))
                    return modalDisplay("{{ trans('site.warning') }}", 'warning', message);
                if (!checkEmptyImage()) {
                    var imageData = $('.cropme').find('img').attr('src');
                    var file = dataURItoBlob(imageData);
                    fd.append('img', file);
                }
                var button = $('#submit-form');
                var submit_url = $('#regForm').attr('action');
                $.ajax({
                    type: "POST",
                    url: submit_url,
                    data: fd,
                    dataType: "JSON",
                    processData: false,
                    contentType: false,
                    beforeSend: showLoader.bind(button),
                    success: function (result) {
                        if (result.success === false) {
                            modalDisplay("{{ trans('site.warning') }}", 'warning', result.message);
                        }
                        else {
                            sessionStorage[window.location.href] = table.page.info().page;
                            window.location.href = 'list';
                        }
                    }
                }).fail(function (e) {
                    $('label.error').remove();
                    var response = e.responseJSON;
                    $('.form-error-message').hide();
                    if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
                    Object.keys(response).forEach(function (i) {
                        var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                        $('#' + i).after(label);
                    });
                }).always(function () {
                    hideLoader.call(button);
                });
            }
        });

    });

    function checkEmptyImage() {
        var cropme = $('.cropme');
        if (!cropme.find('img').length)
            return '{{trans('site.please_choose_image')}}';
        if (cropme.find('img').data('change') == 0)
            return '{{trans('site.no_image_change')}}';
        return false;
    }

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = decodeURI(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type: mimeString});
    }
    $(document).on('click', '.close', function () {
        location.reload();
    });
</script>