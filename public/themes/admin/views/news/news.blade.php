@extends('admin::layouts.master')
@section('css')
    {{--@include('admin::news.list_css')--}}
@stop
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{trans('admin.news_list') }}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                            </a>
                        </li>
                        <li class="active"><a href="/admin/news/list">{{ trans('admin.news_list') }}</a></li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{trans('admin.news')}}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(session('action_succeeded'))
                                <div class="callout callout-success">
                                    {{session('action_succeeded')}}
                                </div>
                            @endif
                            <?php $canAccess = $admin->can('access_news'); ?>
                            <table id="user-list" class="table table-striped table-bordered bulk_action">
                                <thead>
                                <tr>
                                    <th width="15px"><input type="checkbox" id="bulkDelete" class="minimal"/></th>
                                    <th>{{ trans('site.thumbnail')}}</th>
                                    <th>{{ trans('site.title') }}</th>
                                    <th width="200px">{{ trans('site.content_summary') }}</th>
                                    <th>{{ trans('site.location') }}</th>
                                    <th>{{ trans('admin.author') }}</th>
                                    <th>{{ trans('site.created_date') }}</th>
                                    <th>{{ trans('site.action') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($news)>0)
                                    @foreach($news as $ne)
                                        <tr>
                                            <td class="text-center">
                                                <input class="deleteRow minimal" type="checkbox" name="user_id"
                                                       value="{{$ne->id}}"/>
                                            </td>
                                            <td><img src="{{$ne->url?Helper::getThemeNewsImg($ne->url).'?id='.Helper::versionImg():''}}" class="avatar" alt="Avatar"></td>
                                            <?php $newsLang = $ne->news_language()->where('language_code','=','en')->first();
                                            $city = $ne->cities()->select(['city_name','city_id'])->first();
                                            $country = $ne->countries()->select(['country_id','country_name'])->first();
                                            $user = $ne->users()->select(['id', 'username'])->first();
                                            //dd($newsLang);
                                            ?>
                                            <td>{{$newsLang['title']}}</td>
                                            <td style="text-align: justify">@if($newsLang){!!$newsLang['content_summary']!!}@endif</td>
                                            <td>{{$city?$city->city_name.', ':''.$country?$country->country_name:''}}</td>
                                            <td class="new-author">{{$user?$user->username:''}}</td>
                                            <td>{{date('Y-m-d',strtotime(urldecode($ne->created_at)))}}</td>
                                            <td>
                                                @if($canAccess)
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-info btn-xs bg-light-blue news-edit"
                                                       data-url="edit/{{$ne->id}}"><i
                                                                class="fa fa-pencil"></i> {{ trans('site.edit') }}
                                                    </a>
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-danger btn-xs news-delete"
                                                       id="{{$ne->id}}"><i class="fa fa-trash-o"></i>
                                                        {{ trans('site.delete') }} </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                        <div class="x_content">
                            @if($canAccess)
                                <a id="news-delete-checked"
                                   data-url="{{Helper::url('admin/news/delete')}}" href="javascript:void(0)"
                                   class="btn btn-danger">{{ trans('admin.delete_news') }}</a>
                                <a href="javascript:void(0)" class="btn btn-success"
                                   data-url="{{Helper::url('admin/news/edit')}}"
                                   id="create-new">{{ trans('admin.create_new_news') }}</a>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div id="pop-up" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    </div>
                </div>
            </div>
            <div id="news-pop-up" class="face-pop modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-alignment-center">
                        <!-- Modal content-->
                        <div class="modal-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @include('admin::news.news_js')
@stop

