
<script>
    function remove_news(ids_value) {
        $.ajax({
            type: "POST",
            url: "delete",
            data: {ids: ids_value, _token: '{{csrf_token()}}'},
            dataType: "JSON",
            beforeSend: showLoader
        }).done(function (result) {
            if (result.success == false) {
                // open modal error message
                return modalDisplay("{{trans('site.error') }}", 'error', result.message);
            }
            // redirect to news list after delete
            sessionStorage[window.location.href] = 0;
            window.location.replace("list");
        }).fail(function (e) {
            $('label.error').remove();
            var response = e.responseJSON;
            $('.form-error-message').hide();
            if (!response) return modalDisplay("{{ trans('site.warning') }}", 'warning', '{{trans('site.error_occured')}}');
            Object.keys(response).forEach(function (i) {
                var label = '<label id="' + i + '-error" class="error" for="' + i + '">' + response[i][0] + '</label>';
                $('#' + i).after(label);
            });
        }).always(hideLoader)
    }


    function delete_news_popup(ids, message) {
        new Messi(
                message,
                {
                    modal: true,
                    modalOpacity: 0.5,
                    title: '{{trans('site.confirm')}}',
                    titleClass: 'warning',
                    buttons: [
                        {id: 0, label:trans.yes, val: 'Y'},
                        {id: 1, label: trans.no, val: 'N'}
                    ],
                    callback: function (val) {
                        if (val == 'Y') {
                            // sent ajax to remove news
                            remove_news(ids);
                        }
                    }
                }
        );
    }

    $(document).ready(function () {
        table = $("#user-list").DataTable({
            ordering: false,
            "language": {
                "paginate": {
                    "previous": trans.previous,
                    "next": trans.next
                },
                "lengthMenu": trans.show + ' ' + '_MENU_' + ' ' + trans.entries,
                "search": trans.search
            }
        });

        if (sessionStorage[window.location.href] && sessionStorage[window.location.href] !== '0') {
            table.page(parseInt(sessionStorage[window.location.href])).draw(false);
            sessionStorage[window.location.href] = '0';
        }

        $(document).on('click', '.news-delete', function (event) {
            event.preventDefault();
            id = $(this).attr('id');
            delete_news_popup(id, 'Are you sure delete it?');
            $(this).blur();
        });

        $("#bulkDelete").on('ifClicked', function () {
            var status = this.checked;
            if (!status) {
                $(".deleteRow").iCheck('check');
            } else {
                $(".deleteRow").iCheck('uncheck');
            }
        });

        $('#delete-checked').click(function (event) {
            event.preventDefault();
            var rows = $('.deleteRow:checked');
            if (rows.length > 0) {
                var val = rows.map(function (_, el) {
                    return $(el).val();
                }).get();
                val = val.toString();
                delete_news_popup(val, '{{ trans('admin.delete_roles') }}');
            }
            else {
                modalDisplay("{{ trans('site.error') }}", 'error', '{{trans('site.no_roles_selected')}}');
            }
        });

        $('#news-delete-checked').click(function (event) {
            event.preventDefault();
            var rows = $('.deleteRow:checked');
            if (rows.length > 0) {
                var val = rows.map(function (_, el) {
                    return $(el).val();
                }).get();
                val = val.toString();
                delete_news_popup(val, '{{ trans('admin.delete_news_question') }}');
            }
            else {
                modalDisplay("{{ trans('site.error') }}", 'error', '{{trans('site.no_news_selected')}}');
            }
            $(this).blur();
        });

        $(document).on('click', '#create-new, .news-view, .news-edit', function (event) {
            var button = this;
            event.preventDefault();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: showLoader.bind(button)
            }).done(function (e) {
                if (e.success === false) {
                    modalDisplay("{{ trans('site.warning') }}", 'warning', e.message);
                    return false;
                }
                $('#pop-up .modal-content').html(e.message);
                $('#pop-up').modal('show');
            }).always(hideLoader.bind(button))
            console.log("wrwt");
            $(this).blur();
        });

        $(document).on('input', '#display_name', function() {
            // fix bug _ in admin role list
            if (0) {$('#role_name').html(this.value.toSlug());}
            $('#role_name').html(this.value);
        });


    });
</script>