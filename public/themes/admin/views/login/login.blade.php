<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Lifgoo | </title>
    {!! Assets::css() !!}
</head>
<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                {!! Form::open(['url' => Helper::url('admin/login')]) !!}
                <h1>Login Form</h1>
                @if($errors->has('login_required'))
                    <p class="login-box-msg text-red">{{$errors->first('login_required')}}</p>
                @endif
                <div>
                    <input type="text" class="form-control" placeholder="Username" required="" name="username"/>
                </div>
                <div>
                    <input type="password" class="form-control" placeholder="Password" required="" name="password"/>
                </div>
                <div>
                    <button type="submit">Submit</button>
                    {{--<a class="btn btn-default submit">Submit</a>--}}
                </div>
                {!! Form::close() !!}
            </section>
        </div>
    </div>
</div>

{!! Assets::js() !!}
@if($errors->has('login_required'))
    {{--<script>
        modalDisplay("login fail", 'warning', '{{$errors->first('login_required')}}');
    </script>--}}
@endif
{{--<script>--}}
    {{--$.ajaxSetup({--}}
        {{--headers: {--}}
            {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--}--}}
    {{--});--}}
{{--</script>--}}
</body>
</html>
