@extends('admin::layouts.master')
@section('css')
    @include('admin::guides.list_css')
    <style type="text/css">
        .modal-dialog {
            width: 70%;
        }

        .cropme {
            width: 270px;
            height: 270px;
            float: none;
            margin: 0 auto;
        }
    </style>
@stop
@section('content')
    <?php
        function swichLabel($type){
            switch ($type) {
                case 'accepted':
                    return 'label-success';
                case 'declined':
                case 'canceled':
                    return 'label-danger';
                case 'finished':
                    return 'label-success';
                case 'pending':
                    return 'label-primary';
            }
        }
    ?>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{ trans('admin.booking_list') }}</h3>
                    <ol class="breadcrumb">
                        <li><a href="/admin"><i class="fa fa-dashboard"></i>{{ trans('site.homepage') }}
                            </a>
                        </li>
                        <li class="active"><a href="/admin/booking/list">{{ trans('admin.booking_list') }}</a></li>
                    </ol>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{trans('admin.booking_list')}}</h2>
                            <div class="filter-history">
                                <div class="fnt-weight font-size-12 color999" style="margin-bottom: 5px;">Filter by</div>
                                <select id="filter-history" class="selectpicker form-control font-size-14 btn-filterHistory" data-style="btn-primary" name="filter_history">
                                    <option value="all">All</option>
                                    <option value="pending">Pending</option>
                                    <option value="accepted">Accepted</option>
                                    <option value="declined">Declined</option>
                                    <option value="canceled">Canceled</option>
                                    <option value="finished">Finished</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @if(session('action_succeeded'))
                                <div class="callout callout-success">
                                    {{session('action_succeeded')}}
                                </div>
                            @endif
                            <table id="user-list" class="table table-striped table-bordered bulk_action">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th width="200px">Guide {{ trans('site.full_name') }}</th>
                                    <th>Guide {{ trans('site.email') }}</th>
                                    <th>Traveler</th>
                                    <th>Booking email</th>
                                    <th>Booking phone</th>
                                    <th>{{ trans('site.status') }}</th>
                                    <th>{{ trans('site.action') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if(count($bookings)>0)
                                    @foreach($bookings as $booking)
                                        <?php
                                        $guide = $booking->guide()->select('id', 'full_name', 'avatar', 'email')->first();
                                        $traveler = $booking->guide('traveler_id')->select('id', 'full_name', 'avatar', 'email')->first();
                                        ?>

                                        <tr>
                                            <td style="width: 24px;">{{$booking->id}}</td>
                                            <td>{{$guide->full_name}}</td>
                                            <td>{{$guide->email}}</td>
                                            <td>{{$traveler->full_name}}</td>
                                            <td>{{$booking->email}}</td>
                                            <td>{{$booking->phone_number}}</td>
                                            <td>
                                                <span class="badge <?php echo swichLabel($booking->status)?>" style="color: #73879C; background: none;">
                                                    {{$booking->status}}
                                                </span>
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-xs user-view"
                                                   data-url="{{Helper::url('admin/booking/details/'.$booking->id)}}"><i class="fa fa-folder"></i>
                                                    View
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="pop-up" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    </div>
                </div>
            </div>

            <div id="guide-pop-up" class="face-pop modal fade" role="dialog">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-alignment-center">
                        <!-- Modal content-->
                        <div class="modal-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    @include('admin::booking.list_js')
@stop

