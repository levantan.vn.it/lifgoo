<style type="text/css">
    .modal-dialog {
        width: 85%;
    }

    .x_panel {
        border: none;
    }
</style>
<?php
$typeUser = [];
$total_count = 0;
$total_dates = 0;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">Details information</h4>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="color10 font-size">
                    <div class="title-aboutme color2 font-size1 title-aboutme-pad">Location</div>
                    <hr>
                    <div class="date_time_total font-size">
                        Country: {{$booking->getCity()->first()->country()->first()->country_name}}
                    </div>
                    <div class="number_people_total font-size">
                        City: {{$booking->getCity()->first()->city_name}}
                    </div>
                    <div class="expected_total font-size">
                        Pickup Place: {{$booking->pickup_place}}
                    </div>
                    <div class="expected_total font-size">
                        Destination: {{$booking->destination}}
                    </div>
                    <hr>
                    <div class="title-aboutme color2 font-size1 title-aboutme-pad">Specific time</div>
                    <hr>
                    @include('admin::booking.detailBooking')
                    <hr>
                    <div class="title-aboutme color2 font-size1 title-aboutme-pad">Messages</div>
                    <hr>
                    @include('admin::booking.chatBooking')
                </div>
            </div>
        </div>
    </div>
</div>


