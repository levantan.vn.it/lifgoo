<div class="chat-cnt-edit-traveller">
    <div class="content-chat">
        @if(count($messages))
            @foreach($messages as $msg)
                <?php
                if (!array_key_exists($msg->user_id, $typeUser)) {
                    $typeUser[$msg->user_id] = $msg->user()->first();
                }
                $type = $typeUser[$msg->user_id]->type;
                $avatar = $typeUser[$msg->user_id]->avatar;
                $avatar = $avatar?Helper::getThemeAvatar($avatar):Helper::getThemeImg('user.png');
                ?>
                <div class="content-mesage row ">
                    <div style="float:{{$type=='guide'?'right':'left'}};width: 95%;">
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 avatar_traveller">
                            <img class="avatar-chatbox" src="{{$avatar}}"
                                 style="width: 100%; border-radius: 50%;">
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8 font-size-14 {{$type=='guide'?'guide_inbox':'trevaller_inbox'}} item-message-chat text-justify"
                             data-user_id="{{$msg->user_id}}">
                            {{$msg->content}}
                            <time datetime="2009-11-13T20:00">
                                {{$msg->created_at}}
                            </time>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>