<script type="text/javascript">
    $(document).ready(function () {
        table = $("#user-list").DataTable({
            ordering: false,
            "language": {
                "paginate": {
                    "previous": trans.previous,
                    "next": trans.next
                },
                "lengthMenu": trans.show + ' ' + '_MENU_' + ' ' + trans.entries,
                "search": trans.search,
                "pagingType": "full_numbers"
            }
        });

        if (sessionStorage[window.location.href] && sessionStorage[window.location.href] !== '0') {
            table.page(parseInt(sessionStorage[window.location.href])).draw(false);
            sessionStorage[window.location.href] = '0';
        }
        $(document).on('click', '.user-view', function (event) {
            event.preventDefault();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                beforeSend: showLoader
            }).done(function (e) {
                if (e.success == false) {
                    modalDisplay("{{trans('site.error') }}", 'error', e.message);
                    return false;
                }
                $('#pop-up .modal-content').html(e.message);
                $('#pop-up').modal('show');
//                tinyMce();
            }).always(hideLoader);
        });

        // active link when filter booking 
        var value = window.location.pathname.split('/')[5];
        $('#filter-history option').each(function() {
            if ( typeof value === 'undefined'){
                $('#filter-history option[value=all]').attr('selected','true');
            }
            if($(this).val() == value) {
                $(this).prop("selected", true);
            }
        });
    });

</script>