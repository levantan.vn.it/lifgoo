<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$title or 'Lifgoo'}}</title>
    {{--<link rel="shortcut icon" href="/frend/images/camera.ico" type="image/x-icon"/>--}}
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <meta name="robots" content="noindex, nofollow"/>
    <meta name="googlebot" content="noindex, nofollow"/>
    <link rel="shortcut icon" href="{{Helper::getThemeImg('favicon.ico')}}" type="image/x-icon">
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    {!! Assets::css() !!}
    @yield('css')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        @include('admin::layouts.sidebar')
        @include('admin::layouts.top_navigation')
        @yield('content')
        @include('admin::layouts.footer')
    </div>
</div>

{!! Assets::js() !!}

<script type="text/javascript">
    $('a[hreflang]').click(function () {
        var val = $(this).attr('hreflang');
        $.ajax({
            url: '/{{LaravelLocalization::getCurrentLocale()}}/admin/languagetoggle',
            data: {lang: val, _token: '{{csrf_token()}}'},
            type: 'POST',
            datatype: 'JSON'
        })
    });

</script>
@yield('js')
@include('admin::layouts.translate')
@if($errors->has('no_access'))
    <script>
        modalDisplay("{{ trans('site.warning') }}", 'warning', '{{$errors->first('no_access')}}');
    </script>
@endif
@if(\LaravelLocalization::getCurrentLocale() == 'vi')
    {{--<script src="{{url("frend/js/messages_vi.js")}}"></script>--}}
@endif
<script>
    function intervalNotify() {
        var bell = $('.notification-bell');
        var badge = bell.find('.badge');
        var count = badge.text() ? parseInt(badge.text()) : 0;
        $.ajax({
            url: '{{Helper::url('admin/intervalNotify')}}',
            type: 'GET',
            dataType: 'JSON',
            data: {bell:count},
        }).done(function (str) {
            if (str.success) {
                var c_bell = str.data.countBell;
                if (str.data.htmlBell)
                    $('.content-notify').html(str.data.htmlBell);
                var countBell = badge.text() ? parseInt(badge.text()) : 0;
                if (countBell != c_bell && c_bell>0){
                    badge.text(c_bell);
                    bell.addClass('bell-notify');
                }
            }
        }).fail(function (e) {
            var response = e.responseJSON;
        }).always(function () {
        });
    }
    var setIntervalNotify = setInterval(function(){
        intervalNotify();
    }, 300000);
</script>
</body>


</html>