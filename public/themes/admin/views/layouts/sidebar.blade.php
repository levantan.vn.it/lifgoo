<?php
$sessionGlobal = session('admin');
$avatar = $sessionGlobal->avatar ? Helper::getThemeAvatar($sessionGlobal->avatar) : Helper::getThemeImg('user.png');
$is_admin = $sessionGlobal->hasRole('admin');
?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{Helper::url('admin')}}" class="site_title"><i class="fa fa-paw"></i>
                <span>Lifgoo Admin</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{$avatar}}?id={{Helper::versionImg()}}"
                     alt="" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>@if(session('admin')){{session('admin')->full_name}}@endif</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>{{trans('admin.administration')}}</h3>
                <ul class="nav side-menu">
                    <li><a href="{{Helper::url('admin')}}"><i class="fa fa-home"></i> Home</a>
                    </li>
                </ul>
            </div>
            <div class="menu_section">
                <h3>{{trans('admin.users')}}</h3>
                <ul class="nav side-menu">
                    @if($sessionGlobal->can('view_user_group'))
                        <li><a href="{{Helper::url('admin/roles/list')}}" style="word-break: break-word;"><i
                                        class="fa fa-key"></i> {{trans('admin.roles_permissions')}}</a>
                        </li>
                    @endif

                    <li class="{{Helper::set_active('admin/guide','active',true)}}"><a><i
                                    class="fa fa-user"></i>{{trans('admin.guides')}}<span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu"
                            style="{{Helper::set_active('admin/user','display:block',true)}}">
                            <li class="{{Helper::set_active('admin/guide/list','current-page')}}"><a
                                        href="{{Helper::url('admin/guide/list')}}">Guide list</a></li>
                            <li class="{{Helper::set_active('admin/guide/approval','current-page')}}"><a
                                        href="{{Helper::url('admin/guide/approval')}}">Approval</a></li>
                        </ul>
                    </li>
                    <li class="{{Helper::set_active('admin/user','active',true)}}"><a><i
                                    class="fa fa-user"></i>{{trans('admin.users')}}<span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu"
                            style="{{Helper::set_active('admin/user','display:block',true)}}">
                            <li class="{{Helper::set_active('admin/user/list','current-page')}}"><a
                                        href="{{Helper::url('admin/user/list')}}">User list</a></li>
                        </ul>
                    </li>
                    <li class="{{Helper::set_active('admin/booking','active',true)}}"><a><i
                                    class="fa fa-fighter-jet"></i>Booking<span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu"
                            style="{{Helper::set_active('admin/booking','display:block',true)}}">
                            <li class="{{Helper::set_active('admin/booking/list','current-page')}}"><a
                                        href="{{Helper::url('admin/booking/list')}}">Booking list</a></li>
                            @if(false)
                                <li class="{{Helper::set_active('admin/booking/statistics','current-page')}}"><a
                                            href="{{Helper::url('admin/booking/statistics')}}">Statistical booking</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu_section">
                <h3>{{trans('admin.system')}}</h3>
                <ul class="nav side-menu">
                    <li><a href="{{Helper::url('admin/service/list')}}"><i
                                    class="fa fa-shield"></i> {{trans('admin.services')}}</a>
                    </li>
                    <li><a href="{{Helper::url('admin/language/list')}}"><i
                                    class="fa fa-language"></i> {{trans('admin.languages')}}</a>
                    </li>
                    <li><a href="{{Helper::url('admin/news/list')}}"><i
                                    class="fa fa-newspaper-o"></i> {{trans('admin.news')}}</a>
                    </li>
                    <li><a href="{{Helper::url('admin/subcriber/list')}}"><i
                                    class="fa fa-envelope"></i>Subcribers</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        {{--<div class="sidebar-footer hidden-small">
            @if(false)
                <a data-toggle="tooltip" data-placement="top" title="Settings">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Lock">
                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                </a>
            @endif
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{Helper::url('admin/logout')}}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>--}}
        <!-- /menu footer buttons -->
    </div>
</div>
