<header class="main-header">
    <?php
        $sessionGlobal = session('admin');
        $avatar = $sessionGlobal->avatar?Helper::getThemeAvatar($sessionGlobal->avatar):Hepler::getThemeImg('user.png');

    ?>
    <a href="/admin" class="logo">
        <span class="logo-mini">LIF</span>
        <span class="logo-lg">Lifgoo</span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown chat-taskbar-login">
                    <a href="{{Helpers::url('admin/chat')}}" class="dropdown-toggle f-s-14"
                       aria-expanded="true">
                        <i class="fa fa-bell-o"></i>

                        {{--<span class="count-message"><i class="fa fa-bell" aria-hidden="true"></i></span>--}}
                    </a>
                </li>
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li class="header">
                        <a rel="alternate" hreflang="{{$localeCode}}"
                           href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                            {{ $properties['native'] }}
                        </a>
                    </li>
            @endforeach
            <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle admin-logout" data-url="{{Helpers::url('admin/out')}}" data-toggle="adropdown">
                        <img src="{{$avatar}}?id={{Helper::versionImg()}}" class="user-image" alt="Admin Image">
                        <span class="hidden-xs">{{ trans('admin.logout') }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>