<?php
$notification = \Modules\User\Entities\NotificationEntity::notify_system();
$user_notify = [];
$count = \Modules\User\Entities\NotificationEntity::countNotifySystem();
$sessionGlobal = session('admin');
$avatar = $sessionGlobal->avatar?Helper::getThemeAvatar($sessionGlobal->avatar):Helper::getThemeImg('user.png');
?>
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="{{Helper::url('admin/logout')}}"><i class="fa fa-sign-out pull-right" style="margin-top: 7px;"></i> Log Out</a>
                    {{--<a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <img src="{{$avatar}}?id={{Helper::versionImg()}}"
                             alt="">@if(session('admin')){{session('admin')->full_name}}@endif
                        @if(false)<span class=" fa fa-angle-down"></span>@endif
                    </a>--}}
                    @if(false)
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                            {{--<li><a href="javascript:">@if(session('admin')){{session('admin')->full_name}}@endif</a></li>--}}
                            <li><a href="{{Helper::url('admin/logout')}}"><i class="fa fa-sign-out pull-right"></i> Log
                                    Out</a></li>
                        </ul>
                    @endif
                </li>

                <li role="presentation" class="dropdown">
                    <a href="javascript:" class="dropdown-toggle info-number @if($count)bell-notify @endif notification-bell" data-toggle="dropdown" aria-expanded="false" data-url="{{Helper::url('admin/updateIsClick')}}">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge bg-red">@if($count){{$count}}@endif</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list content-notify" role="menu">
                        @if(count($notification))
                            @foreach($notification as $notify)
                                @include('admin::system.notification')
                            @endforeach
                        @endif
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>