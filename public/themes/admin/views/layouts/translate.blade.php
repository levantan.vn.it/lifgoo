<script>
    var trans = (function () {
        return {
            previous: '{{trans('admin.previous')}}',
            next: '{{trans('admin.next')}}',
            show: '{{trans('admin.show')}}',
            entries: '{{trans('admin.entries')}}',
            search: '{{trans('admin.search')}}',
            yes: '{{trans('site.yes')}}',
            no: '{{trans('site.no')}}',
        }
    })();
    var config = (function () {
        return {
            width:270,
            height:270,
        }
    })();
</script>