<footer>
    <div class="pull-right"><a href="https://lifgoo.com">lifgoo</a>
    </div>
    <div class="clearfix"></div>
</footer>
<div class="modal fade">
    <div class="modal-dialog">
        <div class="box box-primary">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div id="loading" style="display:none">
    <div class="cssload-dots">
        <div id="circleG_1" class="circleG"></div>
        <div id="circleG_2" class="circleG"></div>
        <div id="circleG_3" class="circleG"></div>
    </div>
</div>