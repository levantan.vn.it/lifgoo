<?php
/**
 * Created by PhpStorm.
 * User: Yanaro
 * Date: 18/11/2016
 * Time: 1:47 PM
 */
return [
    'view_user_list' => 'View user list',
    'add_new_user' => 'Add new user',
    'edit_user' => 'Edit user',
    'delete_user' => 'Delete user',
    'view_user_group' => 'View user group',
    'add_new_group' => 'Add new user group',
    'edit_group' => 'Edit user group',
    'delete_group' => 'Delete user group',
    'config_language' => 'Config language',
    'access_news' => 'Access news',
    'config_service' => 'Config service',
    'admin_login'=> 'Admin Login',
];