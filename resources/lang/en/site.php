<?php
/**
 * Created by PhpStorm.
 * User: Yanaro
 * Date: 15/11/2016
 * Time: 2:13 PM
 */
return [
    'homepage' => 'Homepage',
    'login' => 'Login',
    'username' => 'Username',
    'email' => 'Email',
    'password' => 'Password',
    'old_password' => 'Old password',
    'password_confirmation' => 'Password confirmation',
    'logout' => 'Logout',
    'full_name' => 'Full name',
    'user_name' => 'User name',
    'address' => 'Address',
    'phone_number' => 'Phone number',
    'remember_me' => 'Remember me',
    'forgot_your_password' => 'Forgot your password?',
    'register' => 'Register',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'title' => 'Title',
    'action' => 'Action',
    'submit' => 'Submit',
    'success' => 'Success',
    'warning' => 'Warning',
    'confirm' => 'Confirm',
    'error' => 'Error',
    'action_succeeded' => 'Your action has been completed',
    'action_failed' => 'Your action has failed',
    'error_occured' => 'An error has occured while processing your request',
    'view' => 'View',
    'status' => 'Status',
    'created_date' => 'Created date',
    'month_jan' => 'January',
    'month_feb' => 'February',
    'month_mar' => 'March',
    'month_apr' => 'April',
    'month_may' => 'May',
    'month_jun' => 'June',
    'month_jul' => 'July',
    'month_aug' => 'August',
    'month_sep' => 'September',
    'month_oct' => 'October',
    'month_nov' => 'November',
    'month_dec' => 'December',
    'avatar' => 'Avatar',
    'cover' => 'Cover',
    'slogan' => 'Slogan',
    'hobby' => 'Hobby',
    'personal_id' => 'Personal identification number',
    'birth_place' => 'Birth place',
    'version' => 'Version',
    'yes' => 'Yes',
    'no' => 'No',
    'no_roles_selected' => 'No role is selected',
    'no_services_selected' => 'No services is selected',
    'group' => 'group',
    'please_select'=>'Please select',
    'icon' => 'Icon',
    'service_name' => 'Service name',
    'type' => 'Type',
    'service' => 'Service',
    'transportation' => 'Transportation',
    'service_name_en'=>'Service name(English)',
    'service_name_vi'=>'Service name(Viet Nam)',
    'language_name'=>'Language name',
    'code'=>'Language code',
    'thumbnail'=>'Thumbnail',
    'content_summary'=>'Content summary',
    'location'=>'Location',
    'author'=>'Author',
    'content'=>'Content',
    'change_thumbnail'=>'Change thumbnail',
    'save_changes'=>'Save changes',
    'close'=>'Close',
    'click_upload_thumbnail'=>'Click <i class="fa fa-hand-o-up"></i> to upload thumbnail',
    'click_upload_avatar'=>'Click <i class="fa fa-hand-o-up"></i> to upload Avatar',
    'upload_thumnail'=>'Upload thumnail',
    'please_choose_image'=>'Please select an image to upload',
    'no_image_change'=>'No Image Available',
    'block' => 'Block',
    'unblock' => 'Unblock',
    'profile' => 'Profile',
    'job_info' => 'Job infomation',
    'please_select_country' => 'Please select country',
    'passport_number' => 'Passport number',
    'id_number' => 'ID number',
    'id_card' => 'ID Card',
    'date_card' => 'Date Card',
    'place_of_issue' => 'Place of issue',
    'International' => 'International',
    'active' => 'Active',
    'language' => 'Language',
    'gender' => 'Gender',
    'birthday' => 'Birthday',
    'male' => 'Male',
    'female' => 'Female',
    'about_me' => 'About me',
    'please_select_city' => 'Please select city',
    'password_confirm'=>'Password confirm',
    'local_activities'=>'Local activities',
    'hourly_rate'=>'Hourly rate',
    'login_success'=>'Login successfull',
    'login_fail'=>'Sign in failed.Please check your account.',
    'my_account'=>'My account',
    'edit_account'=>'Edit account',
    'my_profile'=>'My profile',
    'edit_profile'=>'Edit Profile',
    'edit_password'=>'Edit Password',
    'edit_job_information'=>'Edit Job Information',
    'edit_price'=>'Edit Price',
    'edit_promotion'=>'Edit Promotion',
    'notification'=>'Notification',
    'booking'=>'Booking',
    'gallery'=>'Gallery',
    'edit_profile_success'=>'Your profile has been updated!',
    'edit_price_success'=>'Your profile has been updated!',
    'setup_time' =>'Setup Time',
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'saturday' => 'Saturday',
    'sunday' => 'Sunday',
    'update_avatar_success' => 'Update avatar success',
    'update_cover_success' => 'Update cover success',
    'upload_success' => 'Upload success',
    'send_mail_success' => 'Sent mail success',
    'edit_password_success' => 'Edit password success',
    'action_success' => 'Action success',
    'Please_enter_correct_current_password' => 'Please enter correct current password',
    'no_news_selected'=>'No news is selected',
    'no_file_selected'=>'You must select images to continue upload'
];