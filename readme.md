

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)
###Các thư viện đã cài đặt:###
1. Cafeinated Modules: thư viện hỗ trợ HMVC cho Laravel
https://github.com/caffeinated/modules/wiki
2. Cafeinated Themes: thư viện hỗ trợ theme
https://github.com/caffeinated/themes/wiki
3. Assets: thư viện hỗ trợ quản lý assets
https://github.com/Stolz/Assets
4. ENTRUST: Hổ trợ quản lý role permissions
https://github.com/Zizaco/entrust
5. Image: hổ trợ quản lý ảnh
http://image.intervention.io/
6. Form/html
https://laravelcollective.com/docs/5.2/html
7. socialite
https://github.com/laravel/socialite
8. LaravelLocalization
https://github.com/mcamara/laravel-localization

##########################################
Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).


##Installed libraries##
