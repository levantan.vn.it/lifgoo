<?php

namespace App\Helper;

use Caffeinated\Themes\Facades\Theme;
use Illuminate\Support\Facades\Request;

class Helper
{
    public function getThemeAssets($asset)
    {
        return \Theme::asset(\Theme::getActive() . '::' . $asset);
    }

    public function getThemeCss($css)
    {
        return $this->getThemeAssets('css/' . $css);
    }

    public function getThemeJs($js)
    {
        return $this->getThemeAssets('js/' . $js);
    }

    public function getThemePlugins($plugin)
    {
        return $this->getThemeAssets('plugins/' . $plugin);
    }

    public function getThemeImg($img=false)
    {
        return $this->getThemeAssets('img/' . $img);
    }

    public function getThemeNewsImg($img = '')
    {
        return $this->getThemeAssets('newsimg/' . $img);
    }

    public function getThemeNewsUpload($img = null)
    {
        return '/themes/admin/assets/newsimg/' . $img;
    }

    public function getThemeAvatar($img = false)
    {
        return secure_url('themes/admin/assets/avatar/' . $img);
    }

    public function getThemeAvatarUpload($img = false, $type = 'avatar')
    {
        return '/themes/admin/assets/' . $type . '/' . $img;
    }

    public function getThemeGallery($folder = false, $img = false)
    {
        return '/themes/default/assets/gallery/' . $folder . '/' . $img;
    }

    function getSegment()
    {
        return $segment = Request::segment(2);

    }

    /**
     * get url with localization
     * @param $url
     * @return string
     */
    public function url($url = '', $secure = false)
    {
        $url = \LaravelLocalization::getCurrentLocale() . '/' . $url;
        return $secure ? secure_url($url) : url($url);
    }

    function urlHttps($url = '', $secure = true)
    {
        return $secure ? secure_url($url) : url($url);
    }

    /**
     * redirect url with localization
     * @param $url
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function redirect($url = '')
    {
        $currentLocale = \LaravelLocalization::getCurrentLocale();
        $locale = $currentLocale === 'en' ? '' : $currentLocale;
        return redirect()->to($locale . '/' . $url);
    }

    public function set_active($path, $active = 'active', $typeParent = false)
    {
        $path = \LaravelLocalization::getCurrentLocale() . '/' . $path;
        if ($typeParent)
            return Request::is($path) ? $active : '';
        return (Request::is($path) && !Request::is($path . '/*')) ? $active : '';
    }

    public static function create_unique()
    {
        $agent = Request::header('User-Agent');
        $ip = Request::ip();
        // Read the user agent, IP address, current time, and a random number:

        $data = $agent . $ip . time() . rand();
        // Return this value hashed via sha1
        return strtoupper(sha1($data));
    }

    /**Convert supposedly number to number
     * @param $number
     */
    public static function numericToInt($number)
    {
        return !is_numeric($number) ? $number : (int)$number;
    }

    /** Return data for ajax call.
     * @param bool $success
     * @param string $message
     * @param null $data
     * @param int $flag
     * @return array
     */
    public static function resultData($success = FALSE, $message = '', $data = NULL, $flag = 0)
    {
        $result = [
            'success' => $success,
            'message' => $message,
            'data' => $data,
            'flag' => $flag
        ];
        return $result;
    }

    /**
     * encode data in json
     * @param array $data
     */
    public static function json($data = [], $statusCode = 200)
    {
        return response()->json($data, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }

    /**map array to object
     * @param $obj
     * @param array $data
     * @return object
     */
    public static function mapData($obj, $data = [], $int_fields = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (in_array($key, $int_fields)) {
                    $value = (int)$value;
                }
                $obj->{$key} = $value;
            }
        }
        return $obj;
    }

    function object_to_array($data)
    {
        if (is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }

    /** Change unicode characters to ASCII characters, space is replaced by underscore "_"
     *
     * @param $str
     * @return string
     */
    public static function gen_slug($str)
    {
        # special accents
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'Ẹ', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ẹ', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'đ', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '_', ''), str_replace($a, $b, $str)));
    }

    /**
     * trims text to a space then adds ellipses if desired
     * @param string $input text to trim
     * @param int $length in characters to trim to
     * @param bool $ellipses if ellipses (...) are to be added
     * @param bool $strip_html if html tags are to be stripped
     * @param bool $strip_style if css style are to be stripped
     * @return string
     */
    function trim_text($input, $length, $ellipses = true, $strip_tag = true, $strip_style = true)
    {
        //strip tags, if desired
        if ($strip_tag) {
            $input = strip_tags($input);
        }

        //strip tags, if desired
        if ($strip_style) {
            $input = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $input);
        }

        if ($length == 'full') {

            $trimmed_text = $input;

        } else {
            //no need to trim, already shorter than trim length
            if (strlen($input) <= $length) {
                return $input;
            }

            //find last space within length
            $last_space = strrpos(substr($input, 0, $length), ' ');
            $trimmed_text = substr($input, 0, $last_space);
            //add ellipses (...)
            if ($ellipses) {
                $trimmed_text .= '...';
            }
        }

        return $trimmed_text;
    }

    public function reCaptcha($captcha)
    {
        $google_url = "https://www.google.com/recaptcha/api/siteverify";
        $ip = Request::ip();
        $secret = Api::find(2)->secret_key;
        $url = file_get_contents($google_url . "?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $ip);
        $res = json_decode($url);
        return $res->success;
        //reCaptcha success check
    }
    /**
     * Convert order status to
     */
    /** Cast string to int.
     * @param $string
     * @return int
     */
    public static function stringToInt($string)
    {
        return (int)$string;
    }

    /** Map string array to Int array.
     * @param array $arr
     * @return array
     */
    public static function stringArrayToInt($arr = [])
    {
        if (!is_array($arr)) {
            return $arr[(int)$arr];
        }
        return array_map('self::stringToInt', $arr);
    }


    /** Map Object ID to array
     * @param $obj
     * @return array
     */
    public static function dbObjectIdToArray($obj)
    {
        $arr = [];
        foreach ($obj as $item) {
            $arr[] = $item->id;
        }
        return $arr;
    }

    /**
     * encode password
     * @param $password
     * @return bool|string
     */
    public function hashPassword($password)
    {
        $options = [
            'cost' => 12,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    /**
     * verify password
     * @param $password
     * @param $encodePassword
     * @return bool
     */
    public function verifyPassword($password, $encodePassword)
    {
        return password_verify($password, $encodePassword);
    }

    /**
     * compares given route name with current route name
     * @param $routes
     * @param string $output
     * @return string
     */
    public function activeRoutes($routes, $output = 'active')
    {
        $path = \Route::getCurrentRoute()->getPath();
        if (!is_array($routes)) {
            return (ends_with($path, $routes)) ? $output : '';
        }

        foreach ($routes as $route) {
            if (ends_with($path, $route)) return $output;
        }

        return '';
    }

    /**
     * compare given segment with values
     * @param $segment
     * @param $value
     * @return string
     */
    public function activeSegments($segment, $values, $output = 'active')
    {
        if (!is_array($values)) {
            return \Request::segment($segment) == $values ? $output : '';
        }

        foreach ($values as $value) {
            if (\Request::segment($segment) == $values) return $output;
        }

        return '';
    }

    /**
     * detects if the given string is found in the current URL.
     * @param  string $string
     * @param  string $output
     * @return boolean
     */
    public function activeMatch($string, $output = 'active')
    {
        return (strpos(\URL::current(), $string)) ? $output : '';
    }

    public function css($file)
    {
        $theme = Theme::getActive();
        return url("themes/{$theme}/assets/css/{$file}");
    }

    public function js($file)
    {
        $theme = Theme::getActive();
        return url("themes/{$theme}/assets/js/{$file}");
    }

    public function plugin($file)
    {
        $theme = Theme::getActive();
        return url("themes/{$theme}/assets/{$file}");
    }

    /**
     * get current admin logged-in data
     * @return bool
     */
    public function currentAdmin()
    {
        $session = session('admin');
        if (!empty($session) && !empty($session['user'])) {
            return $session['user'];
        }

        return false;
    }

    /**
     * get current user logged-in data
     * @return bool
     */
    public function currentUser()
    {
        $session = session('frontend');
        if (!empty($session) && !empty($session['user'])) {
            return $session['user'];
        }

        return false;
    }

    /**
     * get admin title with prefix and suffix
     * @param string $title
     * @param bool|true $prefix
     * @param bool|true $suffix
     * @return string
     */
    public function adminTitle($title = '', $prefix = true, $suffix = true)
    {
        $adminTitle = '';
        if ($prefix) {
            $adminTitle .= 'AdminCP - ';
        }
        $adminTitle .= $title;
        if ($suffix) {
            $adminTitle .= ' | Vitzoo';
        }

        return $adminTitle;
    }

    function has_dupes(&$array)
    {
        if (!$array) return false;
        return count(array_keys(array_flip($array))) !== count($array);
    }

    static function trim($str)
    {
        if (is_string($str))
            return trim(preg_replace('/\s\s+/', ' ', $str));
        return $str;
    }

    function age($year)
    {
        return date('Y') - $year;
    }

    public static function editable($status)
    {
        switch ($status) {
            case 'deleted':
            case 'blocked':
            case 'inactive':
            case 'pending':
            case 'active':
                return true;
            default:
                return false;
        }
    }

    /**
     * get status class by status code
     * @param $status
     * @return string
     */
    public function statusClass($status)
    {
        switch ($status) {
            case 'deleted':
            case 'blocked':
                return 'label-danger';
            case 'inactive':
            case 'pending':
                return 'label-warning';
            case 'active':
                return 'label-success';
            default:
                return 'red_glittering';
        }
    }

    public function daysInMonth($month, $year)
    {
        return $month === 2 ? $year & 3 || !($year % 25) && $year & 15 ? 28 : 29 : 30 + ($month + ($month >> 3) & 1);
    }

    /**
     * remove folder and all sub-folders or files in it
     * @param $dir
     */
    function removeFolder($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->removeFolder($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function getValidatorError($validator)
    {
        $html = '';
        $errors = $validator->errors();
        foreach ($errors->all() as $error) {
            if (is_array($error)) {
                foreach ($error as $item) {
                    $html .= '<p>' . $item . '</p>';
                }
            } else
                $html .= '<p>' . $error . '</p>';
        }
        return $this->resultData(false, $html);
    }

    /**
     * Escape special character
     * @param array $data
     * @return array
     */
    function arrayEscape($data = [])
    {
        foreach ($data as $k => $v) {
            $data[$k] = e($v);
        }
        return $data;
    }

    public function errors($error, $message, $code)
    {
        $error = [
            'error' => $error,
            'error_description' => $message
        ];
        return self::json($error, $code);
    }

    function stripVowelAccent($str)
    {
        $foreign_characters = [
            '/ä|æ|ǽ/' => 'ae',
            '/ö|œ/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/À|Á|Â|Ã|Ä|Å|Ǻ|Ā|Ă|Ą|Ǎ|А|Ạ|Ả|Ã|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ằ|Ắ|Ặ|Ẳ|Ẵ/' => 'A',
            '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª|а|ạ|ả|ã|ầ|ấ|ậ|ẩ|ẫ|ằ|ắ|ặ|ẳ|ẵ/' => 'a',
            '/Б/' => 'B',
            '/б/' => 'b',
            '/Ç|Ć|Ĉ|Ċ|Č|Ц/' => 'C',
            '/ç|ć|ĉ|ċ|č|ц/' => 'c',
            '/Ð|Ď|Đ|Д/' => 'D',
            '/ð|ď|đ|д/' => 'd',
            '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě|Е|Ё|Э|Ẹ|Ẻ|Ẽ|Ề|Ế|Ệ|Ể|Ễ/' => 'E',
            '/è|é|ê|ë|ē|ĕ|ė|ę|ě|е|ё|э|ẹ|ẻ|ẽ|ề|ế|ệ|ể|ễ/' => 'e',
            '/Ф/' => 'F',
            '/ф/' => 'f',
            '/Ĝ|Ğ|Ġ|Ģ|Г/' => 'G',
            '/ĝ|ğ|ġ|ģ|г/' => 'g',
            '/Ĥ|Ħ|Х/' => 'H',
            '/ĥ|ħ|х/' => 'h',
            '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ|И|Ị|Ỉ|Ĩ/' => 'I',
            '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı|и|ị|ỉ|ĩ/' => 'i',
            '/Ĵ|Й/' => 'J',
            '/ĵ|й/' => 'j',
            '/Ķ|К/' => 'K',
            '/ķ|к/' => 'k',
            '/Ĺ|Ļ|Ľ|Ŀ|Ł|Л/' => 'L',
            '/ĺ|ļ|ľ|ŀ|ł|л/' => 'l',
            '/М/' => 'M',
            '/м/' => 'm',
            '/Ñ|Ń|Ņ|Ň|Н/' => 'N',
            '/ñ|ń|ņ|ň|ŉ|н/' => 'n',
            '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ|О|O|Ọ|Ỏ|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/' => 'O',
            '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º|о|ọ|ỏ|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/' => 'o',
            '/П/' => 'P',
            '/п/' => 'p',
            '/Ŕ|Ŗ|Ř|Р/' => 'R',
            '/ŕ|ŗ|ř|р/' => 'r',
            '/Ś|Ŝ|Ş|Š|С/' => 'S',
            '/ś|ŝ|ş|š|ſ|с/' => 's',
            '/Ţ|Ť|Ŧ|Т/' => 'T',
            '/ţ|ť|ŧ|т/' => 't',
            '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ|У|Ụ|Ủ|Ũ|Ừ|Ứ|Ự|Ữ/' => 'U',
            '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ|у|ụ|ủ|ũ|ừ|ứ|ự|ử|ữ/' => 'u',
            '/В/' => 'V',
            '/в/' => 'v',
            '/Ý|Ÿ|Ŷ|Ы|Ỳ|Ỵ|Ỷ|Ỹ/' => 'Y',
            '/ý|ÿ|ŷ|ы|ỳ|ỵ|ỷ|ỹ/' => 'y',
            '/Ŵ/' => 'W',
            '/ŵ/' => 'w',
            '/Ź|Ż|Ž|З/' => 'Z',
            '/ź|ż|ž|з/' => 'z',
            '/Æ|Ǽ/' => 'AE',
            '/ß/' => 'ss',
            '/Ĳ/' => 'IJ',
            '/ĳ/' => 'ij',
            '/Œ/' => 'OE',
            '/ƒ/' => 'f',
            '/Ч/' => 'Ch',
            '/ч/' => 'ch',
            '/Ю/' => 'Ju',
            '/ю/' => 'ju',
            '/Я/' => 'Ja',
            '/я/' => 'ja',
            '/Ш/' => 'Sh',
            '/ш/' => 'sh',
            '/Щ/' => 'Shch',
            '/щ/' => 'shch',
            '/Ж/' => 'Zh',
            '/ж/' => 'zh'
        ];
        $str = preg_replace(array_keys($foreign_characters), array_values($foreign_characters), $str);
        $str = strtolower($str);
        return $str;
    }

    /**
     * validator array
     * @param $data
     * @param $rules
     * @return array|bool
     */
    function validate($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $result = [];
            $errors = $validator->errors();
            foreach ($errors->all() as $error) {
                if (is_array($error)) {
                    foreach ($error as $item) {
                        $result[] = $item;
                    }
                } else
                    $result[] = $error;
            }
            return $result;

        }
        return false;
    }

    function checkInArray($array, $value = null, $key = null)
    {

        foreach ($array as $i) {
            if ($value == $i->$key) {
                return 'checked';
            }
        }
        return false;
    }

    public function jsondata($success = FALSE, $message = '', $data = NULL, $flag = 0)
    {
        $result = [
            'success' => $success,
            'message' => $message,
            'data' => $data,
            'flag' => $flag
        ];
        return Helper::json($result);
    }

    public function apiJson($success = FALSE, $message = '', $data = NULL)
    {
        $result = [
            'status' => $success,
            'msg' => $message,
            'data' => $data,
        ];
        return Helper::json($result);
    }

    /**
     * convert array don't have key to have key with value equal empty
     * @param array $data
     * @return array
     */
    public function convertArrayEmpty($data = [])
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$value] = '';
        }
        return $result;
    }

    public function daysOfWeek()
    {
        return ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
    }

    function dayOfWeek($i)
    {
        if ($i)
            return $this->daysOfWeek()[$i];
        return false;
    }

    function formatDate($date = null)
    {
        return date('Y/m/d', strtotime($date));
    }

    function addPlural($x = null, $value = null, $type = 's')
    {
        if ($x > 1)
            return $x . ' ' . $value . $type;
        return $x . ' ' . $value;
    }

    function addZero($x = null)
    {
        return $x < 10 ? '0' . $x : $x;
    }

    function ValueBetween($from, $to)
    {
        $arr = [];
        if ($from < $to)
            for ($i = $from; $i <= $to; $i++) {
                array_push($arr, $i);
            }
        return implode(",", $arr);
    }

    function version()
    {
        return date("YmdHis");
    }
    function versionImg()
    {
        return date("YmdHis");
    }


    /**
     * fields of guide are need check to approved guide
     * @return array
     */
    function arrayCheckMissing()
    {
        return [
            'profile_user' => ['avatar', 'full_name', 'phone_number', 'gender', 'birth_date', 'address'],
            'profile_info' => ['idnumber', 'introduce'],
//            'jobInfo_certification' => [ 'expiry_date', 'type', 'status'],
            'jobInfo_certification' => [ 'expiry_date', 'type'],
            'jobInfo_info' => ['experience', 'diploma_info','hourly_rate'],
//            'jobInfo_location' => ['country_id', 'city_id'],
            'jobInfo_location' => ['city_id'],
            'jobInfo_language' => ['language_id']
        ];
    }

    function typeNotify($type){
        switch ($type) {
            case 'canceled':
                return "cancel_booking";
            case 'accepted':
                return "accept_booking";
            case 'declined':
                return "decline_booking";
        }
        return null;
    }

    function convertDatetimeObject($string,$format = 'Y-m-d H:i:s'){
        $date = \DateTime::createFromFormat($format, $string);
        return $date;
    }
}



