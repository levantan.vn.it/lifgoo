<?php

namespace app\Helper\Facades;

use Illuminate\Support\Facades\Facade;

class HelperFacade extends Facade {

    protected static function getFacadeAccessor() { return "helper"; }
}
