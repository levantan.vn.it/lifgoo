<?php

namespace App\Helper\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class HelperServiceProvider extends ServiceProvider
{
    public function register()
    {
        App::bind("helper", function() {
            return new App\Helper\Helper;
        });
    }
}