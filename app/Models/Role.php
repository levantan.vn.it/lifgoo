<?php

/**
 * Created by PhpStorm.
 * User: Yanaro
 * Date: 15/11/2016
 * Time: 8:27 AM
 */
namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'description'
    ];
}