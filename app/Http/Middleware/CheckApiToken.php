<?php

namespace App\Http\Middleware;
use App\User;
use Closure;
use Helper;
use Modules\Frontend\Models\UsersModel;
class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   $token=$request->input('access_token');
        $user=User::whereToken($token)->first();
        if($user){
            UsersModel::storeSession($user, $user->type);
            return $next($request);
        }
        else{
            return Helper::apiJson(false, 'Token invalid!');

        }
    }
}
