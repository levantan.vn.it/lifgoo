<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use LaravelLocalization;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('admin'))
            return $next($request);
        $currentLocale = LaravelLocalization::getCurrentLocale();
        $locale = $currentLocale === 'en' ? '' : $currentLocale;
        return redirect()->to($locale . '/admin/login');
    }
}
