<?php

namespace App\Http\Middleware;
use Modules\Frontend\Models\UsersModel;
use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('users'))
        {
            $user = session('users.user')? session('users.user'): session('users.guide');
            $new_user=UsersModel::getUserById($user->id);
            UsersModel::storeSession($new_user,$new_user->type);
            return $next($request);
        }
        $currentLocale = \LaravelLocalization::getCurrentLocale();
        $locale = $currentLocale === 'en' ? '' : $currentLocale;
        if(session('flash_search_city')){
            $search_city=session('flash_search_city');
            session()->flash('flash_search_city',$search_city);
        }
        return redirect()->to($locale.'/signin');
    }
}
