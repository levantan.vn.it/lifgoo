<?php

namespace App;

use App\Models\Role;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Frontend\Models\GuidesModel;
use Modules\Frontend\Models\SystemModel;
use Modules\Frontend\Models\UsersModel;
use Modules\User\Entities\BookingEntity;
use Modules\User\Entities\BookingMessagesEntity;
use Modules\User\Entities\GuideCertificationEntity;
use Modules\User\Entities\GuideEntity;
use Modules\User\Entities\NotificationEntity;
use Modules\User\Entities\RoleUserEntity;
use Modules\User\Entities\TravellerEntity;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user__users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'full_name', 'email', 'password', 'idnumber', 'address', 'phone_number', 'birth_date', 'status', 'type', 'avatar', 'hobby', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * traverller information
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function traveller()
    {
        return $this->hasOne(new TravellerEntity(), 'user_id', 'id');
    }

    /**
     * guide information
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function guide()
    {
        return $this->hasOne(new GuideEntity(), 'user_id', 'id');
    }

    /**
     * guide certification
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function certification()
    {
        return $this->hasOne(new GuideCertificationEntity(), 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany(new Role());
    }

    public function roles_users()
    {
        return $this->hasOne(new RoleUserEntity(),'user_id','id');
    }

    public function lackOfInformation($user = null)
    {
        $guideModel = new GuidesModel();
        $result = $guideModel->isMissingInformation($user);
        return $result;
    }

    public function notifications()
    {
        return $this->hasMany(new NotificationEntity(), 'user_id', 'id');
    }

    function bookings(){
        return $this->hasMany(new BookingEntity(),'guide_id','id');
    }

    function countMessage($user_id){
        return BookingMessagesEntity::select('booking_id')->where('receiver_id','=',$user_id)->orderBy('created_at','desc')->where('is_read','=',0)->get();
    }

    function bookingMessages(){
        return $this->hasMany(new BookingMessagesEntity(),'receiver_id','id');
    }

}
