<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailBooking extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    public $full_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content,$full_name='you',$subject='Mail Booking'){
        $this->content = $content;
        $this->full_name = $full_name;
        $this->subject($subject);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->from(env('MAIL_USERNAME'),'LIFGOO')->view('default::mail.sendmail');
    }

}
