<?php
/**
 * Created by PhpStorm.
 * User: lethi
 * Date: 4/10/2017
 * Time: 3:46 PM
 */

namespace App\Providers;


use Laravel\Socialite\SocialiteManager;
use Laravel\Socialite\SocialiteServiceProvider;

class SocialiteCusServiceProvider extends SocialiteServiceProvider
{
    public function register()
    {
        $this->app->singleton('Laravel\Socialite\Contracts\Factory', function ($app) {
            
            return new SocialiteManager($app);
        });
    }
    
    
}