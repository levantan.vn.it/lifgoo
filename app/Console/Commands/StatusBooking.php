<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Frontend\Http\Controllers\HomeController;
use Modules\Frontend\Models\GuidesModel;
use Modules\User\Entities\BookingDatesEntity;
use Helper;
use Modules\User\Entities\BookingEntity;

class StatusBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'StatusBooking:changStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now();
        $tomorrow = $today->addDay();
        $tomorrow = date_format($tomorrow,'Y-m-d');
        $today = date_format($today,'Y-m-d');
        /**
         * update status finished
         */
        $bookingAccepted = BookingEntity::where('status','=','accepted')->get();
        if($bookingAccepted)
            foreach ($bookingAccepted as $value){
                $is_notyet = $value->datesBooking()->where('date','>=',date_format(Carbon::now()->subDay(),'Y-m-d'))->exists();
                if(!$is_notyet){
                    $value->status = 'finished';
                    $value->save();
                }

            }
        /**
         * send mail reminder
         */
        $dates = BookingDatesEntity::whereBetween('date',[$today,$tomorrow])->get();
        $guideModel = new GuidesModel();
        if($dates)
            foreach ($dates as $date){
                $booking = $date->booking()->first();
                $booking->date_temp = $date->date;
                $guide = $booking->guide()->select('email','full_name','id')->first();
                $travel = $booking->guide('traveler_id')->select('full_name')->first();
                $content = $this->contentMail($guide,$travel,$booking);
                $guideModel->sendmail($guide->email, $content, $guide->full_name);
                $guideModel->sendmail($booking->email, $content, $travel->full_name);
            }
    }

    /**
     * content send mail remind
     * @param $guide
     * @param $travel
     * @param $booking
     * @return string
     */
    function contentMail($guide,$travel,$booking){
        $st = '';
        $st .= HomeController::htmlEmail('<strong>This email is a reminder in regards to book a trip to '.$booking->destination.' tomorrow</strong>');
        $st .= HomeController::htmlEmail('The following information:');

        $st_bw = '<table>';
        $st_bw .= '<tbody>';
        $st_bw .= '<tr><td>&#9679; User&#39;s Full name: '.$travel->full_name.'</td></tr>';
        $st_bw .= '<tr><td>&#9679; User&#39;s Phone Number: '.$booking->phone_number.'</td></tr>';
        $st_bw .= '<tr><td>&#9679; User&#39;s email: '.$booking->email.'</td></tr>';
        $st_bw .= '<tr><td>&#9679; Booking&#39;s date: '.$booking->date_temp.'</td></tr>';
        $st_bw .= '<tr><td>&#9679; Guide&#39;s Name: '.$guide->full_name.'</td></tr>';
        $st_bw .= '<tr><td>&#9679; Guide&#39;s Email: '.$guide->email.'</td></tr>';
        $st_bw .= '<tr><td>&#9679; Guide&#39;s Phone Number: '.$guide->phone_number.'</td></tr>';

        $st_bw .= '</tbody>';
        $st_bw .= '</table>';

        $st .= HomeController::htmlEmail($st_bw);
        $st .= HomeController::htmlEmail('Further information can be found at: '.Helper::url('detail/'.$booking->id));
        $st .= HomeController::htmlEmail('If you have any question about this, please contact us via email : '.Helper::url('contact'));
        return $st;
    }
}
